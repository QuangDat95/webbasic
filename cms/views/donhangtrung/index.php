<script type="text/javascript" src="js/donhangtrung.js"></script>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Quản lý đơn hàng Trung Quốc</strong>
                    </div>
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Mã đơn</th>
                                    <th>Khách hàng</th>
                                    <th>Ghi chú</th>
                                    <th>Tổng tiền</th>
                                    <th>Ngày giờ</th>
                                    <th>Trạng thái</th>
                                    <th>Tình trạng</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($this->data as $row) {
                                    echo '<tr>
                                                <td>' . $i . '</td>
                                                <td>' . $row['id'] . '</td>
                                                <td>' . $row['user_code'].' - '.$row['name'] . '</td>
                                                <td style="max-width: 200px">' . $row['comment'] . '</td>
                                                <td class="text-right">' . number_format($row['item_price']*$row['quantity'],2) .' '.$row['currency']. '</td>
                                                <td>' . date("d/m/Y H:i:s", strtotime($row['updated'])) . '</td>
                                                <td>' . functions::generateStatusOrder($row['status']) . '</td>';
                                    echo '<td align="center"><span ';
                                    if ($row['active'] == 1)
                                        echo 'class="badge badge-pill badge-secondary">Chưa đặt cọc</span>';
                                    else
                                        echo 'class="badge badge-pill badge-success">Đã đặt cọc</span></td>';
                                    echo '<td><a href="donhangtrung/edit?id='.$row['id'].'" class="text-primary"><i class="fa fa-eye"></i></a></td>';
                                    echo '<td><a onclick="xoadon(' . $row['id'] . ');" class="text-danger"><i class="fa fa-trash-o"></i></a></td>';
                                    echo '</tr> ';
                                    $i++;
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
