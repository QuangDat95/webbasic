<?php
$donhang = $this->data;
?>
<script type="text/javascript" src="js/donhangtrung.js"></script>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Đơn hàng</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="donhang">Đơn hàng</a></li>
                    <li class="active">Tạo đơn</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<style>
    .img-cn-order {
        width: 100px;
        height: 100px;
        object-fit: cover;
        margin-right: 20px;
    }

    .name-product-cn-order {
        max-width: 400px;
        color: #333;
    }
</style>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-12">
                <div class="card" id="listphieu">
                    <div class="card-header"><strong>Chi tiết đơn hàng</strong></div>
                    <div class="card-body card-block">
                        <form method="post" action="donhangtrung/editsave">
                            <div class="form-group">
                                <div class="col col-md-6">
                                    <label for="khachhang">Khách hàng</label>
                                    <input class="form-control" type="text" name="khachhang" id="khachhang" value="<?php if (count($donhang) > 0) {
                                                                                                                                    echo $donhang[0]['name'];
                                                                                                                                } ?>">
                                    <input class="form-control d-none" readonly type="text" name="idkh" id="idkh" value="<?php if (count($donhang) > 0) {
                                                                                                                                echo $donhang[0]['customer'];
                                                                                                                            } ?>">
                                    <input class="form-control d-none" type="text" name="madon" id="madon" value="<?php if (count($donhang) > 0) {
                                                                                                                        echo $donhang[0]['id'];
                                                                                                                    } ?>">
                                </div>
                                <div class="col col-md-6">
                                    <label for="sdt">Điện thoại</label>
                                    <input class="form-control" readonly type="text" name="sdt" id="sdt" value="<?php if (count($donhang) > 0) {
                                                                                                                    echo $donhang[0]['phone'];
                                                                                                                } ?>">
                                </div>
                                <div class="col col-md-6">
                                    <label for="email">Email</label>
                                    <input class="form-control" readonly type="text" name="email" id="email" value="<?php if (count($donhang) > 0) {
                                                                                                                        echo $donhang[0]['email'];
                                                                                                                    } ?>">
                                </div>
                                <div class="col col-md-6">
                                    <label for="diachi">Địa chỉ</label>
                                    <input class="form-control" type="text" name="diachi" id="diachi" value="<?php if (count($donhang) > 0) {
                                                                                                                    echo $donhang[0]['address'];
                                                                                                                } ?>">
                                </div>
                                <div class="col col-md-6">
                                    <input type="hidden" name="tempTinh" id="tempTinh" value="<?php if (count($donhang) > 0) { echo $donhang[0]['province']; } ?>">
                                    <input type="hidden" name="tempHuyen" id="tempHuyen" value="<?php if (count($donhang) > 0) { echo $donhang[0]['district']; } ?>">
                                    <label>Tỉnh/Thành phố</label>
                                    <select name="tinh" id="tinh" onchange="loadhuyen()" class="form-control" required>
                                        <!-- <option value="0">Chọn tỉnh/thành phố</option> -->
                                        <?php foreach ($this->tinh as $item) {
                                            echo '<option value="'.$item['id'].'">'.$item['name'].'</option>';
                                        } ?>
                                    </select>
                                </div>
                                <div class="col col-md-6">
                                    <label>Quận/Huyện</label>
                                    <select name="huyen" id="huyen" class="form-control" required>
                                        <!-- <option value="0">Chọn Quận/huyện</option> -->
                                    </select>
                                </div>
                                 <div class="col col-md-6">
                                    <label>Trạng thái đơn</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="1" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 1) {
                                                                echo 'selected';
                                                            } ?>>Đang xử lý</option>
                                        <option value="2" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 2) {
                                                                echo 'selected';
                                                            } ?>>Đã xử lý</option>
                                        <option value="3" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 3) {
                                                                echo 'selected';
                                                            } ?>>Đã đặt hàng</option>
                                        <option value="4" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 4) {
                                                                echo 'selected';
                                                            } ?>>Đã phát hàng</option>
                                        <option value="5" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 5) {
                                                                echo 'selected';
                                                            } ?>>Ký nhận kho Trung</option>
                                        <option value="6" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 6) {
                                                                echo 'selected';
                                                            } ?>>Đã về kho Việt Nam</option>
                                        <option value="7" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 7) {
                                                                echo 'selected';
                                                            } ?>>Đã giao</option>
                                        <option value="8" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 8) {
                                                                echo 'selected';
                                                            } ?>>Hết hàng</option>
                                        <option value="9" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 9) {
                                                                echo 'selected';
                                                            } ?>>Đơn hàng đã hủy</option>
                                    </select>
                                </div>
                                <div class="col col-md-6 d-none">
                                    <label for="">Tình trạng đơn</label>
                                    <select class="form-control" name="active" id="active">
                                        <option value="1" <?php if (count($donhang) > 0 && $donhang[0]['active'] == 1) {
                                                                echo 'selected';
                                                            } ?>>
                                            Chưa đặt cọc
                                        </option>
                                        <option value="2" <?php if (count($donhang) > 0 && $donhang[0]['active'] == 2) {
                                                                echo 'selected';
                                                            } ?>>
                                            Đã đặt cọc
                                        </option>
                                    </select>
                                </div>

                                <div class="col col-md-6">
                                    <label for="comment">Ghi chú</label>
                                    <input class="form-control" type="text" name="comment" id="comment" value="<?php if (count($donhang) > 0) {
                                                                                                                    echo $donhang[0]['comment'];
                                                                                                                } ?>">
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-12 mt-5">
                                    <table class="table w-100">
                                        <thead>
                                            <tr>
                                                <td>Hình ảnh</td>
                                                <td>Sản phẩm</td>
                                                <td class="text-center">Đơn vị tiền tệ</td>
                                                <td class="text-right">Đơn giá</td>
                                                <td class="text-center">Số lượng</td>
                                                <td class="text-right">Thành tiền</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="<?php if (count($donhang) > 0) {
                                                                    echo $donhang[0]['item_link'];
                                                                } ?>" target="_blank">
                                                        <img class="img-cn-order" src="<?php if (count($donhang) > 0) {
                                                                                            echo $donhang[0]['item_image'];
                                                                                        } ?>" alt="<?php if (count($donhang) > 0) {
                                                                                                        echo $donhang[0]['item_title'];
                                                                                                    } ?>">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="<?php if (count($donhang) > 0) {
                                                                    echo $donhang[0]['item_link'];
                                                                } ?>" target="_blank">
                                                        <div class="name-product-cn-order">
                                                            <?php if (count($donhang) > 0) {
                                                                echo $donhang[0]['item_title'];
                                                            } ?>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <?php if (count($donhang) > 0) {
                                                        echo $donhang[0]['currency'];
                                                    } ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php if (count($donhang) > 0) {
                                                        echo number_format($donhang[0]['item_price'], 2);
                                                    } ?>
                                                </td>
                                                <td class="text-center"> <?php if (count($donhang) > 0) {
                                                                                echo number_format($donhang[0]['quantity']);
                                                                            } ?></td>
                                                <td class="text-right">
                                                    <?php if (count($donhang) > 0) {
                                                        echo number_format($donhang[0]['item_price'] * $donhang[0]['quantity'], 2);
                                                    } ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" name="btntt" class="btn btn-outline-success w-25 btn-block" style="margin-top: 20px;">Lưu</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->
</div><!-- /#right-panel -->
<!-- Right Panel -->

<script>
    $(function() {
        var tinh = $("#tempTinh").val();
        var huyen = $("#tempHuyen").val();
        $.get("khachhang/gethuyen?tp=" + tinh, function(data, status) {
            var sel = document.getElementById("huyen");
            sel.innerHTML = "<option value='0'>Chọn Quận/huyện</option>";
            var options = JSON.parse(data, true);
            options.forEach(function(item) {
                var option = document.createElement("option");
                option.value = item.id;
                option.text = item.name;
                if (item.id == huyen) {
                    option.selected = true;
                }
                sel.add(option);
            });
        });
        $("#tinh")
            .val(tinh)
            .find("option[value=" + tinh + "]")
            .attr("selected", true);
    });
    
    function loadhuyen() {
        var tinh = document.getElementById("tinh").value;
        $.get("khachhang/gethuyen?tp=" + tinh, function(data, status) {
            var sel = document.getElementById("huyen");
            sel.innerHTML = "<option value='0'>Chọn Quận/huyện</option>";
            var options = JSON.parse(data, true);
            options.forEach(function(item) {
                var option = document.createElement("option");
                option.value = item.id;
                option.text = item.name;
                sel.add(option);
            });
        });
    }
    
</script>