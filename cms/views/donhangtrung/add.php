<?php
if (isset($_SESSION['cart']) && (count($_SESSION['cart']) > 0)) {
    $_SESSION['cart'] = $_SESSION['cart'];
} else {
    $_SESSION['cart'] = array();
}
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Đơn hàng</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="donhang">Đơn hàng</a></li>
                    <li class="active">Tạo đơn</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">


        <div class="row">
            <div class="col-12 col-md-5">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Danh sách hàng hóa</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Hình ảnh</th>
                                    <th>Loại</th>
                                    <th width="30"></th>
                                </tr>
                            </thead>


                            <tbody>
                                <?php
                                $sanpham = $this->sanpham;
                                if (sizeof($sanpham) > 0) {
                                    $i = 1;
                                    foreach ($sanpham as $item) {
                                        echo '<tr>
                                                       <td>' . $item['name'] . '</td>
                                                       <td><img src="' . $item['hinhanh'] . '" alt="" height="60"></td>
                                                       <td>
                                                            <select class="form-control" id="type' . $item['id'] . '">';
                                                            foreach ($this->type as $value) {
                                                                if ($item['id']==$value['product']) {
                                                                    echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                                                                }
                                                            }
                                                        echo'    </select>
                                                        </td>';
                                        echo '<td align="center">';
                                        echo ' <button class="btn btn-warning btn-sm" type="button" title="Thêm vào phiếu" data-toggle="modal" data-target=".bd-example-modal-lg1" onclick="addphieu(' . $item['id'] . ')" ><i class="fa fa-plus"></i></button> ';
                                        echo '</td></tr>';
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div> <!-- .card -->

            </div>
            <!--/.col-->

            <div class="col-12 col-md-7">
                <div class="card">
                    <div class="card-header"><strong>Phiếu bán hàng</strong></div>
                    <label class="w-100 text-center text-danger mt-2" id="thongbao"></label>
                    <div class="card-body card-block">
                        <table class="table table-striped" id="listphieu">
                            <thead>
                                <tr>
                                    <th scope="col">Tên sản phẩm</th>
                                    <th scope="col">Hình ảnh</th>
                                    <th scope="col">Số lượng</th>
                                    <th scope="col">Đơn giá</th>
                                    <th scope="col">Thành tiền</th>
                                    <th scope="col">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $tong = 0;
                                foreach ($_SESSION['cart'] as  $item) {
                                    foreach ($item as  $value) { 
                                    $tt = $value['num'] * $value['price'];
                                ?>
                                    <tr>
                                        <th><?= $value['name'] ?><br>
                                            (<?=$value['typename']?>)
                                        </th>
                                        <td><img src="<?= $value['hinhanh'] ?>" alt="" height="60"></td>
                                        <td><input class="form-control" type="number" style="width: 70px;" id="soluong_<?= $value['id'].$value['type'] ?>" min="0" onchange="suasl(<?= $value['id'] ?>,<?= $value['type'] ?>)" value="<?= $value['num'] ?>"></td>
                                        <td><input class="form-control" type="text" maxlength="15" id="gia_<?= $value['id'].$value['type'] ?>" onchange="suagia(<?= $value['id'] ?>,<?= $value['type'] ?>)" value="<?= number_format($value['price']) ?>" onkeyup="javascript:this.value=Comma(this.value);"></td>
                                        <td><?= number_format($tt) ?></td>
                                        <td><button class="btn btn-danger btn-sm" onclick="xoaphieu(<?= $value['id'] ?>,<?= $value['type'] ?>)"><i class="fa fa-trash-o"></i></button></td>
                                    </tr>
                                <?php $tong = $tong + $tt;
                                } } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3" align="right"> <b>Total:</b></td>
                                    <td colspan="2" align="center"> <?= number_format($tong) ?></td>
                                </tr>
                            </tfoot>
                        </table>
                        <form method="post" action="donhang/save">
                            <div class="form-group">

                                <div class="col col-md-6">
                                    <label for="exampleInputEmail1">Chọn khách hàng</label>
                                    <select class="form-control" name="khachhang" id="khachhang" onchange="loadkhachhang()" required>
                                        <option value="" selected>--Chọn khách hàng--</option>
                                        <?php foreach ($this->khachhang as  $value) {
                                            echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                        } ?>
                                    </select>
                                    <input type="hidden" name="name" id="name" value="">
                                </div>
                                <div class="col col-md-6">
                                    <label for="exampleInputEmail1">Điện thoại</label>
                                    <input class="form-control" type="tel" name="sdt" id="sdt" value="" required>
                                </div>
                                <div class="col col-md-6">
                                    <label for="exampleInputEmail1">Hình thức thanh toán</label>
                                    <select class="form-control" name="thanhtoan" id="thanhtoan">
                                        <option value="0">Thanh toán khi nhận hàng</option>
                                        <option value="1">Hình thức khác</option>
                                    </select>
                                </div>
                                <div class="col col-md-12">
                                    <label for="exampleInputEmail1">Địa chỉ</label>
                                    <input class="form-control" type="text" name="diachi" id="diachi" value="" required>
                                </div>

                                <div class="col col-md-12">
                                    <label for="exampleInputEmail1">Ghi chú</label>
                                    <input class="form-control" type="text" name="ghichu" id="ghichu" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col col-md-6">
                                    <label for="exampleInputEmail1"></label>
                                </div>
                                <div class="col col-md-6">
                                    
                                    <h4 class="d-none" style="margin-top: 20px;">Thành tiền: <label><?= number_format($tong) ?></label>đ</h4>
                                </div>
                            </div>
                            <div id="countcart1">
                            <input class="form-control d-none" type="text" name="thanhtien" id="thanhtien" value="<?= $tong ?>">
                            <input type="hidden" id="countcart" value="<?= count($_SESSION['cart'])?>">
                            </div>
                            <button type="submit" name="btntt" onclick="return checkSubmit()" class="btn btn-outline-success btn-lg btn-block" style="margin-top: 20px;">Lưu</button>
                        </form>
                    </div>
                </div>
            </div>


        </div><!-- .animated -->
    </div><!-- .content -->
</div><!-- /#right-panel -->
<!-- Right Panel -->
<script>
    function addphieu(id) {
        type = $("#type" + id).val();
        $.post("donhang/addphieu", {
            'id': id, 'type':type
        }, function(data) {
            $("#listphieu").load("<?= CMS ?>/donhang/add #listphieu");
            $("#countcart1").load("<?= CMS ?>/donhang/add #countcart1");
        });
    }

    function xoaphieu(id,type) {
        $.post("donhang/deletephieu", {
            'id': id,
            'type':type,
            'num': 0
        }, function(data) {
            $("#listphieu").load("<?= CMS ?>/donhang/add #listphieu");
            $("#countcart1").load("<?= CMS ?>/donhang/add #countcart1");
        });
    }

    function suasl(id,type) {
        num = $("#soluong_" + id+type).val();
        $.post("donhang/deletephieu", {
            'id': id,
            'type':type,
            'num': num
        }, function(data) {
            $("#listphieu").load("<?= CMS ?>/donhang/add #listphieu");
            $("#countcart1").load("<?= CMS ?>/donhang/add #countcart1");
        });
    }

    function suagia(id,type) {
        price = $("#gia_" + id+type).val();
        price = price.replaceAll(',', '')
        $.post("donhang/updategia", {
            'id': id,
            'type':type,
            'price': price
        }, function(data) {
            $("#listphieu").load("<?= CMS ?>/donhang/add #listphieu");
        });
    }

    function loadkhachhang() {
        var kh = $("#khachhang").val();
        $.get("donhang/getkhachhang?id=" + kh, function(data, status) {
            let kh = JSON.parse(data);
            document.getElementById("sdt").value = kh.phone;
            document.getElementById("diachi").value = kh.address;
            document.getElementById("name").value = kh.name;
        });
    }

    function checkSubmit() {
        let countcart = $("#countcart").val();
        if (countcart > 0) {
            return true;
        }else{
            document.getElementById("thongbao").innerHTML="Bạn cần phải chọn mặt hàng";
            return false;
            
        }
    }

    //   function chetkhau(){
    //       var ck= $("#chietkhau").val();
    //       var tong=$("#tong").val();
    //       var tt= tong-(tong*ck/100);
    //       $("#thanhtien").text(comma(tt));
    //       $("#thanhtien1").val(tt);
    //   }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#example').DataTable({
            fixedHeader: true,
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true, // tiền xử lý trước
            "aLengthMenu": [
                [10, 20, 50, 100],
                [10, 20, 50, 100]
            ], // danh sách số trang trên 1 lần hiển thị bảng
        });
    });
</script>