<script type="text/javascript" src="js/donhang.js"></script>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Quản lý đơn hàng website</strong>
                        <a href="donhang/add" class="btn btn-primary ml-4"><i class="fa fa-plus"></i>&nbsp; Tạo đơn mới</a>
                    </div>
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Khách hàng</th>
                                    <th>Số sản phẩm</th>
                                    <th>Tổng tiền</th>
                                    <th>Ngày giờ</th>
                                    <th>Tình trạng</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($this->data as $row) {
                                    echo '<tr>
                                                <td>' . $i . '</td>
                                                <td>' . $row['name'] . '</td>
                                                <td>' . $row['tong'] . '</td>
                                                <td class="text-right">' . number_format($row['total']) . '</td>
                                                <td>' . date("d/m/Y H:i:s", strtotime($row['updated'])) . '</td>';
                                    echo '<td align="center"><span ';
                                    if ($row['status'] == 1)
                                        echo 'class="badge badge-pill badge-primary">Đơn hàng mới</span>';
                                    elseif ($row['status'] == 2)
                                        echo 'class="badge badge-pill badge-warning">Đã xác nhận</span>';
                                    elseif ($row['status'] == 3)
                                        echo 'class="badge badge-pill badge-danger">Đang giao hàng</span></td>';
                                    elseif ($row['status'] == 4)
                                        echo 'class="badge badge-pill badge-success" disabled>Hoàn thành</span>';
                                    else
                                        echo 'class="badge badge-pill badge-secondary" disabled>Đã hủy</span></td>';

                                    echo '<td><a href="donhang/edit?id='.$row['id'].'" class="text-primary"><i class="fa fa-eye"></i></a></td>';
                                    echo '<td><a href="donhang/indon?id=' . $row['id'] . '" class="text-info"><i class="fa fa-print mr-2"></i></a>  <a onclick="xoadon(' . $row['id'] . ');" class="text-danger"><i class="fa fa-trash-o"></i></a></td>';
                                    echo '</tr> ';
                                    $i++;
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
