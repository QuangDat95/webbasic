<?php
// unset($_SESSION['cartedit']);
$donhang = $this->data;
$cart = array();
foreach ($this->cartdetail as $value) {
    $cart[$value['idproduct']] = array(
                    'id' => $value['idproduct'],
                    'name' => $value['name'],
                    'num' => (int)$value['num'],
                    'hinhanh' => $value['hinhanh'],
                    'price' => $value['price'],
    );
}
// $_SESSION['cartold'] = $cart;
if (isset($_SESSION['cartedit']) && (count($_SESSION['cartedit']) > 0)) {
    $_SESSION['cartedit'] = $_SESSION['cartedit'];
} else {
    $_SESSION['cartedit'] = $cart;
}
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Đơn hàng</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="donhang">Đơn hàng</a></li>
                    <li class="active">Tạo đơn</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">


        <div class="row">
        <div class="col-lg-12">
                <div class="card" id="listphieu">
                    <div class="card-header"><strong>Phiếu bán hàng</strong></div>
                    <div class="card-body card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Tên sản phẩm</th>
                                    <th scope="col">Hình ảnh</th>
                                    <th scope="col">Số lượng</th>
                                    <th scope="col">Đơn giá</th>
                                    <th scope="col">Thành tiền</th>
                                    <th scope="col">Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $tong = 0;
                                foreach ($_SESSION['cartedit'] as $value) {
                                    $tt = $value['num'] * $value['price'];
                                    if(count($_SESSION['cartedit'])==1 && count($_SESSION['cartedit'][$value['id']])==1)
                                       $xoa= "disabled";
                                    else
                                        $xoa="";
                                ?>
                                    <tr>
                                        <th><?= $value['name'] ?></th>
                                        <td><img src="<?= $value['hinhanh'] ?>" alt="" height="60"></td>
                                        <td><input class="form-control" type="number" style="width: 70px;" id="soluong_<?= $value['id'] ?>" min="0" onchange="suasl(<?= $value['id'] ?>)" value="<?= $value['num'] ?>"></td>
                                        <td><input class="form-control" type="text" id="gia_<?= $value['id'] ?>" min="0" onchange="suagia(<?= $value['id'] ?>)" value="<?= number_format($value['price']) ?>" onkeyup="javascript:this.value=Comma(this.value);"></td>
                                        <td><?= number_format($tt) ?></td>
                                        <td><button class="btn btn-danger btn-sm" <?=$xoa?> onclick="xoaphieu(<?= $value['id'] ?>)"><i class="fa fa-trash-o"></i></button></td>
                                    </tr>
                                <?php $tong = $tong + $tt;
                                } ?>


                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3" align="right"> <b>Total:</b></td>
                                    <td colspan="2" align="center"> <?= number_format($tong) ?></td>
                                </tr>
                            </tfoot>
                        </table>
                        <form method="post" action="donhang/editsave">
                            <div class="form-group">
                                <div class="col col-md-6">
                                    <label for="exampleInputEmail1">Hình thức thanh toán</label>
                                    <select class="form-control" name="thanhtoan" id="thanhtoan">
                                        <option value="0" <?php if (count($donhang) > 0 && $donhang[0]['paymentMethod'] == 0) {
                                                                echo 'selected';
                                                            } ?>>
                                            Có vận chuyển
                                        </option>
                                        <option value="1" <?php if (count($donhang) > 0 && $donhang[0]['paymentMethod'] == 1) {
                                                                echo 'selected';
                                                            } ?>>
                                            Không vận chuyển
                                        </option>
                                    </select>
                                </div>
                                <div class="col col-md-6">
                                    <label>Tình trạng đơn</label>
                                    <select name="tinhtrang" id="tinhtrang" class="form-control">
                                        <option value="1" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 1) {
                                                                echo 'selected';
                                                            } ?>>Đơn hàng mới</option>
                                        <option value="2" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 2) {
                                                                echo 'selected';
                                                            } ?>>Đã xác nhận</option>
                                        <option value="3" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 3) {
                                                                echo 'selected';
                                                            } ?>>Đang giao hàng</option>
                                        <option value="4" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 4) {
                                                                echo 'selected';
                                                            } ?>>Thành công</option>
                                        <option value="5" <?php if (count($donhang) > 0 && $donhang[0]['status'] == 5) {
                                                                echo 'selected';
                                                            } ?>>Đã thanh toán</option>
                                    </select>
                                </div>
                                <div class="col col-md-6">
                                    <label for="exampleInputEmail1">Khách hàng</label>
                                    <input class="form-control " type="text" name="khachhang" id="khachhang" value="<?php if (count($donhang) > 0) {
                                                                                                                        echo $donhang[0]['name'];
                                                                                                                    } ?>">
                                    <input class="form-control d-none" type="text" name="email" id="email" value="<?php if (count($donhang) > 0) {
                                                                                                                    echo $donhang[0]['email'];
                                                                                                                } ?>">
                                    <input class="form-control d-none" type="text" name="madon" id="madon" value="<?php if (count($donhang) > 0) {
                                                                                                                    echo $donhang[0]['id'];
                                                                                                                } ?>">
                                </div>
                                <div class="col col-md-6">
                                    <label for="exampleInputEmail1">Điện thoại</label>
                                    <input class="form-control" type="text" name="sdt" id="sdt" value="<?php if (count($donhang) > 0) {
                                                                                                            echo $donhang[0]['phone'];
                                                                                                        } ?>">
                                </div>
                                <div class="col col-md-12">
                                    <label for="exampleInputEmail1">Địa chỉ</label>
                                    <input class="form-control" type="text" name="diachi" id="diachi" value="<?php if (count($donhang) > 0) {
                                                                                                                    echo $donhang[0]['address'];
                                                                                                                } ?>">
                                </div>

                                <div class="col col-md-12">
                                    <label for="exampleInputEmail1">Ghi chú</label>
                                    <input class="form-control" type="text" name="ghichu" id="ghichu" value="<?php if (count($donhang) > 0) {
                                                                                                                    echo $donhang[0]['note'];
                                                                                                                } ?>">
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col col-md-6">
                                    <!-- <label for="exampleInputEmail1">Chiết khấu %</label> -->
                                </div>
                                <div class="col col-md-6">
                                    <h4 style="margin-top: 20px;">Thành tiền: <label id="thanhtien"><?= number_format($tong) ?></label>đ</h4>
                                </div>
                            </div>
                            <input type="hidden" id="countcart" value="<?= count($_SESSION['cart'])?>">
                            <input type="hidden" id="thanhtien" name="thanhtien" value="<?= $tong?>">
                            <button type="submit" name="btntt" class="btn btn-outline-success btn-lg btn-block" style="margin-top: 20px;">Lưu</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Danh sách hàng hóa</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Hình ảnh</th>
                                    <th>Size</th>
                                    <th>Màu</th>
                                    <th width="40"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div> <!-- .card -->

            </div>
            <!--/.col-->

            


        </div><!-- .animated -->
    </div><!-- .content -->
</div><!-- /#right-panel -->
<!-- Right Panel -->
<script>
    $(function() {
        $.fn.dataTable.ext.errMode = "none";
        $("#example").DataTable({
            fixedHeader: true,
            ordering: false,
            "processing": true,
            "serverSide": true,
            "ajax": "donhang/getdatasp"
        });

    });
    function addphieu(id) {
        type = $("#type" + id).val();
        color = $("#color" + id).val();
        $.post("donhang/addphieuedit", {
            'id': id, 'type':type, 'color':color
        }, function(data) {
            $("#listphieu").load("<?= CMS ?>/donhang/edit?id=<?= $this->cartdetail[0]['id'] ?> #listphieu");
        });
    }

    function xoaphieu(id,type,color) {
        $.post("donhang/deletephieuedit", {
            'id': id,
            'type':type,
            'color':color,
            'num': 0
        }, function(data) {
            $("#listphieu").load("<?= CMS ?>/donhang/edit?id=<?= $this->cartdetail[0]['id'] ?> #listphieu");
        });
    }

    function suasl(id) {
        num = $("#soluong_"+id).val();
        $.post("donhang/deletephieuedit", {
            'id': id,
            'num': num
        }, function(data) {
            $("#listphieu").load("<?= CMS ?>/donhang/edit?id=<?= $this->cartdetail[0]['id'] ?> #listphieu");
        });
    }

    function suagia(id) {
        price = $("#gia_"+id).val();
        price = price.replaceAll(',', '');
        $.post("donhang/updategiaedit", {
            'id': id,
            'price': price
        }, function(data) {
            $("#listphieu").load("<?= CMS ?>/donhang/edit?id=<?= $this->cartdetail[0]['id'] ?> #listphieu");
        });
    }

    function loadkhachhang() {
        var kh = $("#khachhang").val();
        $.get("donhang/getkhachhang?id=" + kh, function(data, status) {
            let kh = JSON.parse(data);
            document.getElementById("sdt").value = kh.dien_thoai;
            document.getElementById("diachi").value = kh.dia_chi;
        });
    }


    // function chetkhau() {
    //     var ck = $("#chietkhau").val();
    //     var tong = $("#tong").val();
    //     var tt = tong - (tong * ck / 100);
    //     $("#thanhtien").text(comma(tt));
    //     $("#thanhtien1").val(tt);
    // }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#example').DataTable({
            fixedHeader: true,
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            "processing": true, // tiền xử lý trước
            "aLengthMenu": [
                [10, 20, 50, 100],
                [10, 20, 50, 100]
            ], // danh sách số trang trên 1 lần hiển thị bảng
        });
    });
</script>