<script type="text/javascript" src="js/giaodich.js"></script>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Quản lý giao dịch</strong>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#largeModal" onclick="add()"><i class="fa fa-plus"></i>&nbsp; Add</button>
                    </div>
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered" style="width: 100%">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Nội dung</th>
                           <th>Ngày giờ</th>
                           <th>Thành viên</th>
                           <th>Loại</th>
                           <th>Số tiền</th>
                           <th>Số dư</th>
                           <th>KM1</th>
                           <th>KM2</th>
                           <th width="200">Chức năng</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $loaisp = $this->data;
                           if (sizeof($loaisp)>0) {
                               $i=1;
                               foreach ((array)$loaisp as $item) {
                                   echo '<tr>
                                       <td>'.$item['id'].'</td>
                                       <td>'.$item['noi_dung'].'</td>
                                       <td>'.$item['ngaygio'].'</td>
                                       <td>'.$item['nguoidang'].'</td>
                                       <td>'.$item['loaigd'].'</td>
                                       <td>'.number_format($item['so_tien']).'</td>
                                       <td>'.number_format($item['so_du']).'</td>
                                       <td>'.number_format($item['km1']).'</td>
                                       <td>'.number_format($item['km2']).'</td>';
                                     echo '<td align="center">';
                                     echo ' <button type="button" data-toggle="modal" data-target="#largeModal" onclick="edit('.($i-1).')" ><i class="fa fa-edit"></i></button> ';
                                     echo ' <button onclick="xoa('.$item['id'].')"><i class="fa fa-trash-o"></i></button>';
                                     echo '<td>'.$item['nguoi_dang'].'</td>';
                                     echo '<td>'.$item['loai'].'</td>';
                                   $i++;
                               }
                           }
                           ?>
                     </tbody>
                     <tfoot>
                        <tr>
                            <th colspan="5" style="text-align:center;">TỔNG SỐ TIỀN:</th>
                            <th colspan="1"></th>
                            <th colspan="1"></th>
                            <th colspan="1"></th>
                            <th colspan="1"></th>
                            <th colspan="1">Đơn vị VND</th>
                        </tr>
                    </tfoot>
                  </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .animated -->
</div>
<!-- .content -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form method="POST" action="giaodich/save" id="form-client">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Thông tin giao dịch</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="form-group"> 
                <input type="hidden" id="id" name="id">
                  <label for="exampleInputEmail1">Nội dung</label> 
                  <input type="text" class="form-control" id="noidung" name="noidung" placeholder="Nội dung" required> 
               </div>
               <div class="form-group"> 
                  <label for="exampleInputEmail1">Thành viên</label>
                  <select class="form-control" name="thanhvien" id="thanhvien">
                  <option value="">Chọn thành viên</option>
                    <?php foreach ($this->thanhvien as $value) {
                       echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                    } ?>
                  </select>
               </div>
                <div class="form-group"> 
                  <label for="exampleInputEmail1">Số tiền</label> 
                  <input type="text" class="form-control" id="sotien" name="sotien" onkeyup="javascript:this.value=Comma(this.value);" placeholder="Số tiền" required> 
               </div>
               <div class="form-group">
                  <div class="col col-md-3">
                     <div class="form-check">
                        <div class="radio">
                           <label for="radio1" class="form-check-label ">
                           <input type="radio" id="rd1" name="radio" value="1" class="form-check-input" checked="checked">Nạp
                           </label>
                        </div>
                     </div>
                  </div>
                  <div class="col col-md-3">
                     <div class="form-check">
                        <div class="radio">
                           <label for="radio1" class="form-check-label ">
                           <input type="radio" id="rd2" name="radio" value="0" class="form-check-input">Chi
                           </label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
               <button type="submit" class="btn btn-primary"><i class="fa fa-file-text-o"></i> Lưu</button>
            </div>
         </form>
      </div>
   </div>
</div>

