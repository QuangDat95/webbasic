<script type="text/javascript" src="js/sanpham.js"></script>
<script type="text/javascript" src="libs/tinymce/tinymce.min.js"></script>
<link rel="stylesheet" href="libs/imgupload/4image-upload.css">
<link rel="stylesheet" href="libs/bootstrap-tagsinput/tagsinput.css">
<style>
    #image-upload-demo {
        width: 100%;
        margin: auto;
    }
</style>
<div class="col-lg-12">
    <div class="card">
        <form action="sanpham/editsave?id=<?= $this->data['id'] ?>" method="post" enctype="multipart/form-data" id="fm">
            <div class="card-header"><strong>Cập nhật</strong><small> Sản phẩm</small></div>
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col-12 col-md-5">
                        <div class="form-group"><label for="city" class=" form-control-label">Tên</label><input
                                    type="text" id="name" name="name" placeholder="Nhập tên sản phẩm"
                                    class="form-control" value="<?= $this->data['name'] ?>" required="required"></div>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="form-group"><label for="postal-code" class=" form-control-label">Danh mục</label>
                            <select name="danh_muc" class="form-control">
                                <?php foreach ($this->danhmuc as $item) {
                                    if ($this->data['category'] == $item['id']) {
                                        echo '<option value="' . $item['id'] . '" selected>' . $char . ' ' . $item['name'] . '</option>';
                                    } else {
                                        echo '<option value="' . $item['id'] . '">' . $char . ' ' . $item['name'] . '</option>';
                                    }
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-2">
                        <div class="form-group"><label for="postal-code" class=" form-control-label">Tình trạng</label>
                            <select name="status" class="form-control">
                                <option <?php if ($this->data['status'] == 1) echo "selected"; ?> value="1">Hiển thị
                                </option>
                                <option <?php if ($this->data['status'] == 2) echo "selected"; ?> value="2">Ẩn</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row form-group">
                    <!-- <div class="col-12 col-md-3">
                        <div class="form-group"><label for="city" class=" form-control-label">Model</label>
                            <input type="hidden" placeholder="Model" name="ma" value="<?= $this->data['code'] ?>" class="form-control">
                        </div>
                    </div> -->
                    <div class="col-12 col-md-3">
                        <div class="form-group"><label for="city" class=" form-control-label">Tag</label>
                            <input type="text" placeholder="Tag" name="tag" value="<?= $this->data['tag'] ?>"
                                   class="form-control tags">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group"><label for="postal-code" class=" form-control-label">Url</label><input
                                    type="text" name="url" placeholder="Để trống sẽ lấy theo tên"
                                    value="<?= $this->data['url'] ?>" class="form-control"></div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="position" class="form-control-label">Vị trí</label>
                            <select class="form-control" name="position[]" multiple="multiple" id="position">
                                <?php
                                $position = explode(',', $this->data['position']);
                                ?>
                                <option value="1" <?= in_array('1', $position) ? 'selected' : '' ?>>Slider</option>
                                <option value="2" <?= in_array('2', $position) ? 'selected' : '' ?>>Khuyến mãi trong
                                    ngày
                                </option>
                                <option value="2" <?= in_array('2', $position) ? 'selected' : '' ?>>Danh mục nổi bật
                                </option>
                                <option value="2" <?= in_array('2', $position) ? 'selected' : '' ?>>Tin tức</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group"><label for="postal-code" class=" form-control-label">Giá</label>
                            <input type="text" name="price" placeholder="Giá niêm yết"
                                   value="<?= $this->data['price'] ?>" class="form-control"></div>
                    </div>
                    <!-- <div class="col-12 col-md-3">
                        <div class="form-group"><label for="postal-code" class=" form-control-label">Giá Khách lẻ</label><input type="number" name="guestprice" placeholder="Giá khách lẻ" value="<?= $this->data['guest_price'] ?>" class="form-control"></div>
                    </div> -->
                    <?php
                    foreach ($this->properties as $property) {
                        $value = '';
                        foreach ($this->properties_detail as $property_detail)
                            if ($property['id'] == $property_detail['property'])
                                $value = $property_detail['value'];
                        ?>
                        <div class="col-12 col-md-3">
                            <div class="form-group"><label for="postal-code"
                                                           class=" form-control-label"><?= $property['name'] ?></label>
                                <input type="text" name="property_<?= $property['id'] ?>"
                                       placeholder="<?= $property['name'] ?>" value="<?= $value ?>"
                                       class="form-control"></div>
                        </div>
                        <?php

                    } ?>
                </div>

                <div class="row form-group">
                    <div class="col-12">
                        <div id="image-upload-demo">
                            <div id="iu-gallery">
                                <div id="iu-image-like-div">
                                    <div class="iu-image-placeholder" id="iu-image-upload-zone">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-12">
                        <div class="form-group"><label for="city" class=" form-control-label">Thông số kỹ thuật</label>
                            <textarea rows="10" class="form-control"
                                      name="mo_ta"><?= $this->data['description'] ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-12">
                        <div class="form-group"><label for="city" class=" form-control-label">Nội dung</label>
                            <textarea rows="10" class="form-control"
                                      name="noidung"><?= $this->data['content'] ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-12 center">
                        <button type="button" class="btn btn-lg btn-info btn-block" onclick="submitForm()">
                            <i class="ti-save"></i>&nbsp;
                            <span id="payment-button-amount">CẬP NHẬT</span>
                    </div>
                </div>
        </form>
    </div>


    <script>
        tinymce.init({
            mode: "textareas",
            entity_encoding: "raw",
            plugins: ["advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen textcolor", "media",
                "insertdatetime media table contextmenu paste jbimages", "fullscreen", "moxiemanager"
            ],
            image_advtab: true,
            paste_data_images: true,
            browser_spellcheck: true,
            relative_urls: false,
            remove_script_host: false,
            //convert_urls : true,
            image_dimensions: false,
            forced_root_block: false,
            force_br_newlines: true,
            force_p_newlines: false,
            toolbar: " undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media insertfile |  fontsizeselect | forecolor backcolor | fullscreen"
        });
    </script>

    <script src="libs/imgupload/4image-upload.js"></script>
    <script src="libs/bootstrap-tagsinput/tagsinput.js"></script>
    <script>
        $(document).ready(function () {
            $('#position').select2();
            $('input.tags').tagsinput({
                trimValue: true
            });
        });

        function submitForm() {
            var form = document.getElementById("fm");
            form.submit();

        }
    </script>


    <script id="jsNeedToCopy">
        //Detect Internet Explorer. Show alert to customer.
        if (window.document.documentMode) {
            alert("This website doesn't work on Internet Explorer, you should use modern browsers like Chrome or Safari instead");
        }
        var myImageUpload = new ImageUpload({
            imageUploadZoneId: 'iu-image-upload-zone',
            imageGalleryId: 'iu-gallery',
            sendRequestToServer: true,
            saveImageRoute: "sanpham/saveimageedit",
            imageBelongsTo: {
                "post_id": <?= $this->data['id'] ?>
            },
            saveImageOrderRoute: "sanpham/saveImageOrder",
            deleteImageRoute: "sanpham/deleteimage",


            dictUploadImageNote: '<img src="https://4imageupload.com//demo-image/image-uploader-icon.png" class="iu-image-icon"><p class="iu-note-text">Chọn tệp hoặc kéo thả để upload ảnh</p>',
            insertImageItemBeforeElmnt: document.getElementById('iu-image-upload-zone').parentNode,
            getTargetForHighlight: function () {
                return document.getElementById('iu-image-upload-zone').getElementsByClassName('iu-image-note')[0];
            },
            showUploadingLoader: function (imagePlaceholder, showUploadedPercentComplete) {
                var progressBar = document.createElement('div');
                progressBar.className = 'iu-progress-bar';
                var percentBar = document.createElement('div');
                percentBar.className = 'iu-percent-bar';
                if (showUploadedPercentComplete === true) {
                    percentBar.innerHTML = '0%';
                }
                progressBar.appendChild(percentBar);
                imagePlaceholder.appendChild(progressBar);
            },
            updateUploadingLoader: function (percentComplete, imagePlaceholder, showUploadedPercentComplete) {
                var percentComplete = Math.floor(percentComplete);
                var percentBar = imagePlaceholder.getElementsByClassName('iu-percent-bar')[0];

                if (percentBar != null) {
                    percentBar.style.width = percentComplete + "%";
                    if (showUploadedPercentComplete === true) {
                        percentBar.innerHTML = percentComplete + "%";
                    }
                }
            },
            removeUploadingLoader: function (imagePlaceholder, showUploadedPercentComplete) {
                var progressBar = imagePlaceholder.getElementsByClassName('iu-progress-bar')[0];
                var fadeEffect = setInterval(function () {
                    if (!progressBar.style.opacity) {
                        progressBar.style.opacity = 1;
                    }
                    if (progressBar.style.opacity > 0) {
                        progressBar.style.opacity -= 0.1;
                    } else {
                        clearInterval(fadeEffect);
                        progressBar.remove();
                    }
                }, 300);
            },

            addFlashBox: function (showedAlertString, showedTime, backgroundColor) {
                var oldFlashBox = document.getElementsByClassName('iu-flash-box')[0];
                if (oldFlashBox) {
                    oldFlashBox.remove();
                }
                var flashBox = document.createElement('div');
                flashBox.className = 'iu-flash-box';
                if (backgroundColor) {
                    flashBox.style.backgroundColor = backgroundColor;
                }
                flashBox.innerHTML = showedAlertString;
                document.body.appendChild(flashBox);
                setTimeout(function () {
                    fadeEffect(flashBox);
                }, showedTime);

                function fadeEffect(elmnt) {
                    var fadeEffect = setInterval(function () {
                        if (!elmnt.style.opacity) {
                            elmnt.style.opacity = 1;
                        }
                        if (elmnt.style.opacity > 0) {
                            elmnt.style.opacity -= 0.1;
                        } else {
                            clearInterval(fadeEffect);
                            elmnt.remove();
                        }
                    }, 50);
                }
            },


        });

        setTimeout(function () {
            <?php $dem = count($this->hinhanh);
            foreach ($this->hinhanh as $key => $value) { ?>
            var imageSrc = "<?= URL . '/' . $value['link'] ?>";
            var imageName = '<?= $value['link'] ?>';
            var imageId = '<?= $value['id'] ?>';
            <?php if ($dem == ($key + 1)) {
            echo 'myImageUpload.appendServerImage(imageSrc, imageName, imageId);';
        } else {
            echo ' myImageUpload.appendServerImage(imageSrc, imageName, imageId);';
        } ?>
            <?php } ?>})
    </script>