<style type="text/css">
    .w-90 {
        width: 90% !important;
        max-width: 1580px !important;
    }

    .scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow: 0px 0px 0px 0px #000;
        box-shadow: 0px 0px 0px 0px #000;
    }
</style>
<script type="text/javascript" src="js/listuser.js"></script>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Quản lý tài khoản khách hàng</strong>

                    </div>
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered" style="width: 100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Tên khách hàng</th>
                                    <th>Số điện thoại</th>
                                    <th>Địa chỉ</th>
                                    <th>Email</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($this->data as $row) {
                                    echo '<tr>
                                            <td>' . $i . '</td>
                                            <td>' . $row['name'] . '</td>
                                            <td>' . $row['phone'] . '</td>                                   
                                            <td>' . $row['address'] . '</td>                                   
                                            <td>' . $row['email'] . '</td>         
                                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit(' . $i . ')"><i class="fa fa-edit"></i></a></td>
                                            <td><a href="javascript:void(0)" onclick="del(' . $row['id'] . ')"><i class="fa fa-trash-o"></i></a>  </td>
                                        </tr>';
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST" action="listuser/save" id="form-client" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thông tin tài khoản khách hàng</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- <div class="form-group">
                        <input type="hidden" id="id" name="id">
                        <label for="">Tên tài khoản</label>
                        <input type="text" name="name" id="name" placeholder="Tên tài khoản" class="input-sm form-control-sm form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Giới tính</label>
                        <select name="classify" id="classify" class="input-sm form-control-sm form-control">
                            <option value="">Chọn giới tính</option>
                            <option value="1">Nam</option>
                            <option value="2">Nữ</option>
                            <option value="3">Khác</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Số điện thoại</label>
                        <input type="date" placeholder="Số điện thoại" name="phonenumber" id="phonenumber" class="input-sm form-control-sm form-control">
                    </div>

                    <div class="form-group">
                        <label for="">Số điện thoại</label>
                        <input type="text" placeholder="Số điện thoại" name="phonenumber" id="phonenumber" class="input-sm form-control-sm form-control">
                    </div>

                    <div class="form-group">
                        <label for="" class="">CCCD/CMND: </label>
                        <input type="text" name="ten_don_vi" id="ten_don_vi" class="input-sm form-control-sm form-control">
                    </div>

                    <div class="form-group">
                        <label for="" class="">Email: </label>
                        <input type="text" name="nguoi_dai_dien" id="nguoi_dai_dien" class="input-sm form-control-sm form-control">
                    </div>

                    <div class="form-group">
                        <label for="" class="">Địa chỉ: </label>
                        <input type="text" name="ma_so_thue" id="ma_so_thue" class="input-sm form-control-sm form-control">
                    </div>

                    <div class="form-group">
                        <label for="" class="">Tài khoản ngân hàng: </label>
                        <input type="text" name="tai_khoan" id="tai_khoan" class="input-sm form-control-sm form-control">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Phân loại</label>
                        <select name="classify" id="classify" class="input-sm form-control-sm form-control">
                            <option value="0">Khách lẻ</option>
                            <option value="1">Đại lý</option>

                        </select>
                    </div> -->

                    <div class="media mb-2 col-12">
                        <input type="hidden" value="">
                        <div class="col-lg-4 d-flex mt-1 px-0">
                            <img id="avatar" src="" alt="users avatar" class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" height="90" width="90" />
                            <div class="media-body col-lg-12 mt-50">
                                <h4 id="nhanvien">No name</h4>
                                <div class="d-flex mt-1 px-0">
                                    <label class="btn btn-primary mr-75 mb-0" for="hinhanh">
                                        <span class="d-none d-sm-block">Thay ảnh</span>
                                        <input class="form-control" type="file" id="hinhanh" name="hinhanh" hidden accept="image/png, image/jpeg, image/jpg" onchange="thayanh()" />
                                        <span class="d-block d-sm-none">
                                            <i class="mr-0" data-feather="edit"></i>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 d-flex mt-1 px-0">
                            <div class="form-group">
                                <label class="d-block mb-1">Giới tính</label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="male" name="gender" class="custom-control-input" value="1" />
                                    <label class="custom-control-label" for="male">Nam</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="female" name="gender" class="custom-control-input" value="0" />
                                    <label class="custom-control-label" for="female">Nữ</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="hoten">Họ và tên</label>
                                <input id="hoten" type="text" class="form-control" name="hoten" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="ngaysinh">Ngày sinh</label>
                                <input id="ngaysinh" name="ngaysinh" type="date" class="form-control flatpickr-validation flatpickr" placeholder="22 Dec 2001" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="dienthoai">Điện thoại</label>
                                <input id="dienthoai" type="text" class="form-control" name="dienthoai" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="text" class="form-control" placeholder="Email cá nhân" name="email" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="diachi">Địa chỉ</label>
                                <input id="diachi" type="text" class="form-control" placeholder="Chỗ ở hiện tại" name="diachi" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="cmnd">CMND/CCCD</label>
                                <input id="cmnd" type="text" class="form-control" name="cmnd" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="thuongtru">Địa chỉ thường trú</label>
                                <input id="thuongtru" type="text" class="form-control" name="thuongtru" placeholder="Địa chỉ thường trú" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" name="btnluu" class="btn btn-primary"><i class="fa fa-file-text-o"></i> Lưu
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>