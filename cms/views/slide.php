<script type="text/javascript" src="js/slide.js"></script>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Quản lý slide</strong>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#largeModal" onclick="add()"><i class="fa fa-plus"></i>&nbsp; Add</button>
                    </div>
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên</th>
                                    <th>Hình ảnh</th>
                                    <th style="display: none;"></th>
                                    <th style="display: none;"></th>
                                    <th style="display: none;"></th>
                                    <th>Số thứ tự </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ((array)$this->data as $row) {
                                    echo '<tr>
                                            <td>' . $row['id'] . '</td>
                                            <td>' . $row['name'] . '</td>
                                            <td><img src="' . URL . '/' . $row['image'] . '" height="60"></td>
                                            <td style="display: none;">' . $row['image'] . '</td>
                                            <td style="display: none;">' . $row['description'] . '</td>
                                            <td style="display: none;">' . $row['url'] . '</td>
                                            <td>' . $row['order_sort'] . '</td>
                                            <td><a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit(' . $i . ')"><i class="fa fa-edit"></i></a></td>
                                            <td><a href="javascript:void(0)" onclick="del('.$row['id'].')"><i class="fa fa-trash-o"></i></a>  </td>
                                        </tr>';
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Thông tin slide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-client" method="post" enctype="multipart/form-data" action="slide/save">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <input type="hidden" id="id" name="id">
                                <div class="col col-md-3"><label for="name" class=" form-control-label">Tên slide</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="name" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="hinhanh" class=" form-control-label">Hình ảnh</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="file" id="hinhanh" name="hinhanh" placeholder="Hình ảnh">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="url" class=" form-control-label">Link</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="url" name="url" placeholder="Hình ảnh" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="order_sort" class=" form-control-label">Số thứ tự</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="number" id="order_sort" name="order_sort" placeholder="Số thứ tự" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="row form-group">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="mota" class=" form-control-label">Mô tả</label>
                                        <textarea rows="10" class="form-control" name="mota" style="width: 100px"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i>&nbsp;Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="submitForm()"><i class="fa fa-save"></i>&nbsp; Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="libs/tinymce/tinymce.min.js"></script>
<script>
    
    
    tinymce.init({
        mode: "textareas",
        entity_encoding: "raw",
        plugins: ["advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen textcolor", "media",
            "insertdatetime media table contextmenu paste jbimages", "fullscreen", "moxiemanager"
        ],
        image_advtab: true,
        paste_data_images: true,
        browser_spellcheck: true,
        relative_urls: false,
        remove_script_host: false,
        //convert_urls : true,
        image_dimensions: false,
        forced_root_block: false,
        force_br_newlines: true,
        force_p_newlines: false,
        toolbar: " undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media insertfile |  fontsizeselect | forecolor backcolor | fullscreen"
    });
</script>