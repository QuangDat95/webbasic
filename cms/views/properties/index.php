<script type="text/javascript" src="libs/tinymce/tinymce.min.js"></script>
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
      rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Thông tin chung</strong>
                        <?php if ($_SESSION['admin']['group'] == 1) { ?>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#largeModal"
                                    onclick="add()"><i class="fa fa-plus"></i>&nbsp; Add
                            </button>
                        <?php } ?>
                    </div>
                    <div class="card-body">
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Tên</th>
                                <th style="display:none;">Tình trạng</th>
                                <th style="display:none;">ID</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($this->data as $row) {
                                $checked = '';
                                if ($row['status'] == 1) {
                                    $checked = "checked";

                                }
                                echo '<tr>
                                              <td>' . $i . '</td>
                                              <td>' . $row['id'] . '</td>
                                              <td>' . $row['name'] . '</td>
                                              <td style="display:none;">' . $row['status'] . '</td>
                                              <td style="display:none;">' . $row['id'] . '</td><td><a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit(' . ($i - 1) . ')"><i class="fa fa-edit"></i></a></td>
                                              <td><input type="checkbox"  ' . $checked . ' id="status_' . $row['id'] . '"  data-width="50" data-height="15" onclick="battat(' . $row['id'] . ')" /></td>
                                      </tr>';
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Thuộc tính sản phẩm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-client" method="post" action="properties/save">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row form-group">
                                <input type="hidden" id="id" name="id">
                                <div class="col col-md-4"><label for="name" class=" form-control-label">Tên thuộc
                                        tính</label></div>
                                <div class="col-12 col-md-8">
                                    <input type="text" id="name" name="name" placeholder="Tên thuộc tính"
                                           class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i>&nbsp;Cancel
                    </button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#example").DataTable({
            fixedHeader: true,
            ordering: false
        });
    });

    function add() {
        document.getElementById("form-client").reset();
        document.getElementById("id").value = 0;
    }

    function battat(id) {
        $.post(cmsUrl + "/properties/battat", {id: id}, function (data) {
        });
    }

    function edit(index) {
        var table = $('#example').DataTable();
        // alert( table.row(0).data() );
        let id = table.cell(index, 1).data();
        document.getElementById("id").value = id
        document.getElementById("name").value = table.cell(index, 2).data();
        // document.getElementById("testvl").innerText = 'abcdef';
    }
</script>