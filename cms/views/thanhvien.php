<style type="text/css">
   .w-90 {
      width: 90% !important;
      max-width: 1580px !important;
   }

   .scheduler-border {
      border: 1px groove #ddd !important;
      padding: 0 1.4em 1.4em 1.4em !important;
      margin: 0 0 1.5em 0 !important;
      -webkit-box-shadow: 0px 0px 0px 0px #000;
      box-shadow: 0px 0px 0px 0px #000;
   }
</style>
<!-- <?php
      $json = file_get_contents('https://thongtindoanhnghiep.co/api/city');
      $data = json_decode($json, true);
      ?> -->
<script type="text/javascript" src="js/thanhvien.js"></script>
<div class="content mt-3">
   <div class="animated fadeIn">
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <strong class="card-title">Quản lý thành viên</strong>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#largeModal" onclick="add()"><i class="fa fa-plus"></i>&nbsp; Add</button>
               </div>
               <div class="card-body">
                  <table id="example" class="table table-striped table-bordered" style="width: 100%">
                     <thead>
                        <tr>
                           <th>Mã KH</th>
                           <th>Hình ảnh</th>
                           <th>Tên thành viên</th>
                           <th>Số điện thoại</th>
                           <th>Địa chỉ</th>
                           <th>Email</th>
                           <th></th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        $i = 0;
                        foreach ($this->data as $row) {
                           echo '<tr>
                                 <td>' . $row['id'] . '</td>
                                 <td><img src="' . URL . '/' . $row['avatar'] . '" height="60"></td>
                                 <td>' . $row['name'] . '</td>
                                 <td>' . $row['phone'] . '</td>
                                 <td>' . $row['address'] . '</td>
                                 <td>' . $row['email'] . '</td>
                                 <td><a href="javascript:void(0)" data-toggle="modal" data-target="#largeModal" onclick="edit(' . $i . ')"><i class="fa fa-edit"></i></a></td>
                                 <td><a href="javascript:void(0)" onclick="del(' . $row['id'] . ')"><i class="fa fa-trash-o"></i></a>  </td>
                                 <td>' . $row['sex'] . '</td>
                                 <td>' . $row['birthday'] . '</td>
                                 <td>' . $row['cccd'] . '</td>
                                 <td>' . $row['avatar'] . '</td>
                              </tr>';
                           $i++;
                        }

                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade bd-example-modal-lg" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg w-90">
      <div class="modal-content">
         <form method="POST" action="thanhvien/save" id="form-client" enctype="multipart/form-data">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Thông tin thành viên</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <fieldset class="scheduler-border">
                  <legend>Thông tin chung:</legend>
                  <div class="col-sm-12 col-md-12">
                     <div class="col-sm-10 col-md-10">
                        <div class="col-md-6 col-xl-12">
                           <div class="row form-group">
                              <input type="hidden" id="id" name="id">
                              <div class="col-6"><label for="exampleInputEmail1">Mã thành viên</label><input type="text" placeholder="Mã" name="ma" id="ma" value="<?= 'TV' . $this->ma ?>" class="input-sm form-control-sm form-control" readonly></div>
                              <div class="col-6"><label for="exampleInputEmail1">Tên thành viên</label><input type="text" name="name" id="name" placeholder="Họ tên" class="input-sm form-control-sm form-control" required></div>
                           </div>
                           <div class="row form-group">
                              <div class="col-6"><label for="exampleInputEmail1">Số điện thoại</label><input type="number" placeholder="Điện thoại" name="phone" id="phone" class="input-sm form-control-sm form-control" required></div>
                              <div class="col-6"><label for="exampleInputEmail1">Email</label><input type="email" placeholder="Email" name="email" id="email" class="input-sm form-control-sm form-control" required></div>
                           </div>
                           <div class="row form-group">
                              <div class="col-6"><label for="exampleInputEmail1">Ngày sinh</label><input type="date" placeholder="dd/mm/yyyy" name="birthday" id="birthday" class="input-sm form-control-sm form-control"></div>
                              <div class="col-6"><label for="exampleInputEmail1">Giới tính</label>
                                 <select name="sex" id="sex" class="input-sm form-control-sm form-control">
                                    <option value="0">Chọn giới tính</option>
                                    <option value="1">Nam</option>
                                    <option value="2">Nữ</option>
                                 </select>
                              </div>
                           </div>
                           <!-- <div class="row form-group">
                              <div class="col-6"><label for="exampleInputEmail1">Tỉnh/Thành phố</label>
                                <select name="tinh" id="tinh" onchange="loadhuyen()" class="input-sm form-control-sm form-control">
                                    <option value="0">Chọn tỉnh/thành phố</option>
                                    <?php foreach ($data["LtsItem"] as $item) {
                                       echo '<option value="' . $item['ID'] . '">' . $item['Title'] . '</option>';
                                    } ?>
                                </select>
                              </div>
                              <div class="col-3"><label for="exampleInputEmail1">Quận/huyện</label>
                                <select name="huyen" id="huyen" onchange="loadxa()" class="input-sm form-control-sm form-control">
                                    <option value="0">Chọn Quận/huyện</option>
                                </select>
                            </div>
                            <div class="col-3"><label for="exampleInputEmail1">Phường/xã</label>
                                <select name="xa" id="xa" class="input-sm form-control-sm form-control">
                                    <option value="0">Chọn Phường/xã</option>
                                </select>
                            </div>
                           </div> -->
                           <div class="row form-group">
                              <div class="col-6"><label for="exampleInputEmail1">Địa chỉ chi tiết</label>
                                 <input type="text" name="address" id="address" class="input-sm form-control-sm form-control">
                              </div>
                              <!-- <div class="col-6"><label for="exampleInputEmail1">Loại</label>
                                <select name="level" id="level" class="input-sm form-control-sm form-control">
                                  <?php foreach ($this->loai as $value) {
                                       echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                                    } ?>
                                </select>
                                </div> -->
                           </div>
                           <!-- <div class="row form-group">
                              <div class="col-3">
                                  <div class="form-check">
                                    <div class="radio">
                                       <label for="radio1" class="form-check-label ">
                                       <input type="radio" id="rd1" name="loai" value="0" class="form-check-input" checked>Cá nhân
                                       </label>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-3">
                                <div class="form-check">
                                    <div class="radio">
                                       <label for="radio1" class="form-check-label ">
                                       <input type="radio" id="rd2" name="loai" value="1" class="form-check-input">Doanh nghiệp
                                       </label>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-3">
                                    <label for="exampleInputEmail1">Danh bạ: </label>
                                    <label class="switch switch-text switch-success switch-pill">
                                      <input type="checkbox" name="danhba" id="danhba" value="1" class="switch-input"> 
                                      <span data-on="On" data-off="Off" class="switch-label"></span> 
                                      <span class="switch-handle"></span>
                                    </label>
                              </div>
                           </div> -->

                        </div>
                     </div>
                     <div class="col-sm-2 col-md-2">
                        <div class="row form-group">
                           <label for="exampleInputEmail1">Hình ảnh</label>
                           <input type="file" id="file-input" name="avatar" class="form-control-file">
                        </div>
                        <img id="avatar" style="width:100%;">
                     </div>
                  </div>
               </fieldset>
               <div class="col-sm-12 col-md-12">
                  <fieldset class="scheduler-border">
                     <legend> Tài khoản:</legend>
                     <div class="row form-group">
                        <div class="col-4"><label for="exampleInputEmail1">Số CMT/MST</label>
                           <input type="number" name="cccd" id="cccd" class="input-sm form-control-sm form-control">
                        </div>
                        <div class="col-4"><label for="exampleInputEmail1">Mật khẩu</label><input type="password" name="pass" id="pass" class="input-sm form-control-sm form-control"></div>
                        <div class="col-4"><label for="exampleInputEmail1">Nhập lại mật khẩu</label><input type="password" name="repass" id="repass" onchange="check()" class="input-sm form-control-sm form-control"></div>
                     </div>
                  </fieldset>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
               <button type="submit" name="btnluu" class="btn btn-primary"><i class="fa fa-file-text-o"></i> Lưu</button>
            </div>
         </form>
      </div>
   </div>
</div>