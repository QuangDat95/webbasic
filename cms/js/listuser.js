$(function () {
    $("#example").DataTable({
        fixedHeader: true,
        ordering: false,
        // columnDefs: [{targets: [0], visible: false, searchable: false},
        // {targets: [4], visible: false, searchable: false},
        // {targets: [5], visible: false, searchable: false},
        // {targets: [7], visible: true, searchable: false}
        //    ]
    });
});

function add() {
    document.getElementById("form-client").reset();
    document.getElementById("id").value=0;
    $("#password").attr("placeholder", "Mật khẩu đăng nhập");
    $("#password").attr("required", "true");
}

function del(id){
    if (confirm("Bạn có chắc chắn muốn xóa?"))
        window.location.href = 'del?id='+id;
}

function edit(index) {
    var table = $('#example').DataTable();
    document.getElementById("id").value=table.cell(index,0).data();
    document.getElementById("name").value=table.cell(index,1).data();
    document.getElementById("phonenumber").value=table.cell(index,2).data();
    document.getElementById("ten_don_vi").value=table.cell(index,3).data();
    document.getElementById("nguoi_dai_dien").value=table.cell(index,4).data();
    document.getElementById("tai_khoan").value=table.cell(index,5).data();
    document.getElementById("ma_so_thue").value=table.cell(index,6).data();
    
    var classify = table.cell(index,7).data();

    $("#classify").val(classify).find("option[value=" + classify +"]").attr('selected', true);
}

// function thayanh(){
//     var id = $("#id").val();
//     var myform = new FormData($('#thongtin')[0]);
//     myform.append('myid', id);
//     $.ajax({
//         url: cmsUrl + "/listuser/thayanh",
//         type: 'post',
//         data: myform,
//         success: function(data){
//             if (data.success) {
//                $('#avatar').attr('src', data.filename);
//             }
//             else
//                 notify_error(data.msg);
//         },
//     });
// }
