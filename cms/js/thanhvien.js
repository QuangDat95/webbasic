$(function () {
    $("#example").DataTable({
        fixedHeader: true,
        ordering: false,
        columnDefs: [{targets: [0], visible: false, searchable: false},
        {targets: [8], visible: false, searchable: false},
        {targets: [9], visible: false, searchable: false},
        {targets: [10], visible: false, searchable: false},
        {targets: [11], visible: false, searchable: false}]
    });
});

function add() {
    document.getElementById("id").value = 0;
    $("#pass").attr("required", "true");
    $("#repass").attr("required", "true");
    $("#avatar").attr("src", baseUrl+"/template/images/default/user_small.png");
    document.getElementById("form-client").reset();
}

// function loadhuyen(){
//     var tinh=document.getElementById("tinh").value;
//      $.get("khachhang/gethuyen?tp="+tinh, function(data, status){
//        var sel=document.getElementById("huyen")
//            sel.innerHTML = "<option value='0'>Chọn Quận/huyện</option>";
//            var options = JSON.parse(data,true);
//            options.forEach(function(item){
//                var option = document.createElement("option");
//                option.value = item.ID;
//                option.text = item.Title;
//                sel.add(option);
//            });
//       });
// }

function check(){
    var pass=document.getElementById("pass").value;
    var repass=document.getElementById("repass").value;
    if (pass!=repass) {
        alert('Mật khẩu nhập lại chưa đúng bạn ơi');
    }
}

function del(id){
  if (confirm("Bạn có chắc chắn muốn xóa?"))
         window.location.href = 'thanhvien/del?id='+id;
}

// function loadxa(){
//     var huyen=document.getElementById("huyen").value;
//      $.get("khachhang/getxa?huyen="+huyen, function(data, status){
//        var sel=document.getElementById("xa")
//            sel.innerHTML = "<option value='0'>Chọn Phường/xã</option>";
//            var options = JSON.parse(data,true);
//            options.forEach(function(item){
//                var option = document.createElement("option");
//                option.value = item.ID;
//                option.text = item.Title;
//                sel.add(option);
//            });
//       });
// }

function edit(index) {
     var table = $('#example').DataTable();
     document.getElementById("id").value=table.cell(index,0).data();
     document.getElementById("ma").value='TV'+table.cell(index,0).data();
     document.getElementById("name").value=table.cell(index,2).data();
     document.getElementById("phone").value=table.cell(index,3).data();
     document.getElementById("phone").readOnly=true;
     document.getElementById("email").value=table.cell(index,5).data();
     document.getElementById("email").readOnly=true;
     document.getElementById("birthday").value=table.cell(index,9).data();
     document.getElementById("address").value=table.cell(index,4).data();
     document.getElementById("cccd").value=table.cell(index,10).data();
     var sex = table.cell(index,8).data();
    //  var tinh = table.cell(index,10).data();
    //  var huyen = table.cell(index,11).data();
    //  var xa = table.cell(index,12).data();
    //  var level = table.cell(index,14).data();
    //  var loai = table.cell(index,15).data();
     var avatar = table.cell(index,11).data();
    //  var danhba = table.cell(index,18).data();
    //  $.get("khachhang/gethuyen?tp="+tinh, function(data, status){
    //    var sel=document.getElementById("huyen")
    //        sel.innerHTML = "<option value='0'>Chọn Quận/huyện</option>";
    //        var options = JSON.parse(data,true);
    //        options.forEach(function(item){
    //            var option = document.createElement("option");
    //            option.value = item.ID;
    //            option.text = item.Title;
    //            if (item.ID==huyen) {
    //              option.selected = true;
    //            }
    //            sel.add(option);
               
    //        });
    //   });
     $("#sex").val(sex).find("option[value=" + sex +"]").attr('selected', true);
    //  $("#tinh").val(tinh).find("option[value=" + tinh +"]").attr('selected', true);
    //  $("#level").val(level).find("option[value=" + level +"]").attr('selected', true);
    //  $.get("khachhang/getxa?huyen="+huyen, function(data, status){
    //    var sel=document.getElementById("xa")
    //        sel.innerHTML = "<option value='0'>Chọn Phường/xã</option>";
    //        var options = JSON.parse(data,true);
    //        options.forEach(function(item){
    //            var option = document.createElement("option");
    //            option.value = item.ID;
    //            option.text = item.Title;
    //            if (item.ID==xa) {
    //              option.selected = true;
    //            }
    //            sel.add(option);
    //        });
    //   });
    //  if (loai==0) {
    //      $("#rd1").prop("checked", true);
    //    }else{
    //       $("#rd2").prop("checked", true);
    //    }
    //    if (danhba==1) {
    //     $("#danhba").attr("checked", true);
    //    }else{
    //       $("#danhba").attr("checked", false);
    //    }
    $("#pass").attr("placeholder", "Để trống nếu không muốn reset");
    $("#repass").attr("placeholder", "Để trống nếu không muốn reset");
    $("#pass").val('');
    $("#pass").removeAttr("required");
    $("#repass").removeAttr("required");
    $("#avatar").attr("src", baseUrl+avatar);
     
}


