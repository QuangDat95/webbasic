$(function () {
  $("#example").DataTable({
    fixedHeader: true,
    ordering: false,
  });
  
});

function chitiet(id) {
  $("#largeModal").modal("show");
  $.get("donhang/getrow?id=" + id, function (data, status) {
    // document.getElementById("tbody").innerHTML = data;
    let table = JSON.parse(data);
    document.getElementById("madon").value = table[0].don_hang;
    document.getElementById("diachi").value = table[0].dia_chi;
    document.getElementById("khachhang").value = table[0].ten_khach;
    document.getElementById("dienthoai").value = table[0].dien_thoai;
    document.getElementById("ghichu").value = table[0].ghi_chu;
    document.getElementById("idkh").value = table[0].idkh;
    $("#tinhtrang")
    .val(table[0].tinh_trang)
    .find("option[value=" + table[0].tinh_trang + "]")
    .attr("selected", true);
  });
  // detail
  $.fn.dataTable.ext.errMode = "none";
  var table1=$("#detail").DataTable({
    fixedHeader: true,
    ordering: false,
    processing: true,
    serverSide: true,
    ajax: "donhang/getchitietdon?id="+ id
  });
  table1.ajax.url("donhang/getchitietdon?id=" + id).load();
}

function add() {
  document.getElementById("id").value = 0;
  document.getElementById("form-client").reset();
}

function edit(index) {
  var table = $("#example").DataTable();
  var id = table.cell(index, 0).data();
  document.getElementById("madon").value = table.cell(index, 2).data();
  document.getElementById("khachhang").value = table.cell(index, 3).data();
  document.getElementById("diachi").value = table.cell(index, 4).data();
  document.getElementById("dienthoai").value = table.cell(index, 9).data();
  document.getElementById("thanhtoan").value = table.cell(index, 11).data();
  document.getElementById("ghichu").value = table.cell(index, 10).data();
  $.get("views/donhangdetail.php?id=" + id, function (data, status) {
    document.getElementById("tbody").innerHTML = data;
  });
}

function xoadon(id) {
  if (confirm("Bạn có chắc chắn muốn xóa?"))
    window.location.href = "donhang/xoa?id=" + id;
}
