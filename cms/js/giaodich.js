
$(function () {
    $("#example").DataTable({
        fixedHeader: true,
        ordering: false,
        columnDefs: [
        {targets: [10], visible: false, searchable: false},
        {targets: [11], visible: false, searchable: false}
       ],
       dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_sodu = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_km1 = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_km2 = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            // tính tổng theo trang
            // Total over this page
            // pageTotal = api
            //     .column( 5, { page: 'current'} )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                 commaSeparateNumber(total)
            );
            $( api.column( 6 ).footer() ).html(
                 commaSeparateNumber(total_sodu) 
            );
            $( api.column( 7 ).footer() ).html(
                 commaSeparateNumber(total_km1)
            );
            $( api.column( 8 ).footer() ).html(
                 commaSeparateNumber(total_km2)
            );
        }
    });
});


function commaSeparateNumber(val){
       while (/(\d+)(\d{3})/.test(val.toString())){
         val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
       }
       return val;
     }

function xoa(id) {
     if (confirm("Bạn có chắc chắn muốn xóa?"))
         window.location.href = 'giaodich/xoa?id='+id;
   }
function edit(index) {
     var table = $('#example').DataTable();
     document.getElementById("id").value=table.cell(index,0).data();
     document.getElementById("noidung").value=table.cell(index,1).data();
     document.getElementById("sotien").value=table.cell(index,5).data();
    var thanhvien= table.cell(index,10).data();
    var loai= table.cell(index,11).data();
    $("#thanhvien").val(thanhvien).find("option[value=" + thanhvien +"]").attr('selected', true);
    if (loai==0) {
         $("#rd2").prop("checked", true);
       }else{
          $("#rd1").prop("checked", true);
       }
}
function add() {
    document.getElementById("id").value=0;
    document.getElementById("form-client").reset();
}