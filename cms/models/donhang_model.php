<?php
class donhang_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result = array();
        $query = $this->db->query("SELECT *, 
        (SELECT COUNT(id) from orderdetails WHERE id_order = a.id) as tong
         FROM orderlist a WHERE status > 0 ORDER BY updated DESC ");
        if ($query) $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function sanpham($keyword, $offset, $rows)
    {
        $result   = array();
        $dieukien = " WHERE status=1 ";
        if ($keyword != '') {
            $dieukien .= " AND (name LIKE '%$keyword%' OR description LIKE '%$keyword%') ";
        }
        $query = $this->db->query("SELECT count(id) AS total FROM product  $dieukien ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $result['rows'] = array();
        $query = $this->db->query("SELECT id, name,
            (SELECT link FROM productpicture WHERE product=a.id ORDER BY position ASC LIMIT 1) AS hinhanh
            FROM product a $dieukien ORDER BY id DESC LIMIT  $offset ,$rows ");
        if ($query) {
            $arrSP  = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($arrSP as $key => $sanpham) {
                $query = $this->db->query("SELECT id,name FROM producttype WHERE status = 1 AND product='" . $sanpham['id'] . "' ORDER BY position");
                $arrSP[$key]['producttype'] = $query->fetchAll(PDO::FETCH_ASSOC);
                $query = $this->db->query("SELECT id,name FROM productcolor WHERE status = 1 AND product='" . $sanpham['id'] . "' ORDER BY position");
                $arrSP[$key]['productcolor'] = $query->fetchAll(PDO::FETCH_ASSOC);
            }
            $result['rows']  = $arrSP;
        }
        return $result;
    }

    function khachhang1()
    {
        $result = array();
        $query = $this->db->query(" SELECT id,name
         FROM user  WHERE status > 0 ORDER BY id DESC ");
        if ($query) $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getkhachhang($id)
    {
        $query = $this->db->query(" SELECT id,name,phone,address
         FROM user WHERE id=$id ");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result[0];
    }

    function getsanpham($id, $type, $color)
    {
        $query = $this->db->query("SELECT *,
        (SELECT name FROM producttype WHERE id=$type) AS type,
        (SELECT name FROM productcolor WHERE id=$color) AS color,
        (SELECT price FROM producttype WHERE id=$type) AS giaban,
         (SELECT link FROM productpicture WHERE product=a.id ORDER BY position ASC LIMIT 1) AS hinhanh
         FROM product a WHERE status > 0 AND id=$id ");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result[0];
    }

    function type()
    {
        $query = $this->db->query("SELECT *
         FROM producttype  WHERE status > 0 ");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    function color()
    {
        $query = $this->db->query("SELECT *
         FROM productcolor  WHERE status > 0 ");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function savedonhang($data)
    {
        $query = $this->insert("orderlist", $data);
        $iddh = $this->db->lastInsertId();
        foreach ($_SESSION['cart'] as  $item) {
            foreach ($item as  $value) {
                $data1 = array(
                    "id_order" => $iddh,
                    "product" => $value['id'],
                    "quantity" => $value['num'],
                    "id_type_product" => $value['typeid'],
                    "id_type_color" => $value['colorid'],
                    "price" => $value['price'],
                    "status" => 1
                );
                $query = $this->insert("orderdetails", $data1);
            }
        }
        return $query;
    }

    function indon($id)
    {
        $query = $this->db->query("SELECT *
        FROM orderlist a WHERE id=$id ");
        return ($query->fetchAll(PDO::FETCH_ASSOC));
    }

    function cartdetail($id)
    {
        $result = array();
        $query = $this->db->query("SELECT a.id_order as id, a.price,
         a.product as idproduct,
        (SELECT name FROM product WHERE id=a.product) as name,
        a.quantity as num,
        (SELECT link FROM productpicture WHERE product=a.product ORDER BY position ASC LIMIT 1) as hinhanh
        FROM orderdetails a WHERE id_order=$id ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function cartinfo($id)
    {
        $result = array();
        $query = $this->db->query("SELECT *
        FROM orderlist WHERE id=$id ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getchitietdon($keyword, $offset, $rows, $id)
    {
        $query = $this->db->query("SELECT count(id) AS total FROM hanghoa  WHERE don_hang=$id ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $result['rows'] = array();
        $query = $this->db->query("SELECT *,
        (SELECT name FROM sanpham WHERE id=a.hang_hoa) AS sanpham
        FROM hanghoa a WHERE don_hang=$id LIMIT $offset,$rows ");
        if ($query)
            $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getrow($id)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 AND don_hang=$id ";
        $query           = $this->db->query("SELECT don_hang,tinh_trang,
        (SELECT name FROM sanpham WHERE id=hang_hoa) as sanpham,
        (SELECT date FROM donhang WHERE id=don_hang) as ngaygio,
        (SELECT ghi_chu FROM donhang WHERE id=don_hang) as ghi_chu,
        (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=don_hang)) as ten_khach,
        (SELECT dia_chi FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=don_hang)) as dia_chi,
        (SELECT dien_thoai FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=don_hang)) as dien_thoai,
        (SELECT id FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=don_hang)) as idkh
         FROM hanghoa $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function updatedon($sdt, $date, $tt, $data)
    {
        if ($tt == 1) {
            $data['tinh_trang'] = 2;
            $query = $this->update("1_dathang", $data, " sdt=$sdt AND ngay_gio='$date' ");
        } else {
            $data['tinh_trang'] = 3;
            $query = $this->update("1_dathang", $data, " sdt=$sdt AND ngay_gio='$date' ");
        }

        return $query;
    }

    function delObj($id)
    {
        $data['status'] = 0;
        $query = $this->update("orderlist", $data, " id = $id ");
        $query = $this->update("orderdetails", $data, " id_order = $id ");
        return $query;
    }

    function saverow($madon, $datadon)
    {
        $query = $this->update("orderlist", $datadon, "id = $madon");
        $query = $this->db->query("DELETE FROM orderdetails WHERE id_order=" . $madon . " ");
        foreach ($_SESSION['cartedit'] as  $value) {
            $data1 = array(
                "id_order" => $madon,
                "product" => $value['id'],
                "quantity" => $value['num'],
                "price" => $value['price'],
                "status" => 1
            );
            $query = $this->insert("orderdetails", $data1);
        }
        return $query;
    }
}
