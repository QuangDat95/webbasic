<?php
class thanhvien_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $dieukien = " WHERE status = 1 ";
        $query    = $this->db->query("SELECT *,
            (SELECT phone FROM user WHERE customer = a.id) AS phone,
            (SELECT email FROM user WHERE customer = a.id) AS email
            FROM customer a $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getrow($id)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND id = $id ";
        $query           = $this->db->query("SELECT *,
        (SELECT phone FROM user WHERE customer = $id) AS phone,
        (SELECT email FROM user WHERE customer = $id) AS email
        FROM customer $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function matv()
    {
        $query = $this->db->query("SELECT (max(id)+1) AS total FROM customer ");
        $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp[0]['total'] > 0) {
            return ($temp[0]['total']);
        } else {
            return 1;
        }
    }

    function loai()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT id,name FROM loaitv $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function saverow($id, $data1, $data2)
    {
        if ($id > 0){
            $query = $this->update("customer", $data1, "id = $id");
            $query = $this->update("user", $data2, "customer = $id");
        } else {
            $data1['status'] = 1;
            $query = $this->insert("customer", $data1);
            $customer = $this->db->lastInsertId();
            if($customer){
                $data2['customer'] = $customer;
                $data2['status'] = 1;
                $query = $this->insert("user", $data2);
            }
        }
        return $query;
    }
}
