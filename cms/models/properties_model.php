<?php
class properties_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result   = array();
        $query           = $this->db->query("SELECT * FROM properties");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getrow($id)
    {
        $result   = array();
        $dieukien = " WHERE status=1 AND id=$id ";
        $query           = $this->db->query("SELECT * FROM properties $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function save($id, $data)
    {
        if($id>0)
            $query = $this->update("properties", $data, "id = $id");
        else
            $query = $this->insert("properties", $data);
        return $query;
    }

    function battat($id){
        return $this->db->query("UPDATE IGNORE properties SET status=NOT(status) WHERE id=$id");
    }

}

?>
