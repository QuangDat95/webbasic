<?php

class giaodich_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj()
    {
        $dieukien = ' WHERE tinh_trang = 1 ';
        $query = $this->db->query("SELECT *,
        DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s') AS ngaygio,
        (SELECT name FROM customer WHERE id = nguoi_dang) AS nguoidang,
        IF(loai!='1','Chi','Nạp') AS loaigd
        FROM giaodich $dieukien ORDER BY id DESC ");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function getrow($id)
    {
        $query = $this->db->query("SELECT * FROM giaodich  WHERE id=$id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return ($temp[0]);
    }

    function sotienold($id)
    {
        $query = $this->db->query(" SELECT so_tien FROM giaodich WHERE id=$id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return ($temp[0]);
    }

    function khachhang()
    {
        $result = array();
        $dieukien = " WHERE status = 1 ";
        $query = $this->db->query("SELECT id,name FROM customer $dieukien ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function sodu($thanhvien)
    {
        $dieukien = " WHERE tinh_trang = 1 AND nguoi_dang=$thanhvien ";
        $query = $this->db->query(" SELECT so_du FROM giaodich $dieukien ORDER BY id DESC ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if (sizeof($temp) > 0) {
            return $temp[0];
        } else {
            $temp[0]['so_du'] = 0;
            return $temp[0];
        }

    }

    function saverow($id, $data)
    {
        if ($id > 0)
            $query = $this->update("giaodich", $data, " id = $id ");
        else {
            $data['ngay_gio'] = date('Y-m-d H:i:s');
            $data['tinh_trang'] = 1;
            $query = $this->insert("giaodich", $data);
        }

        return $query;
    }

    function delObj($id)
    {
        $query = $this->update("giaodich", array('tinh_trang'=>0),"id=$id");
        return $query;
    }

}

?>
