<?php
class giaodich extends controller
{
    function __construct()
    {
        parent::__construct();
        if ($_SESSION['admin']['nhom'] > 3)
            header('Location: ' . URL);
    }

    function index()
    {
        require 'layouts/header.php';
        $this->view->data = $this->model->getFetObj();
        $this->view->thanhvien = $this->model->khachhang();
        $this->view->render('giaodich');
        require 'layouts/footer.php';
    }

    function xoa()
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        require 'layouts/header.php';
        if ($this->model->delObj($id)) {
            $this->view->thongbao = 'Xóa thành công! <a href="giaodich">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Xóa không thành công! <a href="giaodich">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }


    function save()
    {
        $id = $_REQUEST['id'];
        $noidung = $_REQUEST['noidung'];
        $thanhvien = $_REQUEST['thanhvien'];
        $sotien = str_replace(",", "", $_REQUEST['sotien']);
        $loai = $_REQUEST['radio'];
        if ($id > 0) {
            $check1 = $this->model->sotienold($id);
            $check = $this->model->sodu($thanhvien);
            if ($loai == 1) {
                $sodu = $check['so_du'] - $check1['so_tien'] + $sotien;
            } else {
                $sodu = $check['so_du'] + $check1['so_tien'] - $sotien;
            }
        } else {
            $check = $this->model->sodu($thanhvien);
            if ($loai == 1) {
                $sodu = $check['so_du'] + $sotien;
            } else {
                $sodu = $check['so_du'] - $sotien;
            }
        }

        $data = ['noi_dung' => $noidung, 'ngay_cap_nhat' => date("Y-m-d H:i:s"), 'loai' => $loai, 'nguoi_dang' => $thanhvien, 'so_tien' => $sotien, 'so_du' => $sodu];

        require 'layouts/header.php';
        if ($this->model->saverow($id, $data)) {
            $this->view->thongbao = 'Cập nhật thành công! <a href="giaodich">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Cập nhật không thành công! <a href="giaodich">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }
}
