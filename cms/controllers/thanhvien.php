<?php
class thanhvien extends controller
{
    function __construct()
    {
        parent::__construct();
        if ($_SESSION['admin']['nhom'] > 3)
            header('Location: ' . URL);
    }

    function index()
    {
        require('layouts/header.php');
        $this->view->data = $this->model->getdata();
        $this->view->ma = $this->model->matv();
        //    $this->view->loai = $this->model->loai();
        $this->view->render('thanhvien');
        require('layouts/footer.php');
    }

    function getrow()
    {
        $id = $_REQUEST['id'];
        $data = $this->model->getrow($id);
        if (count($data) > 0) {
            $jsonObj['data'] = $data[0];
            $jsonObj['success'] = true;
        } else {
            $jsonObj['err'] = 'Lỗi đọc dữ liệu từ máy chủ';
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('json');
    }

    // function gethuyen()
    // {
    //     $id = $_REQUEST['tp'];
    //     $json2 = file_get_contents('https://thongtindoanhnghiep.co/api/city/' . $id . '/district');
    //     echo $json2;
    // }

    // function getxa()
    // {
    //     $id = $_REQUEST['huyen'];
    //     $json3 = file_get_contents('https://thongtindoanhnghiep.co/api/district/' . $id . '/ward');
    //     echo  $json3;
    // }

    function save()
    {
        $id = $_REQUEST['id'];
        $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
        $phone = isset($_REQUEST['phone']) ? $_REQUEST['phone'] : '';
        // $phonemd = md5($_REQUEST['phone']);
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
        $birthday = isset($_REQUEST['birthday']) ? $_REQUEST['birthday'] : '';
        $sex = isset($_REQUEST['sex']) ? $_REQUEST['sex'] : 0;
        // $tinh = $_REQUEST['tinh'];
        // $huyen = $_REQUEST['huyen'];
        // if (isset($_REQUEST['danhba'])) {
        //     $danhba = $_REQUEST['danhba'];
        // } else {
        //     $danhba = 0;
        // }
        // $xa = $_REQUEST['xa'];
        $address = isset($_REQUEST['address']) ? $_REQUEST['address'] : '';
        // $level = $_REQUEST['level'];
        
        // $loai = $_REQUEST['loai'];
        $cccd = isset($_REQUEST['cccd']) ? $_REQUEST['cccd'] : '';
        $pass = md5(md5($_REQUEST['pass']));
        if ($_REQUEST['pass'] != '') {
            $data1 = ['name' => $name, 'address' => $address, 'birthday' => $birthday, 'sex' => $sex, 'cccd' => $cccd, 'create_date' => date("Y-m-d")];
            $data2 = ['phone' => $phone, 'email' => $email, 'password' => $pass];
        } else {
            $data1 = ['name' => $name, 'address' => $address, 'birthday' => $birthday, 'sex' => $sex, 'cccd' => $cccd];
            $data2 = [];
        }
        if (isset($_FILES['avatar']['name']) && ($_FILES['avatar']['name'] != '')) {
            $dir  = ROOT_DIR . '/uploads/thanhvien/';
            $name1 = functions::convertname($name);
            $file = functions::uploadfile('avatar', $dir, $name1);
            $avatar = '/uploads/thanhvien/' . $file;
            $data1['avatar'] = $avatar;
        } else {
            if($id = 0){
                $data1['avatar'] = '/template/images/default/user_small.png';
            }
        }

        require 'layouts/header.php';
        if ($this->model->saverow($id, $data1, $data2)) {
            $this->view->thongbao = 'Cập nhật thành công! <a href="thanhvien">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Cập nhật không thành công! <a href="thanhvien">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $data1 = ['status' => 0];
        $data2 = ['status' => 0];
        require 'layouts/header.php';
        if ($this->model->saverow($id, $data1, $data2)) {
            $this->view->thongbao = 'Đã xóa bản ghi! <a href="thanhvien">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Có lỗi khi xóa bản ghi này! <a href="thanhvien">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }
}
