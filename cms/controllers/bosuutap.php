<?php
class bosuutap extends controller {
    function __construct()
    {
        parent:: __construct();
    }

    function index()
    {
        require('layouts/header.php');
        $this->view->data = $this->model->getdata();
        $this->view->render('bosuutap');
        require('layouts/footer.php');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        require 'layouts/header.php';
        if ($this->model->del($id)) {
            $this->view->thongbao = 'Đã xóa bản ghi! <a href="bosuutap">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Có lỗi khi xóa bản ghi này! <a href="banner">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }
}

?>