<?php
class donhang extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        require('layouts/header.php');
        $this->view->data = $this->model->getdata();
        $this->view->render('donhang/index');
        unset($_SESSION['cartedit']);
        require('layouts/footer.php');
    }

    function add()
    {
        require('layouts/header.php');
        // $this->view->sanpham = $this->model->sanpham();
        $this->view->khachhang = $this->model->khachhang1();
        $this->view->type = $this->model->type();
        $this->view->render('donhang/add');
        require('layouts/footer.php');
    }

    function edit()
    {
        require('layouts/header.php');
        $id = $_REQUEST['id'];
        $this->view->cartdetail = $this->model->cartdetail($id);
        $this->view->data = $this->model->cartinfo($id);
        // $this->view->sanpham = $this->model->sanpham();
        // $this->view->khachhang = $this->model->khachhang1();
        // $this->view->type = $this->model->type();
        // $this->view->color = $this->model->color();
        $this->view->render('donhang/edit');

        require('layouts/footer.php');
    }

    function getrow()
    {
        $id = $_REQUEST['id'];
        $data = $this->model->getrow($id);
        if (count($data) > 0) {
            echo json_encode($data);
        } else {
            echo 0;
        }
    }

    function getkhachhang()
    {
        $id = $_REQUEST['id'];
        $data = $this->model->getkhachhang($id);
        if (count($data) > 0) {
            echo json_encode($data);
        } else {
            echo 0;
        }
    }

    function addphieu()
    {
        if (isset($_POST['id'])) {
            $id = $_REQUEST['id'];
            $type = $_REQUEST['type'];
            $color = $_REQUEST['color'];
            $sanpham = $this->model->getsanpham($id,$type,$color);
            if (!isset($_SESSION['cart'])) {
                $cart = array();
                $cart[$id][$type.'-'.$color] = array(
                    'id' => $sanpham['id'],
                    'name' => $sanpham['name'],
                    'num' => 1,
                    'price' => $sanpham['giaban'],
                    'hinhanh' => $sanpham['hinhanh'],
                    'colorid' =>$color,
                    'colorname' =>$sanpham['color'],
                    'typeid' =>$type,
                    'typename' =>$sanpham['type']
                );
            } else {
                $cart = $_SESSION['cart'];
                if (array_key_exists($id, $cart)) {
                    if ($cart[$id][$type.'-'.$color]['typeid']==$type && $cart[$id][$type.'-'.$color]['colorid']==$color) {
                        $cart[$id][$type.'-'.$color] = array(
                        'id' => $sanpham['id'],
                        'name' => $sanpham['name'],
                        'num' => (int)$cart[$id][$type.'-'.$color]['num'] + 1,
                        'price' => $sanpham['giaban'],
                        'hinhanh' => $sanpham['hinhanh'],
                        'colorid' =>$color,
                        'colorname' =>$sanpham['color'],
                        'typeid' =>$type,
                        'typename' =>$sanpham['type']
                        );
                    }else{
                        $cart[$id][$type.'-'.$color] = array(
                        'id' => $sanpham['id'],
                        'name' => $sanpham['name'],
                        'num' => 1,
                        'price' => $sanpham['giaban'],
                        'hinhanh' => $sanpham['hinhanh'],
                        'colorid' =>$color,
                        'colorname' =>$sanpham['color'],
                        'typeid' =>$type,
                        'typename' =>$sanpham['type']
                        );
                    }
                    
                } else {
                    $cart[$id][$type.'-'.$color] = array(
                    'id' => $sanpham['id'],
                    'name' => $sanpham['name'],
                    'num' => 1,
                    'price' => $sanpham['giaban'],
                    'hinhanh' => $sanpham['hinhanh'],
                    'colorid' =>$color,
                    'colorname' =>$sanpham['color'],
                    'typeid' =>$type,
                    'typename' =>$sanpham['type']
                    );
                }
            }
            $_SESSION['cart'] = $cart;
        }
    }
    function addphieuedit()
    {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $type = $_REQUEST['type'];
            $color = $_REQUEST['color'];
            $sanpham = $this->model->getsanpham($id,$type,$color);
            if (!isset($_SESSION['cartedit'])) {
                $cart = array();
                $cart[$id][$type.'-'.$color] = array(
                    'id' => $sanpham['id'],
                    'name' => $sanpham['name'],
                    'num' => 1,
                    'price' => $sanpham['giaban'],
                    'hinhanh' => $sanpham['hinhanh'],
                    'colorid' =>$color,
                    'colorname' =>$sanpham['color'],
                    'typeid' =>$type,
                    'typename' =>$sanpham['type']
                );
            } else {
                $cart = $_SESSION['cartedit'];
                if (array_key_exists($id, $cart)) {
                    if ($cart[$id][$type.'-'.$color]['typeid']==$type && $cart[$id][$type.'-'.$color]['colorid']==$color) {
                        $cart[$id][$type.'-'.$color] = array(
                        'id' => $sanpham['id'],
                        'name' => $sanpham['name'],
                        'num' => (int)$cart[$id][$type.'-'.$color]['num'] + 1,
                        'price' => $sanpham['giaban'],
                        'hinhanh' => $sanpham['hinhanh'],
                        'colorid' =>$color,
                        'colorname' =>$sanpham['color'],
                        'typeid' =>$type,
                        'typename' =>$sanpham['type']
                        );
                    }else{
                        $cart[$id][$type.'-'.$color] = array(
                        'id' => $sanpham['id'],
                        'name' => $sanpham['name'],
                        'num' => 1,
                        'price' => $sanpham['giaban'],
                        'hinhanh' => $sanpham['hinhanh'],
                        'colorid' =>$color,
                        'colorname' =>$sanpham['color'],
                        'typeid' =>$type,
                        'typename' =>$sanpham['type']
                        );
                    }
                } else {
                    $cart[$id][$type.'-'.$color] = array(
                    'id' => $sanpham['id'],
                    'name' => $sanpham['name'],
                    'num' => 1,
                    'price' => $sanpham['giaban'],
                    'hinhanh' => $sanpham['hinhanh'],
                    'colorid' =>$color,
                    'colorname' =>$sanpham['color'],
                    'typeid' =>$type,
                    'typename' =>$sanpham['type']
                    );
                }
            }
            $_SESSION['cartedit'] = $cart;
        }
    }


    function deletephieu()
    {
        if (isset($_POST['id']) && isset($_POST['num'])) {
            $id = $_POST['id'];
            $type = $_POST['type'];
            $color = $_POST['color'];
            $num = $_POST['num'];
            $cart = $_SESSION['cart'];
            if (array_key_exists($id, $cart)) {
                if ($num > 0) {
                    $cart[$id][$type.'-'.$color] = array(
                    'id' => $cart[$id][$type.'-'.$color]['id'],
                    'name' => $cart[$id][$type.'-'.$color]['name'],
                    'num' => $num,
                    'price' => $cart[$id][$type.'-'.$color]['price'],
                    'hinhanh' => $cart[$id][$type.'-'.$color]['hinhanh'],
                    'colorid' =>$cart[$id][$type.'-'.$color]['colorid'],
                    'colorname' =>$cart[$id][$type.'-'.$color]['colorname'],
                    'typeid' =>$cart[$id][$type.'-'.$color]['typeid'],
                    'typename' =>$cart[$id][$type.'-'.$color]['typename']
                    );
                    
                } else {
                    unset($cart[$id][$type.'-'.$color]);
                }

                $_SESSION['cart'] = $cart;
            }
        }
    }

    function deletephieuedit()
    {
        if (isset($_POST['id']) && isset($_POST['num'])) {
            $id = $_POST['id'];
            $num = $_POST['num'];
            $cart = $_SESSION['cartedit'];
            if (array_key_exists($id, $cart)) {
                if ($num > 0) {
                    $cart[$id] = array(
                    'id' => $cart[$id]['id'],
                    'name' => $cart[$id]['name'],
                    'price' => $cart[$id]['price'],
                    'num' => $num,
                    'hinhanh' => $cart[$id]['hinhanh']
                    );
                } else {
                    unset($cart[$id]);
                }
                $_SESSION['cartedit'] = $cart;
            }
        }
    }

    function updategia()
    {
        if (isset($_POST['id']) && isset($_POST['price'])) {
            $id = $_POST['id'];
            $type = $_POST['type'];
            $color = $_POST['color'];
            $price = $_POST['price'];
            $cart = $_SESSION['cart'];
            if (array_key_exists($id, $cart)) {
                $cart[$id][$type.'-'.$color] = array(
                    'id' => $cart[$id][$type.'-'.$color]['id'],
                    'name' => $cart[$id][$type.'-'.$color]['name'],
                    'num' => $cart[$id][$type.'-'.$color]['num'],
                    'price' => $price,
                    'hinhanh' => $cart[$id][$type.'-'.$color]['hinhanh'],
                    'colorid' =>$cart[$id][$type.'-'.$color]['colorid'],
                    'colorname' =>$cart[$id][$type.'-'.$color]['colorname'],
                    'typeid' =>$cart[$id][$type.'-'.$color]['typeid'],
                    'typename' =>$cart[$id][$type.'-'.$color]['typename']
                );
                $_SESSION['cart'] = $cart;
            }
        }
    }
    function updategiaedit()
    {
        if (isset($_POST['id']) && isset($_POST['price'])) {
            $id = $_POST['id'];
            $price = $_POST['price'];
            $cart = $_SESSION['cartedit'];
            if (array_key_exists($id, $cart)) {
                $cart[$id] = array(
                    'id' => $cart[$id]['id'],
                    'name' => $cart[$id]['name'],
                    'num' => $cart[$id]['num'],
                    'price' => $price,
                    'hinhanh' => $cart[$id]['hinhanh'],
                );
                $_SESSION['cartedit'] = $cart;
            }
        }
    }

    function getchitietdon()
    {
        $id = $_REQUEST['id'];
        $keyword = isset($_REQUEST['search']['value']) ? $_REQUEST['search']['value'] : '';
        $offset = $_REQUEST['start'];
        $rows = $_REQUEST['length'];
        $result = $this->model->getchitietdon($keyword, $offset, $rows, $id);
        $totalData = $result['total'];
        $totalFilter = $totalData;
        $data = array();
        $i = 0;
        $tong = 0;
        foreach ($result['rows'] as $key => $item) {
            $subdata = array();
            $tt = $item['so_luong'] * $item['don_gia'];
            $subdata[] = $item['sanpham'];
            $subdata[] = $item['so_luong'];
            $subdata[] = number_format($item['don_gia']);
            $subdata[] =  number_format($tt);
            $data[] = $subdata;
            $tong += $tt;
            $i++;
        }
        $json_data = array(
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFilter),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    function indon()
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $this->view->don = $this->model->indon($id);
         $this->view->sanpham = $this->model->cartdetail($id);
        $this->view->render('donhang/indon');
    }

    function save()
    {
        if (isset($_POST['btntt'])) {
            $email = $_POST['email'];
            $name = $_POST['name'];
            $dienthoai = $_POST['sdt'];
            $diachi = $_POST['diachi'];
            $ghichu = $_POST['ghichu'];
            $thanhtoan = $_POST['thanhtoan'];
            $thanhtien = $_POST['thanhtien'];
            $data = array(
                'email'=>$email,
                'name' => $name,
                'phone' => $dienthoai,
                'address' => $diachi,
                'total' => $thanhtien,
                'note' => $ghichu,
                'paymentMethod'=>$thanhtoan,
                'updated'=>date('Y-m-d H:i:s'),
                'status' => 1
            );
            $ok = $this->model->savedonhang($data);
            require 'layouts/header.php';
            if ($ok) {
                unset($_SESSION['cart']);
                $this->view->thongbao = 'Cập nhật thành công! <a href="donhang">Nhấn vào đây để quay lại</a>';
                $this->view->render('thongbao');
            } else {
                $this->view->thongbao = 'Cập nhật không thành công! <a href="donhang">Nhấn vào đây để quay lại</a>';
                $this->view->render('canhbao');
            }
            require 'layouts/footer.php';
        } else {
            echo "<script>window.location.assign('" . CMS . "/donhang');</script>";
        }
    }

    function editsave()
    {
        $email = $_REQUEST['email'];
        $madon = $_REQUEST['madon'];
        $diachi = $_REQUEST['diachi'];
        $name = $_REQUEST['khachhang'];
        $dienthoai = $_REQUEST['sdt'];
        $ghichu = $_REQUEST['ghichu'];
        $thanhtoan = $_REQUEST['thanhtoan'];
        $tt = $_REQUEST['tinhtrang'];
        $total = $_REQUEST['thanhtien'];
        $datadon = ['email' => $email, 'name'=> $name, 'phone' => $dienthoai, 'address' => $diachi,'total'=>$total,'note'=>$ghichu,'paymentMethod'=>$thanhtoan,'updated'=>date('Y-m-d H:i:s'),'status'=>$tt];
        require 'layouts/header.php';
        if ($this->model->saverow($madon,$datadon)) {
            $this->view->thongbao = 'Cập nhật thành công! <a href="donhang">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Cập nhật không thành công! <a href="donhang">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }

    function updatedon()
    {

        $sdt = isset($_REQUEST['sdt']) ? $_REQUEST['sdt'] : 0;
        $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : 0;
        $tt = isset($_REQUEST['tt']) ? $_REQUEST['tt'] : 0;
        $name = $_REQUEST['name'];
        $email = $_REQUEST['email'];
        $sdt = $_REQUEST['sdt'];
        $diachi = $_REQUEST['dia_chi'];
        $ghichu = $_REQUEST['ghi_chu'];
        $data = ['name' => $name, 'email' => $email, 'sdt' => $sdt, 'dia_chi' => $diachi, 'ghi_chu' => $ghichu];
        $this->model->updatedon($sdt, $date, $tt, $data);
        echo "<script>window.location.assign('" . CMS . "/donhang');</script>";
    }

    function chitiet()
    {
        $sdt = isset($_REQUEST['sdt']) ? $_REQUEST['sdt'] : 0;
        $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : 0;
        $this->view->data = $this->model->sanpham($sdt, $date);
        $this->view->render('donhang/chitiet');
    }

    function xoa()
    {
        $id = $_REQUEST['id'];
        $this->model->delObj($id);
        echo "<script>window.location.assign('" . CMS . "/donhang');</script>";
    }

    function getdatasp()
    {
        $keyword = isset($_REQUEST['search']['value']) ? $_REQUEST['search']['value'] : '';
        $offset = $_REQUEST['start'];
        $rows = $_REQUEST['length'];
        $result = $this->model->sanpham($keyword, $offset, $rows);
        $totalData = $result['total'];
        $totalFilter = $totalData;
        $data = array();
        $i=0;
        foreach ($result['rows'] as $key => $item) {
            $subdata = array();
            $subdata[] = $item['name'];
            $subdata[] =  '<img src="'.$item['hinhanh'].'" height="60">';
            $optionSize = '';
            foreach ($item['producttype'] as $value) {
                $optionSize .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
            }
            $subdata[] =  '<select class="form-control" id="type' . $item['id'] . '">' . $optionSize . '</select>';
            $optionColor = '';
            foreach ($item['productcolor'] as $value) {
                $optionColor .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
            }
            $subdata[] =  '<select class="form-control" id="color' . $item['id'] . '">' . $optionColor . '</select>';
            $subdata[] = '<button class="btn btn-warning btn-sm" type="button" title="Thêm vào phiếu" data-toggle="modal" data-target=".bd-example-modal-lg1" onclick="addphieu(' . $item['id'] . ')" ><i class="fa fa-plus"></i></button>';
            $data[] = $subdata;
            $i++;
        }
        $json_data = array(
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFilter),
            "data" => $data
        );
        echo json_encode($json_data);
    }
}
