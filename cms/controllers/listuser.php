<?php
class listuser extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        require('layouts/header.php');
        $this->view->data = $this->model->getdatauser();
        $this->view->render('listusers');
        require('layouts/footer.php');
    }

    function save()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $phone = $_REQUEST['phonenumber'];
        $tendonvi = $_REQUEST['ten_don_vi'];
        $nguoidaidien = $_REQUEST['nguoi_dai_dien'];
        $masothue = $_REQUEST['ma_so_thue'];
        $taikhoan = $_REQUEST['tai_khoan'];
        $classify = $_REQUEST['classify'];

        $data = [
            'name' => $name,
            'phone' => $phone,
            'ten_don_vi' => $tendonvi,
            'nguoi_dai_dien' => $nguoidaidien,
            'ma_so_thue' => $masothue,
            'tai_khoan' => $taikhoan,
            'classify' => $classify
        ];


        require 'layouts/header.php';
        if ($this->model->saverow1($id, $data)) {
            $this->view->thongbao = 'Cập nhật thành công! <a href="listuser">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Cập nhật không thành công! <a href="listuser">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }

    function thayanh()
    {
        $id = $_REQUEST['myid'];
        $filename = $_FILES['hinhanh']['name'];
        $hinhanh = '';
        if ($filename!='') {
            $dir = ROOT_DIR . '/uploads/nhanvien/';
            $file = functions::uploadfile('hinhanh', $dir, $id);
            if ($file!='')
                $hinhanh = CMS.'/uploads/nhanvien/'.$file;
        }
        if ($this->model->thayanh($hinhanh,$id)) {
            $jsonObj['filename'] = $hinhanh;
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công".$file;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Lỗi khi cập nhật database";
            $jsonObj['success'] = false;
        }
        echo json_encode($jsonObj);
    }
}
