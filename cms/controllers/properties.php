<?php

class properties extends controller
{
    function __construct()
    {
        parent::__construct();
        if ($_SESSION['admin']['group'] > 1)
            header('Location: ' . URL);
    }

    function index()
    {
        require('layouts/header.php');
        $this->view->data = $this->model->getdata();
        $this->view->render('properties/index');
        require('layouts/footer.php');
    }

    function getrow()
    {
        $id = $_REQUEST['id'];
        $data = $this->model->getrow($id);
        if (count($data) > 0) {
            $jsonObj['data'] = $data[0];
            $jsonObj['success'] = true;
        } else {
            $jsonObj['err'] = 'Lỗi đọc dữ liệu từ máy chủ';
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('json');
    }

    function save()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $tinhtrang = 1;
        $data = ['name' => $name, 'status' => $tinhtrang];
        require 'layouts/header.php';
        if ($this->model->save($id, $data)) {
            $this->view->thongbao = 'Cập nhật thành công! <a href="properties">Nhấn vào đây để quay lại</a>';
            $this->view->render('thongbao');
        } else {
            $this->view->thongbao = 'Cập nhật không thành công! <a href="properties">Nhấn vào đây để quay lại</a>';
            $this->view->render('canhbao');
        }
        require 'layouts/footer.php';
    }

    function battat()
    {
        $id = $_REQUEST['id'];
        if ($this->model->battat($id)) {
            echo "Success";
        } else {
            echo "Error";
        }
    }
}

?>
