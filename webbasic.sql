-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 17, 2021 at 04:09 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webbasic`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username_md5` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'md5 của tên đăng nhập',
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `group` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `username_md5`, `password`, `group`, `status`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'e594864995037d740cadc97edd181702', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customer` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `albumsub`
--

CREATE TABLE `albumsub` (
  `id` int(11) NOT NULL,
  `album` int(11) NOT NULL,
  `document` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `url`, `name`, `description`, `image`, `position`, `status`, `sort_order`) VALUES
(1, '', '123job', '', 'uploads/banner/123job.jpg', 0, 1, 1),
(2, '', 'Cùng học cùng vui', '', 'uploads/banner/cung-hoc-cung-vui.jpg', 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `position` int(4) NOT NULL DEFAULT 1,
  `author` int(11) NOT NULL,
  `view` int(15) NOT NULL DEFAULT 0,
  `created_date` date NOT NULL,
  `updated` date NOT NULL,
  `sort_order` int(5) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `url`, `title`, `avatar`, `description`, `content`, `tag`, `category`, `position`, `author`, `view`, `created_date`, `updated`, `sort_order`, `status`) VALUES
(1, '123doc-la-gi', '123doc là gì', 'http://via.placeholder.com/360x225', '', '<p dir=\"ltr\">Xuất phát từ ý tưởng tạo cộng đồng kiếm tiền online bằng tài liệu hiệu quả nhất, uy tín cao nhất. Mong muốn mang lại cho cộng đồng xã hội một nguồn tài nguyên tri thức quý báu, phong phú, đa dạng, giàu giá trị đồng thời mong muốn tạo điều kiện cho cho các users có thêm thu nhập. Chính vì vậy <strong>123doc.net</strong> ra đời nhằm đáp ứng nhu cầu chia sẻ tài liệu chất lượng và kiếm tiền online.<br /> </p>\r\n<p dir=\"ltr\">Sau hơn một năm ra đời, 123doc đã từng bước khẳng định vị trí của mình trong lĩnh vực tài liệu và kinh doanh online. Tính đến thời điểm tháng 5/2014; 123doc vượt mốc 100.000 lượt truy cập mỗi ngày, sở hữu <strong>2.000.000</strong> thành viên đăng ký, lọt vào <strong>top 200</strong> các website phố biến nhất tại Việt Nam, tỷ lệ tìm kiếm thuộc top 3 Google. Nhận được danh hiệu do cộng đồng bình chọn là website kiếm tiền online hiệu quả và uy tín nhất.<br /> Tham gia 123doc, chắc chắn bạn sẽ nhận được sự linh hoạt tuyệt vời của hệ thống trực tuyến.</p>\r\n<br />\r\n<p dir=\"ltr\"><em>TIỆN LỢI<br /> </em></p>\r\n<p dir=\"ltr\">Nhiều event thú vị, event kiếm tiền thiết thực. 123doc luôn luôn tạo cơ hội gia tăng thu nhập online cho tất cả các thành viên của website.<br /> </p>\r\n<p dir=\"ltr\"><em>ĐÁNG TIN CẬY<br /> </em></p>\r\n<p dir=\"ltr\">Support rất nhiệt tình và chuyên nghiệp</p>\r\n<p dir=\"ltr\">Thanh toán rõ ràng, minh bạch.<br /> </p>\r\n<p dir=\"ltr\"><em>HỮU ÍCH<br /> </em></p>\r\n<p dir=\"ltr\">123doc sở hữu một kho thư viện khổng lồ với hơn 2.000.000 tài liệu ở tất cả lĩnh vực: tài chính tín dụng, công nghệ thông tin, ngoại ngữ,...Khách hàng có thể dễ dàng tra cứu tài liệu một cách chính xác, nhanh chóng.</p>\r\n<p dir=\"ltr\">Upload tài liệu đơn giản và dễ dùng.<br /> </p>\r\n<p dir=\"ltr\"><em>CHẤT LƯỢNG<br /> </em></p>\r\n<p dir=\"ltr\">Mang lại trả nghiệm mới mẻ cho người dùng, công nghệ hiện thị hiện đại, bản online không khác gì so với bản gốc. Bạn có thể phóng to, thu nhỏ tùy ý.<br /> </p>\r\n<p dir=\"ltr\">Luôn hướng tới là website dẫn đầu chia sẻ và mua bán tài liệu hàng đầu Việt Nam. Tác phong chuyên nghiệp, hoàn hảo, đề cao tính trách nhiệm đối với từng người dùng. Mục tiêu hàng đầu của <strong>123doc.net</strong> trở thành thư viện tài liệu online lớn nhất Việt Nam, cung cấp những tài liệu độc không thể tìm thấy trên thị trường ngoại trừ <strong>123doc.net</strong><strong>. </strong></p>\r\n<div>Hãy đăng ký cho mình một tài khoản 123doc.net để được hưởng những tính năng mới và cơ hội kiếm tiền tại <strong>123doc.net</strong>. <em>Đăng ký<a href=\"https://123docz.net/trang-chu.htm?register=1\" target=\"_blank\"> tại đây</a></em></div>', '', 2, 2, 1, 1, '0000-00-00', '2021-10-22', 1, 1),
(2, 'nguyen-van-hung', 'Nguyễn Văn Hùng', 'http://via.placeholder.com/360x225', '', '', '', 2, 2, 1, 1, '0000-00-00', '2021-10-22', 0, 0),
(3, 've-chung-toi', 'Về chúng tôi', 'http://via.placeholder.com/360x225', '', '', '', 1, 2, 1, 1, '0000-00-00', '2021-10-26', 1, 0),
(4, 'quy-che-hoat-dong', 'Quy chế hoạt động', 'http://via.placeholder.com/360x225', '', '', 'quy chế,hoạt động', 2, 1, 1, 1, '0000-00-00', '2021-10-26', 4, 1),
(5, 'thoa-thuan-su-dung', 'Thỏa thuận sử dụng', 'http://via.placeholder.com/360x225', '', '', 'thỏa thuận,sử dụng', 2, 1, 1, 1, '0000-00-00', '2021-10-26', 3, 1),
(6, 'quy-dinh-bao-mat', 'Quy định bảo mật', 'http://via.placeholder.com/360x225', '', '', 'Quy định,bảo mật,quy định bảo mật', 2, 1, 1, 1, '0000-00-00', '2021-10-26', 2, 1),
(7, 've-chung-toi', 'Về chúng tôi', 'http://via.placeholder.com/360x225', '', '', 'giới thiệu,về chúng tôi', 2, 1, 1, 1, '0000-00-00', '2021-10-26', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blogcategory`
--

CREATE TABLE `blogcategory` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `parentid` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `sort_order` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `blogcategory`
--

INSERT INTO `blogcategory` (`id`, `url`, `name`, `description`, `image`, `parentid`, `position`, `sort_order`, `status`) VALUES
(1, 'trang-tinh', 'Trang tĩnh', '', '', 0, 0, 0, 0),
(2, 'gioi-thieu', 'Giới thiệu', '', '', 0, 0, 0, 1),
(3, 'danh-cho-nguoi-ban', 'Dành cho người bán', '', '', 0, 0, 0, 1),
(4, 'danh-cho-nguoi-mua', 'Dành cho người mua', '', '', 0, 0, 0, 1),
(5, 'cau-chuyen-thanh-cong', 'Câu chuyện thành công', '', '', 0, 0, 0, 0),
(6, 'thong-bao', 'Thông báo', '', '', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sex` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_no` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `username_md5` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `action_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `address`, `sex`, `birthday`, `avatar`, `id_no`, `views`, `created_date`, `email`, `phone`, `username`, `username_md5`, `password`, `action_code`, `status`) VALUES
(1, 'Nguyễn Thế Quỳnh', '', 0, '0000-00-00', '', 0, 0, '0000-00-00 00:00:00', '', '', '', '', '', '', 1),
(2, 'Nguyễn Văn Hùng', '', 0, '0000-00-00', '', 0, 0, '0000-00-00 00:00:00', '', '', '', '', '', '', 1),
(3, 'Bùi Thị Lợi', '', 0, '0000-00-00', '', 0, 0, '0000-00-00 00:00:00', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_province` int(10) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`, `id_province`, `status`) VALUES
(1, 'Thị Xã Cai Lậy', 1, 1),
(2, 'Huyện Yên Mỹ', 2, 1),
(3, 'Thành Phố Hưng Yên', 2, 1),
(4, 'Quận Nam Từ Liêm', 3, 1),
(5, 'Huyện Mỹ Hào', 2, 1),
(6, 'Quận 3', 4, 1),
(7, 'Quận Bình Tân', 4, 1),
(8, 'Quận 2', 4, 1),
(9, 'Quận 1', 4, 1),
(10, 'Quận 4', 4, 1),
(11, 'Thành Phố  Cà Mau', 5, 1),
(12, 'Quận 8', 4, 1),
(13, 'Huyện Bình Chánh', 4, 1),
(14, 'Quận 11', 4, 1),
(15, 'Quận 10', 4, 1),
(16, 'Quận Thủ Đức', 4, 1),
(17, 'Quận Bình Thạnh', 4, 1),
(18, 'Quận 5', 4, 1),
(19, 'Thành PhốBuôn Ma Thuột', 6, 1),
(20, 'Thành Phố Nam Định', 7, 1),
(21, 'Thành Phố Hạ Long', 8, 1),
(23, 'Thị Xã Gia Nghĩa', 9, 1),
(24, 'Quận Hà Đông', 3, 1),
(25, 'Quận Đống Đa', 3, 1),
(26, 'Quận Thanh Xuân', 3, 1),
(27, 'Quận Hoàn Kiếm', 3, 1),
(28, 'Huyện Chương Mỹ', 3, 1),
(29, 'Quận Cầu Giấy', 3, 1),
(30, 'Huyện Gia Lâm', 3, 1),
(31, 'Quận 7', 4, 1),
(32, 'Quận Liên Chiểu', 10, 1),
(33, 'Quận Sơn Trà', 10, 1),
(34, 'Quận Hải Châu', 10, 1),
(35, 'Quận Ngũ Hành Sơn', 10, 1),
(36, 'Quận Thanh Khê', 10, 1),
(37, 'Thành Phố Hải Dương', 11, 1),
(38, 'Huyện Kim Thành', 11, 1),
(39, 'Huyện Văn Lâm', 2, 1),
(40, 'Huyện Bến Lức', 12, 1),
(41, 'Huyện Đức Hoà', 12, 1),
(42, 'Thành Phố Bến Tre', 13, 1),
(43, 'Huyện Tam Nông', 14, 1),
(44, 'Huyện  Hồng Ngự', 14, 1),
(45, 'Huyện Tân Hồng', 14, 1),
(46, 'Huyện Long Hồ', 15, 1),
(47, 'Huyện Phú Quốc', 16, 1),
(48, 'Huyện Kiên Lương', 16, 1),
(49, 'Huyện Càng Long', 17, 1),
(50, 'Thành Phố Trà Vinh', 17, 1),
(51, 'Thành Phố Sóc Trăng', 18, 1),
(52, 'Huyện Cù Lao Dung', 18, 1),
(53, 'Huyện Long Phú', 18, 1),
(54, 'Huyện Trần Đề', 18, 1),
(55, 'Huyện Mỹ Tú', 18, 1),
(56, 'Thành Phố Bắc Ninh', 19, 1),
(57, 'Huyện Quế Võ', 19, 1),
(58, 'Huyện Tiên Du', 19, 1),
(59, 'Huyện Hà Trung', 20, 1),
(60, 'Huyện Quảng Xương', 20, 1),
(61, 'Thị Xã Bỉm Sơn', 20, 1),
(62, 'Huyện Tĩnh Gia', 20, 1),
(63, 'Thành Phố Thanh Hoá', 20, 1),
(64, 'Bà Rịa', 21, 1),
(65, 'Huyện Long Thành', 22, 1),
(66, 'Thị Xã Bến Cát', 23, 1),
(67, 'TX Thuận An', 23, 1),
(68, 'TX Dĩ An', 23, 1),
(69, 'Thành Phố Thủ Dầu Một', 23, 1),
(70, 'Huyện Phú Bình', 24, 1),
(71, 'Huyện Thanh Trì', 3, 1),
(72, 'Huyện Thường Tín', 3, 1),
(73, 'Quận Ba Đình', 3, 1),
(74, 'Quận Gò Vấp', 4, 1),
(75, 'Quận Tân Bình', 4, 1),
(76, 'Quận 12', 4, 1),
(77, 'Huyện Hóc Môn', 4, 1),
(78, 'Quận Phú Nhuận', 4, 1),
(79, 'Quận Tân Phú', 4, 1),
(80, 'Huyện Củ Chi', 4, 1),
(81, 'Huyện Nghĩa Hưng', 7, 1),
(82, 'Huyện Đông Hưng', 25, 1),
(83, 'Huyện Tân Thạnh', 12, 1),
(84, 'Thị Xã Kiến Tường', 12, 1),
(85, 'Quận Cái Răng', 26, 1),
(86, 'Thành Phố Vinh', 27, 1),
(87, 'Thừa Thiên', 28, 1),
(88, 'Thị Xã  Tân Uyên', 23, 1),
(89, 'Huyện Chơn Thành', 29, 1),
(90, 'Thành Phố Hội An', 30, 1),
(91, 'Thành Phố Tam Kỳ', 30, 1),
(92, 'Thành Phố Quảng Ngãi', 31, 1),
(93, 'Tháp Chàm', 32, 1),
(94, 'Thành Phố Thái Nguyên', 24, 1),
(95, 'Thành Phố Lào Cai', 33, 1),
(96, 'Huyện Bát Xát', 33, 1),
(97, 'Quận Bắc Từ Liêm', 3, 1),
(98, 'Quận Ngô Quyền', 34, 1),
(99, 'Huyện Tân Hưng', 12, 1),
(100, 'Thành Phố Long Xuyên', 35, 1),
(101, 'Thành Phố Rạch Giá', 16, 1),
(102, 'Huyện Châu Thành', 18, 1),
(103, 'Huyện Thạnh Trị', 18, 1),
(104, 'Thành Phố Việt Trì', 36, 1),
(105, 'Thành Phố Biên Hoà', 22, 1),
(106, 'Thị Xã Long Khánh', 22, 1),
(107, 'Huyện Hoà Thành', 37, 1),
(108, 'Thành Phố Tây Ninh', 37, 1),
(109, 'Thành Phố Nha Trang', 38, 1),
(110, 'Huyện Đông Hoà', 39, 1),
(111, 'Huyện Thuận Bắc', 32, 1),
(112, 'Huyện Thuận Nam', 32, 1),
(113, 'Huyện Ninh Sơn', 32, 1),
(114, 'Huyện Bắc Hà', 33, 1),
(115, 'Huyện Hoài Đức', 3, 1),
(116, 'Huyện Kiến Xương', 25, 1),
(117, 'Huyện Lương Sơn', 40, 1),
(118, 'Huyện Châu Thành', 13, 1),
(119, 'Quận Ninh Kiều', 26, 1),
(120, 'Huyện Thạnh Phú', 13, 1),
(121, 'Thành Phố Sa Đéc', 14, 1),
(122, 'Huyện Nam Trực', 7, 1),
(123, 'Huyện Tiền Hải', 25, 1),
(124, 'Quận Tây Hồ', 3, 1),
(125, 'Huyện Ba Tri', 13, 1),
(126, 'Quận Bình Thuỷ', 26, 1),
(127, 'Huyện Đầm Dơi', 5, 1),
(128, 'Thành Phố Tuyên Quang', 41, 1),
(129, 'Thành Phố Lai Châu', 42, 1),
(130, 'Huyện Quỳnh Lưu', 27, 1),
(131, 'Huyện Dương Minh Châu', 37, 1),
(132, 'Quận 9', 4, 1),
(133, 'Quận Hoàng Mai', 3, 1),
(134, 'Quận Hai Bà Trưng', 3, 1),
(135, 'Huyện Đông Anh', 3, 1),
(136, 'Huyện Châu Thành A', 43, 1),
(137, 'Thành Phố Hoà Bình', 40, 1),
(138, 'Thành Phố Đà Lạt', 44, 1),
(139, 'Huyện Lâm Hà', 44, 1),
(140, 'Thành Phố Uông Bí', 8, 1),
(142, 'Thành Phố Lạng Sơn', 45, 1),
(143, 'Thành Phố Phủ Lý', 46, 1),
(144, 'Huyện Bình Liêu', 8, 1),
(145, 'Thành Phố Bắc Cạn', 47, 1),
(146, 'Huyện Bình Sơn', 31, 1),
(147, 'Thành Phố Quy Nhơn', 48, 1),
(148, 'Huyện Thạch Thất', 3, 1),
(149, 'Huyện Duy Tiên', 46, 1),
(150, 'Huyện Mang Thít', 15, 1),
(151, 'Thị Xã Từ Sơn', 19, 1),
(152, 'Huyện Phù Cát', 48, 1),
(153, 'Thành Phố Cao Bằng', 49, 1),
(154, 'Huyện Sa Pa', 33, 1),
(155, 'Huyện Kim Bôi', 40, 1),
(156, 'Huyện Lạc Thuỷ', 40, 1),
(157, 'Thành Phố Cẩm Phả', 8, 1),
(159, 'Huyện Ea Kar', 6, 1),
(160, 'Quận 6', 4, 1),
(161, 'Huyện Nhà Bè', 4, 1),
(162, 'Huyện Mai Sơn', 50, 1),
(163, 'Huyện Phù Mỹ', 48, 1),
(164, 'Huyện Lấp Vò', 14, 1),
(165, 'Thành Phố Đồng Hới', 51, 1),
(166, 'Huyện Đa Krông', 52, 1),
(167, 'Huyện Thanh Miện', 11, 1),
(168, 'Thành Phố  Pleiku', 53, 1),
(169, 'Huyện Bù Đốp', 29, 1),
(170, 'Tp Vị Thanh', 43, 1),
(171, 'Huyện Cần Giuộc', 12, 1),
(172, 'Thành Phố Cao Lãnh', 14, 1),
(173, 'Huyện Vị Thủy', 43, 1),
(174, 'Huyện Ngọc Hiển', 5, 1),
(175, 'Huyện Tuy Phước', 48, 1),
(176, 'Huyện Trảng Bàng', 37, 1),
(177, 'Thành Phố Châu Đốc', 35, 1),
(178, 'Huyện Giồng Riềng', 16, 1),
(179, 'Huyện Cầu Ngang', 17, 1),
(180, 'Thành Phố  Bắc Giang', 54, 1),
(181, 'Huyện Việt Yên', 54, 1),
(182, 'Quận Long Biên', 3, 1),
(183, 'Huyện Tân Kỳ', 27, 1),
(184, 'Thành Phố Hà Tĩnh', 55, 1),
(185, 'Thị Xã Kỳ Anh', 55, 1),
(186, 'Huyện Nghi Xuân', 55, 1),
(187, 'Huyện Cẩm Xuyên', 55, 1),
(188, 'Huyện Vũ Quang', 55, 1),
(189, 'Thị Xã Bình Minh', 15, 1),
(190, 'Huyện Bàu Bàng', 23, 1),
(191, 'Huyện Bù Đăng', 29, 1),
(192, 'Thị Xã Đồng Xoài', 29, 1),
(193, 'Huyện Bến Cầu', 37, 1),
(194, 'Huyện Tân Biên', 37, 1),
(195, 'Huyện Thới Bình', 5, 1),
(196, 'Huyện Mỹ Xuyên', 18, 1),
(197, 'Huyện Đức Thọ', 55, 1),
(198, 'Huyện Mê Linh', 3, 1),
(199, 'Huyện Đắk Song', 9, 1),
(200, 'Thành Phố Ninh Bình', 56, 1),
(201, 'Huyện Gò Dầu', 37, 1),
(202, 'Huyện Chợ Lách', 13, 1),
(203, 'Huyện Thuỷ Nguyên', 34, 1),
(204, 'Quận Hải An', 34, 1),
(205, 'Huyện Văn Bàn', 33, 1),
(206, 'Huyện Tháp Mười', 14, 1),
(207, 'Huyện Thoại Sơn', 35, 1),
(208, 'Huyện Tuy Phong', 57, 1),
(209, 'Huyện Tân Sơn', 36, 1),
(210, 'Huyện Tam Đường', 42, 1),
(211, 'Huyện An Dương', 34, 1),
(212, 'Quận Kiến An', 34, 1),
(213, 'Huyện Thới Lai', 26, 1),
(214, 'Huyện Châu Phú', 35, 1),
(215, 'Thị Xã Tân Châu', 35, 1),
(216, 'Thị Xã Cửa Lò', 27, 1),
(217, 'Huyện Châu Thành', 14, 1),
(218, 'Thành Phố  Phan Thiết', 57, 1),
(219, 'Huyện Tây Giang', 30, 1),
(220, 'Huyện Sóc Sơn', 3, 1),
(221, 'Huyện Nga Sơn', 20, 1),
(222, 'Huyện Nghĩa Đàn', 27, 1),
(223, 'Huyện Nam Sách', 11, 1),
(224, 'Thành Phố Móng Cái', 8, 1),
(225, 'Thành Phố Kon Tum', 58, 1),
(226, 'Huyện Mộc Châu', 50, 1),
(227, 'Huyện Đức Trọng', 44, 1),
(228, 'Huyện Cư Jút', 9, 1),
(229, 'Huyện Yên Lạc', 59, 1),
(230, 'Huyện Vĩnh Thạnh', 26, 1),
(231, 'Huyện Cẩm Thuỷ', 20, 1),
(232, 'Huyện Kinh Môn', 11, 1),
(233, 'Huyện Ý Yên', 7, 1),
(234, 'Thị Xã Giá Rai', 60, 1),
(235, 'Huyện Long Mỹ', 43, 1),
(236, 'Huyện Tiểu Cần', 17, 1),
(237, 'Huyện Hoài Ân', 48, 1),
(238, 'Huyện Nhơn Trạch', 22, 1),
(239, 'Huyện Vĩnh Cửu', 22, 1),
(240, 'Huyện Khoái Châu', 2, 1),
(241, 'Huyện Đồng Phú', 29, 1),
(242, 'Thị Xã An Khê', 53, 1),
(243, 'Huyện Tuy An', 39, 1),
(244, 'Thị Xã Đông Triều', 8, 1),
(245, 'Huyện Krông Pắk', 6, 1),
(246, 'Thành Phố Vĩnh Yên', 59, 1),
(247, 'Huyện Phú Hoà', 39, 1),
(248, 'Thị Xã Hồng Ngự', 14, 1),
(249, 'Huyện Năm Căn', 5, 1),
(250, 'Thành Phố Tân An', 12, 1),
(251, 'Thị Xã Ayun Pa', 53, 1),
(252, 'Quận Ô Môn', 26, 1),
(253, 'Huyện Trảng Bom', 22, 1),
(254, 'Huyện Châu Thành', 16, 1),
(255, 'Huyện Cờ Đỏ', 26, 1),
(256, 'Huyện Thanh Oai', 3, 1),
(257, 'Huyện Tam Bình', 15, 1),
(258, 'Huyện Hòn Đất', 16, 1),
(259, 'Quận Cẩm Lệ', 10, 1),
(260, 'Huyện Can Lộc', 55, 1),
(261, 'Huyện Tân Hiệp', 16, 1),
(262, 'Huyện Bình Đại', 13, 1),
(263, 'Huyện Giồng Trôm', 13, 1),
(264, 'Huyện Mỏ Cày Bắc', 13, 1),
(265, 'Huyện Tuy Đức', 9, 1),
(266, 'Huyện Vĩnh Tường', 59, 1),
(267, 'Huyện Bình Xuyên', 59, 1),
(268, 'Huyện Phú Giáo', 23, 1),
(269, 'Huyện Nam Trà My', 30, 1),
(270, 'Huyện Bình Tân', 15, 1),
(271, 'Huyện Bắc Trà My', 30, 1),
(272, 'Thành Phố Bạc Liêu', 60, 1),
(273, 'Huyện Lâm Thao', 36, 1),
(274, 'Huyện An Phú', 35, 1),
(275, 'Huyện Mường Tè', 42, 1),
(276, 'Huyện Thanh Hà', 11, 1),
(277, 'Huyện Chợ Gạo', 1, 1),
(278, 'Huyện Châu Thành', 1, 1),
(279, 'Huyện Cái Bè', 1, 1),
(280, 'Thành Phố  Vĩnh Long', 15, 1),
(281, 'Huyện Cái Nước', 5, 1),
(282, 'Huyện Tam Nông', 36, 1),
(283, 'Huyện Kim Sơn', 56, 1),
(284, 'Huyện Bố Trạch', 51, 1),
(285, 'Huyện Lệ Thuỷ', 51, 1),
(286, 'Thị Xã  Phước Long', 29, 1),
(287, 'Huyện Hoà Vang', 10, 1),
(288, 'Huyện Cẩm Giàng', 11, 1),
(289, 'Thành Phố Mỹ Tho', 1, 1),
(290, 'Huyện Hoằng Hoá', 20, 1),
(291, 'Huyện Đan Phượng', 3, 1),
(292, 'Huyện Đại Lộc', 30, 1),
(293, 'Huyện Yên Mô', 56, 1),
(294, 'Huyện Phú Xuyên', 3, 1),
(295, 'Huyện Thuận Châu', 50, 1),
(296, 'Thị Xã Điện Bàn', 30, 1),
(297, 'Huyện Quỳ Hợp', 27, 1),
(298, 'Huyện Đắk Mil', 9, 1),
(299, 'Huyện Krông Nô', 9, 1),
(300, 'Huyện Văn Giang', 2, 1),
(301, 'Huyện Kim Động', 2, 1),
(302, 'Huyện Thạch Hà', 55, 1),
(303, 'Huyện Nông Sơn', 30, 1),
(304, 'Huyện Xuân Trường', 7, 1),
(305, 'Huyện Núi Thành', 30, 1),
(306, 'Huyện Gia Viễn', 56, 1),
(307, 'Huyện Lương Tài', 19, 1),
(308, 'Huyện Diên Khánh', 38, 1),
(309, 'Thành Phố Bảo Lộc', 44, 1),
(310, 'Huyện Kông Chro', 53, 1),
(311, 'Huyện Krông Bông', 6, 1),
(312, 'Huyện Than Uyên', 42, 1),
(313, 'Huyện Yên Phong', 19, 1),
(314, 'Huyện Lộc Ninh', 29, 1),
(315, 'Huyện Quỳnh Nhai', 50, 1),
(316, 'Huyện Vĩnh Bảo', 34, 1),
(317, 'Quận Dương Kinh', 34, 1),
(318, 'Quận Lê Chân', 34, 1),
(319, 'Thành Phố Thái Bình', 25, 1),
(320, 'Huyện Phúc Thọ', 3, 1),
(321, 'Huyện Văn Yên', 61, 1),
(322, 'Huyện Mỏ Cày Nam', 13, 1),
(323, 'Thị Xã Phổ Yên', 24, 1),
(324, 'Huyện Trần Văn Thời', 5, 1),
(325, 'Huyện Tam Đảo', 59, 1),
(326, 'Huyện Lập Thạch', 59, 1),
(327, 'Huyện Đô Lương', 27, 1),
(328, 'Thị Xã Thái Hoà', 27, 1),
(329, 'Huyện Hương Sơn', 55, 1),
(330, 'Huyện Lộc Hà', 55, 1),
(331, 'Huyện Hướng Hoá', 52, 1),
(332, 'Huyện Tân Phú', 22, 1),
(333, 'Thị Xã Sầm Sơn', 20, 1),
(334, 'Huyện Gò Quao', 16, 1),
(335, 'Huyện Thiệu Hoá', 20, 1),
(336, 'Huyện Đức Cơ', 53, 1),
(337, 'Huyện Kỳ Anh', 55, 1),
(338, 'Huyện Nghĩa Hành', 31, 1),
(339, 'Huyện Triệu Sơn', 20, 1),
(340, 'Thị Xã La Gi', 57, 1),
(341, 'Huyện Tiên Lãng', 34, 1),
(342, 'Huyện Mỹ Đức', 3, 1),
(343, 'Huyện Ứng Hoà', 3, 1),
(344, 'Huyện Chợ Mới', 47, 1),
(345, 'Huyện Na Rì', 47, 1),
(346, 'Thị Xã An Nhơn', 48, 1),
(347, 'Huyện Quảng Ninh', 51, 1),
(348, 'Huyện Nậm Pồ', 62, 1),
(349, 'Huyện Lục Nam', 54, 1),
(350, 'Huyện Phong Điền', 26, 1),
(351, 'Huyện Vũng Liêm', 15, 1),
(352, 'Huyện Trực Ninh', 7, 1),
(353, 'Huyện Mỹ Lộc', 7, 1),
(354, 'Huyện Yên Dũng', 54, 1),
(355, 'Huyện Vụ Bản', 7, 1),
(356, 'Thị Xã Mường Lay', 62, 1),
(357, 'Huyện Ngọc Hồi', 58, 1),
(358, 'Huyện Châu Thành', 43, 1),
(359, 'Thị Xã Buôn Hồ', 6, 1),
(360, 'Huyện Hoành Bồ', 8, 1),
(361, 'Huyện Điện Biên', 62, 1),
(362, 'Huyện Vị Xuyên', 63, 1),
(363, 'Huyện Đà Bắc', 40, 1),
(364, 'Huyện Tư Nghĩa', 31, 1),
(365, 'Huyện Dầu Tiếng', 23, 1),
(366, 'Huyện Tánh Linh', 57, 1),
(367, 'Huyện Định Quán', 22, 1),
(368, 'Huyện Đắk Glong', 9, 1),
(369, 'Huyện Lý Sơn', 31, 1),
(370, 'Huyện Tuyên Hoá', 51, 1),
(371, 'Huyện Hải Hà', 8, 1),
(372, 'Huyện Tân Phước', 1, 1),
(373, 'Quận Đồ Sơn', 34, 1),
(374, 'Huyện Hàm Thuận Nam', 57, 1),
(375, 'Thành Phố Tuy Hoà', 39, 1),
(376, 'Huyện Thạch Thành', 20, 1),
(377, 'Huyện Thạnh Hoá', 12, 1),
(378, 'Huyện Thủ Thừa', 12, 1),
(379, 'Huyện Ba Tơ', 31, 1),
(380, 'Quận Hồng Bàng', 34, 1),
(381, 'Huyện An Lão', 34, 1),
(382, 'Huyện Thanh Liêm', 46, 1),
(383, 'Huyện Bình Lục', 46, 1),
(384, 'Huyện Thái Thụy', 25, 1),
(385, 'Thị Xã Gò Công', 1, 1),
(386, 'Huyện Gia Bình', 19, 1),
(387, 'Huyện Tịnh Biên', 35, 1),
(388, 'Huyện Đại Từ', 24, 1),
(389, 'Huyện Phú Quý', 57, 1),
(390, 'Huyện Diễn Châu', 27, 1),
(391, 'Huyện Thanh Chương', 27, 1),
(392, 'Thị Xã Hoàng Mai', 27, 1),
(393, 'Huyện Thuận Thành', 19, 1),
(394, 'Huyện Châu Thành', 35, 1),
(395, 'Huyện Vạn Ninh', 38, 1),
(396, 'Huyện Di Linh', 44, 1),
(397, 'Huyện Nho Quan', 56, 1),
(398, 'Huyện Hiệp Hoà', 54, 1),
(399, 'Huyện Phước Sơn', 30, 1),
(400, 'Huyện Quảng Trạch', 51, 1),
(401, 'Huyện Bạch Long Vĩ', 34, 1),
(402, 'Huyện Kế Sách', 18, 1),
(403, 'Thành Phố Sơn La', 50, 1),
(404, 'Huyện Hưng Nguyên', 27, 1),
(405, 'Huyện Trà Ôn', 15, 1),
(406, 'Huyện Cần Giờ', 4, 1),
(407, 'Huyện Kiến Thuỵ', 34, 1),
(408, 'Huyện Ea Súp', 6, 1),
(409, 'Huyện Kon Plông', 58, 1),
(410, 'Huyện Kỳ Sơn', 27, 1),
(411, 'Huyện Chư Pưh', 53, 1),
(412, 'Huyện Phù Yên', 50, 1),
(413, 'Huyện Đơn Dương', 44, 1),
(414, 'Huyện Nậm Nhùn', 42, 1),
(415, 'Huyện Quế Phong', 27, 1),
(416, 'Huyện Thanh Sơn', 36, 1),
(417, 'Huyện Duyên Hải', 17, 1),
(418, 'Huyện Đông Giang', 30, 1),
(419, 'Huyện Thăng Bình', 30, 1),
(420, 'Huyện Cam Lâm', 38, 1),
(421, 'Huyện Như Xuân', 20, 1),
(422, 'Huyện Thọ Xuân', 20, 1),
(423, 'Huyện Phú Ninh', 30, 1),
(424, 'Huyện Quốc Oai', 3, 1),
(425, 'Huyện Cần Đước', 12, 1),
(426, 'Huyện Hàm Thuận Bắc', 57, 1),
(427, 'Huyện Ba Vì', 3, 1),
(428, 'Huyện Sìn Hồ', 42, 1),
(429, 'Thị Xã Sơn Tây', 3, 1),
(430, 'Huyện Vân Đồn', 8, 1),
(431, 'Huyện Duy Xuyên', 30, 1),
(432, 'Huyện Quế Sơn', 30, 1),
(433, 'Huyện Đạ Huoai', 44, 1),
(434, 'Thị Xã Quảng Trị', 52, 1),
(435, 'Thành Phố Cam Ranh', 38, 1),
(436, 'Huyện Minh Long', 31, 1),
(437, 'Huyện Kon Rẫy', 58, 1),
(438, 'Thị Xã Hà Tiên', 16, 1),
(439, 'Huyện Đông Hải', 60, 1),
(440, 'Huyện Châu Thành (hêt H.lực)', 26, 1),
(441, 'Huyện Đức Phổ', 31, 1),
(442, 'Huyện Phú Riềng', 29, 1),
(443, 'Huyện Bạch Thông', 47, 1),
(444, 'Huyện  Tu Mơ Rông', 58, 1),
(445, 'Huyện Quỳnh Phụ', 25, 1),
(446, 'Huyện Sơn Dương', 41, 1),
(447, 'Huyện Lạc Sơn', 40, 1),
(448, 'Huyện Tây Sơn', 48, 1),
(449, 'Huyện Lý Nhân', 46, 1),
(450, 'Huyện Bắc Mê', 63, 1),
(452, 'Thành Phố Điện Biên Phủ', 62, 1),
(453, 'Huyện Sơn Hà', 31, 1),
(454, 'Huyện Đák Hà', 58, 1),
(455, 'Huyện Vĩnh Thuận', 16, 1),
(456, 'Thị Xã Chí Linh', 11, 1),
(457, 'Huyện Văn Quan', 45, 1),
(458, 'Huyện  Tây  Hoà', 39, 1),
(459, 'Huyện Cẩm Khê', 36, 1),
(460, 'Huyện Xuân Lộc', 22, 1),
(461, 'Huyện Cẩm Mỹ', 22, 1),
(462, 'Thành Phố Hà Giang', 63, 1),
(463, 'Huyện Yên Bình', 61, 1),
(464, 'Huyện Bắc Yên', 50, 1),
(465, 'Huyện Hải Hậu', 7, 1),
(466, 'Huyện Phú Lương', 24, 1),
(467, 'Huyện Lạng Giang', 54, 1),
(468, 'Huyện Hương Khê', 55, 1),
(469, 'Huyện Bắc Sơn', 45, 1),
(470, 'Huyện Bắc Bình', 57, 1),
(471, 'Huyện Cầu Kè', 17, 1),
(472, 'Huyện Hồng Dân', 60, 1),
(473, 'Quận Thốt Nốt', 26, 1),
(474, 'Huyện Lộc Bình', 45, 1),
(475, 'Huyện Tây Trà', 31, 1),
(476, 'Thị Xã Phú Thọ', 36, 1),
(477, 'Huyện Thống Nhất', 22, 1),
(478, 'Huyện Chư Prông', 53, 1),
(479, 'Huyện Bảo Thắng', 33, 1),
(480, 'Thành Phố Đông Hà', 52, 1),
(481, 'Huyện Cam Lộ', 52, 1),
(482, 'Huyện Chi Lăng', 45, 1),
(483, 'Huyện Yên Thành', 27, 1),
(484, 'Huyện Yên Thế', 54, 1),
(485, 'Huyện Phù Ninh', 36, 1),
(486, 'Huyện Quản Bạ', 63, 1),
(487, 'Huyện Bình Giang', 11, 1),
(488, 'Thành Phố Tam Điệp', 56, 1),
(489, 'Huyện Chợ Mới', 35, 1),
(490, 'Huyện Vĩnh Hưng', 12, 1),
(491, 'Huyện Cao Lộc', 45, 1),
(492, 'Huyện Cô Tô', 8, 1),
(493, 'Huyện Cao Phong', 40, 1),
(494, 'Huyện Nam Đàn', 27, 1),
(495, 'Huyện Mai Châu', 40, 1),
(496, 'Huyện Ninh Giang', 11, 1),
(497, 'Huyện Kbang', 53, 1),
(498, 'Huyện Đức Huệ', 12, 1),
(499, 'Thị Xã Nghĩa Lộ', 61, 1),
(500, 'Huyện Krông Pa', 53, 1),
(501, 'Thị Xã Ninh Hoà', 38, 1),
(502, 'Huyện Phú Tân', 35, 1),
(503, 'Thành Phố Sông Công', 24, 1),
(504, 'Huyện Trấn Yên', 61, 1),
(505, 'Huyện Văn Lãng', 45, 1),
(506, 'Huyện Yên Sơn', 41, 1),
(507, 'Huyện Vũ Thư', 25, 1),
(508, 'Thị Xã Phúc Yên', 59, 1),
(509, 'Huyện Tam Dương', 59, 1),
(510, 'Huyện Cư Kuin', 6, 1),
(511, 'Huyện U Minh', 5, 1),
(512, 'Huyện Tân Lạc', 40, 1),
(513, 'Thành Phố  Yên Bái', 61, 1),
(514, 'Huyện Krông Buk', 6, 1),
(515, 'Huyện Gia Lộc', 11, 1),
(516, 'Huyện Lục Ngạn', 54, 1),
(517, 'Huyện Tân Yên', 54, 1),
(518, 'Huyện Sơn Động', 54, 1),
(519, 'Huyện Yên Khánh', 56, 1),
(520, 'Huyện Cát Hải', 34, 1),
(521, 'Huyện Hàm Tân', 57, 1),
(522, 'Huyện Gio Linh', 52, 1),
(523, 'Huyện Giao Thuỷ', 7, 1),
(524, 'Huyện Nghi Lộc', 27, 1),
(525, 'Huyện Ninh Hải', 32, 1),
(526, 'Huyện Tri Tôn', 35, 1),
(527, 'Huyện Giang Thành', 16, 1),
(528, 'Huyện Hữu Lũng', 45, 1),
(529, 'Huyện Vĩnh Linh', 52, 1),
(530, 'Huyện Hoa Lư', 56, 1),
(531, 'Huyện Hớn Quản', 29, 1),
(532, 'Huyện Triệu Phong', 52, 1),
(533, 'Huyện Phước Long', 60, 1),
(534, 'Huyện Phú Tân', 5, 1),
(535, 'Huyện Hải Lăng', 52, 1),
(536, 'Huyện Ba Bể', 47, 1),
(537, 'Huyện Lai Vung', 14, 1),
(538, 'Huyện Quan Sơn', 20, 1),
(539, 'Huyện Thường Xuân', 20, 1),
(540, 'Huyện Nông Cống', 20, 1),
(541, 'Huyện Mường Lát', 20, 1),
(542, 'Huyện Bù Gia Mập', 29, 1),
(543, 'Huyện Kim Bảng', 46, 1),
(544, 'Huyện Tiên Yên', 8, 1),
(545, 'Huyện Hậu Lộc', 20, 1),
(546, 'Huyện Vĩnh Lộc', 20, 1),
(547, 'Huyện Kiên Hải', 16, 1),
(548, 'Huyện Hưng Hà', 25, 1),
(549, 'Thị Xã Duyên Hải', 17, 1),
(550, 'Huyện Ân Thi', 2, 1),
(551, 'Tỉnh Đắk Lắk', 6, 1),
(552, 'Huyện Sốp Cộp', 50, 1),
(553, 'Huyện Krông Năng', 6, 1),
(554, 'Huyện Châu Thành', 17, 1),
(555, 'Huyện Phù Cừ', 2, 1),
(556, 'Huyện Ninh Phước', 32, 1),
(557, 'Huyện Châu Thành', 37, 1),
(558, 'Huyện Bắc Quang', 63, 1),
(559, 'Tỉnh Ninh Bình', 56, 1),
(560, 'Huyện Đông Sơn', 20, 1),
(561, 'Huyện Hoà Bình', 60, 1),
(562, 'Thành Phố Cần Thơ (hêt H.lực)', 26, 1),
(563, 'Tỉnh Kiên Giang', 16, 1),
(564, 'Thị  Xã Ngã Bảy', 43, 1),
(565, 'Huyện Xín Mần', 63, 1),
(566, 'Huyện Mường La', 50, 1),
(568, 'Thị Xã Ba Đồn', 51, 1),
(569, 'Huyện An Minh', 16, 1),
(570, 'Huyện Cai Lậy', 1, 1),
(571, 'Huyện Gò Công Đông', 1, 1),
(572, 'Tỉnh Tiền Giang', 1, 1),
(573, 'Tỉnh Nam Định', 7, 1),
(574, 'Tỉnh Hải Dương', 11, 1),
(575, 'Huyện Châu Thành', 12, 1),
(576, 'Huyện Mộc Hoá', 12, 1),
(577, 'Thành Phố Đà Nẵng', 10, 1),
(578, 'Tỉnh Bến Tre', 13, 1),
(579, 'Huyện Chư Sê', 53, 1),
(580, 'Huyện Gò Công Tây', 1, 1),
(581, 'Huyện Định Hoá', 24, 1),
(582, 'Huyện Văn Chấn', 61, 1),
(583, 'Huyện Vĩnh Lợi', 60, 1),
(584, 'Huyện Bắc Tân Uyên', 23, 1),
(585, 'Huyện Đồng Hỷ', 24, 1),
(586, 'Huyện Tiên Lữ', 2, 1),
(587, 'Huyện Anh Sơn', 27, 1),
(588, 'Huyện Quỳ Châu', 27, 1),
(589, 'Huyện Đức Linh', 57, 1),
(590, 'Huyện Sông Mã', 50, 1),
(591, 'Huyện Hiệp Đức', 30, 1),
(592, 'Huyện Buôn Đôn', 6, 1),
(593, 'Huyện Tân Uyên', 42, 1),
(594, 'Huyện Lục Yên', 61, 1),
(595, 'Huyện Hoài Nhơn', 48, 1),
(596, 'Huyện Hạ Hoà', 36, 1),
(597, 'Huyện Vân Hồ', 50, 1),
(598, 'Thị Xã Vĩnh Châu', 18, 1),
(599, 'Thị Xã Hồng Lĩnh', 55, 1),
(600, 'Huyện Ia Grai', 53, 1),
(601, 'Huyện Chư Păh', 53, 1),
(602, 'Thị Xã  Bình Long', 29, 1),
(603, 'Huyện Sơn Tịnh', 31, 1),
(604, 'Huyện Sông Lô', 59, 1),
(605, 'Huyện Đắk Tô', 58, 1),
(606, 'Huyện Yên Thuỷ', 40, 1),
(607, 'Huyện Phong Thổ', 42, 1),
(608, 'Huyện Sơn Hoà', 39, 1),
(609, 'Huyện Lạc Dương', 44, 1),
(610, 'Huyện Đồng Xuân', 39, 1),
(611, 'Huyện Phụng Hiệp', 43, 1),
(612, 'Huyện Nguyên Bình', 49, 1),
(613, 'Huyện Bình Gia', 45, 1),
(614, 'Huyện Yên Lập', 36, 1),
(615, 'Huyện Sơn Tây', 31, 1),
(616, 'Huyện Thanh Ba', 36, 1),
(617, 'Huyện Minh Hoá', 51, 1),
(618, 'Thị Xã Ngã Năm', 18, 1),
(619, 'Thị Xã Sông Cầu', 39, 1),
(620, 'Huyện Bảo Lâm', 44, 1),
(621, 'Huyện Krông A Na', 6, 1),
(622, 'Huyện Vân Canh', 48, 1),
(623, 'Huyện An Lão', 48, 1),
(624, 'Huyện Cao Lãnh', 14, 1),
(625, 'Huyện Tràng Định', 45, 1),
(626, 'Huyện Con Cuông', 27, 1),
(627, 'Thị Xã  Quảng Yên', 8, 1),
(628, 'Huyện Đam Rông', 44, 1),
(629, 'Huyện Thanh Bình', 14, 1),
(630, 'Huyện Thanh Thuỷ', 36, 1),
(631, 'Huyện Ngọc Lặc', 20, 1),
(632, 'Huyện Nam Giang', 30, 1),
(633, 'Huyện Võ Nhai', 24, 1),
(634, 'Huyện An Biên', 16, 1),
(635, 'Huyện Yên Định', 20, 1),
(636, 'Huyện Mường Chà', 62, 1),
(637, 'Huyện Ba Chẽ', 8, 1),
(638, 'Huyện Đầm Hà', 8, 1),
(639, 'Huyện Tứ Kỳ', 11, 1),
(640, 'Huyện Trà Lĩnh', 49, 1),
(641, 'Huyện IaPa', 53, 1),
(642, 'Huyện Đak Đoa', 53, 1),
(643, 'Huyện Khánh Vĩnh', 38, 1),
(644, 'Huyện Bảo Yên', 33, 1),
(645, 'Huyện Đạ Tẻh', 44, 1),
(646, 'Huyện Cát Tiên', 44, 1),
(647, 'Huyện Quang Bình', 63, 1),
(648, 'Huyện Lang Chánh', 20, 1),
(649, 'Huyện Quảng Uyên', 49, 1),
(650, 'Huyện U Minh Thượng', 16, 1),
(651, 'Huyện Tiên Phước', 30, 1),
(652, 'Huyện Chiêm Hoá', 41, 1),
(653, 'Huyện Đắk Glei', 58, 1),
(654, 'Huyện Đoan Hùng', 36, 1),
(655, 'Huyện Tương Dương', 27, 1),
(656, 'Huyện Hoà An', 49, 1),
(657, 'Thành Phố Hồ Chí Minh', 4, 1),
(658, 'Tỉnh Hưng Yên', 2, 1),
(659, 'Huyện Từ Liêm', 3, 1),
(660, 'Thành Phố Hà Nội', 3, 1),
(661, 'Huyện Mù Cang Chải', 61, 1),
(662, 'Tỉnh Thái Nguyên', 24, 1),
(663, 'Tỉnh Bình Định', 48, 1),
(664, 'Huyện Như Thanh', 20, 1),
(665, 'Huyện Mường Ảng', 62, 1),
(666, 'Tỉnh Hoà Bình', 40, 1),
(667, 'Tỉnh Thanh Hoá', 20, 1),
(668, 'Huyện Bảo Lạc', 49, 1),
(669, 'Tỉnh Nghệ An', 27, 1),
(670, 'Huyện Quan Hoá', 20, 1),
(671, 'Tỉnh Lạng Sơn', 45, 1),
(672, 'Tỉnh Đồng Tháp', 14, 1),
(673, 'Tỉnh Bạc Liêu', 60, 1),
(674, 'Huyện ĐakPơ', 53, 1),
(675, 'Huyện Trạm Tấu', 61, 1),
(676, 'Huyện Hoàng Su Phì', 63, 1),
(677, 'Huyện Hàm Yên', 41, 1),
(678, 'Tỉnh Ninh Thuận', 32, 1),
(679, 'Tỉnh Đồng Nai', 22, 1),
(680, 'Huyện Mê Linh (Chuyển Hà Nội)', 59, 1),
(681, 'Huyện Long Mỹ (hêt H.lực)', 26, 1),
(682, 'Huyện Tân Châu', 37, 1),
(683, 'Huyện Mường Khương', 33, 1),
(684, 'Huyện Yên Minh', 63, 1),
(685, 'Huyện Sông Hinh', 39, 1),
(686, 'Tỉnh Sơn La', 50, 1),
(687, 'Tỉnh Vĩnh Phúc', 59, 1),
(688, 'Huyện Tân Phú Đông', 1, 1),
(689, 'Huyện Tân Trụ', 12, 1),
(690, 'Tỉnh Bình Dương', 23, 1),
(691, 'Huyện Lắk', 6, 1),
(692, 'Huyện Đắk Nông (hêt H.lực)', 9, 1),
(693, 'Huyện Điện Biên Đông', 62, 1),
(694, 'Huyện Na Hang', 41, 1),
(695, 'Huyện Pác Nặm', 47, 1),
(696, 'Huyện Tuần Giáo', 62, 1),
(697, 'Tỉnh Điện Biên', 62, 1),
(698, 'Tỉnh Long An', 12, 1),
(699, 'Tỉnh Phú Yên', 39, 1),
(700, 'Huyện Thạch An', 49, 1),
(701, 'Huyện Kỳ Sơn', 40, 1),
(702, 'Huyện Đồng Văn', 63, 1),
(703, 'Huyện Mèo Vạc', 63, 1),
(704, 'Huyện Lâm Bình', 41, 1),
(705, 'Huyện Đình Lập', 45, 1),
(706, 'Huyện Thông Nông', 49, 1),
(707, 'Huyện Trùng Khánh', 49, 1),
(708, 'Huyện Chợ Đồn', 47, 1),
(709, 'Huyện Bác Ái', 32, 1),
(710, 'Huyện Mộ Đức', 31, 1),
(711, 'Huyện Khánh Sơn', 38, 1),
(712, 'Thành Phố Cần Thơ', 26, 1),
(713, 'Tỉnh Trà Vinh', 17, 1),
(714, 'Tỉnh Quảng Bình', 51, 1),
(715, 'Tỉnh Phú Thọ', 36, 1),
(716, 'Tỉnh Lào Cai', 33, 1),
(717, 'Tỉnh Bắc Giang', 54, 1),
(718, 'Tỉnh Sóc Trăng', 18, 1),
(719, 'Tỉnh An Giang', 35, 1),
(720, 'Tỉnh Hà Nam', 46, 1),
(721, 'Tỉnh Kon Tum', 58, 1),
(722, 'Tỉnh Quảng Ninh', 8, 1),
(723, 'Tỉnh Quảng Ngãi', 31, 1),
(724, 'Chưa rõ', 64, 1),
(725, 'Tỉnh Quảng Nam', 30, 1),
(726, 'Tỉnh Tây Ninh', 37, 1),
(727, 'Tỉnh Bình Phước', 29, 1),
(728, 'Huyện Bá Thước', 20, 1),
(729, 'Tỉnh Cà Mau', 5, 1),
(730, 'Tỉnh Hậu Giang', 43, 1),
(731, 'Tỉnh Quảng Trị', 52, 1),
(732, 'Thành Phố Hải Phòng', 34, 1),
(733, 'Tỉnh Tuyên Quang', 41, 1),
(734, 'Tỉnh Gia Lai', 53, 1),
(735, 'Huyện Phú Thiện', 53, 1),
(736, 'Huyện Ngân Sơn', 47, 1),
(737, 'Tỉnh Bắc Cạn', 47, 1),
(738, 'Tỉnh Hà Giang', 63, 1),
(739, 'Huyện Mang Yang', 53, 1),
(740, 'Huyện Yên Châu', 50, 1),
(741, 'Huyện Tủa Chùa', 62, 1),
(742, 'Tỉnh Lai Châu', 42, 1),
(743, 'Tỉnh Yên Bái', 61, 1),
(744, 'Huyện Vĩnh Thạnh', 48, 1),
(745, 'Huyện Trà Cú', 17, 1),
(746, 'Huyện Trà Bồng', 31, 1),
(747, 'Tỉnh Lâm Đồng', 44, 1),
(748, 'Tỉnh Cao Bằng', 49, 1),
(749, 'Tỉnh Đắk Nông', 9, 1),
(750, 'Huyện Đắk Mil (hêt H.lực)', 6, 1),
(751, 'Huyện Phục Hoà', 49, 1),
(752, 'Huyện Hà Quảng', 49, 1),
(753, 'Huyện Sa Thầy', 58, 1),
(754, 'Thị Xã Long Mỹ', 43, 1),
(755, 'Huyện Mường Nhé', 62, 1),
(756, 'Huyện Si Ma Cai', 33, 1),
(757, 'Huyện Bảo Lâm', 49, 1),
(758, 'Huyện Đắk Nông (hêt H.lực)', 6, 1),
(760, 'Huyện Krông Nô (hêt H.lực)', 6, 1),
(761, 'Huyện Cư Jút (hêt H.lực)', 6, 1),
(762, 'Huyện Hạ Lang', 49, 1),
(763, 'Tỉnh Hà Tĩnh', 55, 1),
(764, 'Tỉnh Khánh Hoà', 38, 1),
(765, 'Tỉnh Bình Thuận', 57, 1),
(766, 'Tỉnh Bắc Ninh', 19, 1),
(767, 'Tỉnh Vĩnh Long', 15, 1),
(768, 'Huyện Đảo Cồn Cỏ', 52, 1),
(769, 'Huyện Phụng Hiệp (hêt H.lực)', 26, 1),
(770, 'Huyện Than Uyên (hêt H.lực)', 33, 1),
(771, 'Tỉnh Thái Bình', 25, 1),
(772, 'Huyện Châu Thành A (hêt H.lực)', 26, 1),
(773, 'Huyện Trường Sa', 38, 1),
(777, 'Tp Tuy Hoà', 39, 1),
(778, 'Tp.buôn Ma Thuột', 6, 1),
(779, 'Tp. Sơn La', 50, 1),
(780, 'Tp Hà Giang', 63, 1),
(781, 'Tp Điện Biên Phủ', 62, 1),
(791, 'Tp. Phan Rang-tháp Chàm', 32, 1),
(792, 'Thị Xã Thuận An', 23, 1),
(800, 'Thành Phố Vị Thanh', 43, 1),
(814, 'Thị Xã Dĩ An', 23, 1),
(831, 'Khu Phố 3', 13, 1),
(832, 'Thành Phố Sầm Sơn', 20, 1),
(833, 'Thôn Đồng Giang', 51, 1),
(834, 'T.lộc Đại', 51, 1),
(835, '54 Huỳnh Thúc Kháng', 53, 1),
(836, 'Tổ 5', 53, 1),
(837, '17 Lê Hồng Phong', 53, 1),
(838, '14 Thống Nhất', 53, 1),
(839, 'Số 38 Trần Cao Vân', 53, 1),
(840, '778 Phạm Văn Đồng', 53, 1),
(841, 'Thành Phố Phúc Yên', 59, 1),
(842, 'Ấp Long Thạnh B', 35, 1),
(843, 'Số 363 Đường Lê Lợi', 54, 1),
(844, 'Số 350 Đường Lê Lợi', 54, 1),
(846, 'Đường Ngô Gia Tự', 54, 1),
(847, 'Số 364 Đường Xương Giang', 54, 1),
(848, 'Số 104/2 Đường Lê Lai', 54, 1),
(849, 'Thôn Trung Đồng', 54, 1),
(850, 'Đường Á Lữ', 54, 1),
(851, 'Số 95 Liên Hương', 54, 1),
(852, 'Cụm 5', 54, 1),
(855, 'Số 94 Nguyễn Thị Minh Khai', 39, 1),
(856, 'Số 30 Lương Văn Chánh', 39, 1),
(857, 'Số 200quốc Lộ 1a', 39, 1),
(858, 'Số 68h Hùng Vương', 39, 1),
(859, 'Số 07-lc Lý Thái Tổ', 39, 1),
(860, '466 Phan Chu Trinh', 30, 1),
(861, '275 Phan Chu Trinh', 30, 1),
(862, 'Thành Phố Hà Tiên', 16, 1),
(863, 'Thành Phố Đồng Xoài', 29, 1),
(864, 'Thành Phố Chí Linh', 11, 1),
(865, 'Thị Xã Mỹ Hào', 2, 1),
(866, 'Thành Phố Long Khánh', 22, 1),
(867, 'Huyện Ia Pa', 53, 1),
(868, 'Số 312 Nguyễn Trãi', 20, 1),
(870, 'Thị Xã Kinh Môn', 11, 1),
(872, 'Thôn Quang Châu', 10, 1),
(873, 'Khu Cn Long Mỹ', 48, 1),
(874, 'Ấp Bàu Công', 12, 1),
(875, 'Thành Phố Gia Nghĩa', 9, 1),
(876, 'Thị Xã Sa Pa', 33, 1),
(877, 'Thị Xã Duy Tiên', 46, 1),
(878, 'Khu Công Nghiệp Châu Sơn', 46, 1),
(879, 'Lô C6 - 5 Khu Công Nghiệp Hòa Xá', 7, 1),
(880, 'Thành Phố Dĩ An', 23, 1),
(881, 'Thành Phố Thuận An', 23, 1),
(882, 'Thị Xã Trảng Bàng', 37, 1),
(883, 'Tỉnh Bà Rịa', 21, 1),
(884, 'Thị Xã Hoà Thành', 37, 1),
(885, 'Thành Phố Ngã Bảy', 43, 1),
(886, 'Thị Xã Đức Phổ', 31, 1),
(887, 'Thôn Lương Trung', 57, 1),
(888, 'Đường Số 10', 23, 1),
(889, 'Thị Xã Hà Đông', 3, 1),
(891, 'Hớn Quản', 29, 1),
(892, 'Tx Phước Long', 29, 1),
(893, 'Thôn Phò Nam', 10, 1),
(894, '33 Trần Quý Cáp', 39, 1),
(895, 'Chưa rõ', 3, 1),
(896, 'Chưa rõ', 5, 1),
(897, 'Thị Xã Đông Hoà', 39, 1),
(898, 'Thị Xã Hoài Nhơn', 48, 1),
(899, 'Thị Xã Nghi Sơn', 20, 1),
(900, 'Huyện Quảng Hòa', 49, 1),
(902, 'Tổ 14', 8, 1),
(903, 'Tổ 5', 61, 1),
(904, 'Tx Bình Long', 29, 1),
(905, 'Số 11 Đường Trần Hưng Đạo', 52, 1),
(906, '79a Nguyễn Trãi', 52, 1),
(907, 'Ấp Bàu Ké', 29, 1),
(908, 'Đường Trần Nguyên Hãn', 61, 1);

-- --------------------------------------------------------

--
-- Table structure for table `infomation`
--

CREATE TABLE `infomation` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `infomation`
--

INSERT INTO `infomation` (`id`, `name`, `value`, `status`) VALUES
(1, 'Tên công ty', 'CÔNG TY CỔ PHẦN SẢN XUẤT VẬT LIỆU XÂY DỰNG VIỆT NAM', 1),
(2, 'Mã số thuế', '0107621024', 1),
(3, 'Điện thoại', '0976606969', 1),
(4, 'Điện thoại 2', '0935555981', 1),
(5, 'Địa chỉ', 'Truờng bắn Yên Sở, P. Yên Sở, Q. Hoàng Mai', 1),
(6, 'Địa chỉ 2', 'Số 15 LK6B, C17 Bộ Công an, KĐT Mỗ Lao, Hà Đông, Hà Nội', 1),
(7, 'Email', 'mater.tamika@gmail.com', 1),
(8, 'Ảnh đại diện', 'http://matervn.com/cms/uploads/baiviet/logo.png', 1),
(9, 'Mô tả về website', 'Công ty chuyên môi giới và cung cấp vật liệu xây dựng', 1),
(10, 'Keyword', 'moi gioi, vat lieu, xay dung, mater, matervn', 1),
(11, 'Bản đồ', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6428.5212731653455!2d105.87911172869066!3d20.951832025032555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ae820436c1dd%3A0xa004788664994983!2zVHLGsOG7nW5nIELhuq9uIFnDqm4gU-G7nw!5e0!3m2!1svi!2s!4v1634089210605!5m2!1svi!2s\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 1),
(12, 'Twitter', 'https://twitter.com', 1),
(13, 'Facebook', 'https://www.facebook.com/gemstech.com.vn', 1),
(14, 'Instagram', 'https://www.instagram.com', 1),
(15, 'Google', '#', 1);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `url` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1-hình ảnh, 2-video, 3-audio',
  `path` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `name`, `created_date`, `url`, `type`, `path`, `status`) VALUES
(13, 'BANNER 1', '0000-00-00 00:00:00', '', 1, 'baiviet', 1),
(15, 'logo', '0000-00-00 00:00:00', 'logo', 1, 'home', 1),
(16, 'banner 2', '0000-00-00 00:00:00', '', 1, 'sanpham', 1),
(18, 'hakko', '0000-00-00 00:00:00', '', 1, 'home', 1),
(19, 'hios', '0000-00-00 00:00:00', '', 1, 'home', 1),
(20, 'samsung', '0000-00-00 00:00:00', '', 1, 'home', 1),
(21, 'uniohm', '0000-00-00 00:00:00', '', 1, 'home', 1),
(22, 'yageo', '0000-00-00 00:00:00', '', 1, 'home', 1),
(26, 'logo', '0000-00-00 00:00:00', '', 1, 'baiviet', 1),
(27, 'baner1', '0000-00-00 00:00:00', '', 1, 'baiviet', 1),
(28, 'baner2', '0000-00-00 00:00:00', '', 1, 'baiviet', 1),
(30, 'banner1', '0000-00-00 00:00:00', '', 1, 'baiviet', 1),
(31, 'banner3', '0000-00-00 00:00:00', '', 1, 'baiviet', 1),
(32, 'Nguyễn Văn Hùng', '0000-00-00 00:00:00', '', 1, '', 1),
(33, 'bi-ngan.jpg', '0000-00-00 00:00:00', '', 1, '', 1),
(34, 'bi-ngan', '0000-00-00 00:00:00', '', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '1-blog details, 2 blog category, 3 product detail, 4 category, 5- url',
  `color` int(11) NOT NULL,
  `parentid` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `sub_menu` tinyint(1) NOT NULL COMMENT '1- có sub, 0- không có',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `image`, `url`, `type`, `color`, `parentid`, `sort_order`, `sub_menu`, `status`) VALUES
(1, 'Tài liệu', '', 'document/1/tai-lieu', 4, 0, 0, 1, 1, 0),
(2, 'Quản trị kinh doanh', '', 'document/1/quan-tri-kinh-doanh', 4, 0, 0, 2, 1, 0),
(3, 'Khoa học tự nhiên', '', 'document/1/khoa-hoc-tu-nhien', 4, 0, 0, 3, 1, 0),
(4, 'Tài liệu', '', 'document/1/tai-lieu', 4, 0, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `updated` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  `discount` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `orderlist`
--

CREATE TABLE `orderlist` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer` int(11) NOT NULL DEFAULT 0,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `total` float NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `paymentMethod` tinyint(2) NOT NULL COMMENT '0: TT khi nhận hàng\r\n1: TT hình thức khác',
  `updated` datetime NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1 COMMENT '1-đơn hàng mới, 2, đã xác nhận, 3- đang giao hàng, 4- thành công, 5 khách hủy'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE `partner` (
  `id` int(6) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) CHARACTER SET latin1 NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` tinyint(1) NOT NULL DEFAULT 1,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `view` int(10) NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL,
  `updated` date NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `url`, `code`, `name`, `content`, `description`, `category`, `tag`, `price`, `view`, `avatar`, `position`, `status`, `updated`, `sort_order`) VALUES
(1, 'vua-kho', '', 'Vữa khô', '', '', '1', '', 0, 0, '', '1', 0, '2021-10-12', 0),
(2, 'vv', '', 'vv', 'sdfs', 'sdfs', '3', '', 0, 0, '', '1', 0, '2021-10-12', 0),
(3, 'vua-kho-tron-san-m50', '', 'VỮA KHÔ TRỘN SẴN M50#', '<strong>4. Định mức sử dụng</strong><br />Xây mạch 1cm, 1m3 gạch hết 350kg.<br />Trát dày 1cm hết 16kg cho 1 mặt.<br /><strong>5. Lợi ích khi sử dụng</strong>\r\n<ul>\r\n<li><em><strong>Tiện lợi:</strong> </em>Không phải chuyển từng xe cát, từng bao xi măng lên công trình. Không phải sàng cát trước khi trộn. Dễ dàng vận chuyển, mang vác vật tư đến mọi vị trí thi công. Giảm tối đa diện tích tập kết vật tư và thi công. Rất thuận lợi cho những công trình xây mới trong ngõ nhỏ, cải tạo nhà trong hẻm hay hoàn thiện chung cư có người đang ở.</li>\r\n<li><em><strong>Chất lượng đảm bảo:</strong> </em>Vữa đã được trộn đều tại nhà máy theo một cấp phối định trước. Nguyên vật liệu đầu vào đảm bảo, đồng đều. Do đó, không chủ công trình không phải lo lắng về chất lượng xi măng, cát, hay tỉ lệ trộn, thời gian trộn như trước đây. Loại bỏ hoàn toàn hiện tượng rạn nứt bức tường do trộn không đều. Loại bỏ hoàn toàn hiện tượng nổ tường do phù sa, đất có trong cát không được xử lý. Phù hợp cho những công trình có định hướng ở lâu dài, yêu cầu chất lượng đảm bảo hay chủ công trình không sát sao được toàn thời gian với thợ thi công.</li>\r\n<li><em><strong>Sạch</strong>: </em>Công trình sạch sẽ, hầu như giảm hẳn bụi bặm ở công trường. Không ảnh hưởng tới các hộ gia đình lân cận, không làm bẩn đường xá, cống rãnh.</li>\r\n<li><em><strong>Tiết kiệm</strong>: </em>Thời gian thi công giảm được 1/3 lần, công sức lo vật tư thừa thiếu, kiểm soát chất lượng công trình. Năng suất lao động tăng lên đáng kể.</li>\r\n</ul>', '<strong>1. Vữa khô trộn sẵn là gì?</strong><br />Vữa khô trộn sẵn Mater là sản phẩm vữa khô dân dụng dùng cho xây, trát gạch tuylen truyền thống, gạch bê tông côt liệu; san cán nền dân dụng<br /><strong>2. Thành phần</strong><br />Xi măng, cát sạch sấy khô, phụ gia.<br /><strong>3. Thông số kỹ thuật</strong><br />Sản xuất theo TCVN 4314:2003, đảm bảo các thông số sau đây:\r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA </strong><strong>KHÔ TRỘN SẴN M50#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>V</strong><strong>KM50</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥145</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥65</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥150</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥5,0</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n <br />Yêu cầu cát phải sạch, không lẫn tạp chất hoặc phù sa, độ ẩm nhỏ hơn 1% để không làm ảnh hưởng đến chất lượng vữa.<br /><br />', '5', '', 0, 18, '', '1', 1, '2021-10-12', 0),
(4, 'vua-kho-tron-san-m75', '', 'VỮA KHÔ TRỘN SẴN M75#', '<div style=\"text-align: justify;\"><strong>4. Định mức sử dụng</strong><br />Xây mạch 1cm, 1m3 gạch hết 350kg.<br />Trát dày 1cm hết 16kg cho 1 mặt.<br /><strong>5. Lợi ích khi sử dụng</strong>\r\n<ul>\r\n<li><em><strong>Tiện lợi:</strong> </em>Không phải chuyển từng xe cát, từng bao xi măng lên công trình. Không phải sàng cát trước khi trộn. Dễ dàng vận chuyển, mang vác vật tư đến mọi vị trí thi công. Giảm tối đa diện tích tập kết vật tư và thi công. Rất thuận lợi cho những công trình xây mới trong ngõ nhỏ, cải tạo nhà trong hẻm hay hoàn thiện chung cư có người đang ở.</li>\r\n<li><em><strong>Chất lượng đảm bảo:</strong> </em>Vữa đã được trộn đều tại nhà máy theo một cấp phối định trước. Nguyên vật liệu đầu vào đảm bảo, đồng đều. Do đó, không chủ công trình không phải lo lắng về chất lượng xi măng, cát, hay tỉ lệ trộn, thời gian trộn như trước đây. Loại bỏ hoàn toàn hiện tượng rạn nứt bức tường do trộn không đều. Loại bỏ hoàn toàn hiện tượng nổ tường do phù sa, đất có trong cát không được xử lý. Phù hợp cho những công trình có định hướng ở lâu dài, yêu cầu chất lượng đảm bảo hay chủ công trình không sát sao được toàn thời gian với thợ thi công.</li>\r\n<li><em><strong>Sạch</strong>: </em>Công trình sạch sẽ, hầu như giảm hẳn bụi bặm ở công trường. Không ảnh hưởng tới các hộ gia đình lân cận, không làm bẩn đường xá, cống rãnh.</li>\r\n<li><em><strong>Tiết kiệm</strong>: </em>Thời gian thi công giảm được 1/3 lần, công sức lo vật tư thừa thiếu, kiểm soát chất lượng công trình. Năng suất lao động tăng lên đáng kể.</li>\r\n</ul>\r\n</div>', '<div style=\"text-align: justify;\"><strong>1. Vữa khô trộn sẵn là gì?</strong><br />Vữa khô trộn sẵn Mater là sản phẩm vữa khô dân dụng dùng cho xây, trát gạch tuylen truyền thống, gạch bê tông côt liệu; san cán nền dân dụng.<br /><strong>2. Thành phần</strong><br />Xi măng, cát sạch sấy khô, phụ gia.<br /><strong>3. Thông số kỹ thuật</strong><br />Sản xuất theo TCVN 4314:2003, đảm bảo các thông số sau đây:\r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA </strong><strong>KHÔ TRỘN SẴN M75#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>V</strong><strong>KM75</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥145</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥65</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥150</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥7,5</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n <br />Yêu cầu cát phải sạch, không lẫn tạp chất hoặc phù sa, độ ẩm nhỏ hơn 1% để không làm ảnh hưởng đến chất lượng vữa.</div>', '5', '', 0, 11, '', '1', 1, '2021-10-12', 0),
(5, 'vua-kho-tron-san-m100', '', 'VỮA KHÔ TRỘN SẴN M100#', '<div style=\"text-align: justify;\"><strong>4. Định mức sử dụng</strong><br />Xây mạch 1cm, 1m3 gạch hết 350kg.<br />Trát dày 1cm hết 16kg cho 1 mặt.<br /><strong>5. Lợi ích khi sử dụng</strong>\r\n<ul>\r\n<li><strong><em>Tiện lợi:</em></strong> Không phải chuyển từng xe cát, từng bao xi măng lên công trình. Không phải sàng cát trước khi trộn. Dễ dàng vận chuyển, mang vác vật tư đến mọi vị trí thi công. Giảm tối đa diện tích tập kết vật tư và thi công. Rất thuận lợi cho những công trình xây mới trong ngõ nhỏ, cải tạo nhà trong hẻm hay hoàn thiện chung cư có người đang ở.</li>\r\n<li><strong><em>Chất lượng đảm bảo</em></strong><em>: </em>Vữa đã được trộn đều tại nhà máy theo một cấp phối định trước. Nguyên vật liệu đầu vào đảm bảo, đồng đều. Do đó, không chủ công trình không phải lo lắng về chất lượng xi măng, cát, hay tỉ lệ trộn, thời gian trộn như trước đây. Loại bỏ hoàn toàn hiện tượng rạn nứt bức tường do trộn không đều. Loại bỏ hoàn toàn hiện tượng nổ tường do phù sa, đất có trong cát không được xử lý. Phù hợp cho những công trình có định hướng ở lâu dài, yêu cầu chất lượng đảm bảo hay chủ công trình không sát sao được toàn thời gian với thợ thi công.</li>\r\n<li><strong><em>Sạch</em></strong><em>: </em>Công trình sạch sẽ, hầu như giảm hẳn bụi bặm ở công trường. Không ảnh hưởng tới các hộ gia đình lân cận, không làm bẩn đường xá, cống rãnh.</li>\r\n<li><strong><em>Tiết kiệm</em></strong><em>: </em>Thời gian thi công giảm được 1/3 lần, công sức lo vật tư thừa thiếu, kiểm soát chất lượng công trình. Năng suất lao động tăng lên đáng kể.</li>\r\n</ul>\r\n</div>', '<div style=\"text-align: justify;\"><strong>1. Vữa khô trộn sẵn là gì?</strong><br />Vữa khô trộn sẵn Mater là sản phẩm vữa khô dân dụng dùng cho xây, trát gạch tuylen truyền thống, gạch bê tông côt liệu; san cán nền dân dụng.<br /><strong>2. Thành phần</strong><br />Xi măng, cát sạch sấy khô, phụ gia.<br /><strong>3. Thông số kỹ thuật</strong><br />Sản xuất theo TCVN 4314:2003, đảm bảo các thông số sau đây:\r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA </strong><strong>KHÔ TRỘN SẴN M100#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>V</strong><strong>KM100</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥145</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥65</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥150</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥10,0</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n <br />Yêu cầu cát phải sạch, không lẫn tạp chất hoặc phù sa, độ ẩm nhỏ hơn 1% để không làm ảnh hưởng đến chất lượng vữa.</div>', '5', '', 0, 4, '', '1', 1, '2021-10-12', 0),
(6, 'vua-xay-gach-nhe-aac-m50', '', 'VỮA XÂY GẠCH NHẸ AAC M50#', '<div style=\"text-align: justify;\"><strong><em>* Tại sao phải sử dụng vữa xây gạch nhẹ?</em></strong>\r\n<ul>\r\n<li>5 lý do công trình xây gạch siêu nhẹ AAC bằng vữa chuyên dụng:</li>\r\n<li><strong>Khối tường xây tăng thêm 20% cường độ.</strong></li>\r\n<li><strong>Loại bỏ hiện tượng cướp nước của vữa gây nên rạn nứt mạch vữa.</strong></li>\r\n</ul>\r\nLý do: Gạch AAC hút nước nhiều. Trong vữa chuyên dụng có phụ gia giữ nước giúp xi măng có nước để phát triển cường độ, tạo độ bám dính. Vữa thường không có phụ gia nên gạch hút nước từ khối vữa quá nhanh dẫn đến xi măng không đủ nước để tạo cường độ, mạch vữa bị rạn nứt và tách giữa lớp vữa và gạch.\r\n<ol>\r\n<li value=\"3\"><strong>Kiểm soát được chất lượng vật tư đầu vào, đảm bảo phối trộn và thi công.</strong></li>\r\n<li><strong>Hạn chế thấm nước qua các mạch vữa.</strong></li>\r\n<li><strong>Giảm nhân công vận chuyển, giảm mặt bằng thi công. Công trường sạch sẽ, đỡ bụi bặm hơn. Môi trường xanh - sạch</strong></li>\r\n</ol>\r\n<strong><em>* Ưu điểm của vữa MATER?</em></strong>\r\n<ul>\r\n<li>Điểu khác biệt của vữa Mater với các loại vữa khác đó là cường độ cao, độ bám dính rất tốt vượt xa so với TCVN.</li>\r\n<li>Tương thích với gạch siêu nhẹ và tường bê tông nên không bị rạn nứt, co ngót.</li>\r\n<li>Lượng dùng ít, với xây tường hết 30-50kg/m3 gạch (dày 3-5mm).</li>\r\n<li>Nhanh đóng rắn, sau 24h bức tường đã rất vững chắc.</li>\r\n<li>Dễ thi công</li>\r\n<li>Không hao phí vật tư, không bị rơi vãi. Do vữa đóng sẵn thành bao 50kg nên dễ vận chuyển, mang vác tại công trường, giúp công trường sạch sẽ hơn, xanh hơn.</li>\r\n<li>Giá thành hợp lý.</li>\r\n</ul>\r\n<strong>* Bảng so sánh vữa xây gạch nhẹ Mater với vữa xi măng truyền thống</strong><br /> \r\n<table width=\"606\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xây - trát nhẹ MATER</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt</td>\r\n<td>Bám dính kém, lớp tiếp giáp rất mủn</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Giữ nước tốt, dễ thi công</td>\r\n<td>Giữ nước kém, khó thi công</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Ít co ngót rạn nứt</td>\r\n<td>Hay rạn nứt</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Xây mạch mỏng, trát lớp mỏng tiến độ nhanh</td>\r\n<td>Xây, trát dầy, tiến độ chậm  </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n <br /><strong><em>* Khách hàng có lợi gì khi mua vữa MATER?</em></strong>\r\n<ul>\r\n<li>Yên tâm về chất lượng công trình. Công trình sẽ không bị rạn nứt, co ngót hay bong tróc khi xây và trát gạch AAC.</li>\r\n<li>Giá thành giảm đáng kể so với các loại vữa khác.</li>\r\n<li>Giảm hẳn mối lo về vật tư đầu vào, từ chỗ tập kết đến vận chuyển đến vị trí thi công, không cần đội ngũ thi công tay nghề cao.</li>\r\n<li>Mua hàng nhanh chóng, dịch vụ chu đáo, tư vấn tận tình. Đảm bảo khách hàng mua đúng, mua đủ cho công trình của mình.</li>\r\n</ul>\r\n</div>', '<strong><em>* Vữa xây gạch nhẹ là gì?</em></strong><br />Vữa xây gạch nhẹ là vữa chuyên dụng dùng để xây hoặc trát gạch bê tông khí chưng áp AAC. Khi sử dụng vữa này, mạch xây rất mỏng chỉ dày 3-5mm, lớp trát dày 5-8mm.<br /> <strong>* Đặc tính kỹ thuật vữa xây gạch nhẹ M</strong><strong>50</strong><strong>#</strong><br /> \r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA XÂY </strong><strong>GẠCH NHẸ AAC M50#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>V</strong><strong>KM50</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥185</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥80</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥180</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥6,5</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥0,5</td>\r\n<td>TCVN 3121-12:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong><em>- Tiêu chuẩn áp dụng: TCVN 9028:2011; ISO 9001:2015</em></strong>', '3', '', 0, 12, '', '1', 1, '2021-10-12', 0),
(7, 'vua-xay-gach-nhe-aac-m75', '', 'VỮA XÂY GẠCH NHẸ AAC M75#', '<div style=\"text-align: justify;\"><strong><em>* Tại sao phải sử dụng vữa xây gạch nhẹ?</em></strong>\r\n<ul>\r\n<li>5 lý do công trình xây gạch siêu nhẹ AAC bằng vữa chuyên dụng:</li>\r\n<li><strong>Khối tường xây tăng thêm 20% cường độ.</strong></li>\r\n<li><strong>Loại bỏ hiện tượng cướp nước của vữa gây nên rạn nứt mạch vữa.</strong></li>\r\n</ul>\r\nLý do: Gạch AAC hút nước nhiều. Trong vữa chuyên dụng có phụ gia giữ nước giúp xi măng có nước để phát triển cường độ, tạo độ bám dính. Vữa thường không có phụ gia nên gạch hút nước từ khối vữa quá nhanh dẫn đến xi măng không đủ nước để tạo cường độ, mạch vữa bị rạn nứt và tách giữa lớp vữa và gạch.\r\n<ol>\r\n<li value=\"3\"><strong>Kiểm soát được chất lượng vật tư đầu vào, đảm bảo phối trộn và thi công.</strong></li>\r\n<li><strong>Hạn chế thấm nước qua các mạch vữa.</strong></li>\r\n<li><strong>Giảm nhân công vận chuyển, giảm mặt bằng thi công. Công trường sạch sẽ, đỡ bụi bặm hơn. Môi trường xanh - sạch</strong></li>\r\n</ol>\r\n <br /><strong><em>* Ưu điểm của vữa MATER?</em></strong>\r\n<ul>\r\n<li>Điểu khác biệt của vữa Mater với các loại vữa khác đó là cường độ cao, độ bám dính rất tốt vượt xa so với TCVN.</li>\r\n<li>Tương thích với gạch siêu nhẹ và tường bê tông nên không bị rạn nứt, co ngót.</li>\r\n<li>Lượng dùng ít, với xây tường hết 30-50kg/m3 gạch (dày 3-5mm).</li>\r\n<li>Nhanh đóng rắn, sau 24h bức tường đã rất vững chắc.</li>\r\n<li>Dễ thi công</li>\r\n<li>Không hao phí vật tư, không bị rơi vãi. Do vữa đóng sẵn thành bao 50kg nên dễ vận chuyển, mang vác tại công trường, giúp công trường sạch sẽ hơn, xanh hơn.</li>\r\n<li>Giá thành hợp lý.</li>\r\n</ul>\r\n <br /><strong>* Bảng so sánh vữa xây gạch nhẹ Mater với vữa xi măng truyền thống</strong><br /> \r\n<table width=\"606\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xây gạch nhẹ Mater</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt</td>\r\n<td>Bám dính kém, lớp tiếp giáp rất mủn</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Giữ nước tốt, dễ thi công</td>\r\n<td>Giữ nước kém, khó thi công</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Ít co ngót rạn nứt</td>\r\n<td>Hay rạn nứt</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Xây mạch mỏng, trát lớp mỏng tiến độ nhanh</td>\r\n<td>Xây, trát dầy, tiến độ chậm  </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n <br /><strong><em>* Khách hàng có lợi gì khi mua vữa MATER?</em></strong>\r\n<ul>\r\n<li>Yên tâm về chất lượng công trình. Công trình sẽ không bị rạn nứt, co ngót hay bong tróc khi xây và trát gạch AAC.</li>\r\n<li>Giá thành giảm đáng kể so với các loại vữa khác.</li>\r\n<li>Giảm hẳn mối lo về vật tư đầu vào, từ chỗ tập kết đến vận chuyển đến vị trí thi công, không cần đội ngũ thi công tay nghề cao.</li>\r\n<li>Mua hàng nhanh chóng, dịch vụ chu đáo, tư vấn tận tình. Đảm bảo khách hàng mua đúng, mua đủ cho công trình của mình.</li>\r\n</ul>\r\n</div>', '<div style=\"text-align: justify;\"><strong><em>* Vữa xây gạch nhẹ là gì?</em></strong><br />Vữa xây gạch nhẹ là vữa chuyên dụng dùng để xây hoặc trát gạch bê tông khí chưng áp AAC. Khi sử dụng vữa này, mạch xây rất mỏng chỉ dày 3-5mm, lớp trát dày 5-8mm.<br /> <strong>* Đặc tính kỹ thuật vữa xây gạch nhẹ M75#</strong><br /> \r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA XÂY GACH NHẸ AAC M75#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>VX</strong><strong>M75</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥185</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥80</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥180</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥9,0</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥0,6</td>\r\n<td>TCVN 3121-12:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong><em>- Tiêu chuẩn áp dụng: TCVN 9028:2011; ISO 9001:2015</em></strong></div>', '3', '', 0, 11, '', '1', 1, '2021-10-12', 0),
(8, 'vua-xay-gach-nhe-aac-m100', '', 'VỮA XÂY GẠCH NHẸ AAC M100#', '<div style=\"text-align: justify;\"><strong><em>* Tại sao phải sử dụng v</em></strong><strong><em>ữa xây gach nhẹ </em></strong><strong><em>?</em></strong>\r\n<ul>\r\n<li>5 lý do công trình xây gạch siêu nhẹ AAC bằng vữa chuyên dụng:</li>\r\n<li><strong>Khối tường xây tăng thêm 20% cường độ.</strong></li>\r\n<li><strong>Loại bỏ hiện tượng cướp nước của vữa gây nên rạn nứt mạch vữa.</strong></li>\r\n</ul>\r\nLý do: Gạch AAC hút nước nhiều. Trong vữa chuyên dụng có phụ gia giữ nước giúp xi măng có nước để phát triển cường độ, tạo độ bám dính. Vữa thường không có phụ gia nên gạch hút nước từ khối vữa quá nhanh dẫn đến xi măng không đủ nước để tạo cường độ, mạch vữa bị rạn nứt và tách giữa lớp vữa và gạch.\r\n<ol>\r\n<li value=\"3\"><strong>Kiểm soát được chất lượng vật tư đầu vào, đảm bảo phối trộn và thi công.</strong></li>\r\n<li><strong>Hạn chế thấm nước qua các mạch vữa.</strong></li>\r\n<li><strong>Giảm nhân công vận chuyển, giảm mặt bằng thi công. Công trường sạch sẽ, đỡ bụi bặm hơn. Môi trường xanh - sạch</strong></li>\r\n</ol>\r\n</div>', '<div style=\"text-align: justify;\"><strong><em>* Vữa xây gạch nhẹ là gì?</em></strong><br />Vữa xây gạch nhẹ là vữa chuyên dụng dùng để xây hoặc trát gạch bê tông khí chưng áp AAC. Khi sử dụng vữa này, mạch xây rất mỏng chỉ dày 3-5mm, lớp trát dày 5-8mm.<br /> <strong>* Đặc tính kỹ thuật vữa xây gạch nhẹ M100</strong><strong>#</strong><br /> \r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA XÂY </strong><strong>GẠCH NHẸ AAC M100#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>V</strong><strong>XM100</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥185</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥90</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥180</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥12,0</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥0,65</td>\r\n<td>TCVN 3121-12:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong><em>- Tiêu chuẩn áp dụng: TCVN 9028:2011; ISO 9001:2015</em></strong></div>', '3', '', 0, 3, '', '1', 1, '2021-10-12', 0),
(9, 'vua-xay-gach-nhe-aac-m50', '', 'VỮA TRÁT GẠCH NHẸ AAC M50#', '<div style=\"text-align: justify;\"><strong><em>* Tại sao phải sử dụng vữa xây gạch nhẹ?</em></strong>\r\n<ul>\r\n<li>5 lý do công trình xây gạch siêu nhẹ AAC bằng vữa chuyên dụng:</li>\r\n<li><strong>Khối tường xây tăng thêm 20% cường độ.</strong></li>\r\n<li><strong>Loại bỏ hiện tượng cướp nước của vữa gây nên rạn nứt mạch vữa.</strong></li>\r\n</ul>\r\nLý do: Gạch AAC hút nước nhiều. Trong vữa chuyên dụng có phụ gia giữ nước giúp xi măng có nước để phát triển cường độ, tạo độ bám dính. Vữa thường không có phụ gia nên gạch hút nước từ khối vữa quá nhanh dẫn đến xi măng không đủ nước để tạo cường độ, mạch vữa bị rạn nứt và tách giữa lớp vữa và gạch.\r\n<ol>\r\n<li value=\"3\"><strong>Kiểm soát được chất lượng vật tư đầu vào, đảm bảo phối trộn và thi công.</strong></li>\r\n<li><strong>Hạn chế thấm nước qua các mạch vữa.</strong></li>\r\n<li><strong>Giảm nhân công vận chuyển, giảm mặt bằng thi công. Công trường sạch sẽ, đỡ bụi bặm hơn. Môi trường xanh - sạch</strong></li>\r\n</ol>\r\n<strong><em>* Ưu điểm của vữa MATER?</em></strong>\r\n<ul>\r\n<li>Điểu khác biệt của vữa Mater với các loại vữa khác đó là cường độ cao, độ bám dính rất tốt vượt xa so với TCVN.</li>\r\n<li>Tương thích với gạch siêu nhẹ và tường bê tông nên không bị rạn nứt, co ngót.</li>\r\n<li>Lượng dùng ít, với xây tường hết 30-50kg/m3 gạch (dày 3-5mm).</li>\r\n<li>Nhanh đóng rắn, sau 24h bức tường đã rất vững chắc.</li>\r\n<li>Dễ thi công</li>\r\n<li>Không hao phí vật tư, không bị rơi vãi. Do vữa đóng sẵn thành bao 50kg nên dễ vận chuyển, mang vác tại công trường, giúp công trường sạch sẽ hơn, xanh hơn.</li>\r\n<li>Giá thành hợp lý.</li>\r\n</ul>\r\n<strong>* Bảng so sánh vữa xây gạch nhẹ Mater với vữa xi măng truyền thống</strong><br /> \r\n<table width=\"606\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xây - trát nhẹ MATER</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt</td>\r\n<td>Bám dính kém, lớp tiếp giáp rất mủn</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Giữ nước tốt, dễ thi công</td>\r\n<td>Giữ nước kém, khó thi công</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Ít co ngót rạn nứt</td>\r\n<td>Hay rạn nứt</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Xây mạch mỏng, trát lớp mỏng tiến độ nhanh</td>\r\n<td>Xây, trát dầy, tiến độ chậm  </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n <br /><strong><em>* Khách hàng có lợi gì khi mua vữa MATER?</em></strong>\r\n<ul>\r\n<li>Yên tâm về chất lượng công trình. Công trình sẽ không bị rạn nứt, co ngót hay bong tróc khi xây và trát gạch AAC.</li>\r\n<li>Giá thành giảm đáng kể so với các loại vữa khác.</li>\r\n<li>Giảm hẳn mối lo về vật tư đầu vào, từ chỗ tập kết đến vận chuyển đến vị trí thi công, không cần đội ngũ thi công tay nghề cao.</li>\r\n<li>Mua hàng nhanh chóng, dịch vụ chu đáo, tư vấn tận tình. Đảm bảo khách hàng mua đúng, mua đủ cho công trình của mình.</li>\r\n</ul>\r\n</div>', '<div style=\"text-align: justify;\"><strong><em>* Vữa xây gạch nhẹ là gì?</em></strong><br />Vữa xây gạch nhẹ là vữa chuyên dụng dùng để xây hoặc trát gạch bê tông khí chưng áp AAC. Khi sử dụng vữa này, mạch xây rất mỏng chỉ dày 3-5mm, lớp trát dày 5-8mm.<br /> <strong>* Đặc tính kỹ thuật vữa xây gạch nhẹ M</strong><strong>50</strong><strong>#</strong><br /> \r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA XÂY </strong><strong>GẠCH NHẸ AAC M50#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>V</strong><strong>KM50</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥185</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥80</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥180</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥6,5</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥0,5</td>\r\n<td>TCVN 3121-12:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong><em>- Tiêu chuẩn áp dụng: TCVN 9028:2011; ISO 9001:2015</em></strong></div>', '6', '', 0, 12, '', '1', 1, '2021-10-12', 0),
(10, 'keo-dan-gach-da-m203', '', 'KEO LÁT GẠCH- ĐÁ M203', '<div style=\"text-align: justify;\"><strong><em>3. Tại sao nên sử dụng keo dán gạch – đá?</em></strong>\r\n<ul>\r\n<li><br />-  Keo dán gạch – đá có nhiều phụ gia polymer nên khả năng giữ nước cao, keo lâu bị “chết” hơn xi măng, độ bám dính rất tốt và dễ điều chỉnh gạch – đá khi thi công.</li>\r\n<li>Lớp keo dàn đều trên bề mặt viên gạch – đá nên loại bỏ được những khoảng trống giữa gạch – đá và cốt nền; nhờ vậy không còn hiện tượng ộp gạch – đá, bong rộp như ốp lát truyền thống.</li>\r\n<li>-  Cường độ nén của keo rất cao, độ bám dính tốt nên sau khi sử dụng gạch – đá và cốt nền thành một khối vô cùng bền vững. Có thể ốp lát gạch trong những môi trường khắc nghiệp: dưới nước, trên cao, thay đổi nhiệt độ lớn, …</li>\r\n<li>-  Tăng khả năng chống thấm cho vật liệu.</li>\r\n<li>-  Ngày nay, các loại gạch granit, đá mable, đá tự nhiên, … có độ cứng cao và hút nước rất ít, ốp lát bằng phương pháp truyền thống rất dễ bong tróc do xi măng không thể bám dính tốt trên bề mặt những loại vật liệu này. Chính vì thế, nhiều nhà máy sản xuất gạch – đá đã khuyến cáo nên sử dụng keo dán gạch – đá cho một số dòng sản phẩm của mình.</li>\r\n<li>-  Với các công trình cao trầng, độ rung lắc lớn, keo dán gạch – đá là một giải pháp bắt buộc bởi yêu cầu độ uốn cao, độ mở lớn đối với vật liệu ốp lát. Bộ Xây dựng khuyến cáo đối với các công trình cao trên 9 tầng nên sử dụng keo dán gạch – đá để ốp lát để đảm bảo tính bền vững và an toàn cho người sử dụng.</li>\r\n<li><strong>4. Bảng so sánh:</strong></li>\r\n</ul>\r\n<table width=\"607\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Keo dán gạch - đá MATER</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Hồ xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt gạch, đá vào nền (vách)</td>\r\n<td>Bám dính kém</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Chịu được nước, lão hóa, thay đổi nhiệt</td>\r\n<td>Không chịu được môi trường thay đổi</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Keo dàn đều bề mặt gạch, đá không ộp</td>\r\n<td>Hay ộp, bong tróc</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '<div style=\"text-align: justify;\"><strong><em>1. Keo dán gạch - đá là gì?</em></strong><br />Keo dán gạch – đá là sản phẩm được chế tạo từ xi măng, cát khô sạch, phụ gia polymer trộn đều, dùng để ốp hoặc lát gạch – đá lên tường hoặc sàn. Keo dán gạch – đá thay thế cho xi măng, hồ xi măng trong ốp lát truyền thống.<br /><strong><em>2. Thông số kỹ thuật</em></strong>\r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT KEO DÁN GẠCH – ĐÁ</strong><strong> M203</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>K</strong><strong>M2</strong><strong>03</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2,5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥0,8</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Cường độ bám dính sau ngâm nước 72h</td>\r\n<td>mm</td>\r\n<td>≥0,5</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Cường độ bám dính sau sốc nhiệt</td>\r\n<td>mm</td>\r\n<td>≥0,5</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Cường độ nén</td>\r\n<td>mm</td>\r\n<td>≥20,0</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '4', '', 0, 1, '', '1', 1, '2021-10-12', 0),
(11, 'keo-op-gach-da-m202', '', 'KEO ỐP GẠCH - ĐÁ M202', '<div style=\"text-align: justify;\"><strong><em>3. Tại sao nên sử dụng keo dán gạch – đá?</em></strong>\r\n<ul>\r\n<li>-  Keo dán gạch – đá có nhiều phụ gia polymer nên khả năng giữ nước cao, keo lâu bị “chết” hơn xi măng, độ bám dính rất tốt và dễ điều chỉnh gạch – đá khi thi công.</li>\r\n<li>Lớp keo dàn đều trên bề mặt viên gạch – đá nên loại bỏ được những khoảng trống giữa gạch – đá và cốt nền; nhờ vậy không còn hiện tượng ộp gạch – đá, bong rộp như ốp lát truyền thống.</li>\r\n<li>-  Cường độ nén của keo rất cao, độ bám dính tốt nên sau khi sử dụng gạch – đá và cốt nền thành một khối vô cùng bền vững. Có thể ốp lát gạch trong những môi trường khắc nghiệp: dưới nước, trên cao, thay đổi nhiệt độ lớn, …</li>\r\n<li>-  Tăng khả năng chống thấm cho vật liệu.</li>\r\n<li>-  Ngày nay, các loại gạch granit, đá mable, đá tự nhiên, … có độ cứng cao và hút nước rất ít, ốp lát bằng phương pháp truyền thống rất dễ bong tróc do xi măng không thể bám dính tốt trên bề mặt những loại vật liệu này. Chính vì thế, nhiều nhà máy sản xuất gạch – đá đã khuyến cáo nên sử dụng keo dán gạch – đá cho một số dòng sản phẩm của mình.</li>\r\n<li>-  Với các công trình cao trầng, độ rung lắc lớn, keo dán gạch – đá là một giải pháp bắt buộc bởi yêu cầu độ uốn cao, độ mở lớn đối với vật liệu ốp lát. Bộ Xây dựng khuyến cáo đối với các công trình cao trên 9 tầng nên sử dụng keo dán gạch – đá để ốp lát để đảm bảo tính bền vững và an toàn cho người sử dụng.</li>\r\n<li><strong>4. Bảng so sánh:</strong></li>\r\n</ul>\r\n<table width=\"607\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Keo dán gạch - đá MATER</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Hồ xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt gạch, đá vào nền (vách)</td>\r\n<td>Bám dính kém</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Chịu được nước, lão hóa, thay đổi nhiệt</td>\r\n<td>Không chịu được môi trường thay đổi</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Keo dàn đều bề mặt gạch, đá không ộp</td>\r\n<td>Hay ộp, bong tróc</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '<div style=\"text-align: justify;\"><strong><em>1. Keo dán gạch - đá là gì?</em></strong><br />Keo dán gạch – đá là sản phẩm được chế tạo từ xi măng, cát khô sạch, phụ gia polymer trộn đều, dùng để ốp hoặc lát gạch – đá lên tường hoặc sàn. Keo dán gạch – đá thay thế cho xi măng, hồ xi măng trong ốp lát truyền thống.<br /><strong><em>2. Thông số kỹ thuật</em></strong>\r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT KEO DÁN GẠCH – ĐÁ</strong><strong> M202</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>K</strong><strong>M2</strong><strong>02</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2,5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥1,2</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Cường độ bám dính sau ngâm nước 72h</td>\r\n<td>mm</td>\r\n<td>≥0,9</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Cường độ bám dính sau sốc nhiệt</td>\r\n<td>mm</td>\r\n<td>≥0,9</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Cường độ nén</td>\r\n<td>mm</td>\r\n<td>≥20,0</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '4', '', 0, 0, '', '1', 1, '2021-10-12', 0),
(12, 'bot-ba-skimcoat-lot-thay-trat', '', 'BỘT BẢ SKIMCOAT LÓT XÁM 401', '<div style=\"text-align: justify;\">\r\n<p><strong>2.6 Hướng dẫn sử dụng</strong></p>\r\n<p><strong> - Chuẩn bị</strong></p>\r\n<p>Tường</p>\r\n<ul>\r\n<li>Bề mặt tường phải sạch sẽ, không có các loại dầu mỡ, tạp chất cơ học. Nếu tường xây quá lồi lõm có thể mài trước khi bả.</li>\r\n<li>Nếu là tường cũ thì phải làm sạch lớp sơn vôi cũ, diệt hết rêu nấm mốc</li>\r\n<li>Dụng cụ</li>\r\n</ul>\r\n<ul>\r\n<li>Thùng, cánh khuấy, nước sạch, bàn bả, bột bả Skimcoat Mater.</li>\r\n</ul>\r\n<p> - <strong>Thi công</strong></p>\r\n<p>Trộn vật tư</p>\r\n<ul>\r\n<li>Bột Skimcoat lót M401: Trộn theo tỉ lệ khối lượng: nước/bột bả = 18-22%</li>\r\n<li>Nên đổ bột vào nước để tránh hiện tượng vón cục. Dùng máy khuấy tay khuấy đều đến khi tạo thành hỗn hợp sệt dẻo là được. Sau khi khuấy 5 phút mới được thi công.</li>\r\n<li>Bả</li>\r\n</ul>\r\n<ul>\r\n<li>Dùng bàn bả thép bả hỗn hợp lên tường</li>\r\n<li>Tùy bề mặt tường và chiều dày lớp bả để có số lớp bả thích hợp. Nên bả tối thiểu 2 lớp, mỗi lớp cách nhau tối thiểu 2h.</li>\r\n<li>Sau khi bả, trong thời gian 2 ngày phải đánh ráp bằng loại ráp mịn để tránh trường hợp lớp bả quá cứng khó đánh ráp. Trường hợp chỉ dùng lớp bả lót M401 phải đánh ngay ráp mịn trong vòng 04 giờ.</li>\r\n</ul>\r\n</div>', '<div style=\"text-align: justify;\">\r\n<p><strong>BỘT BẢ SKIMCOAT MATER</strong></p>\r\n<p><strong>1. Ưu điểm</strong></p>\r\n<ul>\r\n<li>Tạo phẳng, lấp những vị trí lồi lõm, tạo độ nhẵn cho bề mặt tường.</li>\r\n<li>Cường độ cao, bám dính tốt, không co ngót, không rạn nứt giúp gia cố tường xây, tăng cường độ cho tường</li>\r\n<li>Tăng khả năng chống thấm</li>\r\n<li>Không cần trát và sơn lót. Giảm đáng kể chi phí xây dựng</li>\r\n<li>Thi công dễ dàng, không hao phí vật tư khi thi công</li>\r\n<li>Giảm rất nhiều thời gian thi công do bỏ được phần trát và thời gian chờ tường khô trước khi sơn.</li>\r\n</ul>\r\n<p> </p>\r\n<p><strong>2. Đặc tính kỹ thuật</strong></p>\r\n<p><strong>2.1 Thành phần:</strong></p>\r\n<p><em>Bột bả Skimcoat M401</em></p>\r\n<ul>\r\n<li>Bao gồm xi măng đen cường độ cao, cốt liệu tinh lọc cực mịn, polymer, phụ gia chống thấm và nhiều phụ gia đặc biệt khác</li>\r\n<li>Màu sắc: Xám</li>\r\n<li>Sử dụng trên các bề mặt lồi lõm nhiều, tường gạch yếu cần tăng cường độ cho tường, giúp bảo vệ gạch phía trong. Công dụng như một lớp trát mỏng cường độ cao.</li>\r\n<li> </li>\r\n<li> </li>\r\n<li>\r\n<p><strong>2.2 Thông số kỹ thuật</strong></p>\r\n<p><em> </em></p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td width=\"53\">\r\n<p><strong>STT</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Chỉ tiêu</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Đơn vị</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Kết quả</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>1</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ bám dính</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>N/mm<sup>2</sup></p>\r\n</td>\r\n<td width=\"132\">\r\n<p>1.0</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>2</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ linh động</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>mm</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>202</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>3</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Cường độ nén</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>N/mm<sup>2</sup></p>\r\n</td>\r\n<td width=\"132\">\r\n<p>18</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>4</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ giữ nước</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>%</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>98</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<em> </em>\r\n<p><strong> 2.3 Đóng gói</strong></p>\r\n<ul>\r\n<li>Bao 2 lớp chống ẩm, khối lượng 50kg</li>\r\n<li>Sử dụng tốt nhất trong vòng 06 tháng kế từ ngày sản xuất</li>\r\n<li>Bảo quản nơi khô ráo.</li>\r\n</ul>\r\n<p> </p>\r\n<ul>\r\n<li><strong>2.4 Ứng dụng</strong></li>\r\n<li>Sử dụng trên tất cả các loại tường gạch: Gạch đỏ nung, gạch xi măng cốt liệu, gạch bê tông bọt, gạch bê tông khí chưng áp (AAC), gạch mát, tấm thạch cao,…</li>\r\n<li>Bả cả nội thất và ngoại thất, chịu được trong các môi trường thời tiết khắc nghiệt.</li>\r\n</ul>\r\n<p><strong> </strong></p>\r\n<p><strong>2.5 Định mức</strong></p>\r\n<ul>\r\n<li>Chiều dày lớp bả: 1-4 mm (tùy chất lượng tường)</li>\r\n<li>Skimcoat lót M401: 1,4 kg/m<sup>2</sup>/mm (Thông thường chiều dày lớp Skimcoat lót từ 2-4mm)</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</div>', '7', '', 0, 0, '', '1', 1, '2021-10-12', 0),
(13, 'vua-trat-gach-nhe-aac-m75', '', 'VỮA TRÁT GẠCH NHẸ AAC M75#', '<div style=\"text-align: justify;\"><strong><em>* Tại sao phải sử dụng vữa xây gạch nhẹ?</em></strong>\r\n<ul>\r\n<li>5 lý do công trình xây gạch siêu nhẹ AAC bằng vữa chuyên dụng:</li>\r\n<li><strong>Khối tường xây tăng thêm 20% cường độ.</strong></li>\r\n<li><strong>Loại bỏ hiện tượng cướp nước của vữa gây nên rạn nứt mạch vữa.</strong></li>\r\n</ul>\r\nLý do: Gạch AAC hút nước nhiều. Trong vữa chuyên dụng có phụ gia giữ nước giúp xi măng có nước để phát triển cường độ, tạo độ bám dính. Vữa thường không có phụ gia nên gạch hút nước từ khối vữa quá nhanh dẫn đến xi măng không đủ nước để tạo cường độ, mạch vữa bị rạn nứt và tách giữa lớp vữa và gạch.\r\n<ol>\r\n<li value=\"3\"><strong>Kiểm soát được chất lượng vật tư đầu vào, đảm bảo phối trộn và thi công.</strong></li>\r\n<li><strong>Hạn chế thấm nước qua các mạch vữa.</strong></li>\r\n<li><strong>Giảm nhân công vận chuyển, giảm mặt bằng thi công. Công trường sạch sẽ, đỡ bụi bặm hơn. Môi trường xanh - sạch</strong></li>\r\n</ol>\r\n<strong><em>* Ưu điểm của vữa MATER?</em></strong>\r\n<ul>\r\n<li>Điểu khác biệt của vữa Mater với các loại vữa khác đó là cường độ cao, độ bám dính rất tốt vượt xa so với TCVN.</li>\r\n<li>Tương thích với gạch siêu nhẹ và tường bê tông nên không bị rạn nứt, co ngót.</li>\r\n<li>Lượng dùng ít, với xây tường hết 30-50kg/m3 gạch (dày 3-5mm).</li>\r\n<li>Nhanh đóng rắn, sau 24h bức tường đã rất vững chắc.</li>\r\n<li>Dễ thi công</li>\r\n<li>Không hao phí vật tư, không bị rơi vãi. Do vữa đóng sẵn thành bao 50kg nên dễ vận chuyển, mang vác tại công trường, giúp công trường sạch sẽ hơn, xanh hơn.</li>\r\n<li>Giá thành hợp lý.</li>\r\n</ul>\r\n<strong>* Bảng so sánh vữa xây gạch nhẹ Mater với vữa xi măng truyền thống</strong><br /> \r\n<table width=\"606\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xây - trát nhẹ MATER</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt</td>\r\n<td>Bám dính kém, lớp tiếp giáp rất mủn</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Giữ nước tốt, dễ thi công</td>\r\n<td>Giữ nước kém, khó thi công</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Ít co ngót rạn nứt</td>\r\n<td>Hay rạn nứt</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Xây mạch mỏng, trát lớp mỏng tiến độ nhanh</td>\r\n<td>Xây, trát dầy, tiến độ chậm  </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n <br /><strong><em>* Khách hàng có lợi gì khi mua vữa MATER?</em></strong>\r\n<ul>\r\n<li>Yên tâm về chất lượng công trình. Công trình sẽ không bị rạn nứt, co ngót hay bong tróc khi xây và trát gạch AAC.</li>\r\n<li>Giá thành giảm đáng kể so với các loại vữa khác.</li>\r\n<li>Giảm hẳn mối lo về vật tư đầu vào, từ chỗ tập kết đến vận chuyển đến vị trí thi công, không cần đội ngũ thi công tay nghề cao.</li>\r\n<li>Mua hàng nhanh chóng, dịch vụ chu đáo, tư vấn tận tình. Đảm bảo khách hàng mua đúng, mua đủ cho công trình của mình.</li>\r\n</ul>\r\n</div>', '<div style=\"text-align: justify;\"><strong><em>* Vữa xây gạch nhẹ là gì?</em></strong><br />Vữa xây gạch nhẹ là vữa chuyên dụng dùng để xây hoặc trát gạch bê tông khí chưng áp AAC. Khi sử dụng vữa này, mạch xây rất mỏng chỉ dày 3-5mm, lớp trát dày 5-8mm.<br /> <strong>* Đặc tính kỹ thuật vữa xây gạch nhẹ M</strong><strong>50</strong><strong>#</strong><br /> \r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA XÂY </strong><strong>GẠCH NHẸ AAC M50#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>V</strong><strong>KM50</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥185</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥80</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥180</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥6,5</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥0,5</td>\r\n<td>TCVN 3121-12:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong><em>- Tiêu chuẩn áp dụng: TCVN 9028:2011; ISO 9001:2015</em></strong></div>', '6', '', 0, 0, '', '1', 1, '2021-10-12', 0);
INSERT INTO `product` (`id`, `url`, `code`, `name`, `content`, `description`, `category`, `tag`, `price`, `view`, `avatar`, `position`, `status`, `updated`, `sort_order`) VALUES
(14, 'vua-xay-gach-nhe-aac-m100', '', 'VỮA TRÁT GẠCH NHẸ AAC M100#', '<div style=\"text-align: justify;\"><strong><em>* Tại sao phải sử dụng vữa xây gạch nhẹ?</em></strong>\r\n<ul>\r\n<li>5 lý do công trình xây gạch siêu nhẹ AAC bằng vữa chuyên dụng:</li>\r\n<li><strong>Khối tường xây tăng thêm 20% cường độ.</strong></li>\r\n<li><strong>Loại bỏ hiện tượng cướp nước của vữa gây nên rạn nứt mạch vữa.</strong></li>\r\n</ul>\r\nLý do: Gạch AAC hút nước nhiều. Trong vữa chuyên dụng có phụ gia giữ nước giúp xi măng có nước để phát triển cường độ, tạo độ bám dính. Vữa thường không có phụ gia nên gạch hút nước từ khối vữa quá nhanh dẫn đến xi măng không đủ nước để tạo cường độ, mạch vữa bị rạn nứt và tách giữa lớp vữa và gạch.\r\n<ol>\r\n<li value=\"3\"><strong>Kiểm soát được chất lượng vật tư đầu vào, đảm bảo phối trộn và thi công.</strong></li>\r\n<li><strong>Hạn chế thấm nước qua các mạch vữa.</strong></li>\r\n<li><strong>Giảm nhân công vận chuyển, giảm mặt bằng thi công. Công trường sạch sẽ, đỡ bụi bặm hơn. Môi trường xanh - sạch</strong></li>\r\n</ol>\r\n<strong><em>* Ưu điểm của vữa MATER?</em></strong>\r\n<ul>\r\n<li>Điểu khác biệt của vữa Mater với các loại vữa khác đó là cường độ cao, độ bám dính rất tốt vượt xa so với TCVN.</li>\r\n<li>Tương thích với gạch siêu nhẹ và tường bê tông nên không bị rạn nứt, co ngót.</li>\r\n<li>Lượng dùng ít, với xây tường hết 30-50kg/m3 gạch (dày 3-5mm).</li>\r\n<li>Nhanh đóng rắn, sau 24h bức tường đã rất vững chắc.</li>\r\n<li>Dễ thi công</li>\r\n<li>Không hao phí vật tư, không bị rơi vãi. Do vữa đóng sẵn thành bao 50kg nên dễ vận chuyển, mang vác tại công trường, giúp công trường sạch sẽ hơn, xanh hơn.</li>\r\n<li>Giá thành hợp lý.</li>\r\n</ul>\r\n<strong>* Bảng so sánh vữa xây gạch nhẹ Mater với vữa xi măng truyền thống</strong><br /> \r\n<table width=\"606\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xây - trát nhẹ MATER</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Vữa xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt</td>\r\n<td>Bám dính kém, lớp tiếp giáp rất mủn</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Giữ nước tốt, dễ thi công</td>\r\n<td>Giữ nước kém, khó thi công</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Ít co ngót rạn nứt</td>\r\n<td>Hay rạn nứt</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Xây mạch mỏng, trát lớp mỏng tiến độ nhanh</td>\r\n<td>Xây, trát dầy, tiến độ chậm  </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n <br /><strong><em>* Khách hàng có lợi gì khi mua vữa MATER?</em></strong>\r\n<ul>\r\n<li>Yên tâm về chất lượng công trình. Công trình sẽ không bị rạn nứt, co ngót hay bong tróc khi xây và trát gạch AAC.</li>\r\n<li>Giá thành giảm đáng kể so với các loại vữa khác.</li>\r\n<li>Giảm hẳn mối lo về vật tư đầu vào, từ chỗ tập kết đến vận chuyển đến vị trí thi công, không cần đội ngũ thi công tay nghề cao.</li>\r\n<li>Mua hàng nhanh chóng, dịch vụ chu đáo, tư vấn tận tình. Đảm bảo khách hàng mua đúng, mua đủ cho công trình của mình.</li>\r\n</ul>\r\n</div>', '<div><strong><em>* Vữa xây gạch nhẹ là gì?</em></strong><br />Vữa xây gạch nhẹ là vữa chuyên dụng dùng để xây hoặc trát gạch bê tông khí chưng áp AAC. Khi sử dụng vữa này, mạch xây rất mỏng chỉ dày 3-5mm, lớp trát dày 5-8mm.<br /> <strong>* Đặc tính kỹ thuật vữa xây gạch nhẹ M</strong><strong>50</strong><strong>#</strong><br /> \r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT VỮA XÂY </strong><strong>GẠCH NHẸ AAC M50#</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>V</strong><strong>KM50</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2.5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Độ lưu động</td>\r\n<td>mm</td>\r\n<td>≥185</td>\r\n<td>TCVN 3121-3:2003</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Khả năng giữ độ lưu động</td>\r\n<td>%</td>\r\n<td>≥80</td>\r\n<td>TCVN 3121-8:2003</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Thời gian bắt đầu đông kết</td>\r\n<td>Phút</td>\r\n<td>≥180</td>\r\n<td>TCVN 3121-9:2003</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Hàm lượng ion clo hòa tan trong nước</td>\r\n<td>%</td>\r\n<td>≤0,1</td>\r\n<td>TCVN 3121-17:2003</td>\r\n</tr>\r\n<tr>\r\n<td>6</td>\r\n<td>Cường độ nén</td>\r\n<td>Mpa</td>\r\n<td>≥6,5</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n<tr>\r\n<td>7</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥0,5</td>\r\n<td>TCVN 3121-12:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong><em>- Tiêu chuẩn áp dụng: TCVN 9028:2011; ISO 9001:2015</em></strong></div>', '6', '', 0, 3, '', '1', 1, '2021-10-12', 0),
(15, 'keo-op-gach-da-m202-trang', '', 'KEO ỐP GẠCH - ĐÁ M202 TRẮNG', '<div style=\"text-align: justify;\"><strong><em>3. Tại sao nên sử dụng keo dán gạch – đá?</em></strong>\r\n<ul>\r\n<li>-  Keo dán gạch – đá có nhiều phụ gia polymer nên khả năng giữ nước cao, keo lâu bị “chết” hơn xi măng, độ bám dính rất tốt và dễ điều chỉnh gạch – đá khi thi công.</li>\r\n<li>Lớp keo dàn đều trên bề mặt viên gạch – đá nên loại bỏ được những khoảng trống giữa gạch – đá và cốt nền; nhờ vậy không còn hiện tượng ộp gạch – đá, bong rộp như ốp lát truyền thống.</li>\r\n<li>-  Cường độ nén của keo rất cao, độ bám dính tốt nên sau khi sử dụng gạch – đá và cốt nền thành một khối vô cùng bền vững. Có thể ốp lát gạch trong những môi trường khắc nghiệp: dưới nước, trên cao, thay đổi nhiệt độ lớn, …</li>\r\n<li>-  Tăng khả năng chống thấm cho vật liệu.</li>\r\n<li>-  Ngày nay, các loại gạch granit, đá mable, đá tự nhiên, … có độ cứng cao và hút nước rất ít, ốp lát bằng phương pháp truyền thống rất dễ bong tróc do xi măng không thể bám dính tốt trên bề mặt những loại vật liệu này. Chính vì thế, nhiều nhà máy sản xuất gạch – đá đã khuyến cáo nên sử dụng keo dán gạch – đá cho một số dòng sản phẩm của mình.</li>\r\n<li>-  Với các công trình cao trầng, độ rung lắc lớn, keo dán gạch – đá là một giải pháp bắt buộc bởi yêu cầu độ uốn cao, độ mở lớn đối với vật liệu ốp lát. Bộ Xây dựng khuyến cáo đối với các công trình cao trên 9 tầng nên sử dụng keo dán gạch – đá để ốp lát để đảm bảo tính bền vững và an toàn cho người sử dụng.</li>\r\n<li><strong>4. Bảng so sánh:</strong></li>\r\n</ul>\r\n<table width=\"607\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Keo dán gạch - đá MATER</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Hồ xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt gạch, đá vào nền (vách)</td>\r\n<td>Bám dính kém</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Chịu được nước, lão hóa, thay đổi nhiệt</td>\r\n<td>Không chịu được môi trường thay đổi</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Keo dàn đều bề mặt gạch, đá không ộp</td>\r\n<td>Hay ộp, bong tróc</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '<div style=\"text-align: justify;\"><strong><em>1. Keo dán gạch - đá là gì?</em></strong><br />Keo dán gạch – đá là sản phẩm được chế tạo từ xi măng, cát khô sạch, phụ gia polymer trộn đều, dùng để ốp hoặc lát gạch – đá lên tường hoặc sàn. Keo dán gạch – đá thay thế cho xi măng, hồ xi măng trong ốp lát truyền thống.<br /><strong><em>2. Thông số kỹ thuật</em></strong>\r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT KEO DÁN GẠCH – ĐÁ</strong><strong> M202</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>K</strong><strong>M2</strong><strong>02</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2,5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥1,2</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Cường độ bám dính sau ngâm nước 72h</td>\r\n<td>mm</td>\r\n<td>≥0,9</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Cường độ bám dính sau sốc nhiệt</td>\r\n<td>mm</td>\r\n<td>≥0,9</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Cường độ nén</td>\r\n<td>mm</td>\r\n<td>≥20,0</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '4', '', 0, 5, '', '1', 1, '2021-10-12', 0),
(16, 'keo-cha-mach-mau-trang', '', 'KEO CHÀ MẠCH MÀU TRẮNG', 'Tác dụng của keo chà mạch MATER:<br />- Điền đầy mạch, đảm bảo độ bám dính theo chiều đứng giữa các viên gạch<br />- Chống thấm tốt<br />- Không làm ố viền viên gạch<br />- Không bị đen mạch trong khi sử dụng', '<div style=\"text-align: justify;\"> </div>', '4', '', 0, 1, '', '1', 1, '2021-10-12', 0),
(17, 'bot-ba-skimcoat-xam-402', '', 'BỘT BẢ SKIMCOAT XÁM 402', '<div style=\"text-align: justify;\">\r\n<p><strong>2.6 Hướng dẫn sử dụng</strong></p>\r\n<p><strong> - Chuẩn bị</strong></p>\r\n<p>Tường</p>\r\n<ul>\r\n<li>Bề mặt tường phải sạch sẽ, không có các loại dầu mỡ, tạp chất cơ học. Nếu tường xây quá lồi lõm có thể mài trước khi bả.</li>\r\n<li>Nếu là tường cũ thì phải làm sạch lớp sơn vôi cũ, diệt hết rêu nấm mốc</li>\r\n<li>Dụng cụ</li>\r\n</ul>\r\n<ul>\r\n<li>Thùng, cánh khuấy, nước sạch, bàn bả, bột bả Skimcoat Mater.</li>\r\n</ul>\r\n<p> - <strong>Thi công</strong></p>\r\n<p>Trộn vật tư</p>\r\n<ul>\r\n<li>Bột Skimcoat lót M402: Trộn theo tỉ lệ khối lượng: nước/bột bả = 18-22%</li>\r\n<li>Nên đổ bột vào nước để tránh hiện tượng vón cục. Dùng máy khuấy tay khuấy đều đến khi tạo thành hỗn hợp sệt dẻo là được. Sau khi khuấy 5 phút mới được thi công.</li>\r\n<li>Bả</li>\r\n</ul>\r\n<ul>\r\n<li>Dùng bàn bả thép bả hỗn hợp lên tường</li>\r\n<li>Tùy bề mặt tường và chiều dày lớp bả để có số lớp bả thích hợp. Nên bả tối thiểu 2 lớp, mỗi lớp cách nhau tối thiểu 2h.</li>\r\n<li>Sau khi bả, trong thời gian 2 ngày phải đánh ráp bằng loại ráp mịn để tránh trường hợp lớp bả quá cứng khó đánh ráp. Trường hợp chỉ dùng lớp bả lót M402 phải đánh ngay ráp mịn trong vòng 04 giờ.</li>\r\n</ul>\r\n</div>', '<div>\r\n<p><strong>BỘT BẢ SKIMCOAT MATER</strong></p>\r\n<p><strong>1. Ưu điểm</strong></p>\r\n<ul>\r\n<li>Tạo phẳng, lấp những vị trí lồi lõm, tạo độ nhẵn cho bề mặt tường.</li>\r\n<li>Cường độ cao, bám dính tốt, không co ngót, không rạn nứt giúp gia cố tường xây, tăng cường độ cho tường</li>\r\n<li>Tăng khả năng chống thấm</li>\r\n<li>Không cần trát và sơn lót. Giảm đáng kể chi phí xây dựng</li>\r\n<li>Thi công dễ dàng, không hao phí vật tư khi thi công</li>\r\n<li>Giảm rất nhiều thời gian thi công do bỏ được phần trát và thời gian chờ tường khô trước khi sơn.</li>\r\n</ul>\r\n<p> </p>\r\n<p><strong>2. Đặc tính kỹ thuật</strong></p>\r\n<p><strong>2.1 Thành phần:</strong></p>\r\n<p><em>Bột bả Skimcoat M402</em></p>\r\n<ul>\r\n<li>Bao gồm xi măng đen cường độ cao, cốt liệu tinh lọc cực mịn, polymer, phụ gia chống thấm và nhiều phụ gia đặc biệt khác</li>\r\n<li>Màu sắc: Xám</li>\r\n<li>Sử dụng trên các bề mặt lồi lõm nhiều, tường gạch yếu cần tăng cường độ cho tường, giúp bảo vệ gạch phía trong. Công dụng như một lớp trát mỏng cường độ cao.</li>\r\n<li> </li>\r\n<li> </li>\r\n<li>\r\n<p><strong>2.2 Thông số kỹ thuật</strong></p>\r\n<p><em> </em></p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td width=\"53\">\r\n<p><strong>STT</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Chỉ tiêu</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Đơn vị</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Kết quả</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>1</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ bám dính</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>N/mm<sup>2</sup></p>\r\n</td>\r\n<td width=\"132\">\r\n<p>1.0</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>2</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ linh động</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>mm</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>202</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>3</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Cường độ nén</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>N/mm<sup>2</sup></p>\r\n</td>\r\n<td width=\"132\">\r\n<p>18</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>4</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ giữ nước</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>%</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>98</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<em> </em>\r\n<p><strong> 2.3 Đóng gói</strong></p>\r\n<ul>\r\n<li>Bao 2 lớp chống ẩm, khối lượng 50kg</li>\r\n<li>Sử dụng tốt nhất trong vòng 06 tháng kế từ ngày sản xuất</li>\r\n<li>Bảo quản nơi khô ráo.</li>\r\n</ul>\r\n<p> </p>\r\n<ul>\r\n<li><strong>2.4 Ứng dụng</strong></li>\r\n<li>Sử dụng trên tất cả các loại tường gạch: Gạch đỏ nung, gạch xi măng cốt liệu, gạch bê tông bọt, gạch bê tông khí chưng áp (AAC), gạch mát, tấm thạch cao,…</li>\r\n<li>Bả cả nội thất và ngoại thất, chịu được trong các môi trường thời tiết khắc nghiệt.</li>\r\n</ul>\r\n<p><strong> </strong></p>\r\n<p><strong>2.5 Định mức</strong></p>\r\n<ul>\r\n<li>Chiều dày lớp bả: 1-4 mm (tùy chất lượng tường)</li>\r\n<li>Skimcoat lót M402: 1,4 kg/m<sup>2</sup>/mm (Thông thường chiều dày lớp Skimcoat lót từ 2-4mm)</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</div>', '2', '', 0, 0, '', '0', 1, '2021-10-22', 0),
(18, 'bot-ba-skimcoat-xam-403', '', 'BỘT BẢ SKIMCOAT XÁM 403', '<div style=\"text-align: justify;\">\r\n<p><strong>2.6 Hướng dẫn sử dụng</strong></p>\r\n<p><strong> - Chuẩn bị</strong></p>\r\n<p>Tường</p>\r\n<ul>\r\n<li>Bề mặt tường phải sạch sẽ, không có các loại dầu mỡ, tạp chất cơ học. Nếu tường xây quá lồi lõm có thể mài trước khi bả.</li>\r\n<li>Nếu là tường cũ thì phải làm sạch lớp sơn vôi cũ, diệt hết rêu nấm mốc</li>\r\n<li>Dụng cụ</li>\r\n</ul>\r\n<ul>\r\n<li>Thùng, cánh khuấy, nước sạch, bàn bả, bột bả Skimcoat Mater.</li>\r\n</ul>\r\n<p> - <strong>Thi công</strong></p>\r\n<p>Trộn vật tư</p>\r\n<ul>\r\n<li>Bột Skimcoat lót M403: Trộn theo tỉ lệ khối lượng: nước/bột bả = 18-22%</li>\r\n<li>Nên đổ bột vào nước để tránh hiện tượng vón cục. Dùng máy khuấy tay khuấy đều đến khi tạo thành hỗn hợp sệt dẻo là được. Sau khi khuấy 5 phút mới được thi công.</li>\r\n<li>Bả</li>\r\n</ul>\r\n<ul>\r\n<li>Dùng bàn bả thép bả hỗn hợp lên tường</li>\r\n<li>Tùy bề mặt tường và chiều dày lớp bả để có số lớp bả thích hợp. Nên bả tối thiểu 2 lớp, mỗi lớp cách nhau tối thiểu 2h.</li>\r\n<li>Sau khi bả, trong thời gian 2 ngày phải đánh ráp bằng loại ráp mịn để tránh trường hợp lớp bả quá cứng khó đánh ráp. Trường hợp chỉ dùng lớp bả lót M403 phải đánh ngay ráp mịn trong vòng 04 giờ.</li>\r\n</ul>\r\n</div>', '<div style=\"text-align: justify;\">\r\n<p><strong>BỘT BẢ SKIMCOAT MATER</strong></p>\r\n<p><strong>1. Ưu điểm</strong></p>\r\n<ul>\r\n<li>Tạo phẳng, lấp những vị trí lồi lõm, tạo độ nhẵn cho bề mặt tường.</li>\r\n<li>Cường độ cao, bám dính tốt, không co ngót, không rạn nứt giúp gia cố tường xây, tăng cường độ cho tường</li>\r\n<li>Tăng khả năng chống thấm</li>\r\n<li>Không cần trát và sơn lót. Giảm đáng kể chi phí xây dựng</li>\r\n<li>Thi công dễ dàng, không hao phí vật tư khi thi công</li>\r\n<li>Giảm rất nhiều thời gian thi công do bỏ được phần trát và thời gian chờ tường khô trước khi sơn.</li>\r\n</ul>\r\n<p> </p>\r\n<p><strong>2. Đặc tính kỹ thuật</strong></p>\r\n<p><strong>2.1 Thành phần:</strong></p>\r\n<p><em>Bột bả Skimcoat M403</em></p>\r\n<ul>\r\n<li>Bao gồm xi măng đen cường độ cao, cốt liệu tinh lọc cực mịn, polymer, phụ gia chống thấm và nhiều phụ gia đặc biệt khác</li>\r\n<li>Màu sắc: Xám</li>\r\n<li>Sử dụng trên các bề mặt lồi lõm nhiều, tường gạch yếu cần tăng cường độ cho tường, giúp bảo vệ gạch phía trong. Công dụng như một lớp trát mỏng cường độ cao.</li>\r\n<li> </li>\r\n<li> </li>\r\n<li>\r\n<p><strong>2.2 Thông số kỹ thuật</strong></p>\r\n<p><em> </em></p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td width=\"53\">\r\n<p><strong>STT</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Chỉ tiêu</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Đơn vị</strong></p>\r\n</td>\r\n<td width=\"132\">\r\n<p><strong>Kết quả</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>1</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ bám dính</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>N/mm<sup>2</sup></p>\r\n</td>\r\n<td width=\"132\">\r\n<p>1.0</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>2</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ linh động</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>mm</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>202</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>3</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Cường độ nén</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>N/mm<sup>2</sup></p>\r\n</td>\r\n<td width=\"132\">\r\n<p>18</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"53\">\r\n<p>4</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>Độ giữ nước</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>%</p>\r\n</td>\r\n<td width=\"132\">\r\n<p>98</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<em> </em>\r\n<p><strong> 2.3 Đóng gói</strong></p>\r\n<ul>\r\n<li>Bao 2 lớp chống ẩm, khối lượng 50kg</li>\r\n<li>Sử dụng tốt nhất trong vòng 06 tháng kế từ ngày sản xuất</li>\r\n<li>Bảo quản nơi khô ráo.</li>\r\n</ul>\r\n<p> </p>\r\n<ul>\r\n<li><strong>2.4 Ứng dụng</strong></li>\r\n<li>Sử dụng trên tất cả các loại tường gạch: Gạch đỏ nung, gạch xi măng cốt liệu, gạch bê tông bọt, gạch bê tông khí chưng áp (AAC), gạch mát, tấm thạch cao,…</li>\r\n<li>Bả cả nội thất và ngoại thất, chịu được trong các môi trường thời tiết khắc nghiệt.</li>\r\n</ul>\r\n<p><strong> </strong></p>\r\n<p><strong>2.5 Định mức</strong></p>\r\n<ul>\r\n<li>Chiều dày lớp bả: 1-4 mm (tùy chất lượng tường)</li>\r\n<li>Skimcoat lót M403: 1,4 kg/m<sup>2</sup>/mm (Thông thường chiều dày lớp Skimcoat lót từ 2-4mm)</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</div>', '2', '', 0, 4, '', '0', 1, '2021-10-22', 0),
(19, 'keo-gan-tam-alc', '', 'KEO GẮN TẤM ALC ', '<div style=\"text-align: justify;\"><strong><em>3. Tại sao nên sử dụng keo dán gạch – đá?</em></strong>\r\n<ul>\r\n<li><br />-  Keo dán gạch – đá có nhiều phụ gia polymer nên khả năng giữ nước cao, keo lâu bị “chết” hơn xi măng, độ bám dính rất tốt và dễ điều chỉnh gạch – đá khi thi công.</li>\r\n<li>Lớp keo dàn đều trên bề mặt viên gạch – đá nên loại bỏ được những khoảng trống giữa gạch – đá và cốt nền; nhờ vậy không còn hiện tượng ộp gạch – đá, bong rộp như ốp lát truyền thống.</li>\r\n<li>-  Cường độ nén của keo rất cao, độ bám dính tốt nên sau khi sử dụng gạch – đá và cốt nền thành một khối vô cùng bền vững. Có thể ốp lát gạch trong những môi trường khắc nghiệp: dưới nước, trên cao, thay đổi nhiệt độ lớn, …</li>\r\n<li>-  Tăng khả năng chống thấm cho vật liệu.</li>\r\n<li>-  Ngày nay, các loại gạch granit, đá mable, đá tự nhiên, … có độ cứng cao và hút nước rất ít, ốp lát bằng phương pháp truyền thống rất dễ bong tróc do xi măng không thể bám dính tốt trên bề mặt những loại vật liệu này. Chính vì thế, nhiều nhà máy sản xuất gạch – đá đã khuyến cáo nên sử dụng keo dán gạch – đá cho một số dòng sản phẩm của mình.</li>\r\n<li>-  Với các công trình cao trầng, độ rung lắc lớn, keo dán gạch – đá là một giải pháp bắt buộc bởi yêu cầu độ uốn cao, độ mở lớn đối với vật liệu ốp lát. Bộ Xây dựng khuyến cáo đối với các công trình cao trên 9 tầng nên sử dụng keo dán gạch – đá để ốp lát để đảm bảo tính bền vững và an toàn cho người sử dụng.</li>\r\n<li><strong>4. Bảng so sánh:</strong></li>\r\n</ul>\r\n<table width=\"607\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><strong>BẢNG SO SÁNH</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Keo dán gạch - đá MATER</strong></td>\r\n<td rowspan=\"2\"><strong>PP. Hồ xi măng truyền thống</strong></td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Bám dính cao liên kết chặt gạch, đá vào nền (vách)</td>\r\n<td>Bám dính kém</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Chịu được nước, lão hóa, thay đổi nhiệt</td>\r\n<td>Không chịu được môi trường thay đổi</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Keo dàn đều bề mặt gạch, đá không ộp</td>\r\n<td>Hay ộp, bong tróc</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Sạch sẽ, gọn gàng, tiến độ</td>\r\n<td>Bẩn, cồng kềnh, chậm</td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Chất lượng cao và đồng đều</td>\r\n<td>Chất lượng không đều và không ổn định</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '<div style=\"text-align: justify;\"><strong><em>1. Keo dán gạch - đá là gì?</em></strong><br />Keo dán gạch – đá là sản phẩm được chế tạo từ xi măng, cát khô sạch, phụ gia polymer trộn đều, dùng để ốp hoặc lát gạch – đá lên tường hoặc sàn. Keo dán gạch – đá thay thế cho xi măng, hồ xi măng trong ốp lát truyền thống.<br /><strong><em>2. Thông số kỹ thuật</em></strong>\r\n<table width=\"559\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"5\"><strong>ĐẶC TÍNH KỸ THUẬT KEO DÁN GẠCH – ĐÁ</strong><strong> M203</strong></td>\r\n</tr>\r\n<tr>\r\n<td rowspan=\"2\"><strong>STT</strong></td>\r\n<td rowspan=\"2\"><strong>Tên chỉ tiêu</strong></td>\r\n<td rowspan=\"2\"><strong>Đơn vị</strong></td>\r\n<td><strong>Kết quả</strong></td>\r\n<td rowspan=\"2\"><strong>P.P Thử</strong></td>\r\n</tr>\r\n<tr>\r\n<td><strong>K</strong><strong>M2</strong><strong>03</strong></td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>Kích thước hạt cốt liệu lớn nhất</td>\r\n<td>mm</td>\r\n<td>≤2,5</td>\r\n<td>TCVN 3121-1:2003</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Cường độ bám dính ở điều kiện chuẩn</td>\r\n<td>mm</td>\r\n<td>≥0,8</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Cường độ bám dính sau ngâm nước 72h</td>\r\n<td>mm</td>\r\n<td>≥0,5</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>4</td>\r\n<td>Cường độ bám dính sau sốc nhiệt</td>\r\n<td>mm</td>\r\n<td>≥0,5</td>\r\n<td>TCVN 7239:2014</td>\r\n</tr>\r\n<tr>\r\n<td>5</td>\r\n<td>Cường độ nén</td>\r\n<td>mm</td>\r\n<td>≥20,0</td>\r\n<td>TCVN 3121-11:2003</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '4', '', 0, 15, '', '1', 1, '2021-10-14', 0),
(20, 'gach-acc', '', 'GẠCH ACC', '<p><span id=\"khoi-luong-gach\" style=\"font-size: 10pt;\"><strong>Khối lượng gạch</strong></span></p>\r\n<p><span style=\"font-size: 10pt;\">Gạch siêu nhẹ AAC có khối lượng từ 600 – 850kg.m3. Tương đương với ½ gạch đặc, 1/3 gạch bê tông thông thường.</span></p>\r\n<p><span style=\"font-size: 10pt;\">Với thông số khối lượng nhẹ nên sản phẩm giúp:</span></p>\r\n<ul>\r\n<li>\r\n<p><span style=\"font-size: 10pt;\">Giảm tải trọng, kết cấu phù hợp với những khu vực có nền đất yếu.</span></p>\r\n</li>\r\n<li>\r\n<p><span style=\"font-size: 10pt;\">Giúp quá trình sửa chữa nhà cửa nhanh chóng.</span></p>\r\n</li>\r\n<li>\r\n<p><span style=\"font-size: 10pt;\">Chi phí xây thô đạt từ 10 đến 15%.</span></p>\r\n</li>\r\n<li>\r\n<p><span style=\"font-size: 10pt;\">Gạch AAC giúp tiêu hao nhân công, đẩy nhanh tiến độ xây dựng cho công trình.</span></p>\r\n</li>\r\n</ul>\r\n<p><span id=\"tinh-cach-nhiet-cao\" style=\"font-size: 10pt;\"><strong>Tính cách nhiệt cao</strong></span></p>\r\n<p><span style=\"font-size: 10pt;\">Gạch AAC có hiệu quả sử dụng dài lâu, mát về mùa hè, ấm về mùa đông.</span></p>\r\n<p><span style=\"font-size: 10pt;\">Sản phẩm này với chiều dày khoảng 200mm, tương đương với hiệu quả bảo ôn gạch đất nung dày 500mm.</span></p>\r\n<p><span style=\"font-size: 10pt;\">Sử dụng gạch siêu nhẹ sẽ giảm đến 40% chi phí năng lượng tiêu thụ điều hòa.</span></p>\r\n<p><span style=\"font-size: 10pt;\"><strong>Thông số gạch AAC</strong> về hệ số dẫn nhiệt chỉ 0.10 – 0.25 w/mok, bằng ¼ tới 1/5 hệ số dẫn nhiệt của gạch nung và chỉ tương đương 1/3 hệ số dẫn nhiệt gạch bê tông thường.</span></p>', '<table style=\"height: 314px;\" border=\"1\" width=\"793\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td rowspan=\"2\">\r\n<p><strong>Stt</strong></p>\r\n</td>\r\n<td rowspan=\"2\">\r\n<p><strong>Chỉ tiêu</strong></p>\r\n</td>\r\n<td colspan=\"2\">\r\n<p><strong>Thông số Kỹ thuật</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p><strong>Cấp cường độ </strong><strong>4~5MPA</strong></p>\r\n</td>\r\n<td>\r\n<p><strong>Cấp cường độ </strong><strong>6~7,5MPA</strong></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>1</p>\r\n</td>\r\n<td>\r\n<p>Khối lượng thể tích</p>\r\n</td>\r\n<td>\r\n<p>600 -&gt; 750 Kg/m<sup>3</sup></p>\r\n</td>\r\n<td>\r\n<p>800 -&gt; 850 Kg/m<sup>3</sup></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>2</p>\r\n</td>\r\n<td>\r\n<p>Hệ số dẫn nhiệt</p>\r\n</td>\r\n<td colspan=\"2\">\r\n<p>0,11 đến 0,22 W/m.<sup>0</sup>k</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>3</p>\r\n</td>\r\n<td>\r\n<p>Cường độ nén</p>\r\n</td>\r\n<td>\r\n<p>4 đến 5Mpa</p>\r\n</td>\r\n<td>\r\n<p>6 đến 7,5Mpa</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>4</p>\r\n</td>\r\n<td>\r\n<p>Dung sai kích thước</p>\r\n</td>\r\n<td colspan=\"2\">\r\n<p>+3 và -3 mm chiều dài</p>\r\n<p>+3 và -3 mm chiều rộng</p>\r\n<p>+3 và -3 mm chiều cao</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>5</p>\r\n</td>\r\n<td>\r\n<p>Độ co ngót</p>\r\n</td>\r\n<td colspan=\"2\">\r\n<p>&lt; 0,02 mm/m</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<br />\r\n<div class=\"product-description-content\">\r\n<div class=\"sub-content\">\r\n<p style=\"text-align: justify;\"><br /><br /></p>\r\n</div>\r\n</div>', '3', '', 0, 15, '', '0', 1, '2021-10-22', 0),
(21, 'nguyen-van-hung', '', 'Nguyễn Văn Hùng', '', '', '3', '', 0, 0, '', '1', 0, '2021-10-21', 0),
(22, 'nguyen-van-hung', '', 'Nguyễn Văn Hùng', '', '', '2', '', 0, 0, '', '1', 0, '2021-10-21', 0),
(23, 'nguyen-van-hung', '', 'Nguyễn Văn Hùng', '', '', '2', '', 0, 0, '', '1', 0, '2021-10-21', 0),
(24, 'nguyen-van-hung', '', 'Nguyễn Văn Hùng', '', '', '2', '', 0, 0, '', '1', 0, '2021-10-21', 0),
(25, 'phuong-phap-giang-day-co-1-0-2-tai-igems', '', 'PHƯƠNG PHÁP GIẢNG DẠY CÓ 1-0-2 TẠI IGEMS', '', '', '4', '', 0, 0, '', '1', 0, '2021-10-21', 0),
(26, 'phuong-phap-giang-day-co-1-0-2-tai-igems', '', 'PHƯƠNG PHÁP GIẢNG DẠY CÓ 1-0-2 TẠI IGEMS', '', '', '2', 'hoc nữa,học mãi', 0, 0, '', '', 1, '2021-11-03', 0),
(27, 'nguyen-van-hung', '', 'Nguyễn Văn Hùng', '', '', '2', '', 0, 0, '', '0', 0, '2021-10-22', 0),
(28, 'san-pham-demo', '', 'Sản phẩm demo ', '', '', '2', 'sản phẩm demo', 0, 0, '', '1', 1, '2021-11-03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE `productcategory` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(160) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `parentid` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`id`, `url`, `name`, `description`, `image`, `parentid`, `position`, `status`, `sort_order`) VALUES
(2, 'bao-cao-thuc-tap', 'Báo Cáo Thực Tập', 'Tổng hợp báo cáo thực tập điện cơ khí, đồ án, doanh nghiệp và công ty, kế toán, marketing', '', 0, 1, 1, 1),
(3, 'giao-duc-dao-tao', 'Giáo Dục - Đào Tạo', 'Tổng hợp 364,409 giáo án, bài giảng, tài liệu tham khảo của tất cả các cấp học.', '', 0, 3, 1, 2),
(4, 'ngoai-ngu', 'Ngoại Ngữ', 'Tổng hợp 142,649 tài liệu tiếng anh chất lượng cao.', '', 0, 3, 1, 3),
(5, 'ky-nang-mem', 'Kỹ Năng Mềm', 'Tổng hợp 45,320 tài liệu chuyên sâu đa dạng về lĩnh vực Kỹ Năng Mềm', '', 0, 1, 1, 4),
(6, 'mau-slide', 'Mẫu Slide', 'Tổng hợp 17,141 tài liệu chuyên sâu đa dạng về lĩnh vực Mẫu Slide', '', 0, 1, 1, 5),
(7, 'nguyen-van-hung', 'Nguyễn Văn Hùng', '', '', 0, 1, 0, 0),
(8, 'ky-thuat-cong-nghe', 'Kỹ Thuật - Công Nghệ', '', '', 0, 2, 1, 6),
(9, 'cong-nghe-thong-tin', 'Công Nghệ Thông Tin', '', '', 0, 2, 1, 7),
(10, 'tai-lieu', 'Tài liệu', '', '', 0, 0, 1, 0),
(11, 'quan-tri-kinh-doanh', 'Quản trị kinh doanh', '', '', 0, 0, 1, 0),
(12, 'khoa-hoc-tu-nhien', 'Khoa học tự nhiên', '', '', 0, 0, 1, 0),
(13, 'khoa-luan-tot-nghiep', 'Khóa luận tốt nghiệp', '', '', 0, 0, 1, 0),
(14, 'van-ban-luat', 'Văn bản luật', '', '', 0, 0, 1, 0),
(15, 'tai-chinh-ngan-hang', 'Tài chính - Ngân hàng', '', '', 10, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `productpicture`
--

CREATE TABLE `productpicture` (
  `id` int(15) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product` int(10) NOT NULL,
  `sort_order` int(2) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `productpicture`
--

INSERT INTO `productpicture` (`id`, `link`, `product`, `sort_order`, `status`) VALUES
(7, 'http://vanda.thuonghieuweb.com/uploads/sanpham/16247926462239239.jpg.jpg', 1, 1, 1),
(8, 'http://vanda.thuonghieuweb.com/uploads/sanpham/16247926462429909.jpg.jpg', 1, 2, 1),
(9, 'http://vanda.thuonghieuweb.com/uploads/sanpham/16247931195089474.jpg.jpg', 2, 1, 1),
(10, 'http://vanda.thuonghieuweb.com/uploads/sanpham/16247931195339409.jpg.jpg', 2, 2, 1),
(62, 'http://vandavn.com/uploads/sanpham/16255620057499115.PNG.png', 21, 2, 1),
(63, 'http://vandavn.com/uploads/sanpham/16255620059459206.PNG.png', 21, 1, 1),
(65, 'http://vandavn.com/uploads/sanpham/16255623963619002.PNG.png', 22, 1, 1),
(66, 'http://vandavn.com/uploads/sanpham/16255625091239411.PNG.png', 22, 8, 1),
(67, 'http://vandavn.com/uploads/sanpham/16255625091719021.PNG.png', 22, 9, 1),
(69, 'http://vandavn.com/uploads/sanpham/16255632730909872.PNG.png', 23, 2, 1),
(70, 'http://vandavn.com/uploads/sanpham/16255632732149591.PNG.png', 23, 3, 1),
(71, 'http://vandavn.com/uploads/sanpham/16255632733259231.PNG.png', 23, 1, 1),
(72, 'http://vandavn.com/uploads/sanpham/16255656700859110.PNG.png', 24, 1, 1),
(73, 'http://vandavn.com/uploads/sanpham/16255659789459166.PNG.png', 25, 1, 1),
(74, 'http://vandavn.com/uploads/sanpham/16256302252619378.jpg.jpg', 21, 3, 1),
(75, 'http://vandavn.com/uploads/sanpham/16256302311179848.jpg.jpg', 21, 4, 1),
(89, 'http://vandavn.com/uploads/sanpham/16256404313979557.jpg.jpg', 22, 2, 1),
(91, 'http://vandavn.com/uploads/sanpham/16256404315389575.jpg.jpg', 22, 3, 1),
(92, 'http://vandavn.com/uploads/sanpham/16256404316259573.jpg.jpg', 22, 4, 1),
(95, 'http://vandavn.com/uploads/sanpham/16256404318249276.jpg.jpg', 22, 5, 1),
(97, 'http://vandavn.com/uploads/sanpham/16256404319479990.jpg.jpg', 22, 7, 1),
(98, 'http://vandavn.com/uploads/sanpham/16256404320379728.jpg.jpg', 22, 6, 1),
(143, 'http://vandavn.com/uploads/sanpham/16256436583449805.jpg.jpg', 17338, 2, 0),
(144, 'http://vandavn.com/uploads/sanpham/16256436584149851.jpg.jpg', 17338, 3, 0),
(145, 'http://vandavn.com/uploads/sanpham/16256436584819487.jpg.jpg', 17338, 4, 0),
(146, 'http://vandavn.com/uploads/sanpham/16256436585649194.jpg.jpg', 17338, 5, 0),
(147, 'http://vandavn.com/uploads/sanpham/16256436581599633.jpg.jpg', 17338, 1, 0),
(148, 'http://vandavn.com/uploads/sanpham/16256436586069304.jpg.jpg', 17338, 6, 0),
(151, 'http://vandavn.com/uploads/sanpham/16256436588949578.jpg.jpg', 17338, 7, 0),
(152, 'http://vandavn.com/uploads/sanpham/16256436589449968.jpg.jpg', 17338, 8, 0),
(153, 'http://vandavn.com/uploads/sanpham/16256436590759598.jpg.jpg', 17338, 9, 0),
(242, 'http://vandavn.com/uploads/sanpham/16257299634159411.png.png', 34, 1, 1),
(243, 'http://vandavn.com/uploads/sanpham/16257309664979477.png.png', 35, 1, 1),
(287, 'http://vandavn.com/uploads/sanpham/16258251989059064.jpg.jpg', 27, 6, 1),
(289, 'http://vandavn.com/uploads/sanpham/16258251990029154.jpg.jpg', 27, 7, 1),
(312, 'http://vandavn.com/uploads/sanpham/16258887102089976.jpg.jpg', 30, 2, 1),
(318, 'http://vandavn.com/uploads/sanpham/16258887106369861.jpg.jpg', 30, 5, 1),
(330, 'http://vandavn.com/uploads/sanpham/16258888351869189.jpg.jpg', 31, 6, 1),
(332, 'http://vandavn.com/uploads/sanpham/16258888352599221.jpg.jpg', 31, 7, 1),
(333, 'http://vandavn.com/uploads/sanpham/16258888352949348.jpg.jpg', 31, 8, 1),
(334, 'http://vandavn.com/uploads/sanpham/16258888353529966.jpg.jpg', 31, 9, 1),
(335, 'http://vandavn.com/uploads/sanpham/16258888354079326.jpg.jpg', 31, 10, 1),
(336, 'http://vandavn.com/uploads/sanpham/16258888354539092.jpg.jpg', 31, 11, 1),
(337, 'http://vandavn.com/uploads/sanpham/16258888354979396.jpg.jpg', 31, 12, 1),
(338, 'http://vandavn.com/uploads/sanpham/16258888355399663.jpg.jpg', 31, 13, 1),
(339, 'http://vandavn.com/uploads/sanpham/16258888355809183.jpg.jpg', 31, 14, 1),
(340, 'http://vandavn.com/uploads/sanpham/16258888356569169.jpg.jpg', 31, 15, 1),
(341, 'http://vandavn.com/uploads/sanpham/16258888356989176.jpg.jpg', 31, 16, 1),
(342, 'http://vandavn.com/uploads/sanpham/16258888357389214.jpg.jpg', 31, 17, 1),
(343, 'http://vandavn.com/uploads/sanpham/16258888357809587.jpg.jpg', 31, 18, 1),
(344, 'http://vandavn.com/uploads/sanpham/16258888358269699.jpg.jpg', 31, 19, 1),
(345, 'http://vandavn.com/uploads/sanpham/16258888358779091.jpg.jpg', 31, 20, 1),
(346, 'http://vandavn.com/uploads/sanpham/16258888359179106.jpg.jpg', 31, 21, 1),
(347, 'http://vandavn.com/uploads/sanpham/16258888359579140.jpg.jpg', 31, 22, 1),
(348, 'http://vandavn.com/uploads/sanpham/16258888360059212.jpg.jpg', 31, 23, 1),
(349, 'http://vandavn.com/uploads/sanpham/16258888360539997.jpg.jpg', 31, 24, 1),
(350, 'http://vandavn.com/uploads/sanpham/16258888361039693.jpg.jpg', 31, 25, 1),
(352, 'http://vandavn.com/uploads/sanpham/16258889885219804.jpg.jpg', 32, 2, 1),
(353, 'http://vandavn.com/uploads/sanpham/16258889885629284.jpg.jpg', 32, 3, 1),
(354, 'http://vandavn.com/uploads/sanpham/16258889886259695.jpg.jpg', 32, 4, 1),
(355, 'http://vandavn.com/uploads/sanpham/16258889886729844.jpg.jpg', 32, 5, 1),
(356, 'http://vandavn.com/uploads/sanpham/16258889887219429.jpg.jpg', 32, 6, 1),
(357, 'http://vandavn.com/uploads/sanpham/16258889887719110.jpg.jpg', 32, 7, 1),
(358, 'http://vandavn.com/uploads/sanpham/16258889888139170.jpg.jpg', 32, 8, 1),
(359, 'http://vandavn.com/uploads/sanpham/16258889888549723.jpg.jpg', 32, 9, 1),
(360, 'http://vandavn.com/uploads/sanpham/16258889889139461.jpg.jpg', 32, 10, 1),
(361, 'http://vandavn.com/uploads/sanpham/16258889889579600.jpg.jpg', 32, 11, 1),
(362, 'http://vandavn.com/uploads/sanpham/16258889889989185.jpg.jpg', 32, 12, 1),
(363, 'http://vandavn.com/uploads/sanpham/16258889890509417.jpg.jpg', 32, 13, 1),
(364, 'http://vandavn.com/uploads/sanpham/16258889890919641.jpg.jpg', 32, 14, 1),
(365, 'http://vandavn.com/uploads/sanpham/16258889891389245.jpg.jpg', 32, 15, 1),
(366, 'http://vandavn.com/uploads/sanpham/16258889891869176.jpg.jpg', 32, 16, 1),
(367, 'http://vandavn.com/uploads/sanpham/16258889892359980.jpg.jpg', 32, 17, 1),
(377, 'http://vandavn.com/uploads/sanpham/16258889900229367.jpg.jpg', 32, 18, 1),
(379, 'http://vandavn.com/uploads/sanpham/16258889901499291.jpg.jpg', 32, 19, 1),
(382, 'http://vandavn.com/uploads/sanpham/16258889903449801.jpg.jpg', 32, 20, 1),
(411, 'http://vandavn.com/uploads/sanpham/16258895496649998.jpg.jpg', 26, 21, 1),
(414, 'http://vandavn.com/uploads/sanpham/16258896276159493.jpg.jpg', 33, 3, 1),
(415, 'http://vandavn.com/uploads/sanpham/16258896275509681.jpg.jpg', 33, 2, 1),
(416, 'http://vandavn.com/uploads/sanpham/16258896277139369.jpg.jpg', 33, 4, 1),
(417, 'http://vandavn.com/uploads/sanpham/16258896277719130.jpg.jpg', 33, 1, 1),
(419, 'http://vandavn.com/uploads/sanpham/16258896279499226.jpg.jpg', 33, 5, 1),
(420, 'http://vandavn.com/uploads/sanpham/16258896280319429.jpg.jpg', 33, 6, 1),
(421, 'http://vandavn.com/uploads/sanpham/16258896281029643.jpg.jpg', 33, 7, 1),
(422, 'http://vandavn.com/uploads/sanpham/16258896281849517.jpg.jpg', 33, 8, 1),
(423, 'http://vandavn.com/uploads/sanpham/16258896282589703.jpg.jpg', 33, 9, 1),
(425, 'http://vandavn.com/uploads/sanpham/16258896284839198.jpg.jpg', 33, 10, 1),
(427, 'http://vandavn.com/uploads/sanpham/16258896286169447.jpg.jpg', 33, 11, 1),
(428, 'http://vandavn.com/uploads/sanpham/16258896287029378.jpg.jpg', 33, 12, 1),
(432, 'http://vandavn.com/uploads/sanpham/16258896288969426.jpg.jpg', 33, 13, 1),
(439, 'http://vandavn.com/uploads/sanpham/16263210215729523.jpg.jpg', 27, 1, 1),
(440, 'http://vandavn.com/uploads/sanpham/16263210217409957.jpg.jpg', 27, 2, 1),
(441, 'http://vandavn.com/uploads/sanpham/16263210218579979.jpg.jpg', 27, 4, 1),
(442, 'http://vandavn.com/uploads/sanpham/16263210218119479.jpg.jpg', 27, 3, 1),
(443, 'http://vandavn.com/uploads/sanpham/16263210220569273.jpg.jpg', 27, 5, 1),
(452, 'http://vandavn.com/uploads/sanpham/16263214755239284.jpg.jpg', 30, 1, 1),
(453, 'http://vandavn.com/uploads/sanpham/16263214755689382.jpg.jpg', 30, 3, 1),
(454, 'http://vandavn.com/uploads/sanpham/16263214756279942.jpg.jpg', 30, 4, 1),
(455, 'http://vandavn.com/uploads/sanpham/16263215595519155.jpg.jpg', 32, 1, 1),
(456, 'http://vandavn.com/uploads/sanpham/16263215885899039.jpg.jpg', 31, 2, 1),
(457, 'http://vandavn.com/uploads/sanpham/16263215886469754.jpg.jpg', 31, 3, 1),
(458, 'http://vandavn.com/uploads/sanpham/16263215887139892.jpg.jpg', 31, 4, 1),
(459, 'http://vandavn.com/uploads/sanpham/16263215887509561.jpg.jpg', 31, 5, 1),
(460, 'http://vandavn.com/uploads/sanpham/16263215934789846.jpg.jpg', 31, 1, 1),
(461, 'http://vandavn.com/uploads/sanpham/16264931696889339.jpg.jpg', 36, 1, 1),
(462, 'http://vandavn.com/uploads/sanpham/16264931697619130.jpg.jpg', 36, 2, 1),
(463, 'http://vandavn.com/uploads/sanpham/16264932901549672.jpg.jpg', 37, 1, 1),
(464, 'http://vandavn.com/uploads/sanpham/16264934042529643.jpg.jpg', 38, 1, 1),
(465, 'http://vandavn.com/uploads/sanpham/16264935282309383.jpg.jpg', 39, 1, 1),
(466, 'http://vandavn.com/uploads/sanpham/16264936776669378.jpg.jpg', 39, 2, 1),
(467, 'http://vandavn.com/uploads/sanpham/16264938040159049.jpg.jpg', 39, 3, 1),
(468, 'http://vandavn.com/uploads/sanpham/16264939220199433.jpg.jpg', 40, 1, 1),
(470, 'http://vandavn.com/uploads/sanpham/16264940599479016.jpg.jpg', 40, 2, 1),
(471, 'http://vandavn.com/uploads/sanpham/16264941646859674.jpg.jpg', 41, 1, 1),
(472, 'http://vandavn.com/uploads/sanpham/16264942998979835.jpg.jpg', 42, 1, 1),
(473, 'http://vandavn.com/uploads/sanpham/16264945986679242.jpg.jpg', 43, 3, 1),
(474, 'http://vandavn.com/uploads/sanpham/16264945987929699.jpg.jpg', 43, 2, 1),
(475, 'http://vandavn.com/uploads/sanpham/16264945988429838.jpg.jpg', 43, 1, 1),
(476, 'http://vandavn.com/uploads/sanpham/16264948717799553.jpg.jpg', 44, 1, 1),
(477, 'http://vandavn.com/uploads/sanpham/16264948718809109.jpg.jpg', 44, 2, 1),
(478, 'http://vandavn.com/uploads/sanpham/16269220847579069.PNG.png', 45, 1, 1),
(479, 'http://vandavn.com/uploads/sanpham/16269451767869412.jpg.jpg', 46, 1, 1),
(480, 'http://vandavn.com/uploads/sanpham/16269455617309728.jpg.jpg', 47, 1, 1),
(481, 'http://vandavn.com/uploads/sanpham/16269456415089741.jpg.jpg', 48, 1, 1),
(483, 'http://vandavn.com/uploads/sanpham/16269457966909879.jpg.jpg', 50, 1, 1),
(484, 'http://vandavn.com/uploads/sanpham/16269458702849948.jpg.jpg', 51, 1, 1),
(485, 'http://vandavn.com/uploads/sanpham/16269459642339697.jpg.jpg', 52, 1, 1),
(486, 'http://vandavn.com/uploads/sanpham/16269462772649971.jpg.jpg', 53, 1, 1),
(487, 'http://vandavn.com/uploads/sanpham/16269463509429874.jpg.jpg', 54, 1, 1),
(488, 'http://vandavn.com/uploads/sanpham/16269463926439066.jpg.jpg', 55, 1, 1),
(489, 'http://vandavn.com/uploads/sanpham/16269465171429322.jpg.jpg', 56, 1, 1),
(490, 'http://vandavn.com/uploads/sanpham/16269466087029221.jpg.jpg', 57, 1, 1),
(491, 'http://vandavn.com/uploads/sanpham/16269467770929056.jpg.jpg', 58, 1, 1),
(492, 'http://vandavn.com/uploads/sanpham/16269468564309794.jpg.jpg', 59, 1, 1),
(493, 'http://vandavn.com/uploads/sanpham/16269468963869547.jpg.jpg', 60, 1, 1),
(494, 'http://vandavn.com/uploads/sanpham/16269469350829400.jpg.jpg', 61, 1, 1),
(495, 'http://vandavn.com/uploads/sanpham/16269470269169165.jpg.jpg', 62, 1, 1),
(496, 'http://vandavn.com/uploads/sanpham/16269470631879759.jpg.jpg', 63, 1, 1),
(497, 'http://vandavn.com/uploads/sanpham/16269471026769826.jpg.jpg', 64, 1, 1),
(498, 'http://vandavn.com/uploads/sanpham/16269471033709475.jpg.jpg', 65, 1, 1),
(499, 'http://vandavn.com/uploads/sanpham/16269471599689508.jpg.jpg', 66, 1, 1),
(500, 'http://vandavn.com/uploads/sanpham/16269472179139050.jpg.jpg', 70, 1, 1),
(501, 'http://vandavn.com/uploads/sanpham/16269472334229529.jpg.jpg', 67, 1, 1),
(502, 'http://vandavn.com/uploads/sanpham/16269472920329990.jpg.jpg', 68, 1, 1),
(503, 'http://vandavn.com/uploads/sanpham/16269473408709735.jpg.jpg', 69, 1, 1),
(504, 'http://vandavn.com/uploads/sanpham/16269473978259329.jpg.jpg', 71, 1, 1),
(505, 'http://vandavn.com/uploads/sanpham/16269474504059954.jpg.jpg', 72, 1, 1),
(506, 'http://vandavn.com/uploads/sanpham/16269474583889817.jpg.jpg', 73, 1, 1),
(507, 'http://vandavn.com/uploads/sanpham/16269478168379633.jpg.jpg', 74, 1, 1),
(508, 'http://vandavn.com/uploads/sanpham/16269479975929036.jpg.jpg', 75, 1, 1),
(509, 'http://vandavn.com/uploads/sanpham/16269482400929652.jpg.jpg', 27595, 1, 0),
(510, 'http://vandavn.com/uploads/sanpham/16269483318059522.jpg.jpg', 77, 1, 1),
(511, 'http://vandavn.com/uploads/sanpham/16269494702689441.jpg.jpg', 78, 1, 1),
(512, 'http://vandavn.com/uploads/sanpham/16269495251759100.jpg.jpg', 79, 1, 1),
(513, 'http://vandavn.com/uploads/sanpham/16270219538769574.jpg.jpg', 80, 1, 1),
(514, 'http://vandavn.com/uploads/sanpham/16270219540239928.jpg.jpg', 80, 2, 1),
(515, 'http://vandavn.com/uploads/sanpham/16270219945179234.jpg.jpg', 81, 1, 1),
(516, 'http://vandavn.com/uploads/sanpham/16270219945869957.jpg.jpg', 81, 2, 1),
(517, 'http://vandavn.com/uploads/sanpham/16270220473749468.jpg.jpg', 82, 1, 1),
(518, 'http://vandavn.com/uploads/sanpham/16270220474859668.jpg.jpg', 82, 2, 1),
(519, 'http://vandavn.com/uploads/sanpham/16270220891319497.jpg.jpg', 83, 1, 1),
(520, 'http://vandavn.com/uploads/sanpham/16270220891149323.jpg.jpg', 83, 2, 1),
(521, 'http://vandavn.com/uploads/sanpham/16270221294219397.jpg.jpg', 84, 1, 1),
(522, 'http://vandavn.com/uploads/sanpham/16270221294479018.jpg.jpg', 84, 2, 1),
(523, 'http://vandavn.com/uploads/sanpham/16270224365429732.jpg.jpg', 85, 1, 1),
(524, 'http://vandavn.com/uploads/sanpham/16270224762009705.jpg.jpg', 86, 1, 1),
(525, 'http://vandavn.com/uploads/sanpham/16270224763309716.jpg.jpg', 86, 2, 1),
(526, 'http://vandavn.com/uploads/sanpham/16270225221349951.jpg.jpg', 87, 1, 1),
(527, 'http://vandavn.com/uploads/sanpham/16270225221929879.jpg.jpg', 87, 2, 1),
(528, 'http://vandavn.com/uploads/sanpham/16270225892839443.jpg.jpg', 88, 1, 1),
(529, 'http://vandavn.com/uploads/sanpham/16270225892659242.jpg.jpg', 88, 2, 1),
(530, 'http://vandavn.com/uploads/sanpham/16270226323139280.jpg.jpg', 89, 1, 1),
(531, 'http://vandavn.com/uploads/sanpham/16270226324269817.jpg.jpg', 89, 2, 1),
(532, 'http://vandavn.com/uploads/sanpham/16270226753279082.jpg.jpg', 90, 1, 1),
(533, 'http://vandavn.com/uploads/sanpham/16270226753539186.jpg.jpg', 90, 2, 1),
(534, 'http://vandavn.com/uploads/sanpham/16270227139189590.jpg.jpg', 91, 1, 1),
(535, 'http://vandavn.com/uploads/sanpham/16270227139039283.jpg.jpg', 91, 2, 1),
(536, 'http://vandavn.com/uploads/sanpham/16270227481209402.jpg.jpg', 92, 1, 1),
(537, 'http://vandavn.com/uploads/sanpham/16270227481489700.jpg.jpg', 92, 2, 1),
(538, 'http://vandavn.com/uploads/sanpham/16270227865699552.jpg.jpg', 93, 1, 1),
(539, 'http://vandavn.com/uploads/sanpham/16270228331709415.jpg.jpg', 94, 1, 1),
(540, 'http://vandavn.com/uploads/sanpham/16270228682059240.jpg.jpg', 95, 1, 1),
(541, 'http://vandavn.com/uploads/sanpham/16270229181679165.jpg.jpg', 96, 1, 1),
(542, 'http://vandavn.com/uploads/sanpham/16270229575459568.jpg.jpg', 97, 1, 1),
(543, 'http://vandavn.com/uploads/sanpham/16270229926729275.jpg.jpg', 98, 1, 1),
(544, 'http://vandavn.com/uploads/sanpham/16270230240059561.jpg.jpg', 99, 1, 1),
(545, 'http://vandavn.com/uploads/sanpham/16270230570429029.jpg.jpg', 100, 1, 1),
(546, 'http://vandavn.com/uploads/sanpham/16270230880999752.jpg.jpg', 101, 1, 1),
(547, 'http://vandavn.com/uploads/sanpham/16270231258119120.jpg.jpg', 102, 1, 1),
(548, 'http://vandavn.com/uploads/sanpham/16270231721559882.jpg.jpg', 103, 1, 1),
(549, 'http://vandavn.com/uploads/sanpham/16270231721429898.jpg.jpg', 103, 2, 1),
(550, 'http://vandavn.com/uploads/sanpham/16270232206569350.jpg.jpg', 104, 1, 1),
(551, 'http://vandavn.com/uploads/sanpham/16270232206849521.jpg.jpg', 104, 2, 1),
(552, 'http://vandavn.com/uploads/sanpham/16270232555649715.jpg.jpg', 105, 1, 1),
(553, 'http://vandavn.com/uploads/sanpham/16270232556649590.jpg.jpg', 105, 2, 1),
(554, 'http://vandavn.com/uploads/sanpham/16270232867189090.jpg.jpg', 106, 1, 1),
(555, 'http://vandavn.com/uploads/sanpham/16270233219659871.jpg.jpg', 107, 1, 1),
(556, 'http://vandavn.com/uploads/sanpham/16270233220039650.jpg.jpg', 107, 2, 1),
(557, 'http://vandavn.com/uploads/sanpham/16270241745089005.jpg.jpg', 108, 1, 1),
(558, 'http://vandavn.com/uploads/sanpham/16270241746029294.jpg.jpg', 108, 2, 1),
(559, 'http://vandavn.com/uploads/sanpham/16270242076779999.jpg.jpg', 109, 1, 1),
(560, 'http://vandavn.com/uploads/sanpham/16270242525329367.jpg.jpg', 110, 1, 1),
(561, 'http://vandavn.com/uploads/sanpham/16270242967209392.jpg.jpg', 111, 1, 1),
(562, 'http://vandavn.com/uploads/sanpham/16270243342489270.jpg.jpg', 112, 2, 1),
(563, 'http://vandavn.com/uploads/sanpham/16270243342629651.jpg.jpg', 112, 1, 1),
(564, 'http://vandavn.com/uploads/sanpham/16270243805159287.jpg.jpg', 113, 1, 1),
(565, 'http://vandavn.com/uploads/sanpham/16270243805449766.jpg.jpg', 113, 2, 1),
(566, 'http://vandavn.com/uploads/sanpham/16270250295639695.jpg.jpg', 114, 2, 1),
(567, 'http://vandavn.com/uploads/sanpham/16270250295019531.jpg.jpg', 114, 1, 1),
(568, 'http://vandavn.com/uploads/sanpham/16270250855859223.jpg.jpg', 115, 1, 1),
(569, 'http://vandavn.com/uploads/sanpham/16270250856059192.jpg.jpg', 115, 2, 1),
(570, 'http://vandavn.com/uploads/sanpham/16270252914299434.jpg.jpg', 116, 1, 1),
(571, 'http://vandavn.com/uploads/sanpham/16270253210019837.jpg.jpg', 117, 1, 1),
(572, 'http://vandavn.com/uploads/sanpham/16270253563269589.jpg.jpg', 118, 1, 1),
(573, 'http://vandavn.com/uploads/sanpham/16270253961259577.jpg.jpg', 119, 1, 1),
(574, 'http://vandavn.com/uploads/sanpham/16270254295089985.jpg.jpg', 120, 1, 1),
(575, 'http://vandavn.com/uploads/sanpham/16270254581149497.jpg.jpg', 121, 1, 1),
(576, 'http://vandavn.com/uploads/sanpham/16270254862929237.jpg.jpg', 122, 1, 1),
(577, 'http://vandavn.com/uploads/sanpham/16270255201569385.jpg.jpg', 123, 1, 1),
(578, 'http://vandavn.com/uploads/sanpham/16270255790209076.jpg.jpg', 124, 1, 1),
(579, 'http://vandavn.com/uploads/sanpham/16270259890919738.jpg.jpg', 125, 1, 1),
(580, 'http://vandavn.com/uploads/sanpham/16270260252479211.jpg.jpg', 126, 1, 1),
(581, 'http://vandavn.com/uploads/sanpham/16270261176749986.jpg.jpg', 127, 1, 1),
(582, 'http://vandavn.com/uploads/sanpham/16270261177559736.jpg.jpg', 127, 4, 1),
(583, 'http://vandavn.com/uploads/sanpham/16270261178229295.jpg.jpg', 127, 3, 1),
(584, 'http://vandavn.com/uploads/sanpham/16270261180529644.jpg.jpg', 127, 2, 1),
(585, 'http://vandavn.com/uploads/sanpham/16270261586419787.jpg.jpg', 128, 1, 1),
(586, 'http://vandavn.com/uploads/sanpham/16270261910289618.jpg.jpg', 129, 1, 1),
(587, 'http://vandavn.com/uploads/sanpham/16270262194659556.jpg.jpg', 131, 1, 1),
(588, 'http://vandavn.com/uploads/sanpham/16270262268649234.jpg.jpg', 130, 1, 1),
(589, 'http://vandavn.com/uploads/sanpham/16270262597489406.jpg.jpg', 132, 1, 1),
(590, 'http://vandavn.com/uploads/sanpham/16270262800789918.jpg.jpg', 133, 1, 1),
(591, 'http://vandavn.com/uploads/sanpham/16270262886379098.jpg.jpg', 134, 1, 1),
(592, 'http://vandavn.com/uploads/sanpham/16270263212199611.jpg.jpg', 135, 1, 1),
(593, 'http://vandavn.com/uploads/sanpham/16270263234369859.jpg.jpg', 136, 1, 1),
(594, 'http://vandavn.com/uploads/sanpham/16270263589499473.jpg.jpg', 137, 1, 1),
(595, 'http://vandavn.com/uploads/sanpham/16270263621609284.jpg.jpg', 138, 1, 1),
(596, 'http://vandavn.com/uploads/sanpham/16270263894609609.jpg.jpg', 139, 1, 1),
(597, 'http://vandavn.com/uploads/sanpham/16270264072889558.jpg.jpg', 140, 1, 1),
(598, 'http://vandavn.com/uploads/sanpham/16270264437229457.jpg.jpg', 142, 1, 1),
(599, 'http://vandavn.com/uploads/sanpham/16270264516709420.jpg.jpg', 141, 1, 1),
(600, 'http://vandavn.com/uploads/sanpham/16270265233509926.jpg.jpg', 143, 1, 1),
(601, 'http://vandavn.com/uploads/sanpham/16270266166799244.jpg.jpg', 144, 1, 1),
(602, 'http://vandavn.com/uploads/sanpham/16270267532409751.jpg.jpg', 145, 1, 1),
(603, 'http://vandavn.com/uploads/sanpham/16270268784209658.jpg.jpg', 146, 1, 1),
(604, 'http://vandavn.com/uploads/sanpham/16270271866569212.jpg.jpg', 147, 1, 1),
(605, 'http://vandavn.com/uploads/sanpham/16270272665799166.jpg.jpg', 148, 1, 1),
(606, 'http://vandavn.com/uploads/sanpham/16270273192909314.jpg.jpg', 149, 1, 1),
(607, 'http://vandavn.com/uploads/sanpham/16270274102459504.jpg.jpg', 150, 1, 1),
(608, 'http://vandavn.com/uploads/sanpham/16270274436939913.jpg.jpg', 151, 1, 1),
(609, 'http://vandavn.com/uploads/sanpham/16270274804899929.jpg.jpg', 152, 1, 1),
(610, 'http://vandavn.com/uploads/sanpham/16270275129339990.jpg.jpg', 153, 1, 1),
(611, 'http://vandavn.com/uploads/sanpham/16270275503919111.jpg.jpg', 154, 1, 1),
(612, 'http://vandavn.com/uploads/sanpham/16270275504599107.jpg.jpg', 154, 2, 1),
(613, 'http://vandavn.com/uploads/sanpham/16270293751379992.jpg.jpg', 155, 1, 1),
(614, 'http://vandavn.com/uploads/sanpham/16270294558069754.jpg.jpg', 156, 1, 1),
(615, 'http://vandavn.com/uploads/sanpham/16270294916769031.jpg.jpg', 157, 1, 1),
(616, 'http://vandavn.com/uploads/sanpham/16270295245759389.jpg.jpg', 158, 1, 1),
(617, 'http://vandavn.com/uploads/sanpham/16270295573629735.jpg.jpg', 159, 1, 1),
(618, 'http://vandavn.com/uploads/sanpham/16270295905949249.jpg.jpg', 160, 1, 1),
(619, 'http://vandavn.com/uploads/sanpham/16270296193249855.jpg.jpg', 24481, 1, 0),
(620, 'http://vandavn.com/uploads/sanpham/16270296539199181.jpg.jpg', 162, 1, 1),
(621, 'http://vandavn.com/uploads/sanpham/16270296871319353.jpg.jpg', 163, 1, 1),
(622, 'http://vandavn.com/uploads/sanpham/16270297251719090.jpg.jpg', 164, 1, 1),
(623, 'http://vandavn.com/uploads/sanpham/16270297579089165.jpg.jpg', 165, 1, 1),
(624, 'http://vandavn.com/uploads/sanpham/16270297908319301.jpg.jpg', 166, 1, 1),
(625, 'http://vandavn.com/uploads/sanpham/16270298266019359.jpg.jpg', 167, 1, 1),
(626, 'http://vandavn.com/uploads/sanpham/16270298789579680.jpg.jpg', 168, 1, 1),
(627, 'http://vandavn.com/uploads/sanpham/16270303077509137.jpg.jpg', 169, 1, 1),
(628, 'http://vandavn.com/uploads/sanpham/16270321919769950.jpg.jpg', 170, 1, 1),
(629, 'http://vandavn.com/uploads/sanpham/16270322298829651.jpg.jpg', 171, 1, 1),
(630, 'http://vandavn.com/uploads/sanpham/16270322698019920.jpg.jpg', 172, 1, 1),
(631, 'http://vandavn.com/uploads/sanpham/16270323071629185.jpg.jpg', 173, 1, 1),
(632, 'http://vandavn.com/uploads/sanpham/16270323416209029.jpg.jpg', 174, 1, 1),
(633, 'http://vandavn.com/uploads/sanpham/16270323738709044.jpg.jpg', 175, 1, 1),
(634, 'http://vandavn.com/uploads/sanpham/16270324101119301.jpg.jpg', 176, 1, 1),
(635, 'http://vandavn.com/uploads/sanpham/16270324425769029.jpg.jpg', 177, 1, 1),
(636, 'http://vandavn.com/uploads/sanpham/16270324902939225.jpg.jpg', 178, 1, 1),
(637, 'http://vandavn.com/uploads/sanpham/16270325215049966.jpg.jpg', 179, 1, 1),
(638, 'http://vandavn.com/uploads/sanpham/16270325564609530.jpg.jpg', 180, 1, 1),
(639, 'http://vandavn.com/uploads/sanpham/16270325882479298.jpg.jpg', 181, 1, 1),
(640, 'http://vandavn.com/uploads/sanpham/16270326323489752.jpg.jpg', 182, 1, 1),
(641, 'http://vandavn.com/uploads/sanpham/16270326641939352.jpg.jpg', 183, 1, 1),
(642, 'http://vandavn.com/uploads/sanpham/16270327957179010.jpg.jpg', 184, 1, 1),
(643, 'http://vandavn.com/uploads/sanpham/16270328891419598.jpg.jpg', 185, 1, 1),
(644, 'http://vandavn.com/uploads/sanpham/16270351763949742.jpg.jpg', 186, 1, 1),
(645, 'http://vandavn.com/uploads/sanpham/16270352346119666.jpg.jpg', 187, 1, 1),
(646, 'http://vandavn.com/uploads/sanpham/16270352692079320.jpg.jpg', 188, 1, 1),
(647, 'http://vandavn.com/uploads/sanpham/16270353006119962.jpg.jpg', 189, 1, 1),
(648, 'http://vandavn.com/uploads/sanpham/16270353327259751.jpg.jpg', 190, 1, 1),
(649, 'http://vandavn.com/uploads/sanpham/16270353773389404.jpg.jpg', 191, 1, 1),
(650, 'http://vandavn.com/uploads/sanpham/16270354092199218.jpg.jpg', 192, 1, 1),
(651, 'http://vandavn.com/uploads/sanpham/16270354391399383.jpg.jpg', 193, 1, 1),
(652, 'http://vandavn.com/uploads/sanpham/16270355224979924.jpg.jpg', 194, 1, 1),
(653, 'http://vandavn.com/uploads/sanpham/16270358103809569.jpg.jpg', 195, 1, 1),
(654, 'http://vandavn.com/uploads/sanpham/16270980661809375.jpg.jpg', 196, 1, 1),
(655, 'http://vandavn.com/uploads/sanpham/16270983529629352.jpg.jpg', 197, 1, 1),
(656, 'http://vandavn.com/uploads/sanpham/16270984706579202.jpg.jpg', 198, 1, 1),
(657, 'http://vandavn.com/uploads/sanpham/16270985120979842.jpg.jpg', 199, 1, 1),
(658, 'http://vandavn.com/uploads/sanpham/16270985412169166.jpg.jpg', 200, 1, 1),
(659, 'http://vandavn.com/uploads/sanpham/16270985926769200.jpg.jpg', 201, 1, 1),
(660, 'http://vandavn.com/uploads/sanpham/16270986199489125.jpg.jpg', 202, 1, 1),
(661, 'http://vandavn.com/uploads/sanpham/16270986490369345.jpg.jpg', 203, 1, 1),
(662, 'http://vandavn.com/uploads/sanpham/16270986761949293.jpg.jpg', 204, 1, 1),
(663, 'http://vandavn.com/uploads/sanpham/16270987061959955.jpg.jpg', 205, 1, 1),
(664, 'http://vandavn.com/uploads/sanpham/16270987341249840.jpg.jpg', 206, 1, 1),
(665, 'http://vandavn.com/uploads/sanpham/16270987993789786.jpg.jpg', 207, 1, 1),
(666, 'http://vandavn.com/uploads/sanpham/16270990219809972.jpg.jpg', 208, 1, 1),
(667, 'http://vandavn.com/uploads/sanpham/16270990540459601.jpg.jpg', 209, 1, 1),
(668, 'http://vandavn.com/uploads/sanpham/16270990940419018.jpg.jpg', 210, 1, 1),
(669, 'http://vandavn.com/uploads/sanpham/16270992953149552.jpg.jpg', 211, 1, 1),
(670, 'http://vandavn.com/uploads/sanpham/16270993331419052.jpg.jpg', 212, 1, 1),
(671, 'http://vandavn.com/uploads/sanpham/16270995331329497.jpg.jpg', 213, 1, 1),
(672, 'http://vandavn.com/uploads/sanpham/16270995757499756.jpg.jpg', 214, 1, 1),
(673, 'http://vandavn.com/uploads/sanpham/16270997003909673.jpg.jpg', 215, 1, 1),
(674, 'http://vandavn.com/uploads/sanpham/16270997288039927.jpg.jpg', 216, 1, 1),
(675, 'http://vandavn.com/uploads/sanpham/16270997978869307.jpg.jpg', 217, 1, 1),
(676, 'http://vandavn.com/uploads/sanpham/16270998657099772.jpg.jpg', 25945, 1, 0),
(677, 'http://vandavn.com/uploads/sanpham/16271000570519735.jpg.jpg', 219, 1, 1),
(678, 'http://vandavn.com/uploads/sanpham/16271002612939605.jpg.jpg', 220, 1, 1),
(679, 'http://vandavn.com/uploads/sanpham/16271005053969705.PNG.png', 221, 1, 1),
(680, 'http://vandavn.com/uploads/sanpham/16271005406699343.jpg.jpg', 222, 1, 1),
(681, 'http://vandavn.com/uploads/sanpham/16271006163579413.jpg.jpg', 223, 1, 1),
(682, 'http://vandavn.com/uploads/sanpham/16271006543389011.jpg.jpg', 224, 1, 1),
(683, 'http://vandavn.com/uploads/sanpham/16271006839059559.jpg.jpg', 225, 1, 1),
(684, 'http://vandavn.com/uploads/sanpham/16271007333619226.jpg.jpg', 226, 1, 1),
(685, 'http://vandavn.com/uploads/sanpham/16271008083099824.jpg.jpg', 227, 1, 1),
(686, 'http://vandavn.com/uploads/sanpham/16271008749349282.jpg.jpg', 228, 1, 1),
(688, 'http://vandavn.com/uploads/sanpham/16271009313639816.PNG.png', 229, 1, 1),
(689, 'http://vandavn.com/uploads/sanpham/16271009768899046.jpg.jpg', 230, 1, 1),
(690, 'http://vandavn.com/uploads/sanpham/16271010245679086.jpg.jpg', 231, 1, 1),
(691, 'http://vandavn.com/uploads/sanpham/16271013068329843.PNG.png', 232, 1, 1),
(694, 'http://vandavn.com/uploads/sanpham/16271017702929879.jpg.jpg', 234, 1, 1),
(695, 'http://vandavn.com/uploads/sanpham/16271017992369836.jpg.jpg', 233, 1, 1),
(701, 'http://localhost/xaydung/cms/uploads/sanpham/16336928550419579.jpg.jpg', 235, 1, 1),
(703, 'http://localhost/xaydung/cms/uploads/sanpham/16336939022829760.jpg.jpg', 49, 1, 1),
(704, 'http://matervn.com/cms/uploads/sanpham/16337493288949206.jpg.jpg', 236, 1, 1),
(705, 'https://matervn.com/cms/uploads/sanpham/16339345372609985.png.png', 237, 1, 1),
(706, 'https://matervn.com/cms/uploads/sanpham/16339345421599905.jpg.jpg', 237, 2, 1),
(707, 'http://matervn.com/cms/uploads/sanpham/16340366339739808.jpg.jpg', 3, 1, 1),
(708, 'http://matervn.com/cms/uploads/sanpham/16340433315859187.jpg.jpg', 4, 1, 1),
(709, 'http://matervn.com/cms/uploads/sanpham/16340434798789544.jpg.jpg', 5, 1, 1),
(710, 'http://matervn.com/cms/uploads/sanpham/16340435600889975.jpg.jpg', 6, 1, 1),
(711, 'http://matervn.com/cms/uploads/sanpham/16340436667289110.jpg.jpg', 7, 1, 1),
(712, 'http://matervn.com/cms/uploads/sanpham/16340437827529169.jpg.jpg', 8, 1, 1),
(713, 'http://matervn.com/cms/uploads/sanpham/16340439658959885.jpg.jpg', 9, 1, 1),
(714, 'http://matervn.com/cms/uploads/sanpham/16340441390259637.jpg.jpg', 10, 1, 1),
(715, 'http://matervn.com/cms/uploads/sanpham/16340442966479653.jpg.jpg', 11, 1, 1),
(716, 'http://matervn.com/cms/uploads/sanpham/16340444962229157.jpg.jpg', 12, 1, 1),
(717, 'http://matervn.com/cms/uploads/sanpham/16340450844599671.jpg.jpg', 13, 1, 1),
(719, 'http://matervn.com/cms/uploads/sanpham/16340451674839983.jpg.jpg', 14, 1, 1),
(720, 'http://matervn.com/cms/uploads/sanpham/16340452535489303.jpg.jpg', 15, 1, 1),
(721, 'http://matervn.com/cms/uploads/sanpham/16340453506689064.jpg.jpg', 16, 1, 1),
(722, 'http://matervn.com/cms/uploads/sanpham/16340456962169041.jpg.jpg', 17, 1, 1),
(723, 'http://matervn.com/cms/uploads/sanpham/16340457470569511.jpg.jpg', 18, 1, 1),
(724, 'http://matervn.com/cms/uploads/sanpham/16340545039939520.jpg.jpg', 19, 1, 1),
(725, 'http://matervn.com/cms/uploads/sanpham/16342697904209743.png.png', 62591, 1, 0),
(726, 'http://matervn.com/cms/uploads/sanpham/16342698421129113.png.png', 20, 1, 1),
(727, 'http://matervn.com/cms/uploads/sanpham/16342700553829166.jpeg.jpeg', 20, 5, 1),
(728, 'http://matervn.com/cms/uploads/sanpham/16342700553659802.jpeg.jpeg', 20, 3, 1),
(729, 'http://matervn.com/cms/uploads/sanpham/16342700553769718.jpeg.jpeg', 20, 4, 1),
(730, 'http://matervn.com/cms/uploads/sanpham/16342700553569049.jpeg.jpeg', 20, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(10) NOT NULL,
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `status`) VALUES
(1, 'Hãng sản xuất', 1),
(2, 'Xuất xứ', 1),
(3, 'Giá khuyến mại', 1),
(4, 'Vị trí', 0),
(5, 'Đơn vị tính', 0);

-- --------------------------------------------------------

--
-- Table structure for table `properties_detail`
--

CREATE TABLE `properties_detail` (
  `id` int(10) NOT NULL,
  `product` int(10) NOT NULL,
  `property` int(11) NOT NULL,
  `value` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `properties_detail`
--

INSERT INTO `properties_detail` (`id`, `product`, `property`, `value`, `status`) VALUES
(1, 28, 1, 'Samsung', 1),
(2, 28, 2, 'Việt Nam', 1),
(3, 28, 3, '1300000', 1),
(4, 26, 1, 'Apple', 1),
(5, 26, 2, 'America', 1),
(6, 26, 3, '1500000', 1),
(7, 26, 4, 'Hihi', 1),
(8, 28, 4, 'Không cần 1 ai nữa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `name`, `status`) VALUES
(1, 'Tiền Giang', 1),
(2, 'Hưng Yên', 1),
(3, 'Hà Nội', 1),
(4, 'TP Hồ Chí Minh', 1),
(5, 'Cà Mau', 1),
(6, 'Đắc Lắc', 1),
(7, 'Nam Định', 1),
(8, 'Quảng Ninh', 1),
(9, 'Đắk Nông', 1),
(10, 'Đà Nẵng', 1),
(11, 'Hải Dương', 1),
(12, 'Long An', 1),
(13, 'Bến Tre', 1),
(14, 'Đồng Tháp', 1),
(15, 'Vĩnh Long', 1),
(16, 'Kiên Giang', 1),
(17, 'Trà Vinh', 1),
(18, 'Sóc Trăng', 1),
(19, 'Bắc Ninh', 1),
(20, 'Thanh Hoá', 1),
(21, 'Vũng Tàu', 1),
(22, 'Đồng Nai', 1),
(23, 'Bình Dương', 1),
(24, 'Thái Nguyên', 1),
(25, 'Thái Bình', 1),
(26, 'Cần Thơ', 1),
(27, 'Nghệ An', 1),
(28, 'Huế', 1),
(29, 'Bình Phước', 1),
(30, 'Quảng Nam', 1),
(31, 'Quảng Ngãi', 1),
(32, 'Ninh Thuận', 1),
(33, 'Lào Cai', 1),
(34, 'Hải Phòng', 1),
(35, 'An Giang', 1),
(36, 'Phú Thọ', 1),
(37, 'Tây Ninh', 1),
(38, 'Khánh Hòa', 1),
(39, 'Phú Yên', 1),
(40, 'Hòa Bình', 1),
(41, 'Tuyên Quang', 1),
(42, 'Lai Châu', 1),
(43, 'Hậu Giang', 1),
(44, 'Lâm Đồng', 1),
(45, 'Lạng Sơn', 1),
(46, 'Hà Nam', 1),
(47, 'Bắc Cạn', 1),
(48, 'Bình Định', 1),
(49, 'Cao Bằng', 1),
(50, 'Sơn La', 1),
(51, 'Quảng Bình', 1),
(52, 'Quảng Trị', 1),
(53, 'Gia Lai', 1),
(54, 'Bắc Giang', 1),
(55, 'Hà Tĩnh', 1),
(56, 'Ninh Bình', 1),
(57, 'Bình Thuận', 1),
(58, 'Kon Tum', 1),
(59, 'Vĩnh Phúc', 1),
(60, 'Bạc Liêu', 1),
(61, 'Yên Bái', 1),
(62, 'Điện Biên', 1),
(63, 'Hà Giang', 1),
(64, 'Chưa rõ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE `search` (
  `id` int(11) NOT NULL,
  `keyword` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `search`
--

INSERT INTO `search` (`id`, `keyword`, `type`, `count`) VALUES
(1, 'sản phẩm', 'document', 3),
(2, 'doc', 'document', 1),
(3, 'demo', 'document', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `avatar` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `avatar`, `content`, `author`, `status`) VALUES
(1, '', 'Chúng tôi đã làm việc với thương hiệu VTRADE trong 10 năm qua. VTRADE đưa rất nhiều chi tiết và sự phát triển vào các loại vải của họ và tiếp tục thúc đẩy ranh giới với các xu hướng mới nhất. Chúng tôi mong đợi mỗi lần ra mắt vải mới, nó tạo ra sự phấn khích trong đội ngũ và khách hàng của chúng tôi', 'Kirsch Stein &amp; Co, Nam Phi', 1),
(2, '', 'VTRADE, đây là thương hiệu mà công ty tôi đã tin tưởng 3 năm nay, ấn tượng nhất là lần đầu tiên gặp ông Ramesh tại SECC (Trung tâm Hội chợ &amp; Triển lãm Sài Gòn). Sự đón tiếp nồng hậu, nhiệt tình và thân mật trong lần gặp đầu tiên đã khiến tôi trở thành đối tác của VTRADE tại Việt Nam.', 'Anh Khang Dang, Việt Nam', 1);

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` int(20) NOT NULL,
  `ip` varchar(25) NOT NULL,
  `sessions` varchar(40) NOT NULL,
  `city` varchar(20) NOT NULL,
  `loc` varchar(30) NOT NULL,
  `times` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `albumsub`
--
ALTER TABLE `albumsub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`description`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_status_date` (`id`);

--
-- Indexes for table `blogcategory`
--
ALTER TABLE `blogcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infomation`
--
ALTER TABLE `infomation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_status_date` (`created_date`,`id`),
  ADD KEY `post_author` (`name`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderlist`
--
ALTER TABLE `orderlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`description`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `productpicture`
--
ALTER TABLE `productpicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties_detail`
--
ALTER TABLE `properties_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `albumsub`
--
ALTER TABLE `albumsub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `blogcategory`
--
ALTER TABLE `blogcategory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=909;

--
-- AUTO_INCREMENT for table `infomation`
--
ALTER TABLE `infomation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderlist`
--
ALTER TABLE `orderlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `productpicture`
--
ALTER TABLE `productpicture`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=731;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `properties_detail`
--
ALTER TABLE `properties_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `search`
--
ALTER TABLE `search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
