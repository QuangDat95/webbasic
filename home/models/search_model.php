<?php

class search_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdocument($keyword)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 ";
        if($keyword != ''){
            $dieukien .= " AND name LIKE '%$keyword%' ";
        }
        $query    = $this->db->query("SELECT *,
            (SELECT link FROM productpicture WHERE product = a.id LIMIT 1) AS hinhanh
            FROM product a $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function updatesearch($keyword, $type)
    {
        $query    = $this->db->query("SELECT id,count
            FROM search WHERE keyword LIKE '$keyword' ");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        if($result){
            $id = $result[0]['id'];
            $data['count'] = $result[0]['count'] + 1;
            $query = $this->update("search", $data, " id = $id ");
        } else {
            $data['keyword'] = $keyword;
            $data['count'] = 1;
            $data['type'] = $type;
            $query = $this->insert("search", $data);
        }
        return $query;
    }

}
