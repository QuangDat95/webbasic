<?php

class document_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata($url, $page)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND url LIKE '$url' ";
        
        $query    = $this->db->query("SELECT name
            FROM productcategory $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getdocuments($url, $xuhuong, $type, $money)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND category = (SELECT id FROM productcategory WHERE url LIKE '$url') ";
        
        if($type != '' && $type != 'all'){
            $dieukien .= " AND type_file LIKE '$type' ";
        } else {
            $dieukien .= " ";
        }

        if($money == 'cophi'){
            $dieukien .= " AND agent_price > 0 ";
        } else if ($money == 'mienphi') {
            $dieukien .= " AND agent_price = 0 ";
        } else {
            $dieukien .= " ";
        }

        if($xuhuong == 'tainhieu'){
            $dieukien .= " ORDER BY download DESC ";
        } else if ($xuhuong == 'xemnhieu') {
            $dieukien .= " ORDER BY view DESC ";
        } else {
            $dieukien .= " ORDER BY id DESC ";
        }

        $query    = $this->db->query("SELECT *,
            (SELECT link FROM productpicture WHERE product = a.id LIMIT 1) AS hinhanh
            FROM product a $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettags($url)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND category = (SELECT id FROM productcategory WHERE url LIKE '$url') AND tag NOT LIKE '' ";
        $query    = $this->db->query("SELECT tag
            FROM product a $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getrelatedkeywords($url)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND url NOT LIKE '$url' AND category = (SELECT category FROM product WHERE url LIKE '$url') ";
        $query    = $this->db->query("SELECT name,url
            FROM product a $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getthuongxem($url)
    {
        $result   = array();
        $query    = $this->db->query("SELECT id,category
        FROM product WHERE url LIKE '$url'");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        $category = $result[0]['category'];
        $id = $result[0]['id'];
        $dieukien = " WHERE status = 1 AND category = $category AND id != $id ";
        $query    = $this->db->query("SELECT *
            FROM product a $dieukien ORDER BY view DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getdocument($url)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND url LIKE '$url'";
        $query    = $this->db->query("SELECT *,
            (SELECT name FROM productcategory WHERE id = a.category) AS category,
            (SELECT url FROM productcategory WHERE id = a.category) AS categoryurl,
            (SELECT link FROM productpicture WHERE product = a.id LIMIT 1) AS hinhanh
            FROM product a $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettailieulienquan($url)
    {
        $result   = array();
        $query    = $this->db->query("SELECT id,category
        FROM product WHERE url LIKE '$url'");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        $category = $result[0]['category'];
        $id = $result[0]['id'];
        $dieukien = " WHERE status = 1 AND category = $category AND id != $id ";
        $query    = $this->db->query("SELECT *
            FROM product a $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getmoinhat()
    {
        $result   = array();
        $dieukien = " WHERE status = 1 ";
        $query    = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY id DESC LIMIT 5");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettainhieu()
    {
        $result   = array();
        $dieukien = " WHERE status = 1 ";
        $query    = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY download DESC LIMIT 5");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getxemnhieu()
    {
        $result   = array();
        $dieukien = " WHERE status = 1 ";
        $query    = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY view DESC LIMIT 5");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getnoibat()
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND position LIKE '%2%' ";
        $query    = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY id DESC LIMIT 5");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function addCollection($data)
    {
        $query = $this->insert("collectioncategory",$data);
        return $query;
    }

}
