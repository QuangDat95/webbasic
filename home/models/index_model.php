<?php

class index_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata()
    {
        $result = array();
        $dieukien = " WHERE status = 1 AND position = 1 ";
        $query = $this->db->query("SELECT *,
            (SELECT link FROM productpicture WHERE product = a.id LIMIT 1) AS hinhanh
            FROM product a $dieukien ORDER BY id DESC");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getsidebar()
    {
        $result = array();
        $dieukien = " WHERE status = 1 AND position IN (1,3) ";
        $query = $this->db->query("SELECT *
            FROM productcategory $dieukien ORDER BY sort_order ASC ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettailieuchung()
    {
        $result = array();
        $dieukien = " WHERE status = 1 AND position IN (2,3) ";
        $query = $this->db->query("SELECT *
            FROM productcategory $dieukien ORDER BY sort_order ASC LIMIT 4");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettailieuchild($id)
    {
        $result = array();
        $dieukien = " WHERE status = 1 AND category = $id ";
        $query = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY sort_order ASC LIMIT 4");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettlnoibat()
    {
        $result = array();
        $dieukien = " WHERE status = 1 AND position = 1 ";
        $query = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY sort_order ASC LIMIT 6");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettlmoi()
    {
        $result = array();
        $dieukien = " WHERE status = 1 ";
        $query = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY id DESC LIMIT 6");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettlxemnhieu()
    {
        $result = array();
        $dieukien = " WHERE status = 1 ";
        $query = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY view DESC LIMIT 6");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettltainhieu()
    {
        $result = array();
        $dieukien = " WHERE status = 1 ";
        $query = $this->db->query("SELECT *
            FROM product $dieukien ORDER BY download DESC LIMIT 6");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getslide()
    {
        $result = array();
        $dieukien = " WHERE status = 1 ";
        $query = $this->db->query("SELECT *
            FROM banner $dieukien ORDER BY sort_order ASC ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    function checkEmail($email){
        $result = array();
        $dieukien = " WHERE status > 0 AND email='$email'";
        $query = $this->db->query("SELECT COUNT(id) as total
            FROM user $dieukien ");
        if ($query) {
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            return $temp[0]['total'];
        }else{
            return 0;
        }

    }

    function checkLogin($username,$password){
        $result = array();
        $dieukien = " WHERE status > 0 AND email='$username' AND password='$password' ";
        $query = $this->db->query("SELECT id,email,phone,customer,
            (SELECT name FROM customer WHERE id = a.customer) AS name,
            (SELECT address FROM customer WHERE id = a.customer) AS address,
            (SELECT birthday FROM customer WHERE id = a.customer) AS birthday,
            (SELECT sex FROM customer WHERE id = a.customer) AS sex,
            (SELECT cccd FROM customer WHERE id = a.customer) AS cccd
            FROM user a $dieukien ");
        if ($query) {
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            return $temp;
        }else{
            return [];
        }
    }

    function insertCustomer($data)
    {
        if ($this->insert("customer", $data))
            return $this->db->lastInsertId();
        else
            return 0;
    }
    function insertUser($data)
    {
        if ($this->insert("user", $data))
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function addCollection($data,$docid)
    {
        $result = $this->insert("collections",$data);
        $id = $this->db->lastInsertId(); 
        if($id){
            $data = array(
                'datetime' => HOMNAY,
                'document_id' => $docid,
                'collection_id' => $id,
                'status' => 1
            );
            $this->insert("collectionsub",$data);
        }
        return $id;
    }

    function getCollection($uid,$docid)
    {
        $result = array();
        $listcoll = array();
        // $listcoll = '';
        $dieukien = " WHERE status = 1 AND customer = $uid ";
        $query = $this->db->query("SELECT *
            FROM collections a $dieukien ORDER BY id DESC ");
        $result['data'] = $query->fetchAll(PDO::FETCH_ASSOC);
       
        $query = $this->db->query("SELECT collection_id FROM collectionsub WHERE document_id = $docid AND status = 1");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp){
            foreach ($temp as $coll) {
                array_push($listcoll,$coll['collection_id']); 
            }
            $result['collid'] = $listcoll;  
        }
        
        return $result;
    }

    function loadData($id)
    {
        $result = array();
        $dieukien = " WHERE id = $id ";
        $query = $this->db->query("SELECT *,
            (SELECT document_id FROM collectionsub WHERE collection_id = $id) as docid
            FROM collections $dieukien ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $result = $result[0];
        return $result;
    }

    function updateColsub($data)
    {
        $collection_id = $data['collection_id'];
        $document_id = $data['document_id'];
        $query = $this->db->query("SELECT id
            FROM collectionsub WHERE collection_id = $collection_id AND document_id = $document_id");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        if($result){
            $id = $result[0]['id'];
            $result = $this->update("collectionsub",$data, "id = $id");
        } else {
            $result = $this->insert("collectionsub",$data);
        }
        return $result;
    }
}
?>