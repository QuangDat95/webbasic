<?php
class user_model extends model
{
    function __construct()
    {
        parent:: __construct();
    }

    function getMyColl($uid)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND customer = $uid ";
        $query  = $this->db->query("SELECT *,
            (SELECT COUNT(id) FROM collectionsub WHERE collection_id = a.id AND status = 1) as total
            FROM collections a $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getColl($url,$id)
    {
        $result   = array();
        if($url != ''){
            $dieukien = " WHERE status = 1 AND url LIKE '$url' ";
        } else {
            $dieukien = " WHERE status = 1 AND id = $id ";
        }
        
        $query  = $this->db->query("SELECT *
            FROM collections a $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    
    function getDocs($url)
    {
        $result   = array();
        $listdoc = '';
        $dieukien = " WHERE status = 1 AND url LIKE '$url' ";
        $query  = $this->db->query("SELECT id
            FROM collections a $dieukien ");
        $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
        $coll_id = $temp[0]['id'];
        if ($coll_id){
            $query  = $this->db->query("SELECT document_id
            FROM collectionsub WHERE collection_id = $coll_id AND status = 1");
            $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp as $doc) {
                $listdoc .= $doc["document_id"] . ",";
            }
            $listdoc = rtrim($listdoc, ",");
            if($listdoc){
                $query  = $this->db->query("SELECT *,
                (SELECT url FROM productcategory WHERE id = a.category) as urlcate,
                (SELECT name FROM productcategory WHERE id = a.category) as category,
                (SELECT link FROM productpicture WHERE product = a.id LIMIT 1) as hinhanh
                FROM product a WHERE id IN ($listdoc) AND status = 1");
                $result = $query->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        return $result;
    }

    function saveeditcoll()
    {
        $id = $_REQUEST['id'];
        $query = $this->update("collections", $data, " id = $id ");
        return $query;
    }

    function dellColl($id, $data)
    {
        $query = $this->update("collections", $data, " id = $id ");
        return $query;
    }

    function dellDoc($docid,$collid,$data)
    {
        $query = $this->update("collectionsub", $data, " document_id = $docid AND collection_id = $collid ");
        return $query;
    }
    
}