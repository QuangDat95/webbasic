<?php

class tag_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata($keyword)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 ";
        if($keyword != ''){
            $dieukien .= " AND tag LIKE '%$keyword%' ";
        }
        $query    = $this->db->query("SELECT *,
            (SELECT name FROM productcategory WHERE id = a.category) AS category,
            (SELECT link FROM productpicture WHERE product = a.id LIMIT 1) AS hinhanh
            FROM product a $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettags($keyword)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND tag NOT LIKE '' ";
        if($keyword != ''){
            $dieukien .= " AND tag NOT LIKE '%$keyword%' ";
        }
        $query  = $this->db->query("SELECT tag FROM product $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}
?>