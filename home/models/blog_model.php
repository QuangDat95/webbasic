<?php

class blog_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getdata($url)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 ";
        $query    = $this->db->query("SELECT *
            FROM blog $dieukien AND category = (SELECT id FROM blogcategory WHERE url LIKE '$url') ORDER BY sort_order ASC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getblogcate()
    {
        $result   = array();
        $dieukien = " WHERE status = 1 ";
        $query    = $this->db->query("SELECT *
            FROM blogcategory $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getblog($url)
    {
        $result   = array();
        $dieukien = " WHERE status = 1 AND url LIKE '$url' ";
        $query    = $this->db->query("SELECT *
            FROM blog $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettinlienquan($url)
    {
        $result   = array();
        $query    = $this->db->query("SELECT id,category
        FROM blog WHERE url LIKE '$url'");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        $category = $result[0]['category'];
        $id = $result[0]['id'];
        $dieukien = " WHERE status = 1 AND category = $category AND id != $id ";
        $query    = $this->db->query("SELECT *
            FROM blog a $dieukien ORDER BY id DESC");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}
