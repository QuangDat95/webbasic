<div class="clear"></div>
<div id="fb-root"></div>
<div id="footer">
    <footer>
        <h3 class="doc_logo_footer text-center p-6"><a href="javascript:;"></a></h3>
        <div class="container flex text-xs text-gray-500 mx-auto justify-center flex-wrap overflow-hidden leading-8">
            <?php
                foreach($this->keywords as $keyword){ ?>
                    <a href="<?= URL ?>/search/<?= $keyword['type'] ?>?keyword=<?= $keyword['keyword'] ?>" rel="dofollow" title="Tai lieu" class="hover:text-primary mx-1 whitespace-no-wrap"><?= $keyword['keyword'] ?></a> 
            <?php
                }
            ?>
        </div>
        <div class="doc_list_footer container mx-auto flex justify-center mt-12 justify-around md:px-24 px-2 flex-col md:flex-row text-center md:text-left">
            <ul class="doc_support_client mb-2">
                <li>
                    <h4>Thông tin</h4>
                </li>
                <?php
                foreach ($this->thongtin as $item) { ?>
                    <li><a rel="nofollow" href="<?= URL ?>/blog/<?= $item['url'] ?>"><?= $item['title'] ?></a></li>
                <?php
                }
                ?>
            </ul>
            <ul class="doc_support_client mb-2">
                <li>
                    <h4>Hỗ trợ khách hàng</h4>
                </li>
                <?php
                foreach ($this->htkh as $item) { ?>
                    <li><a rel="nofollow" href="<?= URL ?>/blog/<?= $item['url'] ?>"><?= $item['title'] ?></a></li>
                <?php
                }
                ?>
            </ul>
            <ul class="doc_help mb-2">
                <li>
                    <h4>Trợ giúp</h4>
                </li>
                <?php
                foreach ($this->trogiup as $item) { ?>
                    <li><a rel="nofollow" href="<?= URL ?>/blog/<?= $item['url'] ?>"><?= $item['title'] ?></a></li>
                <?php
                }
                ?>
            </ul>
            <ul class="doc_support_client mb-2">
                <li>
                    <h4>Liên hệ</h4>
                </li>
                <li><a rel="nofollow" >Hotline: 0123456789</a>
                </li>
                <li><a rel="nofollow" href="mailto:info@123doc.org">Email: <?= $this->info[6]['value'] ?></a>
                </li>
            </ul>
            <ul class="doc_support mb-2">
                <li>
                    <h4>Theo dõi</h4>
                </li>
                <li><a rel="nofollow" href="#" target="__blank">Facebook</a></li>
                <li><a rel="nofollow" href="#" target="__blank">Google</a></li>
                <li><a rel="nofollow" href="#" target="__blank">Zalo</a></li>
            </ul>
        </div>
        <div class="mt-6 bg-main-background text-xs mt-6 p-4 text-center text-text-default">
            <div>
                <p>Copyright © 2020 123Doc. Design by Gemstech</p>
            </div>
        </div>
    </footer>
</div>
<script type="text/javascript">
    window.lazyLoadInstances = [];
    window.lazyLoadOptions = {};
    window.addEventListener("LazyLoad::Initialized", function(event) {
        window.lazyLoadInstance = event.detail.instance;
    }, false);
    $(document).ready(function() {
        $("body").bind("ajaxComplete", function() {
            lazyLoadInstance.update();
        });
    });
</script>
<script async type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2/common/js/lazyload.min.js"></script>
<!-- <div class="over_gray"></div>
<div id="g_id_onload" data-client_id="443577583449-74qbggsv6iumi05hai47rq2dhk0q1ioc.apps.googleusercontent.com"
     data-login_uri="https://socket.123docz.net/callback/google"
     style="position: fixed; z-index: 9999; height: 221px;"></div>
<script src="https://accounts.google.com/gsi/client" async defer></script> -->
<script defer type="text/javascript">
    /*Object global.*/
    // var objGlobal = new Object(), arrUrlSearch, scope_search;
    // objGlobal.isLogin = false;
    // objGlobal.PathImgDocNormal = 'template/images/default/doc_normal.png';
    // objGlobal.PathImgUserTiny = 'template/images/default/user_tiny_small.png';
    // objGlobal.PathIDVGRegister = 'https://vnid.net/register?site=123doc&amp;return_url=aHR0cHM6Ly8xMjNkb2N6Lm5ldC90cmFuZy1jaHUuaHRt';
    // objGlobal.PathIDVGLogin = 'https://id.vatgia.com/dang-nhap?ui.mode=popup&amp;_cont=https://123docz.net/home/idvg_return.php&amp;service=123doc';//array url Search;arrUrlSearch = ["s","doc\/s","book\/s","audio\/s","collection\/s","people\/s"];/*biến quy định option search*/scope_search = 0;openFeedbackBox = 0;__ad123job = 1;
</script>
<script defer type="text/javascript">
    const MAX_SEARCH_TIMES_PER_MINUTES = 4;
    const RECAPTCHA_SITE_KEY = "6LdWCt4ZAAAAAJqD3A-EGGCraFAQCoUSLUoTNkhf";
    var opt = parseInt('0');
</script>
<script defer type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2/common/js/jquery.autocomplete.min3860.js?v=1"></script>
<script defer type="text/javascript" src="<?= URL ?>/template/static_v2/common/js/js.template.min3c56.js?v=3013"></script>
<script defer type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2/common/js/script.min8915.js?v=42"></script>
<script defer type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2/common/js/function-js.min3f56.js?v=11"></script>
<!-- Google Tag Manager (noscript) -->
<!-- <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KBJ9Z6" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript> -->
<!-- End Google Tag Manager (noscript) -->
<script>
    // var loadDeferredStyles = function () {
    //     var addStylesNode = document.getElementById("deferred-styles");
    //     var addStylesDetails = document.getElementById("deffered-style-detail");
    //     if (addStylesDetails != null) {
    //         var addStyle = addStylesNode.textContent + addStylesDetails.textContent;
    //     } else {
    //         var addStyle = addStylesNode.textContent;
    //     }
    //     var replacement = document.createElement("div");
    //     replacement.innerHTML = addStyle;
    //     document.body.appendChild(replacement);
    //     addStylesNode.parentElement.removeChild(addStylesNode);
    // };
    // var raf = requestAnimationFrame || mozRequestAnimationFrame || webkitRequestAnimationFrame || msRequestAnimationFrame;
    // if (raf) raf(function () {
    //     window.setTimeout(loadDeferredStyles, 0);
    // }); else window.addEventListener('load', loadDeferredStyles);
</script>
<!-- Google Tag Manager -->
<!--<script>    -->
<!--    (function (w, d, s, l, i) {-->
<!--    w[l] = w[l] || [];-->
<!--    w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});-->
<!--    var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';-->
<!--    j.async = true;-->
<!--    j.src = '../www.googletagmanager.com/gtm5445.html?id=' + i + dl;-->
<!--    f.parentNode.insertBefore(j, f);-->
<!--})(window, document, 'script', 'dataLayer', 'GTM-KBJ9Z6');-->
<!--</script>-->
<!-- End Google Tag Manager -->
<script defer type="text/javascript" src="<?= URL ?>/template/js/lib_ft.js"></script>
<script src="<?= URL ?>/template/js/app.js" defer></script>
<script src="<?= URL ?>/template/static_v2/web_v2/common/js/jquery-3.5.1.min.js"></script>
<script defer>
    $(document).on("click", ".login-button", function(e) {
        $(this).prop("disabled", true);
        setTimeout(function() {
            $(".login-button").prop("disabled", false)
        }, 2000);
        setUrlBack();
        submitLogin($(this).data("form"));
    });
</script><!-- JS load sau các JS ngoài không cần thiết -->
<script>
    var is_load = 0;

    // function loadjs() {
    //     if (is_load == 0) {
    //         is_load = 1;
    //         $.getScript("../pagead2.googlesyndication.com/pagead/js/f.txt", function () {
    //             $('ins').each(function () {
    //                 (adsbygoogle = window.adsbygoogle || []).push({})
    //             })
    //         });
    //         $.getScript('../connect.facebook.net/en_US/all.js', function () {
    //             FB.init({appId: '389304137790873', status: true, version: 'v2.0', xfbml: true});
    //             var boxCmt_bottom = $('.boxCmt_bottom');
    //             if (boxCmt_bottom.length > 0) boxCmt_bottom.show();
    //         });
    //     }
    // }

    // $(window).scroll(function () {
    //     loadjs();
    // });
    // $(window).mousemove(function () {
    //     loadjs();
    // });
</script>
</body>
<!-- Mirrored from 123docz.net/trang-chu.htm by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Oct 2021 10:21:08 GMT -->

</html>