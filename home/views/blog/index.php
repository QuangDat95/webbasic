<link rel="stylesheet" type="text/css"
      href="<?= URL ?>/template/styles/search.css"/>
<div id="container">
    <div class="container mx-auto">
        <div class="statics_bottom grid grid-cols-12 gap-2 p-2 bg-white">
            <div class="search_bottom_left col-span-12 md:col-span-2 md:py-4">
                <ul class="hidden md:block">
                    <?php
                      $url = isset($_GET['url']) ? $_GET['url'] : null;
                      $url = rtrim($url, '/');
                      $url = explode('/', $url);
                        foreach($this->blogcate as $blogcateItem){?>
                            <li><a href="<?= $blogcateItem['url'] ?>" <?= ($url[2] == $blogcateItem['url']) ? 'class="active"' : '' ?>><?= $blogcateItem['name'] ?></a></li>
                    <?php
                        }
                    ?>
                    <!-- <li><a href="/statics/28-trang-tinh.htm" class="active">Trang tĩnh</a></li> -->

                    <!-- <li><a href="/statics/1841-gioi-thieu.htm">Giới thiệu</a></li>
                    <li><a href="/statics/1842-danh-cho-nguoi-ban.htm">Dành cho người bán</a></li>
                    <li><a href="/statics/1843-danh-cho-nguoi-mua.htm">Dành cho người mua</a></li>
                    <li><a href="/statics/1844-cau-chuyen-thanh-cong.htm">Câu chuyện thành công</a></li>
                    <li><a href="/statics/1845-thong-bao.htm">Thông báo</a></li> -->
                </ul>
            </div>
            <div class=" col-span-12  md:col-span-7 md:py-4">
                <div class="list_item" id="list_item">
                    <?php
                    foreach($this->data as $item){ ?>
                        <div class="mt-6">
                            <div class="max-w-4xl px-10 py-6 mx-auto bg-white rounded-lg shadow">
                                <div class="mt-2"><a target="_blank"
                                                    href="<?= URL ?>/blog/<?= $item['url'] ?>"
                                                    title="<?= $item['title'] ?>" style="color:#00a888;"
                                                    class="text-lg font-bold hover:underline"><?= $item['title'] ?></a>
                                    <p class="mt-2 text-gray-600"></p></div>
                                <div class="flex items-center justify-between mt-4"><span class="font-light text-gray-700"><?= date('d/m/Y',strtotime($item['updated'])) ?></span>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                    
                    <!-- <div class="mt-6">
                        <div class="max-w-4xl px-10 py-6 mx-auto bg-white rounded-lg shadow">
                            <div class="mt-2"><a target="_blank" href="/statics-detail/4-huong-dan-ban-tai-lieu.htm"
                                                 title="Hướng dẫn bán tài liệu" style="color:#00a888;"
                                                 class="text-lg font-bold hover:underline">Hướng dẫn bán tài liệu</a>
                                <p class="mt-2 text-gray-600"></p></div>
                            <div class="flex items-center justify-between mt-4"><span class="font-light text-gray-700">24/09/2020</span>
                            </div>
                        </div>
                    </div>
                    <div class="mt-6">
                        <div class="max-w-4xl px-10 py-6 mx-auto bg-white rounded-lg shadow">
                            <div class="mt-2"><a target="_blank"
                                                 href="/statics-detail/6-quy-dinh-voi-tai-lieu-dang-ban.htm"
                                                 title="Quy định với tài liệu đăng bán" style="color:#00a888;"
                                                 class="text-lg font-bold hover:underline">Quy định với tài liệu đăng
                                    bán</a>
                                <p class="mt-2 text-gray-600"></p></div>
                            <div class="flex items-center justify-between mt-4"><span class="font-light text-gray-700">24/09/2020</span>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <script>fixed_search_bottom_left();

        function fixed_search_bottom_left() {
            var search_bottom = $('.statics_bottom');
            var _top = search_bottom.offset().top - 15;
            var _height_list_item = $('.list_item').height();
            var _height_search_bottom_left = $('.search_bottom_left', search_bottom).height();
            $(window).scroll(function () {
                if ($(this).scrollTop() > _top && $(this).scrollTop() < (_top + _height_list_item - _height_search_bottom_left)) {
                    search_bottom.addClass('fixed');
                } else {
                    search_bottom.removeClass('fixed');
                }
            });
        }

        var limit = 10;

        function showMoreStatics(obj, catId) {
            var _page = $(obj).attr('data-rel');
            $.post('/global/ajax/aja_show_more_statics.php', {
                page: _page,
                catId: catId,
                limit: limit
            }, function (data) {
                $(obj).attr('data-rel', (_page * 1 + 1));
                $('.list_item').append(data);
                fixed_search_bottom_left();
            });
        }</script><!--Script tag (include before the closing </body> tag)-->
    <script type="text/javascript">var abd_media = "media.adnetwork.vn";
        var abd_width = 640; /*width of video player*/
        var abd_height = 360; /*height of video player*/
        var abd_skip = 7;
        var abd_wid = 1461636957;
        var abd_zid = 1461637489;
        var abd_content_id = "#container"; /*.class or #id of content wrapper*/
        var abd_position = 1; /*the paragraph position where ads display*/</script>
    <script src="http://media.adnetwork.vn/assets/js/abd.inpage.preroll.v2.js" type="text/javascript"></script>
</div>