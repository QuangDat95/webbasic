<link rel="stylesheet" type="text/css" href="<?=URL?>/template/styles/search.css"/>
<div id="container">
    <div class="container mx-auto">
        <div class="statics_bottom grid grid-cols-12 gap-2 p-2 bg-white">
            <div class="search_bottom_left col-span-12 md:col-span-2 md:py-4">
                <ul class="hidden md:block">
                    <?php
                        $url = isset($_GET['url']) ? $_GET['url'] : null;
                        $url = rtrim($url, '/');
                        $url = explode('/', $url);
                        foreach($this->blogcate as $blogcateItem){ 
                             ?>
                            <li><a href="1/<?= $blogcateItem['url'] ?>" <?= ($blogcateItem['id'] == $this->blogitem[0]['category']) ? 'class="active"' : ''  ?>><?= $blogcateItem['name'] ?></a></li>
                    <?php
                        }
                    ?>
                    <!-- <li><a href="/statics/28-trang-tinh.htm">Trang tĩnh</a></li> -->

                    <!-- <li><a href="/statics/1841-gioi-thieu.htm">Giới thiệu</a></li>
                    <li><a href="/statics/1842-danh-cho-nguoi-ban.htm">Dành cho người bán</a></li>
                    <li><a href="/statics/1843-danh-cho-nguoi-mua.htm" class="active">Dành cho người mua</a></li>
                    <li><a href="/statics/1844-cau-chuyen-thanh-cong.htm">Câu chuyện thành công</a></li>
                    <li><a href="/statics/1845-thong-bao.htm">Thông báo</a></li> -->
                </ul>
                <div class="flex md:hidden flex-wrap">
                    <div class="px-6 border-r border-solid border-gray-500 mb-2"><a href="/statics/28-trang-tinh.htm">Trang
                            tĩnh</a></div> -->
                    <div class="px-6 border-r border-solid border-gray-500 mb-2"><a href="/statics/1841-gioi-thieu.htm">Giới
                            thiệu</a></div>
                    <div class="px-6 border-r border-solid border-gray-500 mb-2"><a
                                href="/statics/1842-danh-cho-nguoi-ban.htm">Dành cho người bán</a></div>
                    <div class="px-6 border-r border-solid border-gray-500 mb-2"><a
                                href="/statics/1843-danh-cho-nguoi-mua.htm" class="text-primary">Dành cho người mua</a>
                    </div>
                    <div class="px-6 border-r border-solid border-gray-500 mb-2"><a
                                href="/statics/1844-cau-chuyen-thanh-cong.htm">Câu chuyện thành công</a></div>
                    <div class="px-6 border-r border-solid border-gray-500 mb-2"><a href="/statics/1845-thong-bao.htm">Thông
                            báo</a></div>
                </div>
            </div>
            <div class=" col-span-12  md:col-span-7 md:py-4"><h1 class="text-2xl"><?= $this->blogitem[0]['title'] ?></h1>
                <ul class="doc_tk_cnt">
                    <li><i class="icon_watch"></i><?= date('d/m/Y',strtotime($this->blogitem[0]['updated'])) ?></li>
                </ul>
                <div class="social py-2">
                    <div class="fb-like fb_iframe_widget"
                         data-href="http://123doc.org//statics-detail/14-huong-dan-tao-bo-suu-tap.htm"
                         data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"
                         fb-xfbml-state="rendered"
                         fb-iframe-plugin-query="action=like&amp;app_id=389304137790873&amp;container_width=0&amp;href=http%3A%2F%2F123doc.org%2F%2Fstatics-detail%2F14-huong-dan-tao-bo-suu-tap.htm&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=false">
                        <span style="vertical-align: bottom; width: 150px; height: 28px;"><iframe name="f3d2034dfe459c8"
                                                                                                  width="1000px"
                                                                                                  height="1000px"
                                                                                                  data-testid="fb:like Facebook Social Plugin"
                                                                                                  title="fb:like Facebook Social Plugin"
                                                                                                  frameborder="0"
                                                                                                  allowtransparency="true"
                                                                                                  allowfullscreen="true"
                                                                                                  scrolling="no"
                                                                                                  allow="encrypted-media"
                                                                                                  src="https://www.facebook.com/v2.0/plugins/like.php?action=like&amp;app_id=389304137790873&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Dfc2ae03b655298%26domain%3D123docz.net%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252F123docz.net%252Ff94ac2be170fc%26relation%3Dparent.parent&amp;container_width=0&amp;href=http%3A%2F%2F123doc.org%2F%2Fstatics-detail%2F14-huong-dan-tao-bo-suu-tap.htm&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=false"
                                                                                                  style="border: none; visibility: visible; width: 150px; height: 28px;"
                                                                                                  __idm_frm__="580"
                                                                                                  class=""></iframe></span>
                    </div>
                    <div id="___plusone_0" style="position: absolute; width: 450px; left: -10000px;">
                        <iframe ng-non-bindable="" frameborder="0" hspace="0" marginheight="0" marginwidth="0"
                                scrolling="no"
                                style="position:absolute;top:-10000px;width:450px;margin:0px;border-style:none"
                                tabindex="0" vspace="0" width="100%" id="I0_1634299459810" name="I0_1634299459810"
                                src="https://apis.google.com/u/0/se/0/_/+1/fastbutton?usegapi=1&amp;size=medium&amp;hl=vi&amp;origin=https%3A%2F%2F123docz.net&amp;url=http%3A%2F%2F123doc.org%2F%2Fstatics-detail%2F14-huong-dan-tao-bo-suu-tap.htm&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.vi.9bpzdWtHj90.O%2Fam%3DAQ%2Fd%3D1%2Frs%3DAGLTcCON5eUoDq3mFWY-T6CB_FkgnAk5bQ%2Fm%3D__features__#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1634299459810&amp;_gfid=I0_1634299459810&amp;parent=https%3A%2F%2F123docz.net&amp;pfname=&amp;rpctoken=12241616"
                                data-gapiattached="true"></iframe>
                    </div>
                    <div class="g-plusone" data-size="medium"
                         data-href="http://123doc.org//statics-detail/14-huong-dan-tao-bo-suu-tap.htm"
                         data-gapiscan="true" data-onload="true" data-gapistub="true"></div>
                </div>
                <div class="content_static text-base"><p style="text-indent:-18.0pt;mso-list:l1 level1 lfo1;"></p>
                    <?= $this->blogitem[0]['content'] ?>
                </div>
            </div>
            <div class="relase_static col-span-12 md:col-span-3 p-4"><p class="text-xl">Tin liên quan</p>
                <ul>
                    <?php
                        foreach($this->tinlienquan as $tinlienquanItem) { ?>
                            <li><a target="_blank" title="<?= $tinlienquanItem['title'] ?>"
                            href="<?= $tinlienquanItem['url'] ?>"><?= $tinlienquanItem['title'] ?></a></li>
                    <?php
                        }
                    ?>
                   
                    
                    <!-- <li><a target="_blank" title="Lỗi khi mua tài liệu &amp; Cách xử lý"
                           href="/statics-detail/932-loi-khi-mua-tai-lieu-cach-xu-ly.htm">Lỗi khi mua tài liệu &amp;
                            Cách xử lý</a></li>
                    <li><a target="_blank" title="Lưu trữ tài liệu không giới hạn!"
                           href="/statics-detail/18-luu-tru-tai-lieu-khong-gioi-han.htm">Lưu trữ tài liệu không giới
                            hạn!</a></li>
                    <li><a target="_blank" title="Quyền lợi khi nạp tiền trên 123doc.net"
                           href="/statics-detail/9-quyen-loi-khi-nap-tien-tren-123doc-net.htm">Quyền lợi khi nạp tiền
                            trên 123doc.net</a></li>
                    <li><a target="_blank" title="Thỏa thuận sử dụng" href="/statics-detail/110-thoa-thuan-su-dung.htm">Thỏa
                            thuận sử dụng</a></li> -->
                </ul>
            </div>
        </div>
    </div>
</div>