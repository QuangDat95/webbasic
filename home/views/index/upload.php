<!doctype html>
<html lang="vi">

<head>
    <title>Upload tài liệu, luận văn báo cáo, tiểu luận, bài giảng, giáo án</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="Sàn giao dịch tài liệu trực tuyến số 1 Việt Nam - Upload tài liệu, luận văn báo cáo, tiểu luận, bài giảng, giáo án" name="description" />
    <meta content="" name="keywords" />
    <meta property="fb:app_id" content="389304137790873" />
    <meta http-equiv="content-style-type" content="text/css" />
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//css/themes/default.css" />
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//css/app.css" />
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//common/css/index.min.css?v=1002" />
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//common/css/style.min.css?v=1002" />
    <link type="image/x-icon" rel="shortcut icon" href="https://media.store123doc.com/images/layout/favicon.ico" />
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2//common/js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//document/css/upload.css?v=235" />
     <script>
        let baseUrl = '<?= URL ?>';
    </script>
</head>

<body>
    <header class=" py-2 bg-white w-full z-10">
        <div class="container mx-auto flex items-center px-2">
            <div class="icon icon-logo mr-4"><a title="123doc" href="<?= URL ?>">123doc</a></div>
            <span class="showCate relative" data-ready="1" onmousemove="loadSubMenu();" href="javascript:;">
                <span class="icon"></span>
                <div class="listCategory">
                    <ul class="listParentCate">

                    </ul>
                    <div class="listSub">
                        <div class="sub_nav_1" data-rel="1"></div>
                        <div class="sub_nav_2" data-rel="2"></div>
                        <div class="sub_nav_3" data-rel="3"></div>
                    </div>
                </div>
            </span>
        </div>
    </header>

    <div id="container">
        <div class="main-upload container mx-auto pt-2">
            <h2 class="text-xl font-bold text-gray-500">Đăng bán và chia sẻ tài liệu lên thư viện điện tử lớn nhất Việt Nam</h2>
            <h4 class="text-xl font-bold text-gray-500">123Doc sẽ mang đến cho bạn hơn 10 triệu độc giả , thu nhập, danh tiếng và hơn thế nữa</h4>
            <div class="main-wrap" id="mainWrap">
                <h1 class="text-2xl">Tải tài liệu lên 123doc</h1>
                <div class="large-btn-upload" class="btn-upload" show_cl="true" id="btnUpload" onclick="showQD(this, event)"><a href="javascript:;">Tải lên</a></div>

                <p>Chọn nút tải lên để chọn nhiều file từ máy tính của bạn hoặc kéo file thả vào đây</p>
                <p><span>Ấn nút Shift hoặc Ctrl để chọn nhiều file</span></p>
            </div>
            <div class="upload_text" style="margin-bottom: 10px;">
                <h2>Lý do bạn đăng tài liệu tại 123doc.org</h2>
                <ul>
                    <li><a>
                            <i class="icon"></i>
                            Tiếp cận 10 triệu<br /> độc giả hàng tháng
                        </a></li>
                    <li><a>
                            <i class="icon"></i>
                            Xuất hiện tại Top 3<br /> các công cụ tìm kiếm
                        </a></li>
                    <li><a>
                            <i class="icon"></i>
                            Gia tăng thu nhập từ<br /> chính ấn phẩm của bạn
                        </a></li>
                    <li><a>
                            <i class="icon"></i>
                            Trải nghiệm đọc trên<br /> mọi thiết bị
                        </a></li>
                    <li><a>
                            <i class="icon"></i>
                            Phân tích báo cáo<br /> các chỉ số
                        </a></li>
                    <div class="clear"></div>
                </ul>
            </div>
            <div class='title-upload' id='hasDocument'>Upload tài liệu</div>

            <!-- // SAVE ALL MODE -->
            <div class="save-all-mode hidden mx-auto">
                <div class="cb-save-all">
                    <label>
                        <div class="checker check"><input type="checkbox" id="cbSaveAll" /><i class="deactive"></i></div>
                    </label>
                    <span>Thêm thông tin cho toàn bộ tài liệu tải lên</span>
                </div>
                <table class="upload-result hidden mx-auto">
                    <tbody style="background:#fff; border: 1px solid #f0f0f0;">
                        <tr class="tr_info">
                            <td class="colum_left">
                                <div class="change_avarta_doc">
                                    <a href="javascript:;" class="change_avatar"><img src="https://media.store123doc.com/images/web_2/bg_changeImg.jpg" /></a>

                                </div>
                            </td>
                            <td class="colum_right p-2">
                                <div class="grid grid-cols-12 upload-result gap-2  " data-id="${docId}">

                                    <label for='selFirstCat-${docId}' class="col-start-1 col-span-3 text-center">Danh mục <i class="i_req ">(*)</i>
                                    </label>
                                    <div class="col-span-8">
                                        <select class="opts-cat selCat1 px-2 py-2 w-full mb-2" data-next='.selCat2' id='selFirstCat-all'>
                                        </select>
                                        <select class="opts-cat px-2 py-2 selCat2 w-full mb-2" data-next='.selCat3' disabled="disabled">
                                        </select>
                                        <select class="opts-cat px-2 py-2 selCat3 w-full mb-2 hidden" data-next='.selCat4'>
                                        </select>
                                        <select data-next='end' class="opts-cat selCat4 px-2 py-2 w-full mb-2 hidden">
                                        </select>
                                    </div>

                                    <label for='txtKeyword-${docId}' class="col-start-1 col-span-3 text-center">Từ khóa <i class="i_req">(*)</i></label>

                                    <div class='tags col-span-8 flex rounded' data-wmin="100" data-wmax="538">
                                        <ul class='list-tag flex whitespace-no-wrap'>
                                        </ul>
                                        <input class='text-input-tag w-full' id='txtKeyword-${docId}' data-tag='true' type="text" name="" placeholder="Từ khóa" />
                                        <ul class='list-result'>
                                        </ul>
                                    </div>
                                    <div class="suggest_words col-start-3 col-span-8">
                                        <ul></ul>
                                    </div>
                                    <div class="col-start-4 col-span-8 flex justify-between">
                                        <div class="price_div mr-2">
                                            <select class='opts-set-price' id='sel-set-price-${docId}'>
                                                <option> Chọn giá bán </option>
                                                <option value="0">Miễn phí</option>
                                                <option value="2000">2.000đ</option>
                                                <option value="3000">3.000đ</option>
                                                <option value="4000">4.000đ</option>
                                                <option value="5000">5.000đ</option>
                                                <option value="7000">7.000đ</option>
                                                <option value="10000">10.000đ</option>
                                                <option value="14000">14.000đ</option>
                                                <option value="15000">15.000đ</option>
                                                <option value="20000">20.000đ</option>
                                                <option value="37000">37.000đ</option>
                                                <option value="38000">38.000đ</option>
                                                <option value="50000">50.000đ</option>
                                                <option value="77000">77.000đ</option>
                                                <option value="100000">100000đ</option>
                                                <option value="-1">Tự đặt giá</option>
                                            </select>
                                            <input type="text" class="txt-set-price">
                                            <div class="txt-show-price highlight"></div>
                                        </div>
                                        <div class="price_div flex selectView hidden">
                                            <select class="opts-set-percent">
                                                <option> Số trang xem trước </option>
                                                <option value="0"> tự chọn </option>
                                                <option value="20">20%</option>
                                                <option value="50">50%</option>
                                            </select>
                                            <span class='pd9'>trang</span>
                                            <input type="text" class="txt-page-show" id='txtPageShow-${docId}'>
                                        </div>
                                    </div>
                                    <div class="col-start-4 col-span-2">
                                        <div class="btn-save-all text-center py-2 font-semibold">Lưu</div>
                                        <i class="note_save">Hệ thống đang xử lý, vui lòng đợi trong giây lát ...</i>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <table class="upload-result mx-auto w-full" id='uploadResult'>
            </table>
        </div>
        <div class="upload-more">
        </div>
    </div>
    <div id="footer">
        <footer>
            <h3 class="doc_logo_footer text-center p-6"><a href="javascript:;"></a></h3>
            <div class="doc_list_footer container mx-auto flex justify-center justify-around md:px-24 px-2 flex-col md:flex-row text-center md:text-left">
                <ul class="doc_support_client mb-2">
                    <li>
                        <h4>Hỗ trợ khách hàng</h4>
                    </li>
                    <li>
                        <a rel="nofollow" href="mailto:info@123doc.org"><i class="icon_doc_mail"></i>info@123doc.org</a>
                    </li>
                    <li>
                        <a rel="nofollow" href="javascript:;"><i class="icon_doc_yahoo"></i>Yahoo</a>
                    </li>
                    <li>
                        <a rel="nofollow" href="javascript:;"><i class="icon_doc_skype"></i>Skype</a>
                    </li>
                </ul>
                <ul class="doc_help mb-2">
                    <li>
                        <h4>Giúp đỡ</h4>
                    </li>
                    <li>
                        <a rel="nofollow" href="/statics-detail/3-cau-hoi-thuong-gap.htm">Câu hỏi thường gặp</a>
                    </li>
                    <li>
                        <a rel="nofollow" href="/statics-detail/2-dieu-khoan-su-dung.htm">Điều khoản sử dụng</a>
                    </li>
                    <li>
                        <a rel="nofollow" href="/statics-detail/897-chinh-sach-danh-cho-nguoi-ban.htm">Quy định chính sách
                            bán tài liệu</a>
                    </li>
                    <li>
                        <a rel="nofollow" href="/statics-detail/842-huong-dan-rut-tien-qua-tai-khoan-baokim-vn.htm">Hướng
                            dẫn thanh toán</a>
                    </li>
                </ul>
                <ul class="doc_support mb-2">
                    <li>
                        <h4>Giới thiệu</h4>
                    </li>
                    <li>
                        <a rel="nofollow" href="/statics-detail/1-123doc-la-gi.htm">123doc là gì?</a>
                    </li>
                </ul>
            </div>
            <div class="mt-6 bg-main-background text-xs mt-6 p-4 text-center text-text-default">
                <div>
                    <p>Copyright © 2020 123Doc. Design by 123DOC</p>
                </div>
            </div>
        </footer>
    </div>
    <div class="over_gray"></div>
    <script type="text/javascript">
        //Object global.
        var objGlobal = new Object(),
            arrUrlSearch, scope_search;
        objGlobal.isLogin = true;
        objGlobal.PathImgDocNormal = 'https://media.store123doc.com/images/default/doc_normal.png';
        objGlobal.PathImgUserTiny = 'https://media.store123doc.com/images/default/user_tiny_small.png';
        objGlobal.PathIDVGRegister = 'https://vnid.net/register?return_url=aHR0cHM6Ly91cGxvYWQuMTIzZG9jei5uZXQvdXBsb2FkLXRhaS1saWV1Lmh0bQ==';
        objGlobal.PathIDVGLogin = 'https://id.vatgia.com/dang-nhap?ui.mode=popup&_cont=https://upload.123docz.net/home/idvg_return.php&service=123doc';
        //array url Search;
        arrUrlSearch = ["s", "doc\/s", "book\/s", "audio\/s", "collection\/s", "people\/s"];
        //biến quy định option search
        scope_search = 0;
    </script>
    <script type="text/javascript">
        var opt = parseInt('0');
    </script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2//common/js/jquery.autocomplete.js"></script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/common/js/js.template.js?v=3012"></script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2//common/js/script.min.js?v=1001"></script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2//common/js/function-js.min.js?v=1001"></script>

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KBJ9Z6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KBJ9Z6');
    </script>
    <script defer type="text/javascript">
        //Mot so config tong quat.
        var MAX_FILE_UPLOAD = 50;
        var MAX_TAG_INPUT = 6;
        var MAX_UPLOAD_REMAIN = 10000;
        var MAX_UPLOAD_USER = 10000;
        var NUM_FILE_UPLOADED = 0;
        var MAX_UPLOAD_USER_TIME_STR = '24h';
        var IS_USER = 0;
        var DOMAIN = 'https://123docz.net/';
        var MAX_FILE_UPLOAD_SIZE = parseInt('52428800');
        var MAX_FILE_UPLOAD_SIZE_STR = '50MB';
        var MAX_NUM_UPLOAD_FILE_DROPBOX = parseInt('20');
        var LIST_FILE = 'doc,docx,pdf,ppt,pptx,pot,potx,pps,ppsx';
        var uploadListFileExt = 'doc,docx,pdf,ppt,pptx,pot,potx,pps,ppsx';
        var numpageDemoMin = 10;
        var numpageDemoMax = 300;
        var moneyMin = 2000;
        var moneyMinStr = '2.000đ';
        var moneyMax = 100000;
        var moneyMaxStr = '100.000đ';
        var MAX_SIZE_FILE_MORE = 50 * 1024 * 1024; //50MB
    </script>
    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/documents/js/plupload/plupload.js"></script>
    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/documents/js/plupload/plupload.html5.js"></script>
    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/documents/js/plupload/plupload.flash.js"></script>
    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2//document/js/upload.v4.js?v=112322"></script>
    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2//document/js/tags.js?123"></script>
    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/user/js/ajaxForm.js?v=112323702"></script>
    <script defer type="text/template" id='cat_option'>
        {{if cat_parent_id == $item.cpi && cat_type == "document"}}
        <option value="${cat_id}">${cat_name}</option>
        {{/if}}
    </script>
    <script defer type="text/template" id='uploading'>
        <tbody id='${uploadId}' data-data='${$item.countItem} ${index}'{{if ($item.countItem + index) % 2 == 0 }} class="odd"{{/if}}>
{{if state}}
<tr>
	<td colspan="2">
		<div class="upload-process-text">Đang tải lên ${uploadName} <span>0%</span></div>
		<div class="btnCancelUpload">Hủy tải lên</div>
		<div class="upload-process"><div class="upload-process-bar"></div></div>
	</td>
</tr>
{{else}}
{{tmpl(objError) "#uploadError"}}
{{/if}}
</tbody>
</script>
    <script defer type="text/template" id='uploadError'>
        <tr class="tr_uploadNotifi error">
	<td colspan="2"><span><i class="icon"></i>${errorMessage} {{html uploadName}}</span></td>
</tr>
<tr></tr>
</script>
    <script defer type="text/template" id='uploadSuccess'>
        <tr class="tr_processBar">
	<td colspan="2">
		<div class="upload-process-text">100% tải lên &nbsp;•&nbsp; ${uploadName}</div>
		<div class="upload-process"><div class="upload-process-bar"></div></div>
		<input class="cb_saveAll" checked type="checkbox" value="${docId}" name="docName_saveAll[]" onclick="checkSaveAll(this)"></input>
		<input type="hidden" class="user_id" name="user_id" value="${user_id}" />
		<input type="hidden" class="code" name="code" value="${code}" />
		<div class="tr_docName_saveAll">
			<label for='txtName-${docId}'>Tên tài liệu: </label>
			<input type="text" placeholder="Tiêu đề phải dài hơn 20 kí tự."  class="txt-name p-2" id="txtName-${docId}" value="${docName}" onblur="suggestWords(this)">
		</div>
	</td>
</tr>

<tr class="tr_info">
	<td class="colum_left">
		<div class="change_avarta_doc">
			<a href="javascript:;" class="change_avatar"><img id="docImg_${docId}" alt="${docName}"  src="https://media.store123doc.com/images/web_2/bg_changeImg.jpg"/></a>
			<form class="form" name="step_2" class="step_2" id="step_${docId}" action="/documents/ajax/aja_avatar_document.php" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="doc_path" value="${docPath}"/>
				<input type="hidden" name="doc_id" value="${docId}"/>
				<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
				<input type="file" class="input_avatar" name="image" id="image" accept="image/*"  onchange="previewAvatarDocumentUpload(${docId}, this, 2000000)"  />
				<a href="javascript://" class="edit_avatar" ></a>
			</form>
			<input type="hidden" id="txtImage-${docId}" value=""/>
		</div>
	</td>
	<td class="colum_right p-2">
        <div class="grid grid-cols-12 upload-result gap-2  " data-id="${docId}">
            <label class="col-span-3 text-center" for='txtName-${docId}'>Tên tài liệu <i class="i_req">(*)</i></label>
            <input type="text" name="doc_title" placeholder="Tiêu đề phải dài hơn 20 kí tự."
                    class="txt-name col-span-8 p-2" id="txtName-${docId}" value="${docName}" onblur="suggestWords(this)"/>

            <label for='selFirstCat-${docId}' class="col-start-1 col-span-3 text-center">Danh mục <i
                        class="i_req ">(*)</i>
            </label>
            <div class="col-span-8">
                <select class="opts-cat selCat1 px-2 py-2 w-full mb-2" data-next='.selCat2' id='selFirstCat-${docId}'>
                </select>
                <select class="opts-cat px-2 py-2 selCat2 w-full mb-2" data-next='.selCat3' disabled="disabled">
                </select>
                <select class="opts-cat px-2 py-2 selCat3 w-full mb-2 hidden" data-next='.selCat4'>
                </select>
                <select data-next='end'
                        class="opts-cat selCat4 px-2 py-2 w-full mb-2 hidden">
                </select>
            </div>
            <label for='txtDes-${docId}' class="col-start-1 col-span-3 text-center">Mô tả</label>

            <textarea name="doc_des" class="txt-des col-span-8" id='txtDes-${docId}'
                        placeholder="Để có thứ hạng cao tại kết quả tìm kiếm"></textarea>

            <label for='txtKeyword-${docId}' class="col-start-1 col-span-3 text-center">Từ khóa <i
                        class="i_req">(*)</i></label>

            <div class='tags col-span-8 flex rounded' data-wmin="100" data-wmax="538">
                <ul class='list-tag flex whitespace-no-wrap'>
                </ul>
                <input class='text-input-tag w-full' id='txtKeyword-${docId}' data-tag='true'
                        type="text" name=""
                        placeholder="Từ khóa"/>
                <ul class='list-result'>
                </ul>
            </div>
            <div class="suggest_words col-start-3 col-span-8">
                <ul></ul>
            </div>
            <div class="col-start-4 col-span-8 flex justify-between">
                <div class="price_div mr-2">
                    <select class='opts-set-price' id='sel-set-price-${docId}'>
                        <option> Chọn giá bán </option>
                        <option value="0">Miễn phí</option><option value="2000">2.000đ</option><option value="3000">3.000đ</option><option value="4000">4.000đ</option><option value="5000">5.000đ</option><option value="7000">7.000đ</option><option value="10000">10.000đ</option><option value="14000">14.000đ</option><option value="15000">15.000đ</option><option value="20000">20.000đ</option><option value="37000">37.000đ</option><option value="38000">38.000đ</option><option value="50000">50.000đ</option><option value="77000">77.000đ</option><option value="100000">100000đ</option><option value="-1">Tự đặt giá</option>                        </select>
                    <input type="text" class="txt-set-price">
                    <div class="txt-show-price highlight"></div>
                </div>
                <div class="price_div flex selectView hidden">
                    <select class="opts-set-percent" >
                        <option> Số trang xem trước </option>
                        <option value="0"> tự chọn </option>
                        <option value="20">20%</option>
                        <option value="50">50%</option>
                    </select>
                    <span class='pd9'>trang</span>
                    <input type="text" class="txt-page-show" id='txtPageShow-${docId}'>
                </div>
            </div>
            <label for='txtDes-${docId}' class="col-start-1 col-span-3 text-center">File đính kèm</label>
            <div class="txt-des col-span-8" >
                <form id="frmFileMore" method="post" action="/documents/ajax_upload/aja_file_more.php" enctype="multipart/form-data">
                    <div class="fileDoc">
                        <label>File đính kèm</label>
                        <input type="file" name="fileInput" id="fileInput" onchange="changeFileInput(this)"/>
                    </div>
                    <input type="hidden" name="fileDocId" value="${docId}"/>
                    <span>Chỉ chấp nhận định dạng file ZIP/RAR (tối đa 32MB)</span>
                    <input type="text" name="fileDocName" class="fileDoc_name w-full p-2" placeholder="Tên file đính kèm (VD: quản lý lớp học viết bằng C#)"/>
                </form>
            </div>
            <div class="col-start-4 col-span-2">
                <div class="btn-save text-center py-2 font-semibold">Lưu</div>
                <i class="note_save">Hệ thống đang xử lý, vui lòng đợi trong giây lát ...</i>
            </div>
        </div>
	</td>
</tr>
<tr></tr>
</script>

    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2//user/js/user.js"></script>
    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/common/jcrop/js/jquery.Jcrop.min.js"></script>
    <script defer type="text/javascript" src="<?= URL ?>/template/static_v2/user/js/ajaxForm.js"></script>
    <link rel="stylesheet" href="<?= URL ?>/template/static_v2/common/jcrop/css/jquery.Jcrop.min.css" />

    <script defer type="text/template" id='uploadUpdateSuccess'>
        <tr class="tr_uploadNotifi success">
        <td colspan="2"><span><i class="icon"></i>Tải lên thành công</span></td>
    </tr>
    <tr class="tr_infoDoc_success">
        <td><a target="_blank" href="${docUrl}"><img src="${docImg}" onerror="this.src='https://media.store123doc.com/images/web_2/bg_changeImg.jpg'"/></a></td>
        <td>
            <div>
                <h3><a target="_blank" href="${docUrl}">${docName}</a></h3>
                <p><b>Danh mục:</b> {{html docCat}}</p>
                <p><b>Mô tả:</b> ${docDes}</p>
                <p><b>Từ khóa:</b> {{html docTag}}</p>
                <p><b>Giá bán:</b> ${price}</p>
            </div>
        </td>
    </tr>
    <tr></tr>
</script>

    <script defer type="text/template" id='uploadFail'>
        <tr class="tr_processBar">
	<td colspan="2">
		<div class="upload-process-notifi">${notifi}</div>
		<div class="upload-process"><div class="upload-process-bar"></div></div>
		<input class="cb_saveAll" checked type="checkbox" value="${docId}" name="docName_saveAll[]" onclick="checkSaveAll(this)"></input>
		<input type="hidden" class="user_id" name="user_id" value="${user_id}" />
		<input type="hidden" class="code" name="code" value="${code}" />
		<div class="tr_docName_saveAll">
			<label for='txtName-${docId}'>Tên tài liệu: </label>
			<input type="text" placeholder="Tiêu đề phải dài hơn 20 kí tự."  class="txt-name p-2" id="txtName-${docId}" value="${docName}" onblur="suggestWords(this)">
		</div>
	</td>
</tr>



<tr class="tr_info">
	<td class="colum_left">
		<div class="change_avarta_doc">
			<a href="javascript:;" class="change_avatar"><img id="docImg_${docId}" alt="${docName}"  src="https://media.store123doc.com/images/web_2/bg_changeImg.jpg"/></a>
			<form class="form" name="step_2" class="step_2" id="step_${docId}" action="/documents/ajax/aja_avatar_document.php" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="doc_path" value="${docPath}"/>
				<input type="hidden" name="doc_id" value="${docId}"/>
				<input type="hidden" id="x" name="x" />
				<input type="hidden" id="y" name="y" />
				<input type="hidden" id="w" name="w" />
				<input type="hidden" id="h" name="h" />
				<input type="file" class="input_avatar" name="image" id="image" accept="image/*"  onchange="previewAvatarDocumentUpload(${docId}, this, 2000000)"  />
				<a href="javascript://" class="edit_avatar" ></a>
			</form>
			<input type="hidden" id="txtImage-${docId}" value=""/>
		</div>
	</td>
	<td class="colum_right">
		<table class="tbl_info_document" data-id="${docId}">
			<tr>
				 <td class="info-th"><label for='txtName-${docId}'>Tên tài liệu<i class="i_req">(*)</i></label></td>
				 <td>
					  <input type="text" placeholder="Tiêu đề phải dài hơn 20 kí tự."  class="txt-name" id="txtName-${docId}" value="${docName}" onblur="suggestWords(this)">

				 </td>
			</tr>
			<tr>
				<td class="info-th"><label for='selFirstCat-${docId}'></label></td>
				<td>
					{{html catHtml}}
				</td>
			</tr>
			<tr>
				 <td class="info-th"><label for='txtKeyword-${docId}'>Từ khóa <i class="i_req">(*)</i></label></td>
				 <td>
					  <div class='tags' data-wmin="100" data-wmax="538">
							<ul class='list-tag'>
								{{html tagHtml}}
							</ul>
							<input class='text-input-tag' id='txtKeyword-${docId}' data-tag='true' type="text" name="" placeholder="Từ khóa">
							<ul class='list-result'>
							</ul>
					  </div>
				 </td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div class="suggest_words">
						<ul></ul>
				  </div>
				</td>
			</tr>
			<tr>
				 <td class="info-th default-head"><label for='txtDes-${docId}'>Mô tả</label></td>
				 <td>
					  <textarea class="txt-des" id='txtDes-${docId}' placeholder="Để có thứ hạng cao tại kết quả tìm kiếm"></textarea>
				 </td>
			</tr>
			<tr>
				<td class="info-th default-head"></td>
				<td>
					<div class="price_div">
						<select class='opts-set-price' id='sel-set-price-${docId}'>
						<option> Chọn giá bán </option>
							{{html priceHtml}}
						</select>
						<input type="text" class="txt-set-price">
				  		<div class="txt-show-price highlight"></div>
					</div>

					{{html pageViewHtml}}

				</td>
			</tr>
			<tr>
				<td class="info-th default-head"><label>File đính kèm</label></td>
				<td>
					<form id="frmFileMore" method="post" action="/documents/ajax_upload/aja_file_more.php" enctype="multipart/form-data">
						<div class="fileDoc">
							<label>File đính kèm</label>
							<input type="file" name="fileInput" id="fileInput" onchange="changeFileInput(this)"/>
						</div>
						<input type="hidden" name="fileDocId" value="${docId}"/>
						<span>Chỉ chấp nhận định dạng file ZIP/RAR (tối đa 32MB)</span>
						<p><input type="text" name="fileDocName" class="fileDoc_name" placeholder="Tên file đính kèm (VD: quản lý lớp học viết bằng C#)"/></p>
					</form>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<a href="javascript:;" class="addColDoc_btn" uId="8424741" docId="${docId}" onclick="showAddCollection(this)"><i></i>Thêm vào bộ sưu tập</a>
				</td>
			</tr>
			<tr>
				 <td></td>
				 <td>
						<p class="line_space"></p>
					  <div class="btn-save">Lưu</div>
					  <i class="note_save">Hệ thống đang xử lý, vui lòng đợi trong giây lát ...</i>
				 </td>
			</tr>
		</table>
	</td>
</tr>
<tr></tr>
</script>

    <div class="wrap_avatar">
        <div class="wrap_avatar1">
        </div>
        <div class="wrap_avatar2">
            <div class="avatar_action">
                <a class="close_avatar" href="javascript://">Đóng</a>
                <a class="up_avatar" href="javascript://">Lưu lại</a>
            </div>
            <div class="image-jcrop"></div>
        </div>
    </div>
</body>

</html>
<script defer>
    $(function() {
        $('.up_avatar').live('click', function() {
            var form_id = $(this).attr('data-id');
            $('form#step_' + form_id).ajaxForm({
                success: function(data) {
                    if (data.code == 0) {
                        var result = data.data;
                        console.log('https://media.store123doc.com' + result.file);
                        $('img#docImg_' + form_id).attr('src', 'https://media.store123doc.com' + result.file);
                        $(".wrap_avatar").hide();
                    } else {
                        alert(data.message);
                        $(".wrap_avatar").hide();
                    }
                },
                dataType: 'json'
            }).submit();
        });
    });
</script>