<div id="container" class="mainPage">
    <h1 class="hidden"><b>123doc</b> - sàn giao dịch tài liệu số 1 Việt Nam</h1>
    <h2 class="hidden">Luận văn báo cáo - Tài liệu tham khảo - Tài liệu chuyên ngành - Tài liêu kỹ thuật công nghệ, tài
        chính ngân hàng - Thi công chức</h2>
    <div id="wapper">
        <div id="slide_show" class="transition0-3 hidden md:block" style="height: 325px;overflow: hidden;">
            <div id="main_slide_show" class="container h-full mx-auto flex">
                <div class="slide_left hidden md:block">
                    <?php
                    foreach ($this->sidebar as $sidebarItem) { ?>
                        <div class="item_slidebar flex">
                            <a class="item_left_0" target="_blank" href="document/1/<?= $sidebarItem['url'] ?>" data-img="<?= $sidebarItem['sort_order'] ?>">
                                <h4><strong><?= $sidebarItem['name'] ?></strong></h4>
                                <p><?= $sidebarItem['description'] ?></p>
                                <span class="item_border_<?= $sidebarItem['sort_order'] ?>"></span>
                            </a>
                        </div>
                    <?php
                    } ?>
                </div>

                <div class="main_slideshow h-full w-full" style="max-width: 850px">
                    <div class="slideshow h-full relative" id="GTM_Banner_Hover">
                        <div class="slide-main h-full relative">
                            <?php
                            foreach ($this->slide as $slideItem) { ?>
                                <a rel="nofollow" target="_blank" href="<?= URL.'/'.$slideItem['url'] ?>" class="item_slide slick-active sld2 h-full" data-imgs="<?= $slideItem['sort_order'] ?>" style="width:1035px;"> <img gtm-element="GTM_Banner_Click" class="object-cover " style="height:325px;width:1035px;" gtm-label="GTM_Banner_Click" src="<?= $slideItem['image'] ?>" />
                                </a>
                            <?php
                            }
                            ?>
                            <ul class="slick-dots inset-x-1/2 bottom-0 w-20 absolute z-20 flex items-center p-4 transform -translate-x-1/2">
                                <?php
                                foreach ($this->slide as $slideItem) { ?>
                                    <li data-img="<?= $slideItem['sort_order'] ?>">
                                        <button type="button" data-role="none"><?= $slideItem['sort_order'] ?></button>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="doc_main_content border-b border-solid border-gray-300 container mx-auto  grid grid-cols-12 " id="dtm">
            <div class="col-span-12 md:col-span-12 ">
                <h3 class="my-4 px-4 font-bold text-2xl"></h3>
                <div class="doc_item_cnt grid md:grid-cols-4 grid-cols-2 gap-2 md:gap-4 md:pr-4 overflow-hidden p-2">
                    <?php
                    foreach ($this->data as $dataItem) { ?>
                        <div class="card-doc">
                            <a class="card-doc-img" href="document/<?= $dataItem['url'] ?>" title="document/<?= $dataItem['name'] ?>">
                                <i classs="card-doc-title" href="document/<?= $dataItem['url'] ?>"><?= $dataItem['name'] ?></a> <a rel="nofollow" class="card-doc-user-name" href="trang-ca-nhan-50-luanvan02.html" title="luanvan02">luanvan02</a>
                            <ul class="s="icon i_type_doc i_type_doc1"></i> <img style="width:124px; height:176px" width="124" height="176" alt="document/<?= $dataItem['name'] ?>" class="lazy" src="<?= URL.'/'.$dataItem['hinhanh'] ?>" data-src="<?= URL.'/'.$dataItem['hinhanh'] ?>" onerror="this.src='template/images/default/no-image.png'" />
                            </a> <a cladoc_tk_cnt">
                                <li><i class="icon_doc"></i>30</li>
                                <li><i class="icon_view"></i><?= $dataItem['view'] ?></li>
                                <li><i class="icon_down"></i><?= $dataItem['download'] ?></li>
                            </ul>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="doc_common container mx-auto px-2">
            <h3 class="my-4 px-4 font-bold text-2xl">Tài liệu chung</h3>
            <div class="grid md:grid-cols-4 grid-cols-1 md:gap-4">
                <?php
                foreach ($this->tailieuchung as $item) { ?>
                    <div class="item_doc_common mb-4">
                        <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded"><?= $item['name'] ?></h4>
                        <div class="total_doc_item_common px-2 md:px-0" id="tailieu-<?= $item['id'] ?>">
                            <script>
                                $.post('index/gettailieuchild', {
                                    id: '<?= $item['id'] ?>'
                                }, function(result) {
                                    if (result.success) {
                                        var html = '';
                                        
                                        if (result.data) {
                                            var data = result.data;
                                            for (let i = 0; i < data.length; i++) {
                                                html += '<div class="item_common">\
                                                            <a href="document/' + data[i]['url'] + '" title="' + data[i]['name'] + '" class="text-lg md:text-base">\
                                                            <i class="icon i_type_doc i_type_doc1"></i>\
                                                                ' + data[i]['name'] + '\
                                                            </a>\
                                                            <ul class="doc_tk_cnt">\
                                                                <li><i class="icon_doc"></i>8</li>\
                                                                <li><i class="icon_view"></i>' + data[i]['view'] + '</li>\
                                                                <li><i class="icon_down"></i>' + data[i]['download'] + '</li>\
                                                            </ul>\
                                                        </div>';
                                                
                                            }
                                            $('#tailieu-<?= $item['id'] ?>').append(html);
                                        }
                                    } 
                                }, 'json');
                            </script>

                        </div>
                    </div>
                <?php
                }
                ?>
                
            </div>
        </div>
    </div>