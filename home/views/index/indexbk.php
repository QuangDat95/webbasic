<div id="container" class="mainPage">
    <h1 class="hidden"><b>123doc</b> - sàn giao dịch tài liệu số 1 Việt Nam</h1>
    <h2 class="hidden">Luận văn báo cáo - Tài liệu tham khảo - Tài liệu chuyên ngành - Tài liêu kỹ thuật công nghệ, tài
        chính ngân hàng - Thi công chức</h2>
    <div id="wapper">
        <div id="slide_show" class="transition0-3 hidden md:block" style="height: 325px;overflow: hidden;">
            <div id="main_slide_show" class="container h-full mx-auto flex">
                <div class="slide_left hidden md:block">
                    <?php
                    foreach ($this->sidebar as $sidebarItem) { ?>
                        <div class="item_slidebar flex">
                            <a class="item_left_0" target="_blank" href="document/1/<?= $sidebarItem['url'] ?>" data-img="<?= $sidebarItem['sort_order'] ?>">
                                <h4><strong><?= $sidebarItem['name'] ?></strong></h4>
                                <p><?= $sidebarItem['description'] ?></p>
                                <span class="item_border_<?= $sidebarItem['sort_order'] ?>"></span>
                            </a>
                        </div>
                    <?php
                    } ?>
                </div>

                <div class="main_slideshow h-full w-full" style="max-width: 850px">
                    <div class="slideshow h-full relative" id="GTM_Banner_Hover">
                        <div class="slide-main h-full relative">
                            <?php
                            foreach ($this->slide as $slideItem) { ?>
                                <a rel="nofollow" target="_blank" href="<?= URL.'/'.$slideItem['url'] ?>" class="item_slide slick-active sld2 h-full" data-imgs="<?= $slideItem['sort_order'] ?>" style="width:1035px;"> <img gtm-element="GTM_Banner_Click" class="object-cover " style="height:325px;width:1035px;" gtm-label="GTM_Banner_Click" src="<?= $slideItem['image'] ?>" />
                                </a>
                            <?php
                            }
                            ?>
                            <ul class="slick-dots inset-x-1/2 bottom-0 w-20 absolute z-20 flex items-center p-4 transform -translate-x-1/2">
                                <?php
                                foreach ($this->slide as $slideItem) { ?>
                                    <li data-img="<?= $slideItem['sort_order'] ?>">
                                        <button type="button" data-role="none"><?= $slideItem['sort_order'] ?></button>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="doc_main_content border-b border-solid border-gray-300 container mx-auto  grid grid-cols-12 " id="dtm">
            <div class="col-span-12 md:col-span-12 ">
                <h3 class="my-4 px-4 font-bold text-2xl"></h3>
                <div class="doc_item_cnt grid md:grid-cols-4 grid-cols-2 gap-2 md:gap-4 md:pr-4 overflow-hidden p-2">
                    <?php
                    foreach ($this->data as $dataItem) { ?>
                        <div class="card-doc">
                            <a class="card-doc-img" href="document/12047-vai-tro-cua-tich-luy-tu-ban-trong-qua-trinh-hinh-thanh-va-phat-trien-kinh-te-tu-ban-chu-nghia-doc.html" title="VAI TRÒ CỦA TÍCH LUỸ TƯ BẢN TRONG QUÁ TRÌNH HÌNH THÀNH VÀ PHÁT TRIỂN KINH TẾ TƯ BẢN CHỦ NGHĨA.doc">
                                <i class="icon i_type_doc i_type_doc1"></i> <img style="width:124px; height:176px" width="124" height="176" alt="VAI TRÒ CỦA TÍCH LUỸ TƯ BẢN TRONG QUÁ TRÌNH HÌNH THÀNH VÀ PHÁT TRIỂN KINH TẾ TƯ BẢN CHỦ NGHĨA.doc" class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="https://media.123dok.info/images/document/12/pt/ag/medium_agl1346495938.png" onerror="this.src='template/images/default/doc_normal.png'" />
                            </a> <a class="card-doc-title" href="document/12047-vai-tro-cua-tich-luy-tu-ban-trong-qua-trinh-hinh-thanh-va-phat-trien-kinh-te-tu-ban-chu-nghia-doc.html" title="VAI TRÒ CỦA TÍCH LUỸ TƯ BẢN TRONG QUÁ TRÌNH HÌNH THÀNH VÀ PHÁT TRIỂN KINH TẾ TƯ BẢN CHỦ NGHĨA.doc">
                                VAI TRÒ CỦA TÍCH LUỸ TƯ BẢN TRONG QUÁ TRÌNH HÌNH THÀNH VÀ PHÁT TRIỂN KINH TẾ TƯ BẢN CHỦ
                                NGHĨA.doc </a> <a rel="nofollow" class="card-doc-user-name" href="trang-ca-nhan-50-luanvan02.html" title="luanvan02">luanvan02</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>30</li>
                                <li><i class="icon_view"></i>24,319</li>
                                <li><i class="icon_down"></i>63</li>
                            </ul>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="doc_common container mx-auto px-2">
            <h3 class="my-4 px-4 font-bold text-2xl">Tài liệu chung</h3>
            <div class="grid md:grid-cols-4 grid-cols-1 md:gap-4">
                <?php
                foreach ($this->tailieuchung as $item) { ?>
                    <div class="item_doc_common mb-4">
                        <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded"><?= $item['name'] ?></h4>
                        <div class="total_doc_item_common px-2 md:px-0" id="tailieu-<?= $item['id'] ?>">
                            <script>
                                $.post('index/gettailieuchild', {
                                    id: '<?= $item['id'] ?>'
                                }, function(result) {
                                    if (result.success) {
                                        var html = '';
                                        
                                        if (result.data) {
                                            var data = result.data;
                                            for (let i = 0; i < data.length; i++) {
                                                html += '<div class="item_common">\
                                                            <a href="document/' + data[i]['url'] + '" title="' + data[i]['name'] + '" class="text-lg md:text-base">\
                                                            <i class="icon i_type_doc i_type_doc1"></i>\
                                                                ' + data[i]['name'] + '\
                                                            </a>\
                                                            <ul class="doc_tk_cnt">\
                                                                <li><i class="icon_doc"></i>8</li>\
                                                                <li><i class="icon_view"></i>176,237</li>\
                                                                <li><i class="icon_down"></i>2,442</li>\
                                                            </ul>\
                                                        </div>';
                                                
                                            }
                                            $('#tailieu-<?= $item['id'] ?>').append(html);
                                        }
                                    } 
                                }, 'json');
                            </script>

                        </div>
                    </div>
                <?php
                }
                ?>
                <!-- <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Ngoại Ngữ</h4>
                    <div class="total_doc_item_common px-2 md:px-0">
                        <div class="item_common"><a href="document/1275951-viet-doan-van-ngan-tieng-anh-ve-cac-chu-de-cho-truoc.html" title="Viết đoạn văn ngắn tiếng anh về các chủ đề cho trước" class="text-lg md:text-base">
                                <i class="icon i_type_doc i_type_doc1"></i> Viết đoạn văn ngắn tiếng anh về các chủ đề cho
                                trước
                            </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>8</li>
                                <li><i class="icon_view"></i>176,237</li>
                                <li><i class="icon_down"></i>2,442</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/867626-mot-so-cau-truc-viet-lai-cau-trong-tieng-anh.html" title="Một số cấu trúc viết lại câu trong tiếng anh" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc3"></i> Một số cấu trúc viết lại câu trong tiếng
                                anh </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>6</li>
                                <li><i class="icon_view"></i>121,906</li>
                                <li><i class="icon_down"></i>6,602</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/522553-bai-tap-ve-cau-bi-dong-co-dap-an.html" title="Bài tập về câu bị động (có đáp án) " class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc1"></i> Bài tập về câu bị động (có đáp án) </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>4</li>
                                <li><i class="icon_view"></i>112,873</li>
                                <li><i class="icon_down"></i>6,067</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/1329192-bai-tap-trac-nghiem-ve-thi-trong-tieng-anh-co-dap-an-day-du-cac-dang-bai.html" title="bài tập trắc nghiệm về thì trong tiếng anh có đáp án đầy đủ các dạng bài" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc1"></i> bài tập trắc
                                nghiệm về thì trong tiếng anh có đáp án đầy đủ các dạng bài </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>5</li>
                                <li><i class="icon_view"></i>92,223</li>
                                <li><i class="icon_down"></i>8,740</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/1379688-7-bai-luan-tieng-anh-ve-thanh-vien-gia-dinh.html" title="7 bài luận tiếng anh về thành viên gia đình" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc1"></i> 7 bài luận tiếng anh về thành viên gia đình
                            </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>12</li>
                                <li><i class="icon_view"></i>91,436</li>
                                <li><i class="icon_down"></i>38</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/1385928-tu-hoc-tieng-phap-a1-a2-cap-toc.html" title="Tự học tiếng pháp A1-A2 cấp tốc" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Tự học tiếng pháp A1-A2 cấp tốc </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>180</li>
                                <li><i class="icon_view"></i>78,120</li>
                                <li><i class="icon_down"></i>10</li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="item_doc_common mb-4">
                    <h4 class="title_cm1 px-4 py-4 w-full text-white font-bold text-lg rounded">Giáo Dục - Đào Tạo</h4>
                    <div class="total_doc_item_common px-2 md:px-0">
                        <div class="item_common"><a href="document/418400-bang-cong-thuc-tich-phan-dao-ham-mu-logarit.html" title="Bảng công thức tích phân - đạo hàm - Mũ - logarit" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Bảng công thức tích phân - đạo hàm - Mũ -
                                logarit
                            </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>2</li>
                                <li><i class="icon_view"></i>334,025</li>
                                <li><i class="icon_down"></i>10,605</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/745149-64-tinh-huong-su-pham-thuong-gap-nhat-va-cach-giai-quyet-doi-voi-giao-vien.html" title="64 tình huống sư phạm thường gặp nhất và cách giải quyết đối với giáo viên" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc1"></i> 64 tình huống
                                sư
                                phạm thường gặp nhất và cách giải quyết đối với giáo viên </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>62</li>
                                <li><i class="icon_view"></i>214,176</li>
                                <li><i class="icon_down"></i>1,990</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/1117032-nhan-vat-ngo-tu-van-trong-chuyen-chuc-phan-su-den-tan-vien-ppt.html" title="Nhân vật Ngô Tử Văn trong Chuyện chức phán sự đền Tản Viên ppt" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Nhân vật Ngô
                                Tử
                                Văn trong Chuyện chức phán sự đền Tản Viên ppt </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>4</li>
                                <li><i class="icon_view"></i>148,226</li>
                                <li><i class="icon_down"></i>1,370</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/6179-moi-quan-he-bien-chung-giua-vat-chat-va-y-thuc-y-nghia-va-phuong-phap-luan.html" title="Mối quan hệ biện chứng giữa vật chất và ý thức, ý nghĩa và phương pháp luận." class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc3"></i> Mối quan hệ
                                biện
                                chứng giữa vật chất và ý thức, ý nghĩa và phương pháp luận. </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>3</li>
                                <li><i class="icon_view"></i>145,879</li>
                                <li><i class="icon_down"></i>2,903</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/6017684-tuyen-tap-bai-tap-trac-nghiem-hoa-hoc-moi-nhat-2020.html" title="Tuyển tập bài tập trắc nghiệm hóa học mới nhất 2020" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Tuyển tập bài tập trắc nghiệm hóa học mới
                                nhất 2020
                            </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>882</li>
                                <li><i class="icon_view"></i>128,357</li>
                                <li><i class="icon_down"></i>3</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/6015934-bai-tap-thi-nghiem-hoa-hoc-cap-nhat-xu-huong-moi-nhat-2020.html" title="Bài tập thí nghiệm hóa học cập nhật xu hướng mới nhất 2020" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Bài tập thí
                                nghiệm
                                hóa học cập nhật xu hướng mới nhất 2020 </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>23</li>
                                <li><i class="icon_view"></i>128,265</li>
                                <li><i class="icon_down"></i>5</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="item_doc_common mb-4">
                    <h4 class="title_cm2 px-4 py-4 w-full text-white font-bold text-lg rounded">Kỹ Thuật - Công
                        Nghệ</h4>
                    <div class="total_doc_item_common px-2 md:px-0">
                        <div class="item_common"><a href="document/5671902-ky-thuat-thi-cong-xay-dung.html" title="Kỹ thuật thi công xây dựng" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Kỹ thuật thi công xây dựng </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>348</li>
                                <li><i class="icon_view"></i>153,057</li>
                                <li><i class="icon_down"></i>50</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/891155-kinh-nghiem-phong-van-va-xin-viec-tai-samsung.html" title="Kinh nghiệm phỏng vấn và Xin việc tại samsung" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc3"></i> Kinh nghiệm phỏng vấn và Xin việc tại
                                samsung </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>24</li>
                                <li><i class="icon_view"></i>150,543</li>
                                <li><i class="icon_down"></i>215</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/1118316-152-lenh-tat-co-ban-trong-auto-cad.html" title="152 lệnh tắt cơ bản trong auto cad" class="text-lg md:text-base">
                                <i class="icon i_type_doc i_type_doc1"></i> 152 lệnh tắt cơ bản trong auto cad </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>4</li>
                                <li><i class="icon_view"></i>145,337</li>
                                <li><i class="icon_down"></i>5,412</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/5684236-quy-trinh-thi-cong-cap-du-ung-luc-nam-cong.html" title="Quy trình thi công cáp dự ứng lực Nam Công" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Quy trình thi công cáp dự ứng lực Nam Công
                            </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>29</li>
                                <li><i class="icon_view"></i>91,054</li>
                                <li><i class="icon_down"></i>20</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/5671326-bien-phap-thi-cong-gian-giao-truot-cong-trinh-cao-tang.html" title="Biện pháp thi công giàn giáo trượt công trình cao tầng" class="text-lg md:text-base">
                                <i class="icon i_type_doc i_type_doc2"></i> Biện pháp thi công giàn giáo trượt công trình
                                cao
                                tầng </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>45</li>
                                <li><i class="icon_view"></i>89,249</li>
                                <li><i class="icon_down"></i>22</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/5618805-bien-phap-xay-tuong-gach.html" title="Biện pháp xây tường gạch" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Biện pháp xây tường gạch </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>75</li>
                                <li><i class="icon_view"></i>89,019</li>
                                <li><i class="icon_down"></i>131</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="item_doc_common mb-4">
                    <h4 class="title_cm3 px-4 py-4 w-full text-white font-bold text-lg rounded">Công Nghệ Thông Tin</h4>
                    <div class="total_doc_item_common px-2 md:px-0">
                        <div class="item_common"><a href="document/16551-tat-ca-cac-lenh-trong-auto-cad.html" title="Tất cả các lệnh trong Auto CAD" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc1"></i> Tất cả các lệnh trong Auto CAD </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>5</li>
                                <li><i class="icon_view"></i>111,062</li>
                                <li><i class="icon_down"></i>5,854</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/1231711-152-lenh-tat-co-ban-trong-autocad-docx.html" title="152 lệnh tắt cơ bản trong AutoCAD docx" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc3"></i> 152 lệnh tắt cơ bản trong AutoCAD docx </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>10</li>
                                <li><i class="icon_view"></i>99,221</li>
                                <li><i class="icon_down"></i>1,845</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/16593-bao-mat-thong-tin-trong-cac-he-co-so-du-lieu.html" title=" Bảo mật thông tin trong các hệ cơ sở dữ liệu" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc1"></i> Bảo mật thông tin trong các hệ cơ sở dữ
                                liệu </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>7</li>
                                <li><i class="icon_view"></i>67,483</li>
                                <li><i class="icon_down"></i>299</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/1436971-cau-hoi-trac-nghiem-tin-hoc-co-dap-an.html" title="Câu hỏi trắc nghiệm tin học (Có đáp án)" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc1"></i> Câu hỏi trắc nghiệm tin học (Có đáp án)
                            </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>70</li>
                                <li><i class="icon_view"></i>63,496</li>
                                <li><i class="icon_down"></i>328</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/1152121-huong-dan-nap-tien-vao-tai-khoan-123doc-bang-the-cao-va-baokim-vn-ho-tro-tai-tai-lieu-co-thu-phi-email-tantai296-hotmail-com.html" title="Hướng dẫn nạp tiền vào tài khoản 123doc bằng thẻ cào và baokim.vn (Hỗ trợ tải tài liệu có thu phí email: tantai296@hotmail.com)" class="text-lg md:text-base"> <i class="icon i_type_doc i_type_doc2"></i> Hướng dẫn nạp
                                tiền
                                vào tài khoản 123doc bằng thẻ cào và baokim.vn (Hỗ trợ tải tài liệu có thu phí email:
                                tantai296@hotmail.com) </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>12</li>
                                <li><i class="icon_view"></i>62,356</li>
                                <li><i class="icon_down"></i>2,721</li>
                            </ul>
                        </div>
                        <div class="item_common"><a href="document/14658-cac-lenh-ve-co-ban-trong-auto-cad.html" title="Các lệnh vẽ cơ bản trong Auto CAD" class="text-lg md:text-base">
                                <i class="icon i_type_doc i_type_doc1"></i> Các lệnh vẽ cơ bản trong Auto CAD </a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i>8</li>
                                <li><i class="icon_view"></i>60,624</li>
                                <li><i class="icon_down"></i>1,742</li>
                            </ul>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>