<style>
    .options-view-button {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        margin: 0;
        opacity: 0;
        cursor: pointer;
        z-index: 3;
    }

    .option,
    .label {
        font-size: 16px;
    }

    .option:nth-child(2n)>.label {
        margin-left: 1rem;
        border-bottom: 1px solid #ccc;
    }

    .option:nth-child(2n+1)>.label {
        margin-right: 1rem;
        border-bottom: 1px solid #ccc;
        border-left: 1px solid #ccc;
    }

    .option-table {
        position: absolute;
        top: 30px;
        right: 0;
        left: 0;
        width: calc(100% - 10px);
        margin: 0 auto;
        background-color: #fff;
        border-radius: 4px;
    }

    .option {
        position: relative;
        line-height: 1;
        transition: 0.3s ease all;
        z-index: 2;
    }

    .label {
        display: none;
        padding: 0;
    }

    .label span {
        color: #f56565;
    }

    .s-c {
        position: absolute;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        cursor: pointer;
        z-index: 20;
        top: 0;
    }

    .opt-val {
        position: absolute;
        left: -1px;
        width: 200%;
        opacity: 0;
        background-color: #fff;
        transform: scale(0);
        color: #666;
        font-size: 14px;
    }

    .option input[type="radio"]:checked~.opt-val {
        opacity: 1;
        transform: scale(1);
    }

    .option:nth-child(2n+1) input[type="radio"]:checked~.opt-val {
        left: calc(-100% - 1px);
    }

    .option input[type="radio"]:checked~.label,
    .option input[type="radio"]:checked~.label span {
        color: #fff;
    }

    .option input[type="radio"]:checked~.label:before {
        content: '';
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: -1;
        background-color: #00a888;
    }

    .option input[type="radio"]:checked~.opt-val {
        display: none;
    }

    .ncc-icon input[type="radio"]:checked~.label:before {
        content: '';
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-color: #00a8883d;
        border: 2px solid #00a888;
        border-radius: .25rem;
    }
</style>

<div class="box_AddMoney col-span-12 md:col-span-8 md:col-start-3 bg-white px-5 md:px-10 py-6 rounded" id="pop_policy">
    <h3 class="text-xl md:text-2xl font-bold mb-2 text-center text-primary mt-5 md:mt-0">LƯU Ý KHI TẢI KHÔNG ĐĂNG NHẬP</h3>
    <a href="javascript:" onclick="closeBox()">×</a>
    <div style="max-height: 64vh;" class="overflow-y-scroll md:overflow-auto border border-solid border-gray-500 rounded mt-4 p-4">
        <div class="boxAddMoney_content">
            <p class="my-2"><span class="text-base">Mỗi lần thực hiện nạp tiền, toàn bộ số tiền bạn nạp vào được sử dụng để mua tài liệu mà bạn đã chọn. Nếu đăng nhập và thực hiện nạp tiền, có thể bạn sẽ mua được nhiều tài liệu hơn. Đăng nhập <a href="javascript:;" class="text-base underline" onclick="popup_login()">tại đây</a></span></p>
            <p class="my-2"><span class="text-base">Mỗi lần nạp tiền chỉ tải được <strong style="color: red;">duy nhất 1 tài liệu</strong> có phí và tài liệu miễn phí trong 24h tiếp theo.</span></p>
            <p class="my-2"><span class="text-base">Chỉ tải được tài liệu khi nạp đúng số tiền yêu cầu <strong style="color: red;">trong một lần chuyển tiền hoặc nhập thẻ cào.</strong> Bạn sẽ không tải được tài liệu nếu nạp số tiền thấp hơn.</span></p>
        </div>
    </div>
    <div class="flex justify-center md:justify-end mt-5">
        <button onclick="downloadWithLogin()" class="border border-primary hover:bg-primary-darker hover:text-white mx-2 px-6 py-2 text-primary rounded outline-none" id="pop_notlogin_btn_login">Quay lại</button>
        <button onclick="javascript:;" class="bg-primary hover:bg-primary-darker mx-2 px-6 py-2 text-white rounded outline-none" id="pop_notlogin_btn_ok">Tiếp tục</button>
    </div>
</div>

<div class="box_AddMoney col-span-12 md:col-span-8 md:col-start-3 bg-white px-5 md:px-10 py-6 hidden rounded" id="pop_confirm">
    <h3 class="text-2xl font-bold mb-2 text-center text-primary mt-5 md:mt-0">XÁC NHẬN TẢI TÀI LIỆU</h3>
    <a href="javascript:" onclick="closeBox()">×</a>
    <div style="max-height: 60vh;" class="border-t border-solid border-gray-500 mt-4 p-4">
        <p class="font-bold text-xl">Bạn đang tiến hành tải tài liệu: </p>
        <p class="md:text-2xl text-xl text-secondary"> <span class="icon i_type_doc i_type_doc1"></span></p>
        <p class="text-gray-500">
            <span class="mr-2"><i class="icon i_numpage mr-1"></i>0</span>
            <span class="mr-2"><i class="icon i_numview mr-1"></i>0</span>
            <span><i class="icon i_numdown mr-1"></i>0</span>
        </p>
    </div>
    <div class="flex justify-center md:justify-end mt-5">
        <button onclick="javascript:;" class="bg-primary hover:bg-primary-darker mx-2 px-10 py-2 text-white rounded outline-none" id="pop_notlogin_btn_confirm">Đồng ý</button>
    </div>
</div>

<div class="box_AddMoney col-span-12 md:col-span-8 md:col-start-3 bg-white px-2 md:px-4 md:px-10 py-4 hidden rounded" id="pop_addMoney_notLogin">
    <h3 class="text-xl font-bold mb-2">Tải tài liệu</h3>
    <a href="javascript:" onclick="closeBox()">×</a>
    <ul class="listOpt p-2  overflow-x-scroll md:overflow-x-auto">

        <!-- ========================= Icon QR CODE ========================= -->
        <li><a onclick="showForm_addMoney(this)" id="GTM_Naptien_Khongdangnhap_Qrcode" data-rel="qr-code" class="active" href="javascript:"><i class="icon i_m_qrcode"></i><span>QR Code</span></a></li>

        <!-- ========================= Icon VITURAL WALLET ========================= -->
        <li><a onclick="showForm_addMoney(this)" id="GTM_Naptien_KhongDangnhap_Wallet" data-rel="wallet" href="javascript:">
                <i class="icon i_m_mm"></i><span>Ví momo</span>
            </a>
        </li>

        <!-- ========================= Icon THẺ CÀO ========================= -->
        <li><a onclick="showForm_addMoney(this)" id="GTM_Naptien_KhongDangnhap_Card" data-rel="mobile" href="javascript:"><i class="icon i_m_mb"></i><span>Thẻ điện thoại</span></a></li>
    </ul>
    <div style="max-height: 61vh;" class="overflow-y-scroll md:overflow-auto border border-solid border-gray-500 rounded px-2 pb-3">
        <style>
            .payment-wallet {
                float: left;
                width: 100%;
            }

            .payment-wallet .text_input {
                width: 400px;
                float: right;
            }

            .payment-wallet .text_input strong {
                float: right;
            }

            .note-wallet {
                text-align: center;
                margin-top: 20px;
            }

            .image-wallet {
                text-align: center;
                width: 250px;
                float: left;
            }

            .tooltip {
                position: relative;
            }

            .tooltip .tooltiptext {
                visibility: hidden;
                min-width: 150px;
                white-space: nowrap;
                background-color: #555555cc;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 5px 10px;
                position: absolute;
                z-index: 1;
                top: 44px;
                right: 0;
                opacity: 0;
                transition: opacity 0.3s;
            }

            .tooltip:hover .tooltiptext {
                visibility: visible;
                opacity: 1;
            }
        </style>
        <div class="boxAddMoney_content w-full  hidden " id="wallet" style="position: relative">
            <div id="content-wallet">
                <h4 class="border-b mb-3 pl-2 py-3 text-primary text-xl font-bold">Thanh toán qua ví Momo</h4>
                <div class="info wallet-info md:w-1/2 ml-2" style="display: flex">
                    <div>
                        <div class="text_input mt-0 mb-2">
                            <span>Số tiền cần nạp: <b class="text-2xl">10,000 VNĐ</b></span>
                            <input name="pay_money_wallet" value="10000" type="text" class="hidden" />
                        </div>
                        <div class="text_input hidden">
                            <a href="javascript:" class="show_listOption block" onclick="show_listOption(this)" onblur="close_listOption(this)">Lựa chọn ví điện tử <i class="arrow"></i></a>
                            <div class="listOption listOption_bankATM w-full">
                                <span>Lựa chọn ví điện tử</span>
                                <ul style="display: flex;justify-content: flex-start">
                                    <li data-toggle="tooltip" data-placement="top" data-html="true" data-original-title="" data-value="">
                                        <a href="javascript:" class="wallets" title="Ví Momo" style="text-align: center" onclick="selectOption(this)">
                                            <img height="38" width="38" src="<?= URL ?>/template/static_v2/common/logo/logo-momo.png" alt="Ví Momo" />
                                            <input type="radio" name="pm_wallet" value="MOMO" checked />
                                            <span>Momo</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <p>
                        <div class="g-recaptcha" id="g-recaptcha-vwallet"></div>
                        </p>
                        <button type="button" class="bg-primary hover:bg-primary-darker outline-none leading-8 my-2 py-1 text-lg text-white w-full rounded" onclick="open_contact_wallet()">Tiếp tục</button>
                    </div>
                </div>
                <div class="info wallet-present" style="display: none; overflow: hidden;">
                    <!--                        <div class="flex mb-2">-->
                    <!--                            <svg class="mr-2 w-1/4 md:w-auto" width="40" height="45" viewBox="0 0 29 23" fill="none" xmlns="http://www.w3.org/2000/svg">-->
                    <!--                                <path d="M15.8182 15.8182H13.1818V9.22727H15.8182V15.8182ZM15.8182 21.0909H13.1818V18.4545H15.8182V21.0909ZM0 25.0455H29L14.5 0L0 25.0455Z" fill="#FFAA00"/>-->
                    <!--                            </svg>-->
                    <!--                            <span style="color: #FFAA00;">-->
                    <!--                            Tuyệt đối không tắt cửa sổ nạp tiền, thoát hoặc tải lại trang trong quá trình nạp. 123doc không chịu trách nhiệm với các trường hợp nhập sai.-->
                    <!--                        </span>-->
                    <!--                        </div>-->

                    <div class="payment-wallet md:flex md:flex-row-reverse border-b md:ml-4">
                        <div class="info-recharge md:mx-4 mx-2 mb-4">
                            <p class="text-primary"><b>Nội dung chuyển tiền</b></p>
                            <p class="line_ngang flex justify-between">
                                <input class="transaction-msg text-black w-56 uppercase" style="font-family: monospace;" id="wallet-msg" value="123doc 123456" readonly>
                                <a href="javascript:void(0);" class="bg-gray-100 border border-gray-400 hover:bg-gray-300 mb-2 ml-2 px-2 rounded-md tooltip" onclick="coppyToClipboard()">
                                    <svg style="display: inline" width="20" height="30" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M22.7115 6.61536H9.90382C8.08765 6.61536 6.61536 8.08765 6.61536 9.90382V22.7115C6.61536 24.5277 8.08765 26 9.90382 26H22.7115C24.5277 26 26 24.5277 26 22.7115V9.90382C26 8.08765 24.5277 6.61536 22.7115 6.61536Z" stroke="#777777" stroke-width="2.5" stroke-linejoin="round" />
                                        <path d="M21.3558 6.61538L21.3846 5.23077C21.3822 4.37466 21.041 3.55432 20.4357 2.94896C19.8303 2.3436 19.01 2.00244 18.1538 2H5.69231C4.71394 2.00289 3.77646 2.39283 3.08465 3.08465C2.39283 3.77646 2.00289 4.71394 2 5.69231V18.1538C2.00244 19.01 2.3436 19.8303 2.94896 20.4357C3.55432 21.041 4.37466 21.3822 5.23077 21.3846H6.61538" stroke="#777777" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                    Sao chép
                                    <span class="tooltiptext" id="myTooltip">Sao chép</span>
                                </a>
                            </p>
                            <em class="text-red-500">Chú ý:
                                <ul class="ml-4 text-sm">
                                    <li class="list-disc">Nhập đúng nội dung chuyển tiền, 123doc không chịu trách nhiệm với các trường hợp nhập sai.</li>
                                    <li class="list-disc">Giao dịch chuyển tiên phải có nội dung là "<b class="transaction-msg uppercase text-sm">VDTC abcxzy</b>"</li>
                                    <li class="list-disc">Tuyệt đối không nhắn tin nội dung chuyển tiền qua app momo và số điện thoại.</li>
                                </ul>
                            </em>

                            <p class="text-primary mt-2"><b>Tài khoản nhận tiền</b></p>
                            <p class="text-xl">0569.475.584</p>
                            <p class="text-xl" style="line-height: 1">Đặng Xuân Long</p>
                            <p class="text-primary mt-2"><b>Số tiền cần chuyển</b></p>
                            <p class="text-xl money-wallet">50.000đ</p>
                            <p class="text-primary"><b>Đơn hàng hết hạn sau:</b> <span class="text-black text-2xl font-bold"><span id="m-nlg"></span>:<span id="s-nlg">00</span></span></p>
                            <p class="flex items-center justify-center mt-2 mb-2 text-center">
                                <span class="loading_register bg-contain w-6 h-6 mr-2"></span> <span class="text-xs">Đang đợi bạn thanh toán...</span>
                            </p>
                        </div>
                        <div class="image-wallet md:mr-4 w-full md:w-1/2">
                            <p id="QRWalet" class="mb-4"></p>
                            <p class="text-center text-xs my-2">
                                <svg class="inline-block" width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <rect width="30" height="30" fill="url(#pattern0)" />
                                    <defs>
                                        <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                            <use xlink:href="#image0" transform="scale(0.00195312)" />
                                        </pattern>
                                        <image id="image0" width="512" height="512" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAgAElEQVR4Ae2dCbR1VXHn/8zwAQoKmlFk0sYxiUGMzOAARiC9aMAkapKO0Y7GCJFBJEkryEoUQUAFZ1vQpEXMcoptNCZGQWQQ0qZjjCJZWQhhkDmiIJBe9X33hvfd7757zz2n6txdZ//2Wm+99+49p07Vr2rvqjPtLdEgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAArUS2FrSz0r6L5KeIek5ko6QdFSin/0k7S5po1qdiN0QKJTAxqO+aX0005hiY6CNhTYm2thoY6SNlTQIpCSwvaRDJB0n6V2S/lbSjZL+Y0A/N0g6fzTgpHQSSkNgIASeMOqLQxtjzJ6/GY2hNpbamGpjKw0CRRHYRtILJJ0h6euSHhxQop9XtNwv6WxJWxTlEZSBwPAJWJ87V5L1wXn9dCjfPyDpKklvkXSoJBt7aRDoncCWko6R9JnKOuBqA8klknbo3QscEAJ1EthR0qUVJf7Vxh0rfj41uuVhYzINAqEE9pX0Xkl30vk2OOuwIoArAaHhh3AIrO1jJP8Nr3rcIek9kvYhRiDgScAeeLMHVa4g6W+Q9Ccrc7sdQIMABOIInMM4NHcc+pqkw3lYOS4Ia5BsT9UeNrrnNJno+H/DCtyY/FjSHjUEBzZCYAkE7IG/mu75dx1n/0HSSyVtugRfccjEBA6U9I9U2nMr7Wkd9LzEfkd1CJRMwN68mdbn+Gw2FysE9i/ZsehWBoGfkHSBpIfoaK0HGntFkHkCyohntBgOAbsiObRX/fouXC6S9NjhhASWeBGwzvVqHu5rnfQnO/KuXo5BDgQgsJaATcA12c/4f3Em9rDgqzhJoVeNCdjra5+lc7kOLlxuG0cXvyHgQ8BuS5Lw/Rh8gasBPoGZWYolKrtkTcfyZWAPT9IgAAE/AtanGKd8GVwvyV7tplVGwO5R/6Ekm1mKTuXP4JmVxRPmQiCawF6MVSFjtb25dDK3BKLDtxz5m0n6MJ0ppDNZMWUPUNpMZTQIQMCPgD28xsPJ/icr4xPAD0my3EAbMIE1kv6S5B+W/K0z2YRJNAhAwJ+AzYM/Tlj89mdhzwVs6+82JJZA4NGSLqMDhQ8gx5fgbHSAwAAJnMD4FT5+fVXSowYYO1WbZMn//9F5wjuPPVBpV1loEICAP4GtmQsgfAyzKys2cRBFgH/8LkWidRrO/P0vl01egrT7k0cuxcMcFAL1EDiKZwF6KQLsSgAnM8n7lT3UwTv+8cnfioFTk8cK6kMgC4HTuJrZSxFgy73zYGCWXjGhp73q90E6SnhHsVcpT5pgz78QgEAsgdfwGnP42GYnNvbGmM0US0tGwN7zn7xMzf++TOzWyt7J4gJ1ITAUAjaJjS19y7gWy+B1QwmYWuywGf6Y5Me/U9wk6WpJZ0s6gMkzaulO2FkwAbvSadMEnzPqm9ZHKQh8GdhkQfsUHAOotoLAYwqb3vdmSR+T9AZJL5L0C5J2lrQ995dWeI0/IQCBGgnYPXYbC21MtLHxVyW9UdLFkmzsLKWYsWmDbd0YWsEE7F7N5woIGpsM5zhJT+EsueBoQTUIQKBkAnZ1w8bQP5B0ZQHjuk0iZzrRCiVgS/ouq2K8S9IZkvYolA1qQQACEMhM4EmjMfbuJY7zr8wMcMi6/4SkO5cQGLdJ+uPRZawh88U2CEAAAiUQsFsG/1PS7UsY7+9gGeESQmBDHT7SczDYxDcXsPDNho7gEwhAAAI9ELDZ+uzhxwd7Hvv/Vw+2cYgFCNhTsH2ulPUdSc9eQD82hQAEIACBGAL2KvK1PRYBlmvsFUxaAQQ2kfSPPTr/f0t6RAF2owIEIAABCKwj8EhJF/WYB77BBEFlhJ69NtLHg392menYMkxGCwhAAAIQmELA3hjo65bA0VOOz0c9ErBXMqwSiy4A7hu9n9qjaRwKAhCAAARaEPg1Sff3kBf+ntcCW3jHcZcjenCyJf8XOOqMKAhAAAIQiCXwyz0VAYfFmoH0WQRsycbIs3972OOlsxTgOwhAAAIQKJKA3R6Ovh1weZGWV6CUPYUZmfxNNvf8KwgkTIQABAZL4Pge8gTrBCwhfN4b7Fh7opQGAQhAAAK5CfxZcK54d248+bTfUpLNyBR1BcDeKbXXSmgQgAAEIJCbgI3l3w3MFzYD7Va5EeXS/phAZ9p9fyb5yRUPaAsBCEBgFgG7ZRw5WdxRsw7Od74EbFWmqLN/u7VAgwAEIACBYRGwKXyj8sanhoWqXGu2DXy9wxb2Yc3ncn2PZhCAAATaEnhM4K1je118m7aKsV9zAvZOflQV90fN1WBLCEAAAhBIRuCNgfnjkGQsUqp7RpAD72JJ35TxgNIQgAAEmhKwFQTvDsohb26qBNu1J/D1IOdZYUGDAAQgAIFhEzgzKIdcOWxsy7duO0kPBDnvKcs3Dw0gAAEIQCCYwJOCcojlpu2Dda9a/KFBjruiaqoYDwEIQKAuAlcH5RKeAwiMo+OCnGZyaRCAAAQgUAeBqCmCmT4+MH7eFVQAPDlQZ0RDAAIQgEBZBH4uKJecV5aZw9LmSwFOu5k1nYcVJFgDAQhAYA6BjSTdGpBPvjjnuHzdgcCNAQ77WAd92BUCEIAABHIS+HhAPrkhJ4rytbZZliImAHpD+aajIQQgAAEIOBM4NSCn2HoDa5z1RJyknwlwlhUUL4IuBCAAAQhUR+DXg3LKT1ZHsgeD9why1jN60J1DQAACEIBAWQT2DMopTyzLzGFo88wgZ+00DDxYAQEIQAACCxDYOSincFK5gBOabnpQkLNsbmgaBCAAAQjURcBWfo14ruyAujD2Y+0RQc7avB/1OQoEIAABCBREYIugnHJ4QTYORpWjgpw1GEAYAgEIQAACCxGIuAJguYrmTIACwBko4iAAAQhUToACIEkAUAAkcRRqQgACEEhCgAIgiaMoAJI4CjUhAAEIJCFAAZDEURQASRyFmhCAAASSEKAASOIoCoAkjkJNCEAAAkkIUAAkcRQFQBJHoSYEIACBJAQoAJI4igIgiaNQEwIQgEASAhQASRxFAZDEUagJAQhAIAkBCoAkjqIASOIo1IQABCCQhAAFQBJHUQD4O2pjSbYgxt6SnifJGJf0818lPUeSLa7xaH/zlyZxa0lPlXRoYbyX4XtjYCyGtIa6zTH/i6PYtRheBtdZx7S+bn3e+r6NATU3CoAk3reAjnBWEvNd1NxKkq2pcLakqyX9KIhphJ9M5vclfV7SyZJsKc9szdh/StK9ybhH+XOlXGNibDLOo24rlb5e0hdGMbrSrtL//qGkr0t6m6TDJNkYUVOL8I/lKpozAQqA9kAtWb5f0l0DSzz/LOkPJe3YHk0ve+4u6ZKBsY8YOMcyvyxpt1480/4gj5H0R5K+PTC/3inpfaOrbu3p5NlzHHOevykAAvxPAbA41F8anTF7BneJsv59dAZTYiFgy1jfPrAk0UcM3CapxGVVLfGfI+kHFfj0c5L2WnzYSbVHRCxTAASEAAVAc6h2D9LO+B+qYJBa2YEtabyioPuaT5d0T2U+WOmPrn/fPXo+oHnkx21p98p/t8Ji7kFJ7x3YMzgro6RrjE7bnwJgJWGnvykAmoG0s6YbK086duay7KsBdi/1usr9MG1wXPSzayVt2Sz0w7Z6bCVX0mb55nuS9gsjvDzBs2xu+x0FQIA/KQDmQz1e0gMknbUPi9qA9QvzkYVtcRJ+cHto94QwL80XbE/z115QjxOhjS3HzkeWaouxbZ6/KQACQoACYHWoG0k6g4SzQcKxhx7tHnzfbRNJt+CPDfzRdpC9SZIx7bsdLMluQ7TVe6j7vUWSjTlDaBE+ogAIiAwKgNWhkvxXH6TtNae+L13uT9JwT5r7rB7+Id+YDy12IhLEEGRaETCEFuELCoCAyKAAmA7VLvtHBPGQZN4h6cnT8YV8ego+cY9Jm/uhr/Y0SfY63JD6QIQtx/XlkMDjRHChAAhwGAXAhlDtLIV7/s0Gantf+xEbIgz55DySh3vyfEeIpzYUuo2kb+G/Rv6zNwSWcYttQ6+1/4QCoD27XvekAFgft73qx8NJzZL/uJNfsD7CsP8uJIE0SiBjvzT5/dEwb60v+CP4biHf2cO2mafpbhJ7i27DFYD1+5TLfxQA62O09/wXDUy2Xzf//vok/f97K75xj82L/N20gUSbI58+sjiD92xAMs8HEf6mAAjwPwXAw1Bthr/aJvnx6qg2ffBmD6MM+ev3SCTuiTS6ANhCks054BVnNcmxWwG2HkLGFuEnCoCASKAAeBiqLYgTEbi1yPyth1GG/PV4/OMen9EFwMvxWSeffTakJ8ULjRjzKAAC/EYBsA6qLewTEbQ1ybQHAqPfK78SP7nGaWQBYLHA2X/3ccWW7c7WIsY9CoCAKKAAWAeVe//dByrr9M8NiNGVIp9PAZCmADgUX7n4KuOzABQAK0etgv+mAJDWDHBJ34gO2ESmPakf3eystYkubDOfU+QVgD/DTy5xavNtLHvdhkX7dETf4wrAol5osD0FgHQEA5XLQGWd3qYJ3rRB3HXZZGtJV+MzF59FFQCbS7LlpCMSQY0yf7lLh1nCvhE+ogAIcCQFwLp1yCMCtlaZzwqI00mR20r6NAmmc4KNKgBsiuFa4z/CbnsFNlOLYEABEBABFACcTXp3Vluxr49mD5m9mgWCOiXaqALg9RQAnfwy2Sev6qNDOR5jUn+P/ykAHB00FlV7AbAxi5O4DlTW0fuaGXAcw3Y14MWSPibpm5LuIfk09mlUAfBhfNDYB02S4w8k2ViVpTWxadFtKAACvF97AbBz4ED1V5LscnhpD/DYdMfHSLLpRhfthE22vzwgThEpWbJuwn+RbaIKgKjXNa+XdLQki+GSmvVx6+uRc4nsVJLBc3RZJAabbksBMAd6m69rLwD2DhhULaAt+Ze+tvfjgt5+sMKC5k8gUwFwQ0C/spUELWZLbtbno4oAm6k0S2ua1BfZjgIgwPu1FwCHBAxUFtR7BfgqQuSZAfbbmwA0fwKZCoC7A+Iqy4NwdiVgkcTWdFubAyNLa2rTIttRAAR4v/YC4MigzlraZf/VQuclAfbbHOY0fwKZCgCLgUUG9ybbWqxmaNb3m9iz6DY2VmVpi9rWZHsKgADv114AYH/MYBUQqtWLzFQANBnQF90mUwJY1LYm22N/9UOAPwASYN0JsHb/+/eoOIkUAHFsvSU3SeiLbkMB4O0l5Kn2BID9dRdAmYYACoA83lo0uTfZngIgj//TaEoCrDsB1u7/NB1VuV4DbJLQFt2GBJgnWhf1bZPtM/k/jadqTwDYX3cBlKajUgCsvVqZxV9NEtqi22RKgIva1mT7TPZniVNuAQQ9sZslAGovgLL4yfTkFkAebzVJaItukykBLmpbk+0z2Z8mUiNeg3sojfWiAKq8AEoUqhQAiZzVJKEtuk2mBGg5YFH75m2f6TXINKG6f4CjbkpjPQUAVwDyBCtXAPL4al4ya/N9pgLgloC8sl8e9+fRdPcAR9la7Vla7QmwdvuzxKnpSQGQx1ttEvy8fTIVANcE5JVd87g/j6Y2d7UtsDEv+Bb5/m15zOcWgLPvx3GSKATSqEoBkMZVruPpuE9lKgDOdR5XbH2R0tdWyROdE5qe7+ysAybkl/xv7WfAtdtfcmxO6kYBMEmk3P/HSdvzd6YC4CDnnPLOcl2dX7NdJN3n5DBbCjZTpVZ7Aqzd/ky9lwIgj7c8E/9YVqYCwHLAV51yyv2Sdsvj+pyanu3grAck2fK6mVrtCbB2+zPFKgVAHm+Nk7bn70wFgHlqX0kei0KdlcfteTXdTNKXOhYBr01ofu0JsHb7M4UsBUAeb3km/rGsbAWAeevEjjnlUklb5HF7bk13kHRJC4fZO5+nJjW99gRYu/2ZwpYCII+3xknb83fGAsA89iZJbeYF+Ioky0m0HglYtWW3A+y+S5PgvUFS5gkaak+AtdvfY9fqfCgKgM4IexPQZOxcdJusBYBBP1rSjQ1ziuUeu+y/eW/e4kAbELD5Ac6TZK9fTAaqVXNXSjpe0poN9sz1Qe0JsHb7M0UrBUAeb02OmR7/Zy4AzHNbSzphlDumXRGwXGNP+/PAX0Fxbk9zmkNsFqbDJe0laceC9OuqSu0JcCj2P1LSiySdJumDo0lzLGF6/nxkdGbyKknLmJSEAqBrb+9vf4+EPykjewGwkr7lEMslllPsYUHLMZneHltpC38nJjCUBNjWBdnt31nShx1fY50cdGf9b1fBnt8WfIv9KABaQFvSLrPipu13QyoAluQWDguB9QlkT4DrW7P4f5ntf+WSEv/kAG6J2S5vRjcKgGjCfvInY8TjfwoAP/8gCQJrCWROgB4uzGr/W6c8m+IxyLaVYetfbOfhkBkyKABmwCnsq7ZxNGs/CoDCnIw6+QlkTYBe5DPab/fgZw2Uy/ru85I29XLMFDkUAFOgFPpRRAxSABTqbNTKSyBjAvSknc3+x0v6UaEFgA36r/Z0zoQsCoAJIAX/SwFQsHNQDQJjAtkS4Fhvr9/Z7LcH/iIGVy+Ztg76tl7OmZBDATABpOB/veJppRzrqzQIQMCRQLYE6Gj6WlGZ7LdX/bwWrVo5sHr//WJvJ43kUQAEgQ0Q6x1TJo8CIMBRiKybQKYEGOGpTPYfU/jZ/3jQt0Qd0SgAIqjGyBzHgudvCoAYXyG1YgKZEmCEmzLZf3qSAuBbEY4aTWrkmVBMVlSx4q2nycuUAGu3P6gLIBYCvgQyJUBfy9dJy2T/B5IUAPdEOIoCgAIgKK4QC4FqCWRKgBFOymT/+5MUAHdFOEpShP3vC9K19jPg2u0PCivEQsCXQKYE6Gv5OmmZ7LdlRSMGVm+Z34xw1GhZVW9d3xikq7eeJs9iNUur3f4sfkLPyglkSoARrspkf+0PAUbYH5VUa0+AtdsfMVYhEwLuBDIlQHfjR2dVEYNVhK6PSPIa4K9FGC/J236bUClqzoKImIoqViLcVbv9EUyRCQF3AhQAMZfV3R01Enhh4bcBbpa0TZTxkjzt/1CgnrUnwNrtDwwtREPAjwAFQK4CoPSpgG2dgsjmZf8PJT0uUNHaE2Dt9geGFqIh4EeAAiBXAWCe/61CrwL8laRN/EJzVUke9r98Vek+X9SeAGu33yeKkAKBYAK/EpRMdgjW20v87wbYf7+XcjPkvDlA7y6D9lWSbKrivloX+/+kByUtBrrwnLavxWqGtmOA7cbjiAzGoyMEMhE4OKiz2hPbGdrFAfZ/vyfDX1HIyoB/LmlNTzavPMyi9ttl/5etFBD49+0BcfWxQH09Rb8owHYrAA7yVBJZEICAtGdQZ/1e8D1WD9/9apDt13ko11DGTpIuWFIh8DVJz22oZ9RmTey3p/3tgb/Ie/6T9v1LUGxZci25mT+s70+7gtH1s2eUbDi6QSAjAbtU37Vjrra/zQh3piRbHc6eNSjlxy6lRpz5jzn89RICwV6RO1rSqZJsymCb4977x5Yifqsk47fzEmycdchJ+42BTfJjMRf1qt8sff4msF9Z7JoPSulPpsdLJJ0lyfr8uB94/37ULOB8BwEItCNgl6y9O2vN8t7Rzg3sNSAC59OnXMcUe72UBgEIBBD4PIOV62D12wE+QmQuAvaWQc1FsLftn8vlfrSFQB4Cr2ewch2sS7s8nicSh6Pp7vQp1z514nBCA0sgUBaBqAcBvc8CMsj7TlmuRZslEoh6EDBDP/DW8eeX6EcODYHBE/gWZywuZyxRq8sNPgAHaODp9CmXPhW1uuQAQw6TINCOwCkMVp0Hq4ck2aVfGgSMwBMlWUx4nw3XJu9kwgkCEIgl8GhJ9zBYdRqsPxnrIqQnJPCX9KlOferfJdnMgjQIQCCYwNsYrDoNVnsF+wfx+Qg8mz7VqU+dkc/laAyBnATsKsCtDFitBqyP5nQ5WvdA4OP0qVZ96jZJWdYU6SGMOAQE4gnY/Oq13WPsau/dkn463jUcISmBn+X2Wqsxpa91G5KGFWpDwJ/AxpJs0o2uSbGm/W15WhoEZhH4HfrUQmPKZyXZWESDAAR6JmAP3UQt6DG0wsAWmaFBoAmBj1AENCoCrufSf5NwYhsIxBF4mqQ7GLBmDliXLWkZ3DivIzmSwFaSvkKfmtmnbDEhJv2JjEJkQ6AhgYMl3cuANXXA+r+Stm/Ikc0gMCZgK9p9gz41tU/ZWHPQGBS/IQCB5RN4liRWC1z/mQg787c3JmgQaEPACkeuBKzfp26XtG8bmOwDAQjEEniyJKYKXjdg2T1/u5RLg0AXAmskfZgrAWuvBNhUv3t0gcm+EIBALIFtJFnyG9pDfE3tsVf9eNo/NsZqlG6vutU8A+cHJW1do+OxGQIZCRwgySr2polzCNt9WtLjMjoLnVMQ+ClJF1TWp74t6fkpvIOSEIDAegQ2k/RSSf808EHrC5LsGQgaBPogYE+/XzTwBYS+K+nlkmwMoUEAAokJbCLpeaOzF3t9Zwhn+9+RZEv6sqpf4sBMrrqtIniaJEuWQ+hTd45uHz6HyX2SRybqQ2AVAptK+iVJJ0m6UNIVkmxCjxLnEvixJHvq2JL9X0t6p6TflrTzKrbxMQSWRWBXSfacwHmjWL12FLsWw6UVB9bXrc9fPjopsLHArqDZ2ECDAAQgAIEAAo8Y3ZK5eHRbxpZQLS059K2PMbBbVMbkJZKMEQ0CEIAABCAwCAJ2ZnUsKzg2KnZukfT7kuy2FQ0CEIAABCCQloCd0X6GM/1GyX/l1Ye/YgbHtDGP4hCAAASqJ2BzMlxD8l84+Y8Lgat597z6PgQACEAAAikJfJzk3zr5j4sAe92OBgEIQAACEEhD4AUk/87Jf1wE2CusNAhAAAIQgEAKAldRALgVAPaqKg0CEIAABCBQPAF7J3x89spvHxa7FO91FIQABCAAgeoJ/B4FgHsB9MrqowoAEIAABCBQPIEzKQDcC4Azivc6CkIAAhCAQPUEbIplLv37MjCmNAhAAAIQgEDRBGzNAgoAXwZvL9rjKAcBCEAAAhCQdAoFgHsBdDKRBQEIQAACECidwL4UAO4FwLNLdzr6QQACEIAABGwhm5spAtyKgJtYn55OBQEIQAACWQicSAHgVgD8QRanoycEIAABCEBgK0nfpQjoXAR8R9KWhBMEIAABCEAgE4EnSbqLIqB1EXCPpKdmcji6QgACEIAABMYEDpR0G0XAwkXA9yXtP4bIbwhAAAIQgEBGArtJ+gpFQOMi4O8k2XoKNAhAAAIQgMAgCBwu6ZOS7qUY2KAY+IGkT0h64SA8jREQgAAEeiJgr53tN5qA5h2SPiLpLEmvKvBM6pGSXiTpNEkflHRRwE/J9ltIrJH0FEmHSDpqyT8RMxbaWghN7TIGxsKYlNbsKoT1IetLFlPWt2ySJ5vnwfrc0Fvt9g/dv9iXnIA9aX6SpFvmnFFeKen5S7Z1Z0kflnTfHF0jElIJ9i8Z/6qHj+BtyT9zO1TSVXPi1OZ5sFc9rQ8OrdVu/9D8iT0DJPB0SdfNGaQmB3c72956CSxsKddlJP5S7F8C8saHnGTk8X/WAmAbSRcv2KfsVc+hvKlQu/2NOw0bQmCZBA6SZK9ItRmsr5a0XY/Kv7Wlnm1sa7JP3/b3iLrVoZowW3SbjAXA9pKuaRmr1hftbY/MrXb7M/sO3SsisIukW1sOVOOB/POSNu2Bmd0/HR+zpN992d8D4s6HiPBLtgLA7ud/tmOs2queu3f2xnIE1G7/cqhzVAi0IHBJx4FqPOC/usWxF9nl8ZJ+5KTrWGfP39H2L8Jqmdt6Mh3LylYAHOcUp5dJ2miZzmx57Nrtb4mN3SDQLwF7jWw8yHb9bQ8Obhuovj3w11XHyP2j7Q9E6yo6gnGmAuARDlfUVjI8zNU78cJqtz+eMEeAgBOBTzsn1Rc76TUpxl71K+Ghv5UD87S/o+yf5FHy/9O4dP0sUwHwG859yuZ5yNRqtz+Tr9C1YgL2nrT3BDL2VkBEO8Z5UO2akFbbP8r+CKZRMldj0+XzTAXAok/9z+NifTTTq4G12x/Vr5ALAVcC9trfvMFn0e+/5arhw8JOD9B1UduabB9l/8Mkyv+rCadFt8lUAHw7IFYzvRZYu/3l91A0hIAkm5xj0YF43vb2+lJE+0CArvNsafN9lP0RTKNktuE2b59MBUDb12lnMbDZDbO02u3P4if0rJyADaqzBp2230VgfX+Qrm1tnLVfhP2ZZM5i0/a7TAVAWxtn7Yf9mXoAukIgAYFMBcCbKAASRNQ6FWclsrbfkQDTuD/kpCKT//N4Ck2rJpCpAMjyEKAluNpb2yQ/a79MCWCWHW2/w/7aexX2Q8CZQKYCwN4tzvAaoA3wtbe2SW7WfiTAPFE1y49tv8vk/zyeQtOqCWQqAMxRFya5DVB1UAX5KAT9zXkAACAASURBVFMCaJvkZu2H/bX3KuyHgDOBbAVA6VMBjwdwZzelEzfm4PmbBJgnDDz9PpaVyf95PIWmVRPIVgCYs34r6AxzPNB4/K46qIL8kykBeMTQpAzsr71XYT8EnAlkLAAMwZuDkszkoNv2f2c3pRPXltus/UiAecJglh/bfpfJ/3k8haZVE8haAJjTXlHwyoBVB1VQcZYpAbRNcrP2w/7aexX2Q8CZQOYCwFDsJOmCAgsBZzelEzcrkbX9jgSYJwza+njWfpn8n8dTaFo1gewFwNh59org0ZJOlWRTBtuCPE1+vhp4tmpsm/zsJ2n3Qtd833ikm+nYxJbxNrMG8rbfnbmADjbFtc2db4tdLaO1tXHWfsa271aS/5dhf9+8OR4EeiVgnWrWoNP2u16N6HCwKPvbcLtB0vmjhNvBJJddnzDS5cag+GjDp80+torepyQd7kKluZA2us7bp88EWKL/+7S/uafZEgKJCUQlwCxIouyfN5jP+v5+SWdL2mIJEO2Y50oyHWbpmPG7L0varSemEXz6SIAl+78P+3sKDw4DgTIIRCXAMqybr0WU/R4J4BJJO8w3wW2LHSVdOsDEv9IXt0k6wI3Y6oJWHtPr7+gEWLr/o+1f3Zt8A4GBEohKgFlwRdnvNehbQu7jSsBmkr408OQ/9oktVfu04AAdH8vzd2QCzOD/SPuDwwHxECiTQFQCLNPaDbWKst9z4LfbAdHtnEqS/9gv10raMhDq+DievyMTYAb/R9ofGAqIhkC5BKISYLkWr69ZlP2eA/+PJe2xvtqu/9kDX0O85z/PBye4Ulxf2Lxjt/k+KgFm8X+U/et7jv8gUBGBqASYBWGU/W0G+Fn7nBcI1N48mHXsoX53k6RNgrhGMItKgFn8H2V/UAggFgLlE4hKgOVbvk7DKPu9E4C9IrhRAFR7zzv7q35dWO8TwNREdtFptX0jEmAm/0fYH+R+xEIgB4GoBJjD+nWT26w24Jb2+a4BUG0CotLs7FOfkwOYmsgIGyISYCb/R9gf5H7EQiAHAQqAmME6IgHsHxBSBwYlqwj7I2S+I4CpiYzQNSIBZvJ/hP1B7kcsBHIQOCJgsLovh+lrtYywP2LwN5mHBXA1mVH6ZpB7YQBTExlhe0QCzOT/CPuD3I9YCOQgsFfAYPUvOUxfq2WE/RGDv8ncM4BrJvsjuL4lgKmJjNA1IgFm8n+E/UHuRywEchB4rKSHnAesv8lh+lotI+yPGPzNRzZTm3fLYn8EU5P5Sm+gI3kR+kYkwEz+j7A/yP2IhUAeAlc5FwDH5zF9rabe9kcM/lcEMs1gfwRTk7lzENcIfaMSYBb/R9kfFAKIhUAOAjYhiteAZZPW7JLD7P/U0tN+L46TciKLqgz2T/Lw+P/y/4wA/z889JuUEZUAs/g/yn5/7yMRAokIbCXpeqci4F2J7B6r6mn/5KDt8b/NAbD1WNmA36Xb78FwmoznBrAci5x2vK6fRSXALP6Psn/sM35DoFoC1rm6PgtgicruKWZsHvZ3HeCn7W8+ObIHoKXaP42Jx2d/HszUQ8dJGZEJMIP/I+0PDgfEQ6B8Aqd1uApwb9BT6n1S62L/5GDt9f+pPQIo0X4vjivl2D3vNcFcVx7P6+/oBFi6/6PtDw4JxEOgfAKvkfTAgoWAnfk/s3zTGmnYxn6vAX6lHPPBSY009t2oFPtXsvD8+3OStvNFNlWap85jWX0kwJL934f9U53JhxCoicC+kr7WoAiwB/7enfiy/2o+bWr/eGD2/n2ZpL1XU66Hz5dtvzdPk3ezpFcFLv4z6ZYIG/pKgKX6vy/7J33J/xCojoAtPGPThNo64VdLspXTbIa/f5Vk7/mfmPBp/0WcOM3+iEHdZBpbY3y2pAOCFv1ZxHbbtk/7I7jeI+mbki6S9OuStl0UQMftI2zqMwGW6P8+7e/ofnaHAAQgAIFaCWQvALr6rXb7u/JjfwhAAAIQSEqg9gRYu/1Jwxa1IQABCECgK4HaE2Dt9neNH/aHAAQgAIGkBGpPgLXbnzRsURsCEIAABLoSqD0B1m5/1/hhfwhAAAIQSEqg9gRYu/1Jwxa1IQABCECgK4HaE2Dt9neNH/aHAAQgAIGkBGpPgLXbnzRsURsCEIAABLoSqD0B1m5/1/hhfwhAAAIQSEqg9gRYu/1Jwxa1IQABCECgK4GuS2tPS6B9LAvd1e7x/rXbP+bAbwhAAAIQqIzALQ0W05qW5Gd9tl8ihrXbn8hVqAoBCEAAAp4ErgkoAHb1VDBYVu32B+NFPAQgAAEIlErgXOcC4HuFrBLZlHft9jflxHYQgAAEIDAwAgc5FwDvTMandvuTuQt1IQABCEDAi8BGkr7qVATcL2k3L8V6klO7/T1h5jAQgAAEIFAigX0lPehQBJxVonENdKrd/gaI2AQCEIAABIZK4MSOBcClkrZIDKd2+xO7DtUhAAEIQKArgTdJavNe/Fck7dD14AXsX7v9BbgAFSAAAQhAYFkEjpZ0Y8OrAXbP3y77b74sZQOOW7v9AUgRCQEIQAACWQhsLekESVeuckXAXvWzp/2zPfDXlH/t9jflxHYQKILAJpJs9rFTJL1D0kWJfj4o6TRJx0h6ZBE0H1bCJnR51egs7yNBTLG/XP9bJOwoaS9Jh0uyh+Us6duT87W02u2vxc/YmZDAVpJOkhQxneesqU6jvrtP0oWSHr9kXxwq6aqGl4E9WWB/Gf5fcvhxeAhAAAKzCTxd0nVLSFKeCW81WT+S9IrZ5od8u42kiwtgiv3L8X9IUCEUAhCAgCcBm73rngIS1WoJ3OvzN3tCmyNre0kR86J3YYH9c5zG1xCAAARqIrCLpFsrSP7jxPk/enCuPUPx2UKZYn8PAcAhIAABCGQgcEmhiWqcsL1/231xK3oi23EFM8X+eP9HxhayIQABCLgQsCeRvRNsBnkXuNCbLuQRCa6oYP903/EpBCAAgWoIfLrSAsAeirNEHdF+IwFT7I/zf0RMIRMCEICAK4E1ku5NkKyirigc5UrzYWElPPXfhBn2P+wz/oIABCBQFQF77a9JohjqNjZZUET7dhKu2B/hfWRCAAIQSEDAJqcZanJvYtf7g3yU5XVK7A8KAMRCAAIQKJ3AIZUXAO8LctDdSbjWbr9Nb02DAAQgUCWBpyVJVE3O5tts88Ygr/9zEq61208BENQBEAsBCJRPgIcAY3zEQ4A5bi1RAMTEP1IhAIEkBD6V5Gy1zRn+rH3sNbhtg3z0kgRMa7ffYoMCIKgDIBYCEMhB4LAEyWpWIm/73YcC3WOFRemrKdZuPwVAYAdANAQgkIfAlysrAn4o6XHB7vn9gpnWbv+4aOQKQHAnQDwEIFA+gZ0TTF07HrQ9fr+8B5fYYkCfKbQIqN3+cQxRAPTQETgEBCBQPoEDJGV5fW08gLf5/Sc9umI7SVcXVgTUbv/KmKEA6LEzcCgIQKBsAk+VdG1hCWvlgN3lb7vs/bIl4N969LBZF9099q3d/mkMKQCW0CE4JAQgUC6BLSWdIOmmgRQC9rS7PfAWfc9/nkefJ+nyJTCt3f5piX/8GQXAvKjlewhAoEoCdg97H0mvl/T20VmsDZgZfj4gySa5scVuol71axsU9rzFKyWdKenDQTyHaP/1AcXTsgqAx0h6lqQjJO0naXdJG7UNqMD9Nh7pZjpaX4r4Kdn+QLSIhgAEIACBpgQsWY/P3L1+91kA2K2gEyV9XdJDU2y5QdL5o4TblEnUdk8Y6XLjFD292E/KKcn+KK7IhQAEIACBFgQyFwDHSPq3hsn0fklnS9qiBaOuu9gxz5VkOkwm6L7+X6b9XfmxPwQgAAEIBBDIWgCcvsoZ/7yEeomkHQI4riZyR0mXLjHxT/Lo2/7VuPA5BCAAAQgsmUDGAuB1HROqJeQ+rgRsJulLHXWdTOAe//dl/5JDm8NDAAIQgMAsAtkKgP0lPeiQVO12QHQ7x0FPj4Q/TUYf9kfzRT4EIAABCHQgkKkAsCf6L3NKqj+WtEcHbvN2tQf+lnnPf1rSX/lZtP3z+PA9BCAAAQgsmUCmAuBgp+Q/ToTnBbK3Nw/Gxyn1d6T9gWgRDQEIQAACHgQyFQA2b4ZnMrVX5CLmCbD3/Pt81a8tkyj7PeISGRCAAAQgEEwgUwFwjXMBYIlz1wC+NgFR26Tc934R9gcgRSQEIAABCHgTyFQA3BqQWO2hQu92YICeUYVBhP3ePJEHAQhAAAIBBDIVANNm+uuaGI8MYGoyu+rV1/6HBdiPSAhAAAIQSEAgUwEQkRRtHn7vZjIjdI2Quae38ciDAAQgAIEcBCgA/P2UpQCwKyo2UyENAhCAAAQqJEAB4O/0LAXAFf6mIxECEIAABLIQoADw91SWAuB4f9ORCAEIQAACWQhQAPh7KkMBYHMA2FLKNAhAAAIQqJQABYC/40svAOzef8TbD/4kkQgBCEAAAmEEKAD80ZZeAJzqbzISIQABCEAgGwEKAH+PlVoAPCDpJH9zkQgBCEAAAhkJUAD4e63EAsBWUdzb31QkQgACEIBAVgIUAP6eK6EAuEnS1ZLOlnRA0KJH/uSQCAEI/CeBzSTtI+lkSR+RdJUke3r3zgJnGrP1xW+XdJ2kL0qy5VBfLskWRsnYHiHppZIulvRPkv69QOYRM8N5yLSiIqJ56JZZRgRTZEIAAgUR2ETSIZL+bEBJ518knS7piQVxXk2VTSUdKyli4ZnMyWcR3SkAYqYCXi1m+RwCEEhOYPPRGee3Bn6meYmkgwr1lZ31f2bg/BdJ5G23pQCgACi0i6MWBMojYGf811aWeD4u6WcLcsU2kiLWm2+bRDPvRwFAAVBQ10YVCJRJwM447d5+5sG+i+52X/13CnGNFSRdbGHfh/lRADzMwjMuCukqqAEBCHQl8DRJ3ybprE26VgSt6Qq0w/6H4gfX4ocCgAKgQ3dkVwgMm8B+hT7J73m2sqisSyU9aklut7crFtWX7VdnRgGwOpsucbOk7sFhIQABLwLPkfRDEs7UhPuNJRQBu+KLqb7okqgoACgAvMZL5EBgMAR+UdLdJJyZCcfeEujzdsDv4Y+Z/mhTCFAAUAAMZtDGEAh4EHispBtJNo2SzYc9gDeUcSY+aeSTRQoBCgAKgIbdj80gMHwCG0v6PIlmoUTzsp7C4kL8spBfmhQCHw3ynS1j2+T4Q9zGbKdBAAIJCfxuxQNX28H4B5J26sHX78Q37kn17UF+u6ViX9nc/jQIQCAZgceM5shvmwhr3s/m4Y9up1ScVKJiy9auiGg1T9RkC/vQIACBZATOIcF0OsOMXsJ0X/zTyT/TiohnB/XRcyv21duCmCIWAhAIImBn/3Ype9ogyWfNuNjc/JHNFl+6GR+5xahdqrZnXiKarSFRa7+xJX1pEIBAIgJ/VPGA5TVQ28NP0asInoif3BLrawP750aSLq/QV5dJMttpEIBAEgLWYZnq1+eM7bRgn28l6bsVJhavIm0s5zuStgz2ld2yebAiXz0gKfo2WLDLEA+B+gg8s6JBapwAon5bco5uT5V0Dz5rfSXAJrh6SrSTRvJrumJzfE9MOQwEIOBI4PUkk9bJZFohsYujb1YTdaCk2/Dbwn77vqT9V4Ma9PmbJA15XgCz7dQgdoiFAASCCXyBRLJwIpmW+Mef9TUx0G6SvoLvGvvu7yTZegrLaEcPdHbNGyQduQygHBMCEPAhYGdF4+TF7+4sbMKePtvhkj4p6V78uEEc25stn5D0wj4dssqxtpZ0gqQrk18RsDN+s8Eu+fe5FsYqWPkYAhBoS2CHwKRxpySbv/7Fko4q6MdmO7SJe6KKnb9u64yO+9lgbPe2DymI9bL8bgyMRakJakdJe0k6IpGvTFfT2XSnQQACAyCwZ1AivF7S4wrn86Ig268r3G7UgwAEIAABCOjgoCRo9zwztIgrAfZwHg0CEIAABCBQNIFfCSoA7NZChhax+NH9GQxHRwhAAAIQqJuA3aONuBeehWrt9mfxE3pCAAIQgIAzgdoTYO32O4cT4iAAAQhAIAuB2hNg7fZniVP0hAAEIAABZwK1J8Da7XcOJ8RBAAIQgEAWArUnwNrtzxKn6AkBCEAAAs4Eak+AtdvvHE6IgwAEIACBLARqT4C1258lTtETAhCAAAScCdSeAGu33zmcEAcBCEAAAlkI1J4Aa7c/S5yiJwQgAAEIOBOoPQHWbr9zOCEOAhCAAASyEKg9AdZuf5Y4RU8IQAACEHAmUHsCrN1+53BCHAQgAAEIZCFQewKs3f4scYqeEIAABCDgTKD2BFi7/c7hhDgIQAACEMhCoPYEWLv9WeIUPSEAAQhAwJlA7QmwdvudwwlxEIAABCCQhUDtCbB2+7PEKXpCAAIQ6J3AxpJ2l7SfpCMkPUvSY3rXIu6AtSfAIdm/taSnSjpUktmV5cf6lvWxjeLCvLXkTP1/pf8tBiwW1rS2PHZHG0NtLLUxtWT/R1Go3f4orm5ynyDpfEk3SvqPiZ+HJF0l6QRJ1ukytyElwDZ+GIL9Noh+StK9E3E6Gbel/3/DqM9ZMbDslqn/z/K/xYTFxuHLBjoaK0+U9HVJNoZOxmNJ/o/AZbmiZvsjmLrL3ELSuZLunxKgkwFr/1uBYEkkaxtCAuzCPrP9ligvaRin02K31M+s750tyfpi3y1T/1/U/1+WtFvfQEfHO0bSvzWM1WX6PwpP7fZHcXWVu6OkSxsG6crB06rZ01w16U9Y5gToQSmr/QdJur1FrK6M29L/tuJmBw8nN5SRqf+39f9tkg5oyMNrs9NXOeOfF399+9/L3kk5tds/yaPI/zeT9KWOA6rdEsjWsiZAL84Z7X+SpLs6xuq8wbeU760g7+NKQKb+39X/90h6mlcHmiPndR3jtC//zzGj9de1298aXN87ntMxUG3AfEDSvn0r3vF4GRNgR5PX2z2b/VtJus4hVktJ8E30sNsB0S1L//fy/7WStgyGur+kBx1itQ//R6Co3f4IpiEy7YGfpvf85w1YVxT6NPNq4LIlwNXsaPt5Nvu7nlHMi98Sv/+xpD3aOrjBfpn6v6f/I69Y2hsdlzkkf4vHaP83CJGFN6nd/oWBLXMHe9rfc+A7cJnGLHjsbAlwQfPmbp7J/k0k3eIcq55xHynrvLmebL9Blv7v7f+bJJnMiHawc5xG+h/7IwgkkWnv+U571a/LYJbpklWmBBgRUpnst0uKXeIy8772iljEPAGZ+n+E//eJ6FSS3u4cq1H+DzK/evujuLrLtVdpvAfGq921jBOYKQFGUMhk/ykBseod+5Hydg0IgEz9P8L/JwcwNZHXBMRqhP+DzK/e/iiu7nLtcr33oHWzu5ZxAjMlwAgKmey3y6DesZpJnp0Be7dM/T/C/+/wBjqSd2tArEb4P8h81W5/FFd3uUcGBKrNC5ClZUqAEUwz2f/RgFjNVAAcFhAAmfp/hP9NZkSbNtNf11gzX2VptdufxU9rZ/HrGpjT9s8CIFMCjGCayf6LKi8A9gwIgNr9bzEV0aaNiV0/M19laV1tnbZ/Jvuz+IkCICipZAmA2hPAtIGmxM/sjMpm6vNutfufAsA7otbJi+hDFAABvso0AASYTwGUqACq+QqAza8R0TL1/wj/UwBERFXMszoUAAG+yjQABJhPAUABkOLBwuMjgn+0kFfE2VqEuhQAEVRjZEbEFAVAgK8oAGKq1QBXhYjM5P+IBBAxUHnLtHfAo5bdrt3/XAEIGVZCimoKgABfZRoAAsznCgBXAEIGK68iwO79Rz79nan/RxSAFAARo2rMSRUFQICvMg0AAeavHVy9BuuVcqIXGvFi8ZKAAsAWQIloEQlgpc9K/PvUCJArZGbq/xH+pwBYEQyOf0b0JQoARweNRWUaAMY6e/4+JCABWvA/y1PJQFlnBdhvS/VGtIgEEDFQeci0lTVfGwFxQmam/h/hfwqAiYBw+tejD0zKoABwcs5KMZkGgJV6e/29d0ACtMD9fNDc7V52m5ydJFmynuxoXf//nqeSK2RFJICutkbsb6vIWVz20TL1/wj/UwDERFlEv6AACPBVpgEgwHztHJAAx8FvRYBdCSjtdsAOkl4kyRL1WFfP35dHOEpSRALwtLutLFuVztbPsEW0Dui5cMzU/yP8TwEQ01nb9oVZ+1EABPgq0wAQYL5sNbR7gxLhrGAe8ncfinBUUAEQlQCCELiLjer/Q47vZdmWKQFGMMpkv3tHjRIYNQBE6Rsh186+IgK2VpknRTiJAiCEalT/rzX2I+3OlAAjOGSyP6SzRgiNGgAidI2S+TYKANcCaK8gR2W6BByEwF1sVP+PSAC1y8yUACN8lcl+944aJTBqAIjSN0Lu4RQAbgXAnZI2jXASVwBCqEb1/4gEULvMTAkwwleZ7A/prBFCowaACF2jZNpDendQBLgUAVH3/833XAHw7wFR/T8iAdQuM1MCjPBVJvv9e2qQxKgBIEjdMLHvowBwKQCeE+YhCoAItFH9PyIB1C4zUwKM8FUm+yP6aojMqAEgRNlAoc+gAOhcAHxLWvtWRZSbuALgTzaq/0ckgNplZkqAEb7KZL9/Tw2SGDUABKkbKvZzFAGdioDfDPUOVwAi8Eb1/4gEULvMTAkwwleZ7I/oqyEyowaAEGWDhdrT6zaPfUTwDl3mP0naLNg/XAHwBxzV/4ce78uwL1MCjOCTyX7/nhokMWoACFI3XOx7KQBaFUAHh3uGKwARiKP6f0QCqF1mpgQY4atM9kf01RCZUQNAiLI9CH104BS5EZ2iBJkf7MEvdoiPBhRnJrPmZksNlxBD6DDfD5HLQnv3gQh/UgB4e0kSBcCGUPeT9GMGxkaJ4Z8lbbshwpBP3hngk7eHaJpH6P4BTCMGf2RKNi5laRH+ogAI8D4FwHSoxzIwzi0Abpf0pOn4Qj49JcAnJ4domkfo7gFMIwZ/ZEq75gmruWNHG39SAAQEAAXA6lDfzOC4ake2BZT2WR1dyDf7Bvjj2SGa5hG6kaTrA7i2GeDZZ/XbALZyp/kqS4vwJQVAgPcpAFaHah3uLQyOGxQBd0k6cHVsYd9sIulmR3/YMry2GmTt7XxHphEDPzIlu/2VqUX4jAIgIAIoAOZDPU7SAwySawsBO1v8ufnIwrY40dEPrw3TMpdguw1wvyPXiMG/Zpnmm91yhdQGJw0e/qMACAgCCoBmUO0BHLsM5xHIWWV8VtIOzXCFbbWVpO86+OE7kmwNCNo6Amc7MM0a16XrfVbCII1gSgEQEAgUAM2h2iuC76lwsqBbJb2soHuQT5V0T4eEdbekpzR3exVbbiHpkg5MIwZ8ZEpfkbR5wgiM8B0FQEAgUAAsDvWZkuxsOCLIS5JpSfYMSVb4lNbsGYTbWvjg+5Ls1TfahgTs6g5FQDn92pL/sq+4bRglzT6JGMcoAJqxX2grCoCFcK23sS0gZFcEhraU8Dcl2etxJSb+lQ6w+6I2SDYdbP5OuV6lWmlrX3/blQC7HcAzAc3jqmn8Nd3O2Ntl/4xn/uM4bWrrIttRAIzpOv6mAOgO0+4l//Ko014lyV6RWySwl73tLZL+jyR7wO7nu+PoXcLhkj65CvcfSPqEpBf2rlXuA9qDgefx3Euv/dieMbKn/bM98Dct0iPGNAqAaaQ7fkYB0BHglN3t9cGdJP2SpOeOZls0zqX8/Iqk54yS/fZT9M/60ZrRvf1DJNmP3ee3z2jtCVgsW0Kyh2BLid+h6WHzWxjjTO/5z4soCoB5hAr53jpThLMKMQ81IAABCECgZwIROcVyFc2ZAAWAM1DEQQACEKicAAVAkgCgAEjiKNSEAAQgkIQABUASR1EAJHEUakIAAhBIQoACIImjKACSOAo1IQABCCQhQAGQxFEUAEkchZoQgAAEkhCgAEjiKAqAJI5CTQhAAAJJCFAAJHEUBUASR6EmBCAAgSQEKACSOIoCIImjUBMCEIBAEgIUAEkcRQGQxFGoCQEIQCAJAQqAJI46ImgmwMwLWSRxHWpCAAIQKI6ALSgVUQDYmh80ZwIHBTnrUc56Ig4CEIAABMonYEsYRxQAB5Rvej4NbW37CGfZYjg0CEAAAhCoi8DOQTnFll+nORPYA2c5E0UcBCAAgXoJ7BmUU55YL9I4y38myFm/GqcykiEAAQhAoFACLw7KKT9ZqL2p1domyFlvSE0F5SEAAQhAoA2BUwNyykOS1rRRhn3mE7gxwGEXzz8sW0AAAhCAwMAI/EVAPvnewBgVZc7fBjjsZkkbFWUlykAAAhCAQCSBjSXdGpBPvhipdO2y3xXgMHuz4Cm1g8V+CEAAAhUR+PmgXHJeRQx7N/XYIKcd17slHBACEIAABJZF4ISgXPKaZRlUw3EPCXLalTXAw0YIQAACEFhL4JqgXPJ8+MYR2E7SA0GO4zZAnN+QDAEIQKAUAk8KyiGWmyxH0QIJXBXkvDMCdUY0BCAAAQiUQeDMoBxyRRnmDVuLtwQ5725J2w8bHdZBAAIQqJqArf1iY33EtPJ/UjXZnow/NMh5FhB/3JMNHAYCEIAABPonEDH5z7iYeF7/5tR3RJsR8P6gIuB2STvWhxSLIQABCAyewGMl3RmUO+6TtPXgCRZi4KeDnGiV3PsLsRE1IAABCEDAj8AFgXnjE35qImkegaMCHWlzOe89TwG+hwAEIACBNAT2k2Rj+/hyvffvI9OQGICiW0q6I9CZ10p65AA4YQIEIACB2gnYq3nfDcwXdut4i9oh923/ewIdatXhx/o2iONBAAIQgIA7gY8H5wqbop7WM4F9gp1qRcAf9GwTh4MABCAAAT8CJ/aQJ7hl7OevhSRdGuxcu2f0mwtpxMYQgAAEIFACgV+T9GBwjvhaCYbWqsPhwc61qwD2yuELagWM3RCAAAQSEnhh4OviKx8gtOPQlkRgI0lX91AE/FjSf1+SjRwWAhCAAASaE3hxT8n/7yVZDqItkcAxPRQAVvHZ7YDjl2gnh4YARh9g+wAABtBJREFUBCAAgdUJWDK2e/6Rr/utPPv/b6urwjd9EdhE0j/0VASY8y/mFcG+XMtxIAABCDQiYK/6RT/tvzL529n/xo00Y6NwAvv3WPVZENg7pfuGW8UBIAABCEBgHgGb5Oe6Hk8C7QoDT/7P80rP30dO87iy8hv/bUFgx3xMz3ZyOAhAAAIQkGxlv3f3fPJn4/8HgF8eAVvoIXJ2wHHin/xtx3yj1gVjeVTQCAIQgMCwCDxakq3qF7Wwz+QYv/L/21gwrtxgelWPl4FWBoX9fY+kMyU9uVw8aAYBCEAgLYGnSDprNNZOjr99/f+KtPQqUNyeAv3LJRYB4yC0VxPtjYGn86BIBVGHiRCAQAQBe8ju5ySdIOmaAsZ1W4WW1/4iPO0ocwdJ1xcQLONi4NbR06mnSbKZqX5R0i6jWwabO9qNKAhAAALZCNgYaPfybUy0sfHXJdlY+ReSbOwcj6PL/v2vkuzWAy0BAXtC3ybvWXbQDO34N40q8XMlHUQ1nKAnoOLQCdgZ6cGS3i7JXk2zPjq0cWfZ9thssM8eeiANzb6T6QjhA8HlkuwVHBoEINA/AXv9+UrGufBxzm5B0JIRsMr4/XSO8M5hr0O+IVlsoC4EshN4TQ8L3Sz7zLuE41/Ilc68XWUzSZ+hCAgvAqyjvilvmKA5BFIROJ0xrZcx7VOSNk0VGSi7AYE1kr5KhwnvMHYl4OgN6PMBBCDgScDWPrG+VsLZ8ZB1sKXmLXfQBkDAnjLtc72AIXeMWbbdKGnrAcQLJkCgRALWt/6N5B9e/HyDid1KDP9uOlkRwJWA+DMHHpjpFqfsDYHVCNhKd7MKcL7rzsfO/C1X0AZIwC7p8ExA904ya6Cxp5JpEICAPwGbYGxW3+O7bnxsoh8u+/vHbVES7aGOD9GRwgYSuz/JAklFhTzKDICArXXCvf9uCX5WgWQL/PDA3wA6ShMT7BXB1zFZUFgR8MwmTmAbCECgMYG9OGkJGa9swji7bckUv41DcTgb7lPYtMGzKtRM3x02nBDBEggUQcD6VKYxIIOuNr0vM/wVEd7LU8LWDuC5AN/BhdkBlxfPHHmYBGzWvwxJNYuOdr+fuf2H2VcWtsou/7xS0h10MpdBZteFPcAOEIDALAK7Mza5jE23S7IlfbnkPyvaKv3OXv94Nw/bdOpo36NzVdp7MDuSgCWsklY5zXKmP9bTHqC8gAeUI0N0OLJtNUGbDGIcPPxuzuKdwwkDLIFAUQTOZ0xqNSbbKol7F+VJlCmewMaS7MEbVttqnvxt2czdivcsCkIgJ4FdJN1HEdC4CLCTuJdK2iSnu9G6BAJ26c0Kga/R8eZ2vLNKcBg6QGDABM5mHJo7Dl0m6YXcihxwL1iSafba4Ht4WHBqB/yKpM2X5BcOC4FaCGwh6RKKgA3GIHu4711c6q+lGyzXzi0lHSXJlou0y961PyNgyd9ep6RBAALxBKyvUQSsux3ySUlHSrLCiAaB3glsI+kQSW8ZPS/wQEUFgRU/dtmfM//ew44DVk7AEp7dDqjpBMTG1isk/amk57PyaOU9oFDztxsVBMdKsqd2vyjJXo0b0lUCs8ee9ueBv0KDELWqIWDzA5w30DHGxk6z7TWjhG9jKw0CKQnYKlM/LekJkp4h6WBJR4xuJdjthAw/9nqkJX0m0UgZgig9YALWJ61v2iycGcaSsY42BtpYaGOijY0/xYp8A45STIMABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQgAIE5BP4/N3Rcw1rEMcgAAAAASUVORK5CYII=" />
                                    </defs>
                                </svg>
                                Sử dụng App <b>Momo</b> hoặc<br>ứng dụng Camera có hỗ trợ QR code để quét mã.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hint ml-2">
                    <p class="hidden md:block">Nếu gặp vấn đề cần hỗ trợ <a target="_blank" rel="nofollow" href="https://chat.zalo.me/?phone=0877633123">Chat ngay</a></p>
                    <p class="md:hidden">Nếu gặp vấn đề cần hỗ trợ <a class="md:hidden" target="_blank" rel="nofollow" href="https://zalo.me/0877633123">Chat ngay</a></p>
                </div>
            </div>
            <div id="contact-wallet" class="hidden">
                <h4 class="border-b mb-3 pl-2 py-3 text-primary text-xl font-bold">Thông tin liên hệ</h4>
                <div class="md:w-1/2 ml-2">
                    <p class="text-gray-700 text-sm">• Số điện thoại liên kết với ví điện tử của bạn</p>
                    <p><input class="border-2 border-gray-500 leading-7 mb-2 px-2 rounded text-gray-900 w-full" type="text" name="phone_wallet" id="phone_wallet" placeholder="Số điện thoại liên kết ví của bạn" /></p>
                    <p><input type="checkbox" id="checkbox-wallet" class="mr-1"><label for="checkbox-wallet" class="text-gray-700 text-sm">Nhận tài liệu qua email</label></p>
                    <p><input class="border-2 border-gray-500 leading-7 mb-2 px-2 rounded text-gray-900 w-full hidden" type="text" name="email_wallet" id="email_wallet" placeholder="Nhập Email nếu có hoặc bỏ trống" /></p>
                    <p class="appendPay">
                        <a href="javascript:" onclick="pay_wallet_not_login(undefined, 'guest', 8, 'guest_1755678');" id="btn_Naptiien_Khongdangnhap_Wallet" class="bg-secondary hover:bg-secondary-darker outline-none block btn py-1 my-3 rounded text-center text-lg">Nạp tiền</a>
                    </p>
                </div>
            </div>
        </div>
        <style>
            #options-view-button-mobile:checked~#options-mobile {
                border: 1px solid #00a888;
            }

            #options-view-button-mobile:checked~#options-mobile .label {
                display: block;
                padding: 7px 14px;
            }

            #options-view-button-mobile:not(:checked)~#options-mobile .option input[type="radio"]:checked~.opt-val {
                top: -26px;
                display: block;
                padding: 4px 5px;
            }
        </style>
        <div class="boxAddMoney_content   hidden " id="mobile">
            <h4 class="font-bold">Thẻ điện thoại</h4>
            <div class="md:flex md:ml-2" id="content-mobile">
                <div class="md:w-1/2 w-full">
                    <div class="grid grid-cols-3 gap-2">
                        <div class="bg-gray-100 hover:bg-gray-300 ncc-icon py-2 relative rounded shadow-md text-center z-10" style="outline: 1px solid #cbd5e0; outline-offset: -1px;">
                            <i class="ncc_popup icon viettel"></i>
                            <input class="absolute bottom-0 h-full left-0 opacity-0 w-full" type="radio" name="ncc" value="107" />
                            <span class="label block border-0"></span>
                        </div>
                        <div class="bg-gray-100 hover:bg-gray-300 ncc-icon py-2 relative rounded shadow-md text-center z-10" style="outline: 1px solid #cbd5e0; outline-offset: -1px;">
                            <i class="ncc_popup icon mobifone"></i>
                            <input class="absolute bottom-0 h-full left-0 opacity-0 w-full" type="radio" name="ncc" value="92" />
                            <span class="label block border-0"></span>
                        </div>
                        <div class="bg-gray-100 hover:bg-gray-300 ncc-icon py-2 relative rounded shadow-md text-center z-10" style="outline: 1px solid #cbd5e0; outline-offset: -1px;">
                            <i class="ncc_popup icon vinaphone"></i>
                            <input class="absolute bottom-0 h-full left-0 opacity-0 w-full" type="radio" name="ncc" value="93" />
                            <span class="label block border-0"></span>
                        </div>
                    </div>
                    <p>
                        <span>Số tiền cần nạp: <b class="text-2xl">10,000 VNĐ</b></span>
                        <input name="pay_money_popup" value="10000" type="text" class="hidden" />
                    </p>
                    <p><input style="width:310px" name="code_popup" id="code_popup" type="text" placeholder="Mã số thẻ" /></p>
                    <p><input style="width:310px" name="seri_popup" id="seri_popup" type="text" placeholder="Seri thẻ" name="txt_seri" /></p>
                    <p>
                    <div class="g-recaptcha" id="g-recaptcha-mobile-card"></div>
                    </p>
                    <button type="button" style="width:310px" class="bg-primary hover:bg-primary-darker outline-none leading-8 my-2 py-1 text-lg text-white rounded" onclick="open_contact_mobile()">Tiếp tục</button>
                </div>

                <div class="text-sm md:ml-4">
                    <img src="https://media.store123doc.com/images/web_2//hd_mobile.png" class="md:mx-auto" style="max-height: 70%" />
                    <div class="hint mt-2">
                        <p>Mua thẻ nạp online <a href="https://www.baokim.vn/store/card-mobile/create" rel="nofollow" target="_blank">tại đây</a>
                        </p>
                    </div>
                    <div class="hint">
                        <p class="hidden md:block">Nếu gặp vấn đề cần hỗ trợ <a target="_blank" rel="nofollow" href="https://chat.zalo.me/?phone=0877633123">Chat ngay</a></p>
                        <p class="md:hidden">Nếu gặp vấn đề cần hỗ trợ <a class="md:hidden" target="_blank" rel="nofollow" href="https://zalo.me/0877633123">Chat ngay</a></p>
                    </div>
                </div>
            </div>
            <div id="contact-mobile" class="hidden">
                <h4 class="border-b mb-3 pl-2 py-3 text-primary text-xl">Thông tin liên hệ</h4>
                <div class="md:w-1/2 ml-2">
                    <p class="text-gray-700 text-sm">• Số điện thoại liên hệ với bạn trong trường hợp cần hỗ trợ</p>
                    <p><input class="border-2 border-gray-500 leading-7 mb-2 px-2 rounded text-gray-900 w-full" name="phone_popup" id="phone_popup" type="text" placeholder="Số điện thoại của bạn" /></p>
                    <p><input type="checkbox" id="checkbox-mobile" class="mr-1"><label for="checkbox-mobile" class="text-gray-700 text-sm">Nhận
                            tài liệu qua email</label></p>
                    <p><input class="border-2 border-gray-500 leading-7 mb-2 px-2 rounded text-gray-900 w-full hidden" type="text" name="email_mobile" id="email_mobile" placeholder="Nhập Email nếu có hoặc bỏ trống" /></p>
                    <p class="appendPay flex w-full">
                        <a href="javascript:" id="back-mobile" class="bg-primary hover:bg-primary-darker outline-none rounded btn py-1 mr-1 my-3 text-center text-lg w-1/2">Quay lại</a>
                        <a href="javascript:" id="GTM_Naptien_Khongdangnhap_Mobile" class="bg-secondary hover:bg-secondary-darker outline-none rounded py-1 btn ml-1 my-3 text-center text-lg w-1/2" onclick="pay_mobilecard_not_login(undefined, 'guest_1755678');">
                            Nạp tiền
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <style>
            .payment-qrcode {
                /*float: left;*/
                /*width: 100%;*/
            }

            .payment-qrcode .text_input {
                /*width: 400px;*/
                /*float: right;*/
            }

            .payment-qrcode .text_input strong {
                /*float: right;*/
            }

            .note-qrcode {
                /*text-align: center;*/
                /*margin-top: 20px;*/
            }

            .image-qr-code {
                text-align: center;
                width: 250px;
                /*float: left;*/
            }
        </style>
        <div class="boxAddMoney_content " id="qr-code">
            <div id="content-qrcode">
                <h4 class="border-b mb-3 pl-2 py-3 text-primary text-xl font-bold">Thanh toán qua QR code</h4>
                <div class="info qrcode-info w-full relative md:flex">
                    <div class="ml-2">
                        <p class="mt-0 mb-2">
                            <span>Số tiền cần nạp: <b class="text-2xl">10,000 VNĐ</b></span>
                            <input name="pay_money_qrcode" value="10000" type="text" class="hidden" />
                        </p>
                        <p>
                        <div class="g-recaptcha" id="g-recaptcha-qr-code"></div>
                        </p>
                        <button type="button" style="width:310px" class="bg-primary hover:bg-primary-darker outline-none rounded leading-8 my-2 py-1 text-lg text-white" onclick="open_contact_qrcode()">Tiếp tục</button>
                        <p class="h-0"></p>
                    </div>
                    <div class="ml-2 md:ml-4">
                        <p style="display: inline-flex; font-size: 13px; margin-bottom: 10px; margin-top: 0.75rem"><i class="icon i_check"></i>Thanh toán bằng QR code bảo mật tiện lợi hơn so với ATM Online</p>
                        <p style="display: inline-flex; font-size: 13px; margin-bottom: 10px"><i class="icon i_check"></i>Sử dụng QR Code trên ứng dụng Mobile Banking của các ngân hàng</p>
                        <p style="font-size: 13px" class="mb-3"><i class="icon i_hand_list"></i>Xem hướng dẫn quét QR với <a href="https://www.youtube.com/watch?v=oR2MIAJ1HDw" style="color: #00a888;" target="_blank">AGRIBANK</a></p>
                        <p style="font-size: 13px" class="mb-3"><i class="icon i_hand_list"></i>Xem hướng dẫn quét QR với <a href="https://www.youtube.com/watch?v=n-Z0pZB2GDw" style="color: #00a888;" target="_blank">VIETINBANK</a></p>
                        <p style="font-size: 13px" class="mb-3"><i class="icon i_hand_list"></i>Xem hướng dẫn quét QR với <a href="https://www.youtube.com/watch?v=JOzpVm84a64" style="color: #00a888;" target="_blank">VIETCOMBANK</a></p>
                        <p style="font-size: 13px" class="mb-3"><i class="icon i_hand_list"></i>Xem hướng dẫn quét QR với <a href="https://www.youtube.com/watch?v=iQTenDhOy0I" style="color: #00a888;" target="_blank">BIDV</a></p>
                    </div>
                </div>
                <div class="payment-qrcode" style="display: none">
                    <div class="md:flex mx-2 md:mx-4">
                        <div class="image-qr-code mx-auto md:mx-0 md:w-1/2" style="">
                            <p id="QRCode"></p>
                        </div>
                        <div class="text_input text-lg md:ml-6 mt-6 md:mt-0">
                            <p class="mb-3 text-base text-red-500">
                                Chú ý: Tuyệt đối không tắt cửa sổ nạp tiền, thoát hoặc tải lại trang trong quá trình nạp.
                            </p>
                            <p>Thanh toán cho: <strong class="text-black" id="qrcode_name_provider">CTCP TMTD BAO KIM</strong></p>
                            <p>Số tiền nạp: <strong class="text-black" id="qrcode_money_add"></strong></p>
                            <p>Phí nạp: <strong class="text-black" id="qrcode_fee"></strong></p>
                            <hr class="my-4">
                            <p class="flex items-center justify-center text-center">
                                <span class="loading_register bg-contain w-6 h-6 mr-2"></span> <span class="text-xs">Đang đợi bạn thanh toán...</span>
                            </p>
                            <p class="text-center">
                                <span class="text-black text-2xl font-bold"><span id="qrm-nlg"></span>:<span id="qrs-nlg">00</span></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hint ml-2">
                    <p class="hidden md:block">Nếu gặp vấn đề cần hỗ trợ <a target="_blank" rel="nofollow" href="https://chat.zalo.me/?phone=0877633123">Chat ngay</a></p>
                    <p class="md:hidden">Nếu gặp vấn đề cần hỗ trợ <a class="md:hidden" target="_blank" rel="nofollow" href="https://zalo.me/0877633123">Chat ngay</a></p>
                </div>
            </div>
            <div id="contact-qrcode" class="hidden">
                <h4 class="border-b mb-3 pl-2 py-3 text-primary text-xl">Thông tin liên hệ</h4>
                <div class="md:w-1/2 ml-2">
                    <p class="text-gray-700 text-sm">• Số điện thoại liên hệ với bạn trong trường hợp cần hỗ trợ</p>
                    <p><input class="border-2 border-gray-500 leading-7 mb-2 px-2 rounded text-gray-900 w-full" type="text" name="phone_qrcode" id="phone_qrcode" placeholder="Số điện thoại của bạn" /></p>
                    <p><input type="checkbox" id="checkbox-qrcode" class="mr-1"><label for="checkbox-qrcode" class="text-gray-700 text-sm">Nhận tài liệu qua email</label></p>
                    <p><input class="border-2 border-gray-500 leading-7 mb-2 px-2 rounded text-gray-900 w-full hidden" type="text" name="email_qrcode" id="email_qrcode" placeholder="Nhập Email nếu có hoặc bỏ trống" /></p>
                    <p class="appendPay">
                        <a href="javascript:" id="btn_Naptiien_Khongdangnhap_Qr" class="bg-secondary hover:bg-secondary-darker outline-none rounded block btn py-1 my-3 rounded text-center text-lg" onclick="pay_qrcode_not_login(undefined, 'guest_1755678');">
                            Nạp tiền
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <p class="my-1 text-right text-text-default text-xs">Phiên hoạt động: 7454088575</p>
</div>

<script type="text/javascript" src="<?= URL ?>/template/static_v2/common/js/qrcode.min.js"></script>
<script>
    function showTab(obj) {
        var _h4 = $(obj).parent(),
            _divShow = $(obj).attr('data-rel');
        $('a', _h4).removeClass('active');
        $(obj).addClass('active');
        $('.boxDownNotLogin').not('#' + _divShow).addClass('hidden');
        $('.boxDownNotLogin#' + _divShow).removeClass('hidden');
    }
</script>
<script type="text/javascript">
    var running_paymentAtm = false;

    function downloadWithLogin() {
        closeBox();
        popup_down_not_login();
    }

    function sendSMSComplete(docId) {
        $.post('/documents/ajax/aja_sms_download.php', {
            docId: docId
        }, function(data) {
            if (data.code == 1) {
                window.parent.colorboxClose();
                window.parent.location.href = data.url;
            } else {
                window.parent.alert(data.mess);
            }
        }, 'json');
    }
</script>

<script>
    var timeout_nlg = null; // Timeout
    $(document).ready(function() {
        setTimeout(function() {
            joinSocket("7454088575", ".NotificationEvent");
        }, 500);

        var cart_pending = GetCookie('CART_PENDING');
        if (!cart_pending) {
            cart_pending = '[]';
            SetCookie_path('CART_PENDING', '[]', 1, '/');
        } else {
            cart_pending = JSON.parse(cart_pending);
        }

        $('#pop_notlogin_btn_ok').click(function() {
            $('#pop_confirm').removeClass('hidden');
            $('#pop_policy').addClass('hidden');
        });
        $('#pop_notlogin_btn_confirm').click(function() {
            $('#pop_addMoney_notLogin').removeClass('hidden');
            $('#pop_confirm').addClass('hidden');
        });
        $('#checkbox-wallet').change(function() {
            if (this.checked) {
                $('#email_wallet').removeClass('hidden');
            } else {
                $('#email_wallet').addClass('hidden');
                $('#email_wallet').val('');
            }
        });
        $('#checkbox-qrcode').change(function() {
            if (this.checked) {
                $('#email_qrcode').removeClass('hidden');
            } else {
                $('#email_qrcode').addClass('hidden');
                $('#email_qrcode').val('');
            }
        });
        $('#checkbox-mobile').change(function() {
            if (this.checked) {
                $('#email_mobile').removeClass('hidden');
            } else {
                $('#email_mobile').addClass('hidden');
                $('#email_mobile').val('');
            }
        });
        $('#back-mobile').click(function() {
            $('#content-mobile').attr("style", "");
            $('#mobile>h4').removeClass('hidden');
            $('#contact-mobile').addClass('hidden');
        });
    });

    function open_contact_wallet() {
        var capcha = getCaptcha(g_recaptcha_vwallet);
        if (capcha === "") {
            alert('Xin vui lòng hoàn tất captcha!');
        } else {
            $('#content-wallet').addClass('hidden');
            $('#contact-wallet').removeClass('hidden');
        }
    }

    function open_contact_qrcode() {
        var capcha = getCaptcha(g_recaptcha_qr_code);
        if (capcha === "") {
            alert('Xin vui lòng hoàn tất captcha!');
        } else {
            $('#content-qrcode').addClass('hidden');
            $('#contact-qrcode').removeClass('hidden');
        }
    }

    function open_contact_mobile() {
        var ncc = $('input[name="ncc"]:checked');
        var capcha = getCaptcha(g_recaptcha_mobile_card);
        var seri = $('#seri_popup').val();
        seri = seri.replace(/ /g, '');
        var code = $('#code_popup').val();
        code = code.replace(/ /g, '');

        if (ncc.val() === undefined) {
            alert('Xin vui lòng chọn nhà mạng!');

        } else if (code === '') {
            alert('Xin vui lòng nhập mã cào!');
            $('#code_popup').focus();

        } else if (seri === '') {
            alert('Xin vui lòng nhập số serie!');
            $('#seri_popup').focus();

        } else if (capcha === "") {
            alert('Xin vui lòng hoàn tất captcha!');
        } else {
            $('#content-mobile').attr("style", "display:none!important");
            $('#mobile>h4').addClass('hidden');
            $('#contact-mobile').removeClass('hidden');
        }
    }
</script>
<script src="https://socket.123docz.net/js/app.js" defer></script>
<script type="text/javascript">
    function changeSecurity() {
        $('.security-code').attr('src', '/common/ajax/securitycode.php?securitycode=' + Math.random());
    }
    var running_paymentMobilecard = false;
    var docSale = 0;
    var docId = 0;
</script>

<script src="https://www.google.com/recaptcha/api.js?hl=vi&onload=CaptchaCallback&render=explicit" async defer></script>
<script type="text/javascript">
    var CaptchaCallback = function() {
        g_recaptcha_atm = genCaptcha('g-recaptcha-atm');
        g_recaptcha_mobile_card = genCaptcha('g-recaptcha-mobile-card');
        g_recaptcha_qr_code = genCaptcha('g-recaptcha-qr-code');
        g_recaptcha_vwallet = genCaptcha('g-recaptcha-vwallet');
    };

    function coppyToClipboard() {
        var copyText = document.getElementById("wallet-msg");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Đã sao chép: " + copyText.value.toUpperCase();
    }

    function outFunc() {
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Sao chép";
    }
    $(window).on('beforeunload', function(e) {
        return e.originalEvent.returnValue = "Quá trình nạp tiền đang được xử lý. Vui lòng không tắt trình duyệt...";
    });
</script>