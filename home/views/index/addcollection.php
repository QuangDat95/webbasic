
<!-- <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//common/css/style.css"/> -->
<link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//document/css/upload.css" />

<h3>Thêm vào bộ sưu tập</h3>
<form action="<?= URL ?>/index/saveaddCollection" method="POST" id="addCollectionForm">
   <input id="docid" type="hidden" value="<?= $_REQUEST['doc_id'] ?>" />
   <input type="text" class="ipt_coll" name="col_name" required="true" placeholder="Nhập tiêu đề. 80% mọi người biết đến bộ sưu tập qua tiêu đề. Tối thiểu 20 kí tự." />
   <textarea rows="3" class="ipt_description" name="col_des" required="true" placeholder="Nhập mô tả. Giúp người khác khái quát được nội dung bộ sưu tập"></textarea>
   <div class='tags flex' data-wmin="100" data-wmax="580">
      <ul class='list-tag whitespace-no-wrap flex-no-wrap'></ul>
      <input class='text-input-tag ipt_tag' id='txtKeyword-${docId}' data-tag='true' type="text" name="col_tags" placeholder="Nhập từ khóa cách nhau bằng dấu phẩy" />
      <ul class='list-result'></ul>
   </div>
   <p><button class="smt_edit_info" id="addNew" >Tạo bộ sưu tập</button></p>
</form>