<html>

<head>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/common/js/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/common/css/style.css?v=123" />
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/global/css/statics.css?v=126" />
</head>

<body>
    <div class="popupStatic">

        <div class="popupStatic_content">
            <p dir="ltr" style="margin-top:0pt;margin-bottom:0pt;text-align:center;"><span style="line-height:21.6000003814697px;"><span style="font-size:24px;"><b><br />
                            <br />
                            Các tài liệu cấm đăng tải&nbsp;trên&nbsp;website 123doc.net</b><br />
                    </span><br />
                </span></p>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong style="line-height:1.5;">a. Tài Liệu tuyệt mật</strong></p>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">Tài Liệu chiến lược an ninh quốc gia; kế hoạch phòng thủ đất nước; kế hoạch động viên đối phó với chiến tranh; các loại vũ khí, phương tiện có ý nghĩa quyết định khả năng phòng thủ đất nước;</p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Các chủ trương, chính sách về đối nội, đối ngoại của Đảng Cộng sản Việt Nam và Nhà nước Cộng hòa xã hội chủ nghĩa Việt Nam không công bố hoặc chưa công bố.</li>
                <li>Những tin của nước ngoài hoặc của tổ chức quốc tế chuyển giao cho Việt Nam mà Chính phủ xác định thuộc độ Tuyệt Mật;</li>
                <li>Tổ chức và hoạt động tình báo, phản gián do Chính Phủ quy định;</li>
                <li>Mật mã quốc gia;</li>
                <li>Dự trữ chiến lược quốc gia; các số liệu dự toán, quyết toán ngân sách nhà nước về những lĩnh vực chưa công bố; kế hoạch phát hành tiền, khóa an toàn của từng mẫu tiền và các loại giấy tờ có giá trị như tiền; phương án, kế hoạch thu đổi tiền chưa công bố;</li>
                <li>Khu vực, địa điểm cấm; tin, Tài Liệu khác mà Chính phủ xác định thuộc độ Tuyệt mật.</li>
            </ul>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong style="line-height:1.5;">b. Tài Liệu tối mật</strong></p>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">Các cuộc đàm phán và tiếp xúc cấp cao giữa nước ta với nước ngoài hoặc các tổ chức quốc tế về chính trị, quốc phòng, an ninh, đối ngoại, kinh tế, khoa học, công nghệ và các lĩnh vực khác chưa công bố.</p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Những tin của nước ngoài hoặc của các tổ chức quốc tế chuyển giao cho Việt Nam mà Chính phủ xác định thuộc độ Tối mật;</li>
                <li>Tổ chức hoạt động, trang bị, phương án tác chiến của các đơn vị vũ trang nhân dân, trừ tổ chức và hoạt động được quy định tại khoản 3 Điều 5 của Pháp lệnh; phương án sản xuất, vận chuyển và cất giữ vũ khí; công trình quan trọng phòng thủ biên giới, vùng trời, vùng biển, hải đảo;</li>
                <li>Bản đồ quân sự; tọa độ điểm hạng I, hạng II nhà nước của mạng lưới quốc gia hoàn chỉnh cùng với các ghi chú điểm kèm theo.</li>
                <li>Vị trí và trị số độ cao các mốc chính của các trạm khí tượng, thủy văn, hải văn; số liệu độ cao và số không tuyệt đối của các mốc hải văn;</li>
                <li>Số lượng tiền in, phát hành; tiền dự trữ bằng đồng Việt Nam và ngoại tệ; các số liệu về bội chi, lạm phát tiền mặt chưa công bố; phương án giá các mặt hàng chiến lược thuộc Nhà nước quản lý chưa công bố;</li>
                <li>Nơi lưu giữ và số lượng kim loại quý hiếm, đá quý, ngoại hối và vật quý hiếm khác của Nhà nước;</li>
                <li>Công trình khoa học, phát minh, sáng chế, giải pháp hữu ích, bí quyết nghề nghiệp đặc biệt quan trọng đối với quốc phòng, an ninh, kinh tế, khoa học, công nghệ mà Nhà nước chưa công bố;</li>
                <li>Kế hoạch xuất khẩu, nhập khẩu các mặt hàng đặc biệt giữ vị trí trọng yếu trong việc phát triển và bảo vệ đất nước không công bố hoặc chưa công bố;</li>
            </ul>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong style="line-height:1.5;">c. Tài Liệu không được duyệt theo quy định của website 123doc.net:</strong></p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Tài Liệu không là nguyên gốc, cắt xén 1 vài trang đầu hoặc cuối.</li>
                <li>Tài Liệu không phải là tiếng Anh, tiếng Việt.</li>
                <li>Các thông tư, nghị định, văn bản pháp luật của Nhà nước.</li>
                <li>Tài Liệu thuộc các thể loại truyện tranh, truyện ngắn và truyện dài, truyện dịch, truyện cười.</li>
                <li>Tài Liệu thuộc thể loại báo chí, ngoại trừ thể loại nghiên cứu tham khảo.</li>
                <li>Tài Liệu cắt nhỏ theo từng phần hoặc đã tồn tại trên 123doc.net nhưng nội dung trong Tài Liệu bị xáo trộn.</li>
                <li>Cách trình bày trong Tài Liệu lộn xộn, thông tin Tài Liệu không mang lại giá trị cho người dùng.</li>
                <li>Các đề thi riêng lẻ từ 1 tới 2 trang, đề thi chỉ có đáp án không có câu hỏi, các bìa tiểu luận có 1 tới 2 trang.</li>
                <li>Tài Liệu tính năng in bài trên trình duyệt để scan.</li>
                <li>Tài Liệu được định dạng chụp ảnh và copy trực tiếp lên word.</li>
                <li>Tài Liệu là danh sách thống kế 1 danh sách các Tài Liệu và được upload dưới dạng danh sách + file đính kèm.</li>
                <li>Tài Liệu tiếng Anh thuộc lĩnh vực Y tế – Sinh học.</li>
            </ul>
            <ol start="2" style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li><strong>Danh mục Tài Liệu liên quan đến hàng hóa, dịch vụ cấm quảng cáo</strong></li>
            </ol>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong>a. 123doc.net&nbsp;</strong><strong>cấm Thành Viên đăng tải các Tài Liệu liên quan đến các hàng hóa, dịch vụ sau:</strong></p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Hàng hóa, dịch vụ bị cấm kinh doanh;</li>
                <li>Thuốc lá;</li>
                <li>Rượu có nồng độ cồn từ 15 độ trở lên;</li>
                <li>Sản phẩm sữa thay thế sữa mẹ dùng cho trẻ dưới 24 tháng tuổi, sản phẩm dinh dưỡng bổ sung dùng cho trẻ dưới 06 tháng tuổi; bình bú và vú ngậm nhân tạo;</li>
                <li>Thuốc kê đơn; thuốc không kê đơn nhưng được cơ quan nhà nước có thẩm quyền khuyến cáo hạn chế sử dụng hoặc sử dụng có sự giám sát của thầy thuốc;</li>
                <li>Các loại sản phẩm, hàng hóa có tính chất kích dục;</li>
                <li>Súng săn và đạn súng săn, vũ khí thể thao và các loại sản phẩm, hàng hóa có tính chất kích động bạo lực;</li>
                <li>Các sản phẩm, hàng hóa, dịch vụ cấm quảng cáo khác do Chính phủ quy định khi có phát sinh trên thực tế.</li>
            </ul>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong>b. 123doc.net&nbsp;</strong><strong>cấm Thành Viên có các hành vi sau khi thực hiện đăng tải Tài Liệu:</strong></p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Đăng tải các hàng hóa, dịch vụ bị cấm đăng tải nêu trên;</li>
                <li>Quảng cáo làm tiết lộ bí mật nhà nước, phương hại đến độc lập, chủ quyền quốc gia, an ninh, quốc phòng;</li>
                <li>Quảng cáo thiếu thẩm mỹ, trái với truyền thống lịch sử, văn hóa, đạo đức, thuần phong mỹ tục Việt Nam;</li>
                <li>Quảng cáo làm ảnh hưởng đến mỹ quan đô thị, trật tự an toàn giao thông, an toàn xã hội;</li>
                <li>Quảng cáo gây ảnh hưởng xấu đến sự tôn nghiêm đối với Quốc kỳ, Quốc huy, Quốc ca, Đảng kỳ, anh hùng dân tộc, danh nhân văn hóa, lãnh tụ, lãnh đạo Đảng, Nhà nước;</li>
                <li>Quảng cáo có tính chất kỳ thị dân tộc, phân biệt chủng tộc, xâm phạm tự do tín ngưỡng, tôn giáo, định kiến về giới, về người khuyết tật;</li>
                <li>Quảng cáo xúc phạm uy tín, danh dự, nhân phẩm của tổ chức, cá nhân;</li>
                <li>Quảng cáo có sử dụng hình ảnh, lời nói, chữ viết của cá nhân khi chưa được cá nhân đó đồng ý, trừ trường hợp được pháp luật cho phép;</li>
                <li>Quảng cáo không đúng hoặc gây nhầm lẫn về khả năng kinh doanh, khả năng cung cấp sản phẩm, hàng hóa, dịch vụ của tổ chức, cá nhân kinh doanh sản phẩm, hàng hóa, dịch vụ; về số lượng, chất lượng, giá, công dụng, kiểu dáng, bao bì, nhãn hiệu, xuất xứ, chủng loại, phương thức phục vụ, thời hạn bảo hành của sản phẩm, hàng hoá, dịch vụ đã đăng ký hoặc đã được công bố;</li>
                <li>Quảng cáo bằng việc sử dụng phương pháp so sánh trực tiếp về giá cả, chất lượng, hiệu quả sử dụng sản phẩm, hàng hóa, dịch vụ của mình với giá cả, chất lượng, hiệu quả sử dụng sản phẩm, hàng hóa, dịch vụ cùng loại của tổ chức, cá nhân khác;</li>
                <li>Quảng cáo có sử dụng các từ ngữ “nhất”, “duy nhất”, “tốt nhất”, “số một” hoặc từ ngữ có ý nghĩa tương tự mà không có tài liệu hợp pháp chứng minh theo quy định của Bộ Văn hóa, Thể thao và Du lịch;</li>
                <li>Quảng cáo có nội dung cạnh tranh không lành mạnh theo quy định của pháp luật về cạnh tranh.</li>
                <li>Quảng cáo vi phạm pháp luật về sở hữu trí tuệ;</li>
                <li>Quảng cáo tạo cho trẻ em có suy nghĩ, lời nói, hành động trái với đạo đức, thuần phong mỹ tục; gây ảnh hưởng xấu đến sức khỏe, an toàn hoặc sự phát triển bình thường của trẻ em;</li>
                <li>Ép buộc cơ quan, tổ chức, cá nhân thực hiện quảng cáo hoặc tiếp nhận quảng cáo trái ý muốn.</li>
            </ul>
            <ol start="3" style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li><strong>Quy định về upload Tài Liệu</strong></li>
            </ol>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong>a. Tiêu đề: &nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Tiêu đề Tài Liệu không giống với tiêu đề trong nội dung Tài Liệu (Nếu có). Ví dụ như của đồ án, luận văn, tên Tài Liệu phải là tên đề tài.</li>
                <li>Tiêu đề phải phù hợp với nội dung của Tài Liệu, không quá chung chung, phải khái quát được nội dung Tài Liệu như: lời nói đầu, bìa luận văn,…</li>
                <li>Tiêu đề là tiếng Việt chuẩn, có dấu (đối với các Tài Liệu tiếng Việt), tối thiểu 20 ký tự và không chứa các ký tự đặc biệt sau: “, / , [, ] , -, _, $, #, &amp;, *, @, !.</li>
                <li>Các từ của tiêu đề phải được phân biệt bằng dấu cách, không để chữ bị dính vào nhau.</li>
            </ul>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong>b. Danh mục</strong></p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Danh mục được chọn phải phù hợp với nội dung của Tài Liệu.</li>
                <li>Tối thiểu phải chọn danh mục cấp 2. Website 123doc.net khuyến khích chọn danh mục cấp sâu hơn.</li>
            </ul>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong>c. Mô tả (không bắt buộc):</strong></p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Mô tả không được giống tiêu đề.</li>
                <li>Mô tả là tiếng Việt chuẩn, có dấu (đối với các Tài Liệu tiếng Việt), tối thiểu 200 ký tự và không chứa các ký tự đặc biệt sau: &nbsp;/ , [, ] , _, $, #, &amp;, *, @,!.</li>
            </ul>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong>d. Từ khóa</strong></p>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">Từ khóa là tiếng Việt chuẩn, có dấu (đối với các Tài Liệu tiếng Việt), tối thiểu 3 từ và không chứa các ký tự đặc biệt sau:‘,”, / , [, ] , -, _, $, #, &amp;, *, (, ), @, !.</p>
            <ol start="4" style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li><strong>Các biện pháp quản lý đối với các thông tin/Tài Liệu vi phạm bản quyền</strong></li>
            </ol>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">123doc.net thực hiện các biện pháp quản lý cần thiết để bảo vệ các Tài Liệu có bản quyền bằng cách đưa ra các quy định cụ thể như sau:</p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Trong trường hợp, Ban quản trị 123doc.net bằng các biện pháp rà soát, kiểm tra kỹ thuật và chuyên môn của mình phát hiện ra Tài Liệu/thông tin mà Thành Viên đăng tải trên 123doc.net là vi phạm bản quyền, không được sự cho phép của tác giả/người nắm giữ quyền thì Ban quản trị 123doc.net sẽ xóa Tài Liệu/thông tin đó ngay khi phát hiện ra sự vi phạm này.</li>
                <li>Trong trường hợp, Người Dùng/Thành Viên có phản ánh hoặc khiếu nại về Tài Liệu vi phạm bản quyền được đăng tải trên 123doc.net. Nếu phản ánh hoặc khiếu nại được gửi kèm cùng các tài liệu/giấy tờ chứng minh phản ánh hoặc khiếu nại này là có căn cứ và chính xác. Ban Quản trị 123doc.net tiến hành kiểm tra, rà soát các thông tin liên quan đến sự việc và xử lý như sau:</li>
            </ul>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">-Vi phạm lần 1: xóa Tài Liệu vi phạm bản quyền và gửi tin nhắn cảnh cáo tới Thành Viên đăng tải Tài Liệu đó trong vòng 8 tiếng kể từ khi nhận được phản ánh/khiếu nại có căn cứ.</p>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">-Vi phạm lần 2: xóa Tài Liệu vi phạm và phạt tài khoản doanh thu 100.000 đồng (Một trăm nghìn đồng).</p>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">-Vi phạm lần 3: xóa Tài Liệu vi phạm và xóa tài khoản Thành Viên.</p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Trong trường hợp, giữa Người Dùng và Thành Viên xảy ra tranh chấp liên quan đến sự vi phạm bản quyền các Tài Liệu được Thành Viên đăng tải trên 123doc.net. Ban quản trị 123doc.net xóa Tài Liệu ngay trong vòng 8 tiếng kể từ khi nhận được căn cứ chứng minh sự vi phạm trên và cam kết cung cấp đầy đủ và toàn bộ thông tin mà 123doc.net có được liên quan đến sự việc để phục vụ cho quá trình giải quyết tranh chấp giữa các bên.</li>
            </ul>
            <ol start="5" style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li><strong>Quy định xử phạt</strong></li>
            </ol>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong>a. Mức độ vi phạm</strong></p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Vi phạm nghiêm trọng: thuộc phạm vi điều chỉnh khoản 1 và khoản 2 Điều này.</li>
                <li>Vi phạm thông thường: thuộc phạm vi điều chỉnh khoản 3 Điều này.</li>
            </ul>
            <p style="padding:7px 0px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;"><strong>b.&nbsp;Hình thức xử phạt vi phạm</strong></p>
            <ul style="padding:9px 40px;color:#555555;font-family:monda, sans-serif;font-size:13px;line-height:23px;">
                <li>Đối với Thành Viên vi phạm nghiêm trọng:<ul style="padding:9px 40px;">
                        <li>Vi phạm lần 1: Ban Quản trị website 123doc.net gửi thông báo bằng email và trao đổi tực tiếp với Thành Viên.</li>
                        <li>Vi phạm lần 2: phạt 50.000 VND trừ vào tài khoản của Thành Viên.</li>
                        <li>Vi phạm lần 3: phạt 50.000 VND trừ vào tài khoản của Thành Viên.</li>
                        <li>Vi phạm lần 4: phạt 50.000 VND trừ vào tài khoản của Thành Viên.</li>
                        <li>Vi phạm lần 5: loại bỏ tài khoản khỏi website 123doc.net.</li>
                    </ul>
                </li>
                <li>Đối với Thành Viên vi phạm thông thường:</li>
                <li>Vi phạm lần 1,2,3: gửi thông báo cảnh cáo đến Người Dùng bằng hình thức email và đối thoại trực tiếp.</li>
                <li>Vi phạm lần 4 trở đi: phạt 15.000 VND cho mỗi lần vi phạm trừ vào tài khoản của Thành Viên.</li>
            </ul>
            <p dir="ltr" style="line-height:1.7999999999999998;margin-top:0pt;margin-bottom:0pt;text-align:justify;"><span style="font-size:15px;font-family:arial;color:#000000;font-weight:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre-wrap;background-color:transparent;"><i><br />
                    </i></span></p>
            <p dir="ltr" style="line-height:1.5;margin-top:0pt;margin-bottom:0pt;text-align:center;"><span id="docs-internal-guid-ad81e1f1-ce18-870f-16dc-345a6420f654"><i><br />
                        <br />
                        <br />
                        <br />
                    </i><br />
                </span></p>
        </div>
        <div class="popupStatic_foot">
            <span>
                <input type="checkbox" checked="checked" onclick="checkAccess(this)" />
                Cam kết không tải lên website 123doc.org tài liệu cấm được quy định như trên
            </span>
            <span style="margin-bottom: 15px;">
                <input type="checkbox" checked="checked" onclick="checkAccess(this)" />
                Đồng ý với các <a title="Tôi đồng ý với các điều khoản hợp đồng" href="http://blog.123doc.org/hop-dong-dich-vu-dang-tai-tai-lieu-len-website-123doc-org/" target="_blank">điều khoản của hợp đồng</a> và <a href="http://blog.123doc.org/quy-che-hoat-dong-123doc-org/" target="_blank">quy chế hoạt động</a> của website 123doc.org</a>
            </span>
            <input type="button" id="btnAcess" onclick="acess()" value="Tiếp tục tải lên" />
            <input type="button" onclick="deny()" value="Không đồng ý" />
        </div>
    </div>
</body>

</html>
<script>
    function acess() {
        $('#btnUpload', window.parent.document).attr('show_cl', false);
        $('#btnUpload', window.parent.document).click();
        window.parent.colorboxClose();
    }

    function deny() {
        window.parent.colorboxClose();
        window.parent.location.href = '/trang-chu.htm';
    }

    function checkAccess(obj) {
        var parent = $(obj).parents('div');
        if ($(obj).prop("checked")) {
            $('#btnAcess').removeAttr('disabled');
        } else {
            $('#btnAcess').attr('disabled', 'true');
        }
    }
</script>