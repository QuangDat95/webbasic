<html>

<head>
    <script>
        var MAX_TAG_INPUT = 6;
    </script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/common/js/jquery.js"></script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/common/js/js.template.js"></script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/common/js/common.js"></script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2//document/js/tags.js?v=232"></script>
    <script type="text/javascript" src="<?= URL ?>/template/static_v2/user/js/ajaxForm.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//common/css/style.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//document/css/upload.css" />
    <link rel="stylesheet" type="text/css" href="<?= URL ?>/template/static_v2/web_v2//css/app.css" />
</head>

<body style="padding: 15px; border-top: 4px solid #00a888;background: #fff">
    <div class="hide_add_collection overflow-hidden h-full">
        <div class="wp_collection">
            <ul class="tab_collection flex">
                <li data-tab="#tab_list_coll" class="active"><a href="javascript:;">Danh sách</a></li>
                <li data-tab="#tab_list_add"><a href="javascript:;">Thêm mới</a></li>
            </ul>
            <div class="tab_col_cnt h-full scrollbar-thin scrollbar-thumb-gray-500 scrollbar-track-gray-400 scrollbar-thumb-rounded-full scrollbar-track-rounded-full">
                <div id="tab_list_coll">
                    <h3>Danh sách bộ sưu tập</h3>
                    <input id="docid" type="hidden" value="<?= $_REQUEST['docid'] ?>" />
                    <input id="uid" type="hidden" value="<?= $_SESSION['customer']['customer'] ?>" />
                    <div class="myCollection">
                        <ul class="pb-8" id="list_coll">
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab_col_cnt">
                <div id="tab_list_add"></div>
            </div>
        </div>
    </div>
</body>
<script>
    var u_id = $('#uid').val();
    var doc_id = $('#docid').val();
    var list_coll = $('#list_coll');

    $.post('<?= URL ?>/index/getcollection', {
        uid: u_id,
        docid: doc_id
    }, function(result) {
        if (result.success) {
            list_coll.html('');
            let data = result.data.data;
            let coll = result.data.collid;
            data.map(row => {
                let active = '';
                let active_2 = '';
                if(coll){
                    if(coll.includes(row.id)){
                        active ='active';
                        active_2 ='active_2';
                    }
                }
                return list_coll.append('<li><a title="'+row.name+'" class="block '+ active_2 +'" onclick="addDocToCol('+doc_id+','+row.id+',this)">'+row.name+'<i class="icon_active_collection '+ active +'"></i></a></li>')
            });
        }
    }, 'json');

    $(document).ready(function() {
        $('.list_title_collection li').click(function() {
            $(this).addClass('active_coll');
            $('.icon_active_collection').addClass('active_coll');
        });
        //Xử lý tab collection
        $('.tab_collection li').click(function() {
            $('.tab_col_cnt').hide();
            $('.tab_collection li').removeClass('active');
            var _id = $(this).attr('data-tab');
            $(_id).parent('.tab_col_cnt').show();
            if ($(_id).html() == '') {
                var doc_id = $('#docid').val(),
                    u_id = $('#uid').val();

                $.post('<?= URL ?>/index/addcollection', {
                    u_id: u_id,
                    doc_id: doc_id
                }, function(data) {
                    $(_id).html(data);
                });
            } else {

            }
            $(this).addClass('active');
            return;
        });
    });

    function addDocToCol(doc, col, obj) {
        $(obj).toggleClass('active_2');
        $(obj).find('.icon_active_collection').toggleClass('active');
        var fin = $(obj).find('.active ').length;
        // alert(fin);
        //console.log(fin);
        //$(obj).find('input[type=checkbox]').attr('checked');
        //var state      = $(obj).find('input[type=checkbox]').is(':checked');
        $.post('<?= URL ?>/index/addtocoll', {
            doc_id: doc,
            col_id: col,
            status: fin
        }, function(data) {
            // alert();
            if (data.success) {
                // list_coll.html('');
                // $.post('<?= URL ?>/index/getcollection', {
                //     uid: u_id
                // }, function(result) {
                //     if (result.success) {
                //         let data = result.data;
                //         data.map(row => {
                //             console.log(row.docid);
                //             let active = '';
                //             if(row.docid.includes(doc_id)) active='active';
                //             return list_coll.append('<li><a title="'+row.name+'" class="block" onclick="addDocToCol('+doc_id+','+row.id+',this)">'+row.name+'<i class="icon_active_collection '+ active +'"></i></a></li>')
                //         });
                //     }
                // }, 'json');
                // alert(data.msg);
            }
        }, 'json');
    }
    $('#addNew').live('click', function() {
        var parent = $(this).parents('.wp_collection'),
            tags = getIdTag(parent),
            docid = $('#docid').val(),
            newtags = getNewTags(parent);
        $('#addCollectionForm').ajaxForm({
            data: {
                doc_id: docid,
                tag: tags,
                newtag: newtags
            },
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    $('.tab_collection').find("[data-tab= '#tab_list_coll']").addClass('active');
                    $('.tab_collection').find("[data-tab= '#tab_list_add']").removeClass('active');
                    $('#tab_list_coll').parent('.tab_col_cnt').show();
                    $('#tab_list_add').parent('.tab_col_cnt').hide();
                    var li = '<li><a onclick="addDocToCol(' + data.data.docid + ',' + data.data.id + ',this)" class="block active_2">' + data.data.name + '<i class="icon_active_collection active"></i></a></li>';
                    $(li).insertBefore($('.myCollection ul li:first-child'));
                }
            },
        });
    });
</script>

</html>