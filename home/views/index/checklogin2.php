
<div class="border-t-4 border-primary pb-4 px-4 bg-white rounded">
    <a href="javascript:" onclick="closeBox()" class="absolute right-0 text-4xl text-gray-700 top-0 mr-4">×</a>
    <div class="justify-between text-center items-center mt-3">
        <p class="text-3xl font-bold">TẢI TÀI LIỆU</p>
    </div>

    <div class="list-disc mt-3 text-gray-900">
        <div class="md:flex">
            <div class="border-2 border-primary rounded-lg md:w-1/2 m-4 p-4">
                <p class="font-bold text-xl">Đăng nhập</p>
                <ul class="list-disc md:mb-10 ml-4 mt-3">
                    <li class="list-disc mb-2">Nạp tiền vào tài khoản, tiền thừa có thể tải tài liệu khác bất kỳ lúc nào</li>
                    <li class="list-disc mb-2">Tiết kiệm chi phí</li>
                </ul>
                <button type="button" class="bg-primary hover:bg-primary-darker py-2 rounded text-center text-white w-full outline-none" onclick="popup_login()">Tiếp tục</button>
            </div>
            <div class="border-2 border-secondary rounded-lg md:w-1/2 m-4 p-4">
                <p class="font-bold text-xl">Không đăng nhập</p>
                <ul class="list-disc md:mb-10 ml-4 mt-3">
                    <li class="list-disc mb-2">Nạp tiền tải duy nhất một tài liệu đã chọn</li>
                    <li class="list-disc mb-2">Nhanh chóng, tiết kiệm thời gian</li>
                </ul>
                <button type="button" class="bg-secondary hover:bg-secondary-darker py-2 rounded text-center text-white w-full outline-none" onclick="downloadNotLogin()">Tiếp tục</button>
            </div>
        </div>
    </div>
</div>
