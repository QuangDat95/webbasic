<style>
    body {
        background-color: #fff;
    }

    .form-auth a {
        display: -webkit-inline-box !important;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<div class="frm_login login-form mx-auto px-8 border-t-4 border-primary bg-white hidden">
    <h3 class="text-2xl font-medium mt-3">ĐĂNG NHẬP</h3>
    <ul class="flex flex-wrap ">
        <li class="w-full mb-4 rounded">
            <a class="flex items-center" rel="nofollow" href="https://socket.123docz.net/redirect/facebook"
               onclick="setUrlBack()">
                <i class="icon i_facebook mx-4"></i>
                <span class="rounded-r py-2 text-base w-full">Đăng nhập bằng Facebook</span></a>
        </li>
        <li class="w-full rounded">
            <a class="flex items-center" rel="nofollow" href="https://socket.123docz.net/redirect/google"
               onclick="setUrlBack()">
                <i class="icon i_google mx-4"></i><span
                    class="rounded-r py-2 text-base w-full">Đăng nhập bằng Google</span></a>
        </li>
    </ul>
    <div id="frm_login" class="mt-3">
        <input id="url" type="text" hidden value="<?= URL ?>/index/login">
        <p class="mess_error" style="color: red"></p>
        <p>
            <input type="text" class="txtName" name="txtName" onfocus="focusLabelInput(this);"
                   onblur="blurLabelInput(this);" onkeyup="checkVal(this)"/>
            <label onclick="focusLabelInput(this);">Email/Số điện thoại</label>
        </p>
        <p>
            <input type="password" class="txtPass" name="txtPass" onfocus="focusLabelInput(this);"
                   onblur="blurLabelInput(this);" onkeyup="checkVal(this)"/>
            <label onclick="focusLabelInput(this);">Mật khẩu</label>
        </p>
        <p>
            <input type="checkbox" checked="" name="remember"/>
            <span>Nhớ mật khẩu</span>
        </p>

        <input id="submitLogin" class="login-button mt-3" type="submit" data-form="#frm_login" value="Đăng Nhập"/>
        <p class="pb-8">
            <a class="forgetPass" rel="nofollow" href="javascript:">Quên mật khẩu</a>
            <a class="register" rel="nofollow" href="javascript:">Đăng ký tài khoản mới</a>
        </p>
    </div>
</div>
<div class="frm_login form-auth px-8 pb-8 border-t-4 border-primary bg-white  register-form">
    <h3 class="text-2xl font-medium mt-3">ĐĂNG KÝ</h3>
    <div class="flex flex-col">
        <div class="container mx-auto flex-1 flex flex-col items-center justify-center">
            <div class="bg-white py-2 rounded  text-black w-full">
                <form id="register_form" action="javascript:">
                    <input
                        type="text"
                        class="block border border-grey-light w-full p-2 rounded mb-2"
                        name="use_fullname" required
                        placeholder="Tên đầy đủ"/>
                    <input
                        type="text"
                        class="block border border-grey-light w-full p-2 rounded mb-2"
                        name="use_phone" required
                        placeholder="Số điện thoại"/>
                    <input
                        type="email"
                        class="block border border-grey-light w-full p-2 rounded mb-2"
                        name="use_email" required
                        placeholder="Email"/>
                    <input
                        type="password" required
                        class="block border border-grey-light w-full p-2 rounded mb-2"
                        name="use_password"
                        placeholder="Mật khẩu"/>
                    <input
                        type="password" required
                        class="block border border-grey-light w-full p-2 rounded mb-2"
                        name="re_use_password"
                        placeholder="Nhập lại mật khẩu"/>
                    <div class="flex flex-wrap mb-2">
                        <div class="g-recaptcha" id="g-recaptcha-register"></div>
                    </div>

                    <button
                        type="submit"
                        class="uppercase w-full text-center py-3 rounded bg-primary text-white hover:bg-primary-dark focus:outline-none my-1"
                    >Đăng ký
                    </button>
                </form>
                <div class="text-center text-sm text-grey-dark mt-4 md:px-8">
                    Bằng cách nhập vào Đăng ký, bạn đồng ý với
                    <a class="no-underline border-b border-primary  text-primary"
                       href="/statics-detail/2-dieu-khoan-su-dung.htm">
                        Điều khoản
                    </a> và

                    <a class="no-underline border-b border-primary text-primary"
                       href="/statics-detail/896-chinh-sach-bao-mat-thong-tin.htm">
                        Chính sách bảo mật thông tin
                    </a> của chúng tôi
                </div>
            </div>
            <div class="text-grey-dark mt-4">
                Đã có tài khoản?
                <a class="no-underline border-b border-primary text-primary login" href="javascript:">
                    Đăng nhập
                </a>.
            </div>
        </div>
    </div>
</div>
<div class="frm_login form-auth hidden confimation-form px-8 pb-8 border-t-4 border-primary bg-white">
    <h3 class="text-2xl font-medium uppercase mt-3">Xác thực địa chỉ email</h3>
    <div class=" flex flex-col">
        <div class="container  mx-auto flex-1 flex flex-col items-center justify-center">
            <div class="text-justify text-sm text-grey-dark">
                <p>Chúng tôi vừa gửi một email xác thực tới địa chỉa email <span class="email"
                                                                                 style="color: #ffaa00">example@email.com</span>.
                    Vui lòng kích hoạt tài khoản của bạn bằng cách làm theo hướng dẫn trong email.</p>
                <p>Nếu bạn không tìm thấy email, hãy kiểm tra trong thư mục rác hoặc spam</p>
            </div>
            <button
                type="button"
                id="resend-btn"
                class="uppercase w-full text-center py-3 rounded bg-primary text-white hover:bg-primary-dark focus:outline-none my-1"
            >Gửi lại email
            </button>
        </div>
        <div class="text-grey-dark mt-4 text-center">
            <a class="no-underline border-b border-primary text-primary register" href="javascript:">
                Tạo tài khoản mới
            </a>.
        </div>
        <div class="text-grey-dark mt-2 text-center">
            Đã có tài khoản?
            <a class="no-underline border-b border-primary text-primary login" href="javascript:">
                Đăng nhập
            </a>.
        </div>
    </div>
</div>
<div class="frm_login form-auth px-8 pb-8 border-t-4 border-primary bg-white hidden forgetPass-form">
    <h3 class="text-2xl font-medium uppercase mt-3">ĐẶT LẠI MẬT KHẨU</h3>
    <div class=" flex flex-col">
        <div class="container mx-auto flex-1 flex flex-col items-center justify-center">
            <div class="text-center text-sm my-2 text-red-500">
                Bạn quên mật khẩu đăng nhập?<br>Nhập email đăng ký tài khoản ở đây, chúng tôi sẽ giúp bạn lấy lại mật
                khẩu
            </div>
            <form action="javascript:" id="reset-pwd-form" class="w-full">
                <input
                    type="text"
                    class="block border border-grey-light w-full p-3 rounded mb-4"
                    name="email"
                    placeholder="Email"/>
                <div class="g-recaptcha mb-2" id="g-recaptcha-fwpass"></div>
                <button
                    type="submit"
                    class="uppercase w-full text-center py-3 rounded bg-primary text-white hover:bg-primary-dark focus:outline-none my-1"
                >Gửi yêu cầu
                </button>
            </form>
            <div class="text-grey-dark mt-4">
                <a class="no-underline border-b border-primary text-primary register" href="javascript:">
                    Tạo tài khoản mới
                </a>.
            </div>
            <div class="text-grey-dark mt-2">
                Đã có tài khoản?
                <a class="no-underline border-b border-primary text-primary login" href="javascript:">
                    Đăng nhập
                </a>.
            </div>
        </div>
    </div>
</div>

<script>
    function focusLabelInput(obj) {
        var _parent = $(obj).parent(), label = $('label', _parent), input = $('input', _parent),
            form = $(obj).parents('form');
        $('label, input', form).removeClass('focus');
        label.addClass('focus');
        input.addClass('focus');
        setTimeout(function () {
            $('input.focus').focus();
        }, 1000);
    }

    function blurLabelInput(obj) {
        var _parent = $(obj).parent();
        $('label, input', _parent).removeClass('focus');
    }

    function checkVal(obj) {
        var _val = $(obj).val();
        if (_val.length > 0) {
            $(obj).parent().addClass('active');
        } else {
            $(obj).parent().removeClass('active');
        }
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    function setUrlBack(url = window.location.href) {
        Cookies.remove('current-url');
        Cookies.set("current-url", url);
    }
</script>
<script>
    $(document).ready(function () {
        $(document).on("click", ".forgetPass", function () {
            $(".frm_login").addClass("hidden");
            $(".forgetPass-form").removeClass("hidden");
        });
        $(document).on("click", ".register", function () {
            $(".frm_login").addClass("hidden");
            $(".register-form").removeClass("hidden");
        });
        $(document).on("click", ".login", function () {
            $(".frm_login").addClass("hidden");
            $(".login-form").removeClass("hidden");
        });
        $(document).on("submit", "#register_form", function () {
            $(this).find('.border-red').removeClass("border-red");
            let formData = new FormData($(this)[0]);
            $.ajax({
                url: baseUrl+"/index/register",
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (res) {
                    if (res.success) {
                        $(".frm_login").addClass("hidden");
                        $(".confimation-form").removeClass("hidden");
                        $(".email").text(res.email);
                    }else{
                        $("input[name='" + res.key + "']").addClass("border-red");
                        $("input[name='" + res.key + "']").focus();
                        toastr.error(res.msg);
                    }
                },
            });
            //$("#captcha").trigger("click");
        });
        $(document).on("click", "#resend-btn", function () {
            $.ajax({
                type: "get",
                url: "https://123docz.net/global/ajax/aja_resend_confirmation.php",
                dataType: "json",
                success: function (res) {
                    if (res.error) {
                        toastr.error(res.error);
                    }
                    if (res.success) {
                        toastr.success(res.success);
                    }
                }
            });
        });
        $(document).on("submit", "#reset-pwd-form", function () {
            let formData = new FormData($(this)[0]);
            $.ajax({
                url: "https://123docz.net/global/ajax/aja_send_reset_pwd.php",
                type: "post",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (res) {
                    if (res.error) {
                        toastr.error(res.error);
                    }
                    if (res.success) {
                        toastr.success(res.success);
                        $("#reset-pwd-form").find("button").addClass("cursor-not-allowed  opacity-50").prop('disabled', true);
                    }
                },
            });
        });
    });
    function getCaptcha(containerID) {
        return grecaptcha.getResponse(containerID);
    }
</script>
<script src="https://www.google.com/recaptcha/api.js?hl=vi&onload=CaptchaCallback&render=explicit"
        async defer>
</script>
<script type="text/javascript">
    // var CaptchaCallback = function () {
    //     g_recaptcha_register = genCaptcha('g-recaptcha-register');
    //     g_recaptcha_fwpass = genCaptcha('g-recaptcha-fwpass');
    // };
</script>