<link rel="stylesheet" type="text/css" href="<?= URL ?>/template/styles/search.css" />
<div id="container">
        <div class="container searchPage mx-auto">
                <h1 class="text-xl md:text-2xl p-2">Kết quả với từ khóa "<strong class="font-semibold"><?= $this->keyword ?></strong>"</h1>
                <div class="search_top px-2"><label>Bạn có muốn tìm thêm với từ khóa: </label>
                        <ul>
                                <li><a title="việc học" href="/doc/s/việc+học">việc <b>học</b></a></li>
                                <li><a title="học tiếng anh" href="/doc/s/học+tiếng+anh"><b>học</b> tiếng anh</a></li>
                                <li><a title="học thuyết" href="/doc/s/học+thuyết"><b>học</b> thuyết</a></li>
                                <li><a title="phương pháp học" href="/doc/s/phương+pháp+học">phương pháp <b>học</b></a></li>
                                <li><a title="phương pháp học tập" href="/doc/s/phương+pháp+học+tập">phương pháp <b>học</b> tập</a></li>
                                <li><a title="khoa học kỹ thuật" href="/doc/s/khoa+học+kỹ+thuật">khoa <b>học</b> kỹ thuật</a></li>
                                <li><a title="đại học" href="/doc/s/đại+học">đại <b>học</b></a></li>
                                <li><a title="trình độ đại học" href="/doc/s/trình+độ+đại+học">trình độ đại <b>học</b></a></li>
                                <li><a title="khoa học công nghệ" href="/doc/s/khoa+học+công+nghệ">khoa <b>học</b> công nghệ</a></li>
                                <li><a title="hướng dẫn học sinh" href="/doc/s/hướng+dẫn+học+sinh">hướng dẫn <b>học</b> sinh</a></li>
                        </ul>
                </div>
                <div class="flex md:hidden justify-between mb-2 bg-white py-2">
                        <div class="px-2 w-full text-center"><a class="text-primary whitespace-no-wrap font-bold text-base" href="javascript:;"> <i class="icon"></i>Tài liệu (10514k+) </a>
                        </div>
                        <div class="px-2 border-l border-r border-solid border-gray-500 w-full text-center"><a class="whitespace-no-wrap font-bold text-base" href="<?= URL ?>/search/users"> <i class="icon"></i>Thành
                                        viên </a></div>
                        <div class="px-2 w-full text-center"><a class="whitespace-no-wrap font-bold text-base" href="<?= URL ?>/search/collection"> <i class="icon"></i>Bộ sưu tập </a></div>
                </div>
                <div class="px-2 filter_search overflow-x-scroll md:overflow-x-visible text-base md:text-sm">
                        <div class="filterItem filterCate"><a href="javascript:;" title="Danh mục" onclick="showFilterMobi(this)" class=" md:px-16"> -- Danh mục -- </a>
                                <div>
                                        <ul class="list_cate">
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="169" style="">Luận Văn - Báo
                                                                Cáo</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="184" style="padding-left: 20px;">
                                                                                |-- Thạc sĩ - Cao học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="308" style="padding-left: 20px;">
                                                                                |-- |-- Kinh tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="309" style="padding-left: 20px;">
                                                                                |-- |-- Khoa học xã hội</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="310" style="padding-left: 20px;">
                                                                                |-- |-- Khoa học tự nhiên</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="311" style="padding-left: 20px;">
                                                                                |-- |-- Công nghệ thông tin</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="312" style="padding-left: 20px;">
                                                                                |-- |-- Kỹ thuật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="313" style="padding-left: 20px;">
                                                                                |-- |-- Nông - Lâm - Ngư</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="314" style="padding-left: 20px;">
                                                                                |-- |-- Kiến trúc - Xây dựng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="315" style="padding-left: 20px;">
                                                                                |-- |-- Luật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="316" style="padding-left: 20px;">
                                                                                |-- |-- Sư phạm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="317" style="padding-left: 20px;">
                                                                                |-- |-- Y dược - Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="282" style="padding-left: 20px;">
                                                                                |-- Kinh tế - Quản lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="319" style="padding-left: 20px;">
                                                                                |-- |-- Dịch vụ - Du lịch</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="320" style="padding-left: 20px;">
                                                                                |-- |-- Bất động sản</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="170" style="padding-left: 20px;">
                                                                                |-- |-- Tài chính - Ngân hàng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="172" style="padding-left: 20px;">
                                                                                |-- |-- Quản trị kinh doanh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="171" style="padding-left: 20px;">
                                                                                |-- |-- Kế toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="321" style="padding-left: 20px;">
                                                                                |-- |-- Kiểm toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="322" style="padding-left: 20px;">
                                                                                |-- |-- Xuất nhập khẩu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="325" style="padding-left: 20px;">
                                                                                |-- |-- Chứng khoán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="324" style="padding-left: 20px;">
                                                                                |-- |-- Tài chính thuế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="326" style="padding-left: 20px;">
                                                                                |-- |-- Marketing</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="327" style="padding-left: 20px;">
                                                                                |-- |-- Bảo hiểm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="328" style="padding-left: 20px;">
                                                                                |-- |-- Định giá - Đấu thầu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="181" style="padding-left: 20px;">
                                                                                |-- Khoa học tự nhiên</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="329" style="padding-left: 20px;">
                                                                                |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="330" style="padding-left: 20px;">
                                                                                |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="331" style="padding-left: 20px;">
                                                                                |-- |-- Hóa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="332" style="padding-left: 20px;">
                                                                                |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="333" style="padding-left: 20px;">
                                                                                |-- |-- Địa lý - Địa chất</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="173" style="padding-left: 20px;">
                                                                                |-- Kinh tế - Thương mại</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="283" style="padding-left: 20px;">
                                                                                |-- Lý luận chính trị</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="334" style="padding-left: 20px;">
                                                                                |-- |-- Triết học Mác - Lênin</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="335" style="padding-left: 20px;">
                                                                                |-- |-- Đường lối cách mạng Đảng cộng sản Việt Nam</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="336" style="padding-left: 20px;">
                                                                                |-- |-- Kinh tế chính trị</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="337" style="padding-left: 20px;">
                                                                                |-- |-- Chủ nghĩa xã hội khoa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="338" style="padding-left: 20px;">
                                                                                |-- |-- Tư tưởng Hồ Chí Minh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="174" style="padding-left: 20px;">
                                                                                |-- Công nghệ thông tin</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="339" style="padding-left: 20px;">
                                                                                |-- |-- Quản trị mạng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="340" style="padding-left: 20px;">
                                                                                |-- |-- Lập trình</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="341" style="padding-left: 20px;">
                                                                                |-- |-- Đồ họa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="342" style="padding-left: 20px;">
                                                                                |-- |-- Web</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="343" style="padding-left: 20px;">
                                                                                |-- |-- Hệ thống thông tin</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="344" style="padding-left: 20px;">
                                                                                |-- |-- Thương mại điện tử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="346" style="padding-left: 20px;">
                                                                                |-- Kỹ thuật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="347" style="padding-left: 20px;">
                                                                                |-- |-- Giao thông - Vận tải</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="175" style="padding-left: 20px;">
                                                                                |-- |-- Điện - Điện tử - Viễn thông</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="176" style="padding-left: 20px;">
                                                                                |-- |-- Cơ khí - Vật liệu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="177" style="padding-left: 20px;">
                                                                                |-- |-- Kiến trúc - Xây dựng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="348" style="padding-left: 20px;">
                                                                                |-- |-- Hóa dầu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="182" style="padding-left: 20px;">
                                                                                |-- Nông - Lâm - Ngư</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="349" style="padding-left: 20px;">
                                                                                |-- |-- Lâm nghiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="350" style="padding-left: 20px;">
                                                                                |-- |-- Nông học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="351" style="padding-left: 20px;">
                                                                                |-- |-- Chăn nuôi</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="352" style="padding-left: 20px;">
                                                                                |-- |-- Thú y</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="353" style="padding-left: 20px;">
                                                                                |-- |-- Thủy sản</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="354" style="padding-left: 20px;">
                                                                                |-- |-- Công nghệ thực phẩm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="178" style="padding-left: 20px;">
                                                                                |-- Công nghệ - Môi trường</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="179" style="padding-left: 20px;">
                                                                                |-- Y khoa - Dược</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="180" style="padding-left: 20px;">
                                                                                |-- Khoa học xã hội</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="603" style="padding-left: 20px;">
                                                                                |-- |-- Quan hệ quốc tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="604" style="padding-left: 20px;">
                                                                                |-- |-- Giáo dục học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="605" style="padding-left: 20px;">
                                                                                |-- |-- Đông phương học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="606" style="padding-left: 20px;">
                                                                                |-- |-- Việt Nam học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="607" style="padding-left: 20px;">
                                                                                |-- |-- Văn hóa - Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="608" style="padding-left: 20px;">
                                                                                |-- |-- Xã hội học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="609" style="padding-left: 20px;">
                                                                                |-- |-- Báo chí</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="610" style="padding-left: 20px;">
                                                                                |-- |-- Tâm lý học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="611" style="padding-left: 20px;">
                                                                                |-- |-- Văn học - Ngôn ngữ học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="183" style="padding-left: 20px;">
                                                                                |-- Báo cáo khoa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="185" style="padding-left: 20px;">
                                                                                |-- Tiến sĩ</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="154" style="">Kỹ Năng Mềm</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="155" style="padding-left: 20px;">
                                                                                |-- Tâm lý - Nghệ thuật sống</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="162" style="padding-left: 20px;">
                                                                                |-- Kỹ năng tư duy</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="158" style="padding-left: 20px;">
                                                                                |-- Kỹ năng quản lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="156" style="padding-left: 20px;">
                                                                                |-- Kỹ năng giao tiếp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="157" style="padding-left: 20px;">
                                                                                |-- Kỹ năng thuyết trình</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="159" style="padding-left: 20px;">
                                                                                |-- Kỹ năng lãnh đạo</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="160" style="padding-left: 20px;">
                                                                                |-- Kỹ năng phỏng vấn </a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="164" style="padding-left: 20px;">
                                                                                |-- Kỹ năng đàm phán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="163" style="padding-left: 20px;">
                                                                                |-- Kỹ năng tổ chức</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="161" style="padding-left: 20px;">
                                                                                |-- Kỹ năng làm việc nhóm</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="672" style="">Mẫu Slide</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="673" style="padding-left: 20px;">
                                                                                |-- Mẫu Slide - Template</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="674" style="padding-left: 20px;">
                                                                                |-- Hình Nền - Background</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="675" style="padding-left: 20px;">
                                                                                |-- |-- Kinh Doanh - Business</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="676" style="padding-left: 20px;">
                                                                                |-- |-- Giáo Dục - Education</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="677" style="padding-left: 20px;">
                                                                                |-- |-- Tài Chính - Financial</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="678" style="padding-left: 20px;">
                                                                                |-- |-- Sản Phẩm - Product</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="679" style="padding-left: 20px;">
                                                                                |-- |-- Dịch Vụ - Service</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="680" style="padding-left: 20px;">
                                                                                |-- |-- Khoa Học - Science</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="681" style="padding-left: 20px;">
                                                                                |-- |-- Công Nghệ - Technology</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="682" style="padding-left: 20px;">
                                                                                |-- |-- Thiên Nhiên - Natural</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="683" style="padding-left: 20px;">
                                                                                |-- |-- Con Người - People</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="684" style="padding-left: 20px;">
                                                                                |-- |-- Thể Thao - Sport</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="685" style="padding-left: 20px;">
                                                                                |-- |-- Giải Trí - Entertainment</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="686" style="padding-left: 20px;">
                                                                                |-- |-- Trừu Tượng - Abstract</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="687" style="padding-left: 20px;">
                                                                                |-- Sơ Đồ - Diagram</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="688" style="padding-left: 20px;">
                                                                                |-- |-- Tiến Trình - Process</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="689" style="padding-left: 20px;">
                                                                                |-- |-- Lắp Hình - Puzzle</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="690" style="padding-left: 20px;">
                                                                                |-- |-- Giai Đoạn - Stage</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="691" style="padding-left: 20px;">
                                                                                |-- |-- Hình Cây - Tree</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="692" style="padding-left: 20px;">
                                                                                |-- Biểu Đồ - Chart</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="693" style="padding-left: 20px;">
                                                                                |-- |-- Thanh - Bar chart</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="694" style="padding-left: 20px;">
                                                                                |-- |-- Đường - Line chart</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="695" style="padding-left: 20px;">
                                                                                |-- |-- Hình Tròn - Pie chart</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="696" style="padding-left: 20px;">
                                                                                |-- |-- Ma Trận - Matrix chart</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="697" style="padding-left: 20px;">
                                                                                |-- |-- Tổ Chức - Org chart</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="698" style="padding-left: 20px;">
                                                                                |-- Hình Vẽ - Shape</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="699" style="padding-left: 20px;">
                                                                                |-- |-- Hình Khối - Cube</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="700" style="padding-left: 20px;">
                                                                                |-- |-- Kim Tự Tháp - Pyramid</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="701" style="padding-left: 20px;">
                                                                                |-- |-- Mũi Tên - Arrow</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="702" style="padding-left: 20px;">
                                                                                |-- |-- Hình Cầu - Sphere</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="703" style="padding-left: 20px;">
                                                                                |-- |-- Bánh Xe - Gear Wheel</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="704" style="padding-left: 20px;">
                                                                                |-- Văn Bản - Text</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="705" style="padding-left: 20px;">
                                                                                |-- |-- Box Hình - Picture</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="706" style="padding-left: 20px;">
                                                                                |-- |-- Box mũi tên - Arrow</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="707" style="padding-left: 20px;">
                                                                                |-- |-- Box vòng tròn - Circular</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="708" style="padding-left: 20px;">
                                                                                |-- |-- Box Ghi Chú - Note</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="709" style="padding-left: 20px;">
                                                                                |-- |-- Box chữ nhật - Rectangular</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="710" style="padding-left: 20px;">
                                                                                |-- |-- Box Thẻ - Tabbed</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="711" style="padding-left: 20px;">
                                                                                |-- |-- Box Chú Giải - Bubbles</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="712" style="padding-left: 20px;">
                                                                                |-- Biểu Tượng - Clipart</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="713" style="padding-left: 20px;">
                                                                                |-- |-- Hình Người - 3D Man</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="714" style="padding-left: 20px;">
                                                                                |-- |-- Biểu Tượng - Clipart</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="715" style="padding-left: 20px;">
                                                                                |-- |-- Minh Họa - Illustration</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="716" style="padding-left: 20px;">
                                                                                |-- |-- Hình Động - Animation</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="717" style="padding-left: 20px;">
                                                                                |-- Phân tích - Analysis</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="718" style="padding-left: 20px;">
                                                                                |-- Kế Hoạch - Planning</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="719" style="padding-left: 20px;">
                                                                                |-- |-- Lịch - Calendars</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="720" style="padding-left: 20px;">
                                                                                |-- |-- Sơ Đồ Gantt</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="721" style="padding-left: 20px;">
                                                                                |-- |-- Thời Gian - Timlines</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="722" style="padding-left: 20px;">
                                                                                |-- |-- Công Việc Phải Làm - To-do</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="723" style="padding-left: 20px;">
                                                                                |-- Hình Minh Họa - Image</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="724" style="padding-left: 20px;">
                                                                                |-- |-- Giáo Dục - Education</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="725" style="padding-left: 20px;">
                                                                                |-- |-- Kinh Tế - Economic</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="726" style="padding-left: 20px;">
                                                                                |-- |-- Khoa Học - Science</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="727" style="padding-left: 20px;">
                                                                                |-- |-- Công Nghệ - Technology</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="728" style="padding-left: 20px;">
                                                                                |-- |-- Văn Hóa - Culture</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="729" style="padding-left: 20px;">
                                                                                |-- |-- Thiên Nhiên - Natural</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="730" style="padding-left: 20px;">
                                                                                |-- |-- Đất Nước - Country</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="731" style="padding-left: 20px;">
                                                                                |-- |-- Con Người - People</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="732" style="padding-left: 20px;">
                                                                                |-- |-- Nghệ Thuật - Art Picture</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="733" style="padding-left: 20px;">
                                                                                |-- |-- Ảnh Vui - Funny</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="35" style="">Kinh Doanh - Tiếp
                                                                Thị</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="37" style="padding-left: 20px;">
                                                                                |-- Quản trị kinh doanh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="36" style="padding-left: 20px;">
                                                                                |-- Internet Marketing</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="38" style="padding-left: 20px;">
                                                                                |-- Kế hoạch kinh doanh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="41" style="padding-left: 20px;">
                                                                                |-- Thương mại điện tử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="39" style="padding-left: 20px;">
                                                                                |-- Tiếp thị - Bán hàng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="42" style="padding-left: 20px;">
                                                                                |-- PR - Truyền thông</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="40" style="padding-left: 20px;">
                                                                                |-- Kỹ năng bán hàng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="43" style="padding-left: 20px;">
                                                                                |-- Tổ chức sự kiện</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="44" style="">Kinh Tế - Quản
                                                                Lý</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="45" style="padding-left: 20px;">
                                                                                |-- Quản lý nhà nước</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="46" style="padding-left: 20px;">
                                                                                |-- Tiêu chuẩn - Qui chuẩn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="47" style="padding-left: 20px;">
                                                                                |-- Quản lý dự án</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="48" style="padding-left: 20px;">
                                                                                |-- Quy hoạch - Đô thị</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="54" style="">Tài Chính - Ngân
                                                                Hàng</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="1846" style="padding-left: 20px;">
                                                                                |-- Báo cáo tài chính</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="55" style="padding-left: 20px;">
                                                                                |-- Ngân hàng - Tín dụng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="56" style="padding-left: 20px;">
                                                                                |-- Kế toán - Kiểm toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="57" style="padding-left: 20px;">
                                                                                |-- Tài chính doanh nghiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="58" style="padding-left: 20px;">
                                                                                |-- Đầu tư Chứng khoán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="59" style="padding-left: 20px;">
                                                                                |-- Đầu tư Bất động sản</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="60" style="padding-left: 20px;">
                                                                                |-- Bảo hiểm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="61" style="padding-left: 20px;">
                                                                                |-- Quỹ đầu tư</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="49" style="">Biểu Mẫu - Văn
                                                                Bản</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="51" style="padding-left: 20px;">
                                                                                |-- Biểu mẫu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="52" style="padding-left: 20px;">
                                                                                |-- Đơn từ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="50" style="padding-left: 20px;">
                                                                                |-- Thủ tục hành chính</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="53" style="padding-left: 20px;">
                                                                                |-- Hợp đồng</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="99" style="">Giáo Dục - Đào
                                                                Tạo</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="1810" style="padding-left: 20px;">
                                                                                |-- Đề thi</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1811" style="padding-left: 20px;">
                                                                                |-- |-- Tuyển sinh lớp 10</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1812" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ Văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1813" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1814" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngoại Ngữ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1815" style="padding-left: 20px;">
                                                                                |-- |-- THPT Quốc Gia </a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1816" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1817" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1818" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1819" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngoại Ngữ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1820" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ Văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1821" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1822" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="1823" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1836" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi dành cho sinh viên</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1837" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi tuyển dụng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1824" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 1</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1825" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 2</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1826" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 3</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1827" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 4</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1840" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 5</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1829" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 6</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1830" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 7</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1831" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 8</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1832" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 9</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1833" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 10</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1834" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 11</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="1835" style="padding-left: 20px;">
                                                                                |-- |-- Đề thi lớp 12</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="100" style="padding-left: 20px;">
                                                                                |-- Mầm non - Tiểu học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="307" style="padding-left: 20px;">
                                                                                |-- |-- Mầm non</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="355" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lứa tuổi 3 - 12 tháng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="356" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lứa tuổi 12 - 24 tháng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="357" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lứa tuổi 24 - 36 tháng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="358" style="padding-left: 20px;">
                                                                                |-- |-- |-- Mẫu giáo bé</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="359" style="padding-left: 20px;">
                                                                                |-- |-- |-- Mẫu giáo nhỡ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="360" style="padding-left: 20px;">
                                                                                |-- |-- |-- Mẫu giáo lớn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="361" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 1</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="362" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 2</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="363" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 3</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="364" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 4</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="365" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 5</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="103" style="padding-left: 20px;">
                                                                                |-- Trung học cơ sở - phổ thông</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="366" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 6</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="367" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="375" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="376" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="377" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="378" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="379" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="380" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="381" style="padding-left: 20px;">
                                                                                |-- |-- |-- Âm nhạc</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="382" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tin học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="383" style="padding-left: 20px;">
                                                                                |-- |-- |-- Mỹ thuật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="384" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="385" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="386" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giáo dục công dân</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="387" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thể dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="369" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 7</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="388" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="389" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="390" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="391" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa học </a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="392" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="393" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="395" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="394" style="padding-left: 20px;">
                                                                                |-- |-- |-- Âm nhạc</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="396" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tin học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="397" style="padding-left: 20px;">
                                                                                |-- |-- |-- Mỹ thuật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="398" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="399" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="400" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giáo dục công dân</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="401" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thể dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="370" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 8</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="402" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="403" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="404" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="405" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa học </a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="406" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="407" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="408" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="409" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tin học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="410" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="411" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="412" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giáo dục công dân</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="413" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thể dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="371" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 9</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="414" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="415" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="416" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="417" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="418" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="419" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="420" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="421" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tin học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="422" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="423" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="424" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giáo dục công dân</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="425" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thể dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="372" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 10</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="426" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="368" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="427" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="428" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="429" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="430" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="431" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="432" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tin học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="433" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="435" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giáo dục công dân</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="434" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="436" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thể dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="373" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 11</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="437" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="438" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="439" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="440" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="441" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="442" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="443" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="444" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tin học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="445" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="446" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="447" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giáo dục công dân</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="448" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thể dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="374" style="padding-left: 20px;">
                                                                                |-- |-- Lớp 12</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="449" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngữ văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="450" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="451" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="452" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="453" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="454" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="456" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="455" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tin học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="457" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="458" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="459" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giáo dục công dân</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="460" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thể dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="102" style="padding-left: 20px;">
                                                                                |-- Ôn thi Đại học - Cao đẳng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="461" style="padding-left: 20px;">
                                                                                |-- |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="462" style="padding-left: 20px;">
                                                                                |-- |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="463" style="padding-left: 20px;">
                                                                                |-- |-- Hóa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="464" style="padding-left: 20px;">
                                                                                |-- |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="465" style="padding-left: 20px;">
                                                                                |-- |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="466" style="padding-left: 20px;">
                                                                                |-- |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="467" style="padding-left: 20px;">
                                                                                |-- |-- Văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="468" style="padding-left: 20px;">
                                                                                |-- |-- Ngoại ngữ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="101" style="padding-left: 20px;">
                                                                                |-- Cao đẳng - Đại học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="469" style="padding-left: 20px;">
                                                                                |-- |-- Đại cương</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="478" style="padding-left: 20px;">
                                                                                |-- |-- |-- Pháp luật đại cương</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="479" style="padding-left: 20px;">
                                                                                |-- |-- |-- Triết học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="480" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán cao cấp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="481" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lý thuyết xác suất - thống kê</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="482" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kinh tế lượng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="483" style="padding-left: 20px;">
                                                                                |-- |-- |-- Toán rời rạc</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="484" style="padding-left: 20px;">
                                                                                |-- |-- |-- Chủ nghĩ xã hội </a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="485" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kinh tế chính trị</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="486" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tư tưởng Hồ Chí Minh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="487" style="padding-left: 20px;">
                                                                                |-- |-- |-- Đường lối cách mạng Đảng cộng sản Việt Nam</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="488" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kinh tế vi mô</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="489" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kinh tế vĩ mô</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="490" style="padding-left: 20px;">
                                                                                |-- |-- |-- Logic học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="491" style="padding-left: 20px;">
                                                                                |-- |-- |-- Phương pháp học tập và nghiên cứu khoa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="492" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tin học đại cương</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="493" style="padding-left: 20px;">
                                                                                |-- |-- |-- Marketing căn bản</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="494" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lý thuyết tài chính tiền tệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="470" style="padding-left: 20px;">
                                                                                |-- |-- Y - Dược</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="495" style="padding-left: 20px;">
                                                                                |-- |-- |-- Nội khoa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="496" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngoại khoa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="497" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sản phụ khoa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="498" style="padding-left: 20px;">
                                                                                |-- |-- |-- Nhi khoa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="499" style="padding-left: 20px;">
                                                                                |-- |-- |-- Điều dưỡng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="500" style="padding-left: 20px;">
                                                                                |-- |-- |-- Nhãn khoa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="501" style="padding-left: 20px;">
                                                                                |-- |-- |-- Răng - Hàm - Mặt</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="502" style="padding-left: 20px;">
                                                                                |-- |-- |-- Chẩn đoán hình ảnh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="503" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tai - Mũi - Họng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="504" style="padding-left: 20px;">
                                                                                |-- |-- |-- Gây mê hồi sức</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="505" style="padding-left: 20px;">
                                                                                |-- |-- |-- Da liễu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="506" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vi sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="507" style="padding-left: 20px;">
                                                                                |-- |-- |-- Truyền nhiễm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="508" style="padding-left: 20px;">
                                                                                |-- |-- |-- Huyết học - Truyền máu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="509" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tâm thần</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="510" style="padding-left: 20px;">
                                                                                |-- |-- |-- Y học cổ truyền</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="511" style="padding-left: 20px;">
                                                                                |-- |-- |-- Y học gia đình</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="512" style="padding-left: 20px;">
                                                                                |-- |-- |-- Y học công cộng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="513" style="padding-left: 20px;">
                                                                                |-- |-- |-- Bào chế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="514" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa dược</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="471" style="padding-left: 20px;">
                                                                                |-- |-- Chuyên ngành kinh tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="515" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thị trường chứng khoán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="516" style="padding-left: 20px;">
                                                                                |-- |-- |-- Nguyên lý kế toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="517" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kế toán tài chính</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="518" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kế toán ngân hàng thương mại</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="519" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kế toán quản trị</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="520" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thanh toán quốc tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="521" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thuế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="522" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lý thuyết kiểm toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="523" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kiểm toán hành chính sự nghiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="524" style="padding-left: 20px;">
                                                                                |-- |-- |-- Quản trị tài chính doanh nghiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="525" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kiểm toán phần hành</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="526" style="padding-left: 20px;">
                                                                                |-- |-- |-- Tài chính công</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="527" style="padding-left: 20px;">
                                                                                |-- |-- |-- Đầu tư quốc tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="528" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thẩm định dự án đầu tư</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="529" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kinh tế công cộng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="530" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kinh tế môi trường</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="531" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thị trường tài chính</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="532" style="padding-left: 20px;">
                                                                                |-- |-- |-- Phân tích tài chính doanh nghiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="533" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vận tải trong ngoại thương</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="534" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giao dịch thương mại quốc tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="535" style="padding-left: 20px;">
                                                                                |-- |-- |-- Marketing quốc tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="536" style="padding-left: 20px;">
                                                                                |-- |-- |-- Bảo hiểm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="537" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hải quan</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="538" style="padding-left: 20px;">
                                                                                |-- |-- |-- Dịch vụ - Du lịch</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="472" style="padding-left: 20px;">
                                                                                |-- |-- Khoa học xã hội</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="539" style="padding-left: 20px;">
                                                                                |-- |-- |-- Quan hệ quốc tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="540" style="padding-left: 20px;">
                                                                                |-- |-- |-- Giáo dục học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="541" style="padding-left: 20px;">
                                                                                |-- |-- |-- Đông phương học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="542" style="padding-left: 20px;">
                                                                                |-- |-- |-- Văn hóa - Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="543" style="padding-left: 20px;">
                                                                                |-- |-- |-- Xã hội học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="544" style="padding-left: 20px;">
                                                                                |-- |-- |-- Báo chí</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="545" style="padding-left: 20px;">
                                                                                |-- |-- |-- Địa lý học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="546" style="padding-left: 20px;">
                                                                                |-- |-- |-- Quản lý đô thị - Đất đai - Công tác xã hội</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="547" style="padding-left: 20px;">
                                                                                |-- |-- |-- Nhân học - Tâm lý học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="548" style="padding-left: 20px;">
                                                                                |-- |-- |-- Việt Nam học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="549" style="padding-left: 20px;">
                                                                                |-- |-- |-- Văn học - Ngôn ngữ học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="591" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hành chính - Văn thư</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="473" style="padding-left: 20px;">
                                                                                |-- |-- Luật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="550" style="padding-left: 20px;">
                                                                                |-- |-- |-- Luật hình sự - Luật tố tụng hình sự</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="551" style="padding-left: 20px;">
                                                                                |-- |-- |-- Luật thương mại</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="552" style="padding-left: 20px;">
                                                                                |-- |-- |-- Luật doanh nghiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="553" style="padding-left: 20px;">
                                                                                |-- |-- |-- Luật tài nguyên môi trường</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="554" style="padding-left: 20px;">
                                                                                |-- |-- |-- Luật dân sự</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="474" style="padding-left: 20px;">
                                                                                |-- |-- Công nghệ thông tin</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="555" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lập trình ứng dụng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="556" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lập trình web</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="557" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lập trình ứng dụng di động</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="558" style="padding-left: 20px;">
                                                                                |-- |-- |-- Ngôn ngữ nhúng và một số ngôn ngữ khác</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="559" style="padding-left: 20px;">
                                                                                |-- |-- |-- Database</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="560" style="padding-left: 20px;">
                                                                                |-- |-- |-- Lập trình trên social network platform</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="561" style="padding-left: 20px;">
                                                                                |-- |-- |-- Mã hóa - Giải mã và thuật toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="562" style="padding-left: 20px;">
                                                                                |-- |-- |-- Mạng căn bản</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="563" style="padding-left: 20px;">
                                                                                |-- |-- |-- Chuyên đề mạng không dây</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="564" style="padding-left: 20px;">
                                                                                |-- |-- |-- Quản trị mạng Linux</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="565" style="padding-left: 20px;">
                                                                                |-- |-- |-- Quản trị mạng Windows</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="566" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hệ thống mạng Cisco</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="567" style="padding-left: 20px;">
                                                                                |-- |-- |-- Bảo mật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="475" style="padding-left: 20px;">
                                                                                |-- |-- Sư phạm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="568" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm mầm non</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="569" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm tiểu học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="570" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm toán</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="571" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="572" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm hóa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="573" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="574" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm ngoại ngữ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="575" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="576" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm địa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="577" style="padding-left: 20px;">
                                                                                |-- |-- |-- Sư phạm sinh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="578" style="padding-left: 20px;">
                                                                                |-- |-- |-- Quản lý giáo dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="476" style="padding-left: 20px;">
                                                                                |-- |-- Kiến trúc - Xây dựng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="579" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thiết kế kiến trúc - Quy hoạch</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="580" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thiết kế nội thất</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="581" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thiết kế ngoại thất</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="582" style="padding-left: 20px;">
                                                                                |-- |-- |-- Màu sắc kiến trúc</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="583" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kỹ thuật nền móng - Tầng hầm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="584" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công trình giao thông, thủy lợi</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="585" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kết cấu - Thi công công trình</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="586" style="padding-left: 20px;">
                                                                                |-- |-- |-- Quy hoạch và khảo sát xây dựng </a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="587" style="padding-left: 20px;">
                                                                                |-- |-- |-- Thi công - Nghiệm thu và Thiết bị xây dựng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="588" style="padding-left: 20px;">
                                                                                |-- |-- |-- Vật liệu xây dựng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="589" style="padding-left: 20px;">
                                                                                |-- |-- |-- Văn bản pháp luật, quy chuẩn xây dựng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="590" style="padding-left: 20px;">
                                                                                |-- |-- |-- Phong thủy</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="477" style="padding-left: 20px;">
                                                                                |-- |-- Kỹ thuật - Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="592" style="padding-left: 20px;">
                                                                                |-- |-- |-- Cơ khí - Luyện kim</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="593" style="padding-left: 20px;">
                                                                                |-- |-- |-- Điện - Điện tử - Viễn thông</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="594" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hóa dầu - Tàu thủy</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="595" style="padding-left: 20px;">
                                                                                |-- |-- |-- Cơ điện tử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="596" style="padding-left: 20px;">
                                                                                |-- |-- |-- Hàng không</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="597" style="padding-left: 20px;">
                                                                                |-- |-- |-- Điều khiển và tự động hóa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="598" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kỹ thuật hạt nhân</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="599" style="padding-left: 20px;">
                                                                                |-- |-- |-- Kỹ thuật nhiệt lạnh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="600" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="4" value="601" style="padding-left: 20px;">
                                                                                |-- |-- |-- Công nghệ thực phẩm</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="284" style="">Giáo án - Bài
                                                                giảng</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="285" style="padding-left: 20px;">
                                                                                |-- Hóa học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="734" style="padding-left: 20px;">
                                                                                |-- Văn Mẫu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="735" style="padding-left: 20px;">
                                                                                |-- |-- Văn Bản Mẫu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="736" style="padding-left: 20px;">
                                                                                |-- |-- Văn Biểu Cảm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="737" style="padding-left: 20px;">
                                                                                |-- |-- Văn Chứng Minh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="738" style="padding-left: 20px;">
                                                                                |-- |-- Văn Kể Chuyện</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="739" style="padding-left: 20px;">
                                                                                |-- |-- Văn Miêu Tả</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="740" style="padding-left: 20px;">
                                                                                |-- |-- Văn Nghị Luận</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="741" style="padding-left: 20px;">
                                                                                |-- |-- Văn Thuyết Minh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="3" value="742" style="padding-left: 20px;">
                                                                                |-- |-- Văn Tự Sự</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="286" style="padding-left: 20px;">
                                                                                |-- Ngữ văn</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="287" style="padding-left: 20px;">
                                                                                |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="288" style="padding-left: 20px;">
                                                                                |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="289" style="padding-left: 20px;">
                                                                                |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="290" style="padding-left: 20px;">
                                                                                |-- Lịch sử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="291" style="padding-left: 20px;">
                                                                                |-- Địa lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="292" style="padding-left: 20px;">
                                                                                |-- GDCD-GDNGLL</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="293" style="padding-left: 20px;">
                                                                                |-- Âm nhạc</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="294" style="padding-left: 20px;">
                                                                                |-- Mỹ thuật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="295" style="padding-left: 20px;">
                                                                                |-- Thể dục</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="296" style="padding-left: 20px;">
                                                                                |-- Công nghệ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="297" style="padding-left: 20px;">
                                                                                |-- Tin học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="298" style="padding-left: 20px;">
                                                                                |-- Tiếng anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="299" style="padding-left: 20px;">
                                                                                |-- Giáo dục hướng nhiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="300" style="padding-left: 20px;">
                                                                                |-- Mầm non - Mẫu giáo</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="301" style="padding-left: 20px;">
                                                                                |-- Tiểu học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="302" style="padding-left: 20px;">
                                                                                |-- Cao đẳng - Đại học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="303" style="padding-left: 20px;">
                                                                                |-- Tư liệu khác</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="62" style="">Công Nghệ Thông
                                                                Tin</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="68" style="padding-left: 20px;">
                                                                                |-- Kỹ thuật lập trình</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="65" style="padding-left: 20px;">
                                                                                |-- Quản trị mạng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="72" style="padding-left: 20px;">
                                                                                |-- Thiết kế - Đồ họa - Flash</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="64" style="padding-left: 20px;">
                                                                                |-- Hệ điều hành</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="67" style="padding-left: 20px;">
                                                                                |-- Cơ sở dữ liệu</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="70" style="padding-left: 20px;">
                                                                                |-- Tin học văn phòng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="71" style="padding-left: 20px;">
                                                                                |-- An ninh - Bảo mật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="66" style="padding-left: 20px;">
                                                                                |-- Quản trị Web</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="69" style="padding-left: 20px;">
                                                                                |-- Chứng chỉ quốc tế</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="63" style="padding-left: 20px;">
                                                                                |-- Phần cứng</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="85" style="">Kỹ Thuật - Công
                                                                Nghệ</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="88" style="padding-left: 20px;">
                                                                                |-- Cơ khí - Chế tạo máy</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="86" style="padding-left: 20px;">
                                                                                |-- Điện - Điện tử</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="87" style="padding-left: 20px;">
                                                                                |-- Kĩ thuật Viễn thông</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="90" style="padding-left: 20px;">
                                                                                |-- Kiến trúc - Xây dựng</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="91" style="padding-left: 20px;">
                                                                                |-- Tự động hóa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="89" style="padding-left: 20px;">
                                                                                |-- Hóa học - Dầu khí</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="92" style="padding-left: 20px;">
                                                                                |-- Năng lượng</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="73" style="">Ngoại Ngữ</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="1847" style="padding-left: 20px;">
                                                                                |-- Luận văn báo cáo - ngoại ngữ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="80" style="padding-left: 20px;">
                                                                                |-- TOEFL - IELTS - TOEIC</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="78" style="padding-left: 20px;">
                                                                                |-- Ngữ pháp tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="77" style="padding-left: 20px;">
                                                                                |-- Anh ngữ phổ thông</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="75" style="padding-left: 20px;">
                                                                                |-- Anh văn thương mại</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="76" style="padding-left: 20px;">
                                                                                |-- Anh ngữ cho trẻ em</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="81" style="padding-left: 20px;">
                                                                                |-- Kỹ năng nghe tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="74" style="padding-left: 20px;">
                                                                                |-- Kỹ năng nói tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="82" style="padding-left: 20px;">
                                                                                |-- Kỹ năng đọc tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="83" style="padding-left: 20px;">
                                                                                |-- Kỹ năng viết tiếng Anh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="79" style="padding-left: 20px;">
                                                                                |-- Chứng chỉ A, B, C</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="187" style="padding-left: 20px;">
                                                                                |-- Tổng hợp</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="93" style="">Khoa Học Tự
                                                                Nhiên</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="94" style="padding-left: 20px;">
                                                                                |-- Toán học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="95" style="padding-left: 20px;">
                                                                                |-- Vật lý</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="96" style="padding-left: 20px;">
                                                                                |-- Hóa học - Dầu khí</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="97" style="padding-left: 20px;">
                                                                                |-- Sinh học</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="98" style="padding-left: 20px;">
                                                                                |-- Môi trường</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="121" style="">Y Tế - Sức
                                                                Khỏe</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="126" style="padding-left: 20px;">
                                                                                |-- Y học thưởng thức</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="122" style="padding-left: 20px;">
                                                                                |-- Sức khỏe trẻ em</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="123" style="padding-left: 20px;">
                                                                                |-- Sức khỏe phụ nữ</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="124" style="padding-left: 20px;">
                                                                                |-- Sức khỏe người cao tuổi</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="125" style="padding-left: 20px;">
                                                                                |-- Sức khỏe giới tính</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="104" style="">Văn Hóa - Nghệ
                                                                Thuật</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="109" style="padding-left: 20px;">
                                                                                |-- Âm nhạc</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="106" style="padding-left: 20px;">
                                                                                |-- Ẩm thực</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="107" style="padding-left: 20px;">
                                                                                |-- Khéo tay hay làm</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="112" style="padding-left: 20px;">
                                                                                |-- Chụp ảnh - Quay phim</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="110" style="padding-left: 20px;">
                                                                                |-- Mỹ thuật</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="111" style="padding-left: 20px;">
                                                                                |-- Điêu khắc - Hội họa</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="105" style="padding-left: 20px;">
                                                                                |-- Thời trang - Làm đẹp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="108" style="padding-left: 20px;">
                                                                                |-- Sân khấu điện ảnh</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="119" style="padding-left: 20px;">
                                                                                |-- Du lịch</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="165" style="">Nông - Lâm -
                                                                Ngư</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="166" style="padding-left: 20px;">
                                                                                |-- Nông nghiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="167" style="padding-left: 20px;">
                                                                                |-- Lâm nghiệp</a></li>
                                                                <li><a onclick="filter_category(this)" lev="2" value="168" style="padding-left: 20px;">
                                                                                |-- Ngư nghiệp</a></li>
                                                        </ul>
                                                </li>
                                                <li><em>+</em><a onclick="filter_category(this)" lev="1" value="186" style="">Thể loại khác</a>
                                                        <ul class="hide">
                                                                <li><a onclick="filter_category(this)" lev="2" value="188" style="padding-left: 20px;">
                                                                                |-- Tài liệu khác</a></li>
                                                        </ul>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                        <div class="filterItem"><a href="javascript:;" onclick="showFilterMobi(this)"> -- Sắp xếp -- </a>
                                <div>
                                        <ul>
                                                <li><a href="javascript:;" data-rel="1" onclick="filter_sort(this)"> Lượt tải về </a></li>
                                                <li><a href="javascript:;" data-rel="2" onclick="filter_sort(this)"> Lượt xem </a></li>
                                        </ul>
                                </div>
                        </div>
                        <div class="filterItem"><a href="javascript:;" onclick="showFilterMobi(this)"> -- Loại file -- </a>
                                <div>
                                        <ul>
                                                <li><a href="javascript:;" data-rel="1" onclick="filter_extension(this)"> .doc </a></li>
                                                <li><a href="javascript:;" data-rel="2" onclick="filter_extension(this)"> .pdf </a></li>
                                                <li><a href="javascript:;" data-rel="3" onclick="filter_extension(this)"> .docx </a></li>
                                                <li><a href="javascript:;" data-rel="4" onclick="filter_extension(this)"> .ppt </a></li>
                                                <li><a href="javascript:;" data-rel="5" onclick="filter_extension(this)"> .pptx </a></li>
                                                <li><a href="javascript:;" data-rel="6" onclick="filter_extension(this)"> .pot </a></li>
                                                <li><a href="javascript:;" data-rel="7" onclick="filter_extension(this)"> .potx </a></li>
                                                <li><a href="javascript:;" data-rel="8" onclick="filter_extension(this)"> .pps </a></li>
                                                <li><a href="javascript:;" data-rel="9" onclick="filter_extension(this)"> .ppsx </a></li>
                                        </ul>
                                </div>
                        </div>
                        <div class="filterItem"><a href="javascript:;" class="w-40" onclick="showFilterMobi(this)"> -- Độ dài
                                        -- </a>
                                <div>
                                        <ul>
                                                <li><a href="javascript:;" data-rel="1" onclick="filter_numpage(this)"> Từ 1 đến 4 trang </a>
                                                </li>
                                                <li><a href="javascript:;" data-rel="2" onclick="filter_numpage(this)"> Từ 5 đến 20 trang </a>
                                                </li>
                                                <li><a href="javascript:;" data-rel="3" onclick="filter_numpage(this)"> Từ 21 đến 100 trang </a>
                                                </li>
                                                <li><a href="javascript:;" data-rel="4" onclick="filter_numpage(this)"> Dài hơn 100 trang </a>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                        <div class="filterItem"><a href="javascript:;" onclick="showFilterMobi(this)"> -- Giá cả -- </a>
                                <div>
                                        <ul>
                                                <li><a href="javascript:;" data-rel="1" onclick="filter_sale(this)"> Miễn phí </a></li>
                                                <li><a href="javascript:;" data-rel="2" onclick="filter_sale(this)"> Thu phí </a></li>
                                        </ul>
                                </div>
                        </div>
                </div>
                <div class="search_bottom grid grid-cols-12 gap-2 p-2 bg-white">
                        <div class="search_bottom_left col-span-12 md:col-span-3">
                                <ul class="hidden md:block">
                                        <li><a class="active" href="javascript:;"> <i class="icon"></i>Tài liệu (514k+) </a></li>
                                        <li><a href="<?= URL ?>/search/users?keyword=<?=$this->keyword?>"> <i class="icon"></i>Thành viên </a></li>
                                        <li><a href="<?= URL ?>/search/collection?keyword=<?=$this->keyword?>"> <i class="icon"></i>Bộ sưu tập </a></li>
                                </ul>
                                <div style="height: 100%; position: relative" class="hidden md:block text-center mt-2">
                                        <div style="position: sticky;top: 10px;">
                                                <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="3807520840"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                </div>
                        </div>
                        <div class="search_bottom_right col-span-12  md:col-span-7">
                                <div class="list_item">
                                        <?php
                                        foreach ($this->data as $item) { ?>
                                                <div class="item grid grid-cols-12 gap-2">
                                                        <a href="<?= URL ?>/document/<?= $item['url'] ?>" title="<?= $item['name'] ?>"> <img class="border border-solid border-gray-400" alt="<?= $item['name'] ?>" src="<?= URL ?>/<?= $item['hinhanh'] ?>" onerror="this.src='<?= URL ?>/template/images/default/no-image.png';">
                                                        </a>
                                                        <div class="col-span-9 md:col-span-10 px-2 ">
                                                                <h2 class="text-xl"><a href="<?= URL ?>/document/<?= $item['url'] ?>" title="<?= $item['name'] ?>"> <strong><?= $item['name'] ?></strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                                <div class="text-base item_description"><?= $item['description'] ?></div>
                                                                <ul class="doc_tk_cnt">
                                                                        <li><i class="icon_doc"></i>9</li>
                                                                        <li><i class="icon_view"></i><?= $item['view'] ?></li>
                                                                        <li><i class="icon_down"></i><?= $item['download'] ?></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        <?php
                                        }
                                        ?>


                                        <!-- <div class="item grid grid-cols-12 gap-2"><a href="/document/185-bai-hoc-tu-nhung-vi-sep-khong-giong-ai.htm" title="Bài học từ những vị sếp không giống ai" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Bài học từ những vị sếp không giống ai" src="https://media.123dok.info/images/document/12/gu/wb/small_wbf1344063892.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/185-bai-hoc-tu-nhung-vi-sep-khong-giong-ai.htm" title="Bài học từ những vị sếp không giống ai"> <strong>Bài học từ những vị sếp
                                                                                không giống ai</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Không phải chỉ những vị sếp tốt mới để lại những
                                                                bài học về cách lãnh đạo, mà chúng ta có thể học được từ những vị sếp “không giống
                                                                ai”.Tất nhiên đó là những bài học giúp bạn tránh x 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>5</li>
                                                                <li><i class="icon_view"></i>2,011</li>
                                                                <li><i class="icon_down"></i>8</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/186-bai-hoc-tu-nhung-ceo-mat-viec.htm" title="Bài học từ những CEO mất việc" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Bài học từ những CEO mất việc" src="https://media.123dok.info/images/document/12/gu/gk/small_gkh1344063904.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/186-bai-hoc-tu-nhung-ceo-mat-viec.htm" title="Bài học từ những CEO mất việc"> <strong>Bài học từ những CEO mất
                                                                                việc</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Trong mấy tháng qua, cuộc khủng hoảng trên thị
                                                                trường cho vay tín chấp “dưới chuẩn” và sự đóng băng của thị trường địa ốc Mỹ đã khiến
                                                                nền kinh tế nước này phải hứng chịu hàng loạt tác đ 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>8</li>
                                                                <li><i class="icon_view"></i>1,180</li>
                                                                <li><i class="icon_down"></i>6</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/187-bai-hoc-rut-ra-sau-30-nam-lam-lanh-dao.htm" title="Bài học rút ra sau 30 năm làm lãnh đạo" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Bài học rút ra sau 30 năm làm lãnh đạo" src="https://media.123dok.info/images/document/12/gu/qf/small_qfd1344063915.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/187-bai-hoc-rut-ra-sau-30-nam-lam-lanh-dao.htm" title="Bài học rút ra sau 30 năm làm lãnh đạo"> <strong>Bài học rút ra sau 30
                                                                                năm làm lãnh đạo</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Tác giả bài viết này đã có cuộc gặp gỡ thân thiện
                                                                với Dick Harrington, cựu CEO của Thomson Reuters và hiện là thành viên tại Cue Ball, một
                                                                hãng đầu tư mạo hiểm có trụ sở tại Boston. Chúng tôi đã có c 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>8</li>
                                                                <li><i class="icon_view"></i>1,415</li>
                                                                <li><i class="icon_down"></i>8</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/314-cac-ky-nang-can-thiet-trong-hoc-tap-lam-viec-theo-nhom.htm" title="Các kỹ năng cần thiết trong học tập,làm việc theo nhóm" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Các kỹ năng cần thiết trong học tập,làm việc theo nhóm" src="https://media.123dok.info/images/document/12/gu/ka/small_kak1344183088.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';">
                                                </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/314-cac-ky-nang-can-thiet-trong-hoc-tap-lam-viec-theo-nhom.htm" title="Các kỹ năng cần thiết trong học tập,làm việc theo nhóm"> <strong>Các kỹ
                                                                                năng cần thiết trong học tập,làm việc theo nhóm</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> "Trong việc giáo dục, vị trí rộng lớn nhất cho quá
                                                                trình tự bồi dưỡng… chỉ có qua con đường tự học, loài người mới có thể phát triển mạnh
                                                                mẽ lên được" HERBERT SPENCER 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>27</li>
                                                                <li><i class="icon_view"></i>3,550</li>
                                                                <li><i class="icon_down"></i>9</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/316-hoc-cach-lam-viec-chuyen-nghiep.htm" title="Học cách làm việc chuyên nghiệp" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Học cách làm việc chuyên nghiệp" src="https://media.123dok.info/images/document/12/gu/ca/small_cai1344183110.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/316-hoc-cach-lam-viec-chuyen-nghiep.htm" title="Học cách làm việc chuyên nghiệp"> <strong>Học cách làm việc chuyên
                                                                                nghiệp</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Học cách làm việc chuyên nghiệp Không bao giờ la
                                                                mắng nhân viên, kể cả khi họ không hoàn thành nhiệm vụ; khi giao việc cho nhân viên luôn
                                                                kèm theo những lời động viên, khuyến khích... Noel năm nào chị 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>2</li>
                                                                <li><i class="icon_view"></i>2,397</li>
                                                                <li><i class="icon_down"></i>4</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/329-sinh-vien-hoc-ky-nang-lam-viec-nhom.htm" title="Sinh viên học kỹ năng làm việc nhóm" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Sinh viên học kỹ năng làm việc nhóm" src="https://media.123dok.info/images/document/12/gu/ov/small_ovs1344183793.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/329-sinh-vien-hoc-ky-nang-lam-viec-nhom.htm" title="Sinh viên học kỹ năng làm việc nhóm"> <strong>Sinh viên học kỹ năng làm
                                                                                việc nhóm</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Làm việc theo nhóm có khó không? Theo đánh giá của
                                                                các nhà doanh nghiêp, kỹ năng làm việc theo nhóm chính là điểm yếu của người Việt Nam.
                                                                Có 1 cách đánh giá cho rằng "Một người Việt Nam nếu làm việc 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>2</li>
                                                                <li><i class="icon_view"></i>4,253</li>
                                                                <li><i class="icon_down"></i>36</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/348-hoc-tu-dan-seu.htm" title="Học từ đàn sếu" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Học từ đàn sếu" src="https://media.123dok.info/images/document/12/gu/jh/small_jhn1344184587.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/348-hoc-tu-dan-seu.htm" title="Học từ đàn sếu"> <strong>Học từ
                                                                                đàn sếu</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Bài học bổ ích từ những chú sếu bé nhỏ cho cuộc
                                                                sống của chúng ta. 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>20</li>
                                                                <li><i class="icon_view"></i>1,056</li>
                                                                <li><i class="icon_down"></i>0</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/409-kinh-nghiem-hoc-english-pdf.htm" title="Kinh Nghiem Hoc English.pdf" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Kinh Nghiem Hoc English.pdf" src="https://media.123dok.info/images/document/12/gu/kb/small_kbj1344237346.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/409-kinh-nghiem-hoc-english-pdf.htm" title="Kinh Nghiem Hoc English.pdf"> <strong>Kinh Nghiem Hoc
                                                                                English.pdf</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> kinh nghiệm . cõu tr li s l 3 hoc 4 con s. Cũn nu
                                                                tớnh theo dng 24 gi thỡ cõu tr li s cú 4 con s. Trong c hai trng hp, nhng con s s c ngn
                                                                cỏch bi du phy , hoc du hai chm. Bước 5: Ở bước này, bạn sẽ dựa trên tapescript để phát
                                                                triển kĩ năng nói. Theokinh nghiệm của những người Việt “nói tiếng Anh như người bản
                                                                ngữ” và đã từng
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>58</li>
                                                                <li><i class="icon_view"></i>1,969</li>
                                                                <li><i class="icon_down"></i>9</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/411-vui-hoc-anh-ngu-pdf.htm" title="Vui hoc Anh Ngu.pdf" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Vui hoc Anh Ngu.pdf" src="https://media.123dok.info/images/document/12/gu/od/small_ods1344238090.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/411-vui-hoc-anh-ngu-pdf.htm" title="Vui hoc Anh Ngu.pdf">
                                                                        <strong>Vui hoc Anh Ngu.pdf</strong> <i class="icon i_type_doc i_type_doc2"></i>
                                                                </a></h2>
                                                        <div class="text-base item_description"> thú vị . mà anh ta muốn có. Một thanh niên có thể
                                                                đau khổ vì con quái vật mắt xanh nếu bạn gái của anh ta bắt đầu đi chơi với ai khác.
                                                                Hoặc con quái vật mắt xanh. Côấy dùng nhiều cách để lôi cuốn anh ấy hoặc làm cho anh ấy
                                                                chú ý đến cô. Khi anh ấy nhìn cô, cô có thể tìm cách làm cho anh ấy thích cô bằng
                                                                cáchlàm điệu
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>36</li>
                                                                <li><i class="icon_view"></i>1,603</li>
                                                                <li><i class="icon_down"></i>15</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/600-con-nguoi-duoi-goc-nhin-cua-triet-hoc-va-van-de-con-nguoi-trong-qua-trinh-doi-moi-hien-nay.htm" title="Con người dưới góc nhìn của triết học và vấn đề con người trong quá trình đổi mới hiện nay" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Con người dưới góc nhìn của triết học và vấn đề con người trong quá trình đổi mới hiện nay" src="https://media.123dok.info/images/document/12/gu/fu/small_fuz1344331546.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';">
                                                </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/600-con-nguoi-duoi-goc-nhin-cua-triet-hoc-va-van-de-con-nguoi-trong-qua-trinh-doi-moi-hien-nay.htm" title="Con người dưới góc nhìn của triết học và vấn đề con người trong quá trình đổi mới hiện nay">
                                                                        <strong>Con người dưới góc nhìn của triết học và vấn đề con người trong quá trình
                                                                                đổi mới hiện nay</strong> <i class="icon i_type_doc i_type_doc1"></i> </a></h2>
                                                        <div class="text-base item_description"> góc nhìn triết học . trình đổi mới của nớc ta cũng
                                                                nh xu hớng phát triển kinh tế nói chung trên thế giới .Đề tài: Vấn đề triết học về con
                                                                ngời và con ngời trong quá trình đổi mới. khi bớc vào công cuộc đổi mới, vấn đề quan
                                                                trọng đợc đặt ra giữa đổi mới kinh tế và đổi mới chính trị là phải có sự kết hợp ngay từ
                                                                đầu, lấy đổi mới kinh
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>18</li>
                                                                <li><i class="icon_view"></i>3,404</li>
                                                                <li><i class="icon_down"></i>20</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/628-co-so-va-phuong-phap-sinh-hoc-phan-tu.htm" title="Cơ sở và phương pháp sinh học phân tử" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Cơ sở và phương pháp sinh học phân tử" src="https://media.123dok.info/images/document/12/gu/sh/small_shl1344394324.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/628-co-so-va-phuong-phap-sinh-hoc-phan-tu.htm" title="Cơ sở và phương pháp sinh học phân tử"> <strong>Cơ sở và phương pháp sinh
                                                                                học phân tử</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Cơ sở và phương pháp sinh học phân tử .
                                                                bố.....................155CƠ SỞ VÀ PHƯƠNG PHÁP SINH HỌC PHÂN TỬ
                                                                .....................................................162 3 Lời nói đầu Sinh học phân tử
                                                                hiện. nghệ sinh học góp phần vào sự nghiệp Công nghiệp hoá, hiện đại hoá Cơ sở và phương
                                                                pháp Sinh học phân tử được biên soạn từ nhiều tài liệu, bài giảng và công
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>162</li>
                                                                <li><i class="icon_view"></i>2,580</li>
                                                                <li><i class="icon_down"></i>43</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/698-sinh-hoc-phan-tu.htm" title="Sinh Hoc Phan Tu" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Sinh Hoc Phan Tu" src="https://media.123dok.info/images/document/12/gu/nf/small_nfw1344398213.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/698-sinh-hoc-phan-tu.htm" title="Sinh Hoc Phan Tu"> <strong>Sinh
                                                                                Hoc Phan Tu</strong> <i class="icon i_type_doc i_type_doc1"></i> </a></h2>
                                                        <div class="text-base item_description"> Sinh Hoc Phan Tu . Hiện nay, sinh học phân tử và
                                                                sinh học tế bào được xem là nền tảng quan trọng của công nghệ sinh học. Nhờ phát triển
                                                                các công cụ cơ bản của sinh học phân. này có phần trùng lặp với một số môn học khác
                                                                trong sinh học đặc biệt là di truyền học và hóa sinh học. Sinh học phân tử chủ yếu tập
                                                                trung nghiên cứu mối
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>220</li>
                                                                <li><i class="icon_view"></i>2,893</li>
                                                                <li><i class="icon_down"></i>45</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/716-vi-sinh-vat-hoc-thu-y.htm" title="vi-sinh-vat-hoc-thu-y" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="vi-sinh-vat-hoc-thu-y" src="https://media.123dok.info/images/document/12/gu/vv/small_vvr1344399214.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/716-vi-sinh-vat-hoc-thu-y.htm" title="vi-sinh-vat-hoc-thu-y">
                                                                        <strong>vi-sinh-vat-hoc-thu-y</strong> <i class="icon i_type_doc i_type_doc2"></i>
                                                                </a></h2>
                                                        <div class="text-base item_description"> vi-sinh-vat-hoc-thu-y 123doc.vn</div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>260</li>
                                                                <li><i class="icon_view"></i>2,259</li>
                                                                <li><i class="icon_down"></i>20</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/730-ky-nang-ban-hang-hoan-hao-hoc-tu-the-gioi-dong-vat.htm" title="Kỹ năng bán hàng hoàn hảo học từ thế giới động vật" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Kỹ năng bán hàng hoàn hảo học từ thế giới động vật" src="https://media.123dok.info/images/document/12/gu/zy/small_zyu1344400816.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';">
                                                </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/730-ky-nang-ban-hang-hoan-hao-hoc-tu-the-gioi-dong-vat.htm" title="Kỹ năng bán hàng hoàn hảo học từ thế giới động vật"> <strong>Kỹ năng bán
                                                                                hàng hoàn hảo học từ thế giới động vật</strong> <i class="icon i_type_doc i_type_doc3"></i> </a></h2>
                                                        <div class="text-base item_description"> Mỗi một loài động vật đều có những đặc điểm nổi
                                                                trội giúp cho chúng sinh tồn trong môi trường tự nhiên khắc nghiệt. Và mỗi nhân viên bán
                                                                hàng đều có thể học hỏi được rất nhiều thứ từ th . Kỹ năng bán hàng hoàn hảo học từ thế
                                                                giới động vật! Mỗi một loài động vật đều có những đặc điểm nổi trội giúp cho. cứu về
                                                                hoạt động bán hàng nhằm lôi cuốn, thuyết phục khách hàng cho thấy có rất ...
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>3</li>
                                                                <li><i class="icon_view"></i>2,718</li>
                                                                <li><i class="icon_down"></i>18</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/798-10-bai-hoc-tren-chiec-khan.htm" title="10 bài học trên chiếc khăn " class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="10 bài học trên chiếc khăn " src="https://media.123dok.info/images/document/12/gu/jo/small_joh1344412466.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/798-10-bai-hoc-tren-chiec-khan.htm" title="10 bài học trên chiếc khăn "> <strong>10 bài học trên chiếc
                                                                                khăn </strong> <i class="icon i_type_doc i_type_doc1"></i> </a></h2>
                                                        <div class="text-base item_description"> Trước khi đi sâu vào chi tiết của “ các bài học”,
                                                                cho phép tôi trả lời câu hỏi cơ bản nhất và thường được đưa ra nhiềunhất: “MLM là gì ?”.
                                                                Trong cuốn sách này ta sẽ sử dụng các khái niệm: . 2. Ðại lý viên có đủ các điều kiện
                                                                sau: a) Tốt nghiệp Ðại học Hàng hải hoặc Ðại học Ngoại thương hoặc có thời gian thực
                                                                hiện nghiệp vụ liên quan. vụ hàng hải; 2. Nhân viên mơi giới hàng hải tốt ...
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>8</li>
                                                                <li><i class="icon_view"></i>1,937</li>
                                                                <li><i class="icon_down"></i>3</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/971-ly-luan-vua-la-mot-mon-khoa-hoc-va-mon-nghe-thuat.htm" title=" Lý Luận vừa là Một Môn Khoa Học và Môn Nghệ Thuật" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt=" Lý Luận vừa là Một Môn Khoa Học và Môn Nghệ Thuật" src="https://media.123dok.info/images/document/12/gu/rl/small_rlb1344439817.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';">
                                                </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/971-ly-luan-vua-la-mot-mon-khoa-hoc-va-mon-nghe-thuat.htm" title=" Lý Luận vừa là Một Môn Khoa Học và Môn Nghệ Thuật"> <strong> Lý Luận vừa
                                                                                là Một Môn Khoa Học và Môn Nghệ Thuật</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Có phải lý luận học là một môn khoa học (như thiên
                                                                văn học hay di truyền học), hoặc là một môn nghệ thuật thực hành (như thể dục hay nấu
                                                                ăn) không? Có phải mục tiêu của nó là mô tả sự tự nhiên 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>4</li>
                                                                <li><i class="icon_view"></i>1,136</li>
                                                                <li><i class="icon_down"></i>1</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/1080-thuyet-trinh-bao-cao-khoa-hoc.htm" title="Thuyết trình báo cáo khoa học" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Thuyết trình báo cáo khoa học" src="https://media.123dok.info/images/document/12/gu/dn/small_dnp1344524615.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/1080-thuyet-trinh-bao-cao-khoa-hoc.htm" title="Thuyết trình báo cáo khoa học"> <strong>Thuyết trình báo cáo khoa
                                                                                học</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Sử dụng hình ảnh trực quanNếu bạn dự định sử dụng
                                                                các hình ảnh trực quan, hãy chú ý là không nên có quá nhiều thông tin trên mỗi slide.
                                                                Tổng quát, hình ảnh trực quan chỉ nên sử dụng để minh họ 123doc.vn
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>10</li>
                                                                <li><i class="icon_view"></i>2,510</li>
                                                                <li><i class="icon_down"></i>19</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="ads_show hidden md:flex">
                                                <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="ads_show md:hidden">
                                                <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/1117-phat-minh-khoa-hoc-cua-nhan-loai-pdf.htm" title="Phát minh khoa học của nhân loại.pdf" class="col-span-3 md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Phát minh khoa học của nhân loại.pdf" src="https://media.123dok.info/images/document/12/gu/kg/small_kgo1344574442.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                <div class="col-span-9 md:col-span-10 px-2 ">
                                                        <h2 class="text-xl"><a href="/document/1117-phat-minh-khoa-hoc-cua-nhan-loai-pdf.htm" title="Phát minh khoa học của nhân loại.pdf"> <strong>Phát minh khoa học của
                                                                                nhân loại.pdf</strong> <i class="icon i_type_doc i_type_doc2"></i> </a></h2>
                                                        <div class="text-base item_description"> Tài liệu bổ ích và thú vị về những phát minh khoa
                                                                học của nhân loại. . lọc tự nhiên theo học thuyế của Darwin. Một vài nhà khoa học đã gọi
                                                                “loài” mới này là Robot sapiens, người máy khôn ngoan. Hình dạng của. .. robot khôn.
                                                                hành động của các nhà khoa học cho công trình này là “tiến hóa để thích nghi”. Agnès
                                                                Guillot, chuyên gia của AnimatLab thuộc phòng thí nghiệm tin ...
                                                        </div>
                                                        <ul class="doc_tk_cnt">
                                                                <li><i class="icon_doc"></i>93</li>
                                                                <li><i class="icon_view"></i>1,516</li>
                                                                <li><i class="icon_down"></i>5</li>
                                                        </ul>
                                                </div>
                                        </div>
                                        <div class="item grid grid-cols-12 gap-2"><a href="/document/1150-xu-ly-nhung-cau-hoi-hoc.htm" title="Xử lý những câu hỏi " hóc""="" class="col-span-3
                                                md:col-span-2 "> <img class="border border-solid border-gray-400" alt="Xử lý những câu hỏi " hóc""="" src="https://media.123dok.info/images/document/12/gu/ih/small_ihu1344580632.png" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';"> </a>
                                                        <div class="col-span-9 md:col-span-10 px-2 ">
                                                                <h2 class="text-xl"><a href="/document/1150-xu-ly-nhung-cau-hoi-hoc.htm" title="Xử lý những câu hỏi " hóc""=""> <strong>Xử lý những câu hỏi "hóc"</strong> <i class="icon i_type_doc i_type_doc2"></i> </a>
                                                                </h2>
                                                                <div class="text-base item_description"> Được các sếp trực tiếp phỏng vấn, điều đầu tiên bạn
                                                                        có thể cảm nhận là các câu hỏi khó đến toát mồ hôi. Bạn sẽ trả lời như thế nào? 1. Xem
                                                                        hồ sơ của anh/chị, tôi thấy có một số môn h 123doc.vn
                                                                </div>
                                                                <ul class="doc_tk_cnt">
                                                                        <li><i class="icon_doc"></i>4</li>
                                                                        <li><i class="icon_view"></i>1,345</li>
                                                                        <li><i class="icon_down"></i>7</li>
                                                                </ul>
                                                        </div>
                                                </div>
                                                <div class="ads_show hidden md:flex">
                                                        <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                                                        <script defer="">
                                                                (adsbygoogle = window.adsbygoogle || []).push({});
                                                        </script>
                                                </div>
                                                <div class="ads_show md:hidden">
                                                        <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="2330787642"></ins>
                                                        <script defer="">
                                                                (adsbygoogle = window.adsbygoogle || []).push({});
                                                        </script>
                                                </div>
                                        </div>
                                        <div class="paging"><a class="pagging_active">1</a> <a href="/global/home/search.php?q=H%E1%BB%8Dc&amp;page=2" class="pagging_normal">2</a> <a href="/global/home/search.php?q=H%E1%BB%8Dc&amp;page=3" class="pagging_normal">3</a> <a href="/global/home/search.php?q=H%E1%BB%8Dc&amp;page=4" class="pagging_normal">4</a> <a href="/global/home/search.php?q=H%E1%BB%8Dc&amp;page=5" class="pagging_normal">..</a> <a href="/global/home/search.php?q=H%E1%BB%8Dc&amp;page=5" class="pagging_normal">&gt;</a>
                                        </div> -->
                                </div>
                                <div style=" position: relative" class="col-span-2 hidden md:block">
                                        <div style="position: sticky;top: 10px;">
                                                <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="3807520840"></ins>
                                                <script defer="">
                                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>