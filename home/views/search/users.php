<link rel="stylesheet" type="text/css"
      href="<?=URL?>/template/styles/search.css"/>
<div id="container">
    <div class="container searchPage mx-auto"><h1 class="text-xl md:text-2xl p-2">Kết quả với từ khóa
            "<strong>Học</strong>"</h1>
        <div class="search_top"><label>Bạn có muốn tìm thêm với từ khóa: </label>
            <ul>
                <li><a title="việc học" href="/doc/s/việc+học">việc <b>học</b></a></li>
                <li><a title="học tiếng anh" href="<?=URL?>/search/document+tiếng+anh"><b>học</b> tiếng anh</a></li>
                <li><a title="học thuyết" href="<?=URL?>/search/document+thuyết"><b>học</b> thuyết</a></li>
                <li><a title="phương pháp học" href="/doc/s/phương+pháp+học">phương pháp <b>học</b></a></li>
                <li><a title="phương pháp học tập" href="/doc/s/phương+pháp+học+tập">phương pháp <b>học</b> tập</a></li>
                <li><a title="khoa học kỹ thuật" href="/doc/s/khoa+học+kỹ+thuật">khoa <b>học</b> kỹ thuật</a></li>
                <li><a title="đại học" href="/doc/s/đại+học">đại <b>học</b></a></li>
                <li><a title="trình độ đại học" href="/doc/s/trình+độ+đại+học">trình độ đại <b>học</b></a></li>
                <li><a title="khoa học công nghệ" href="/doc/s/khoa+học+công+nghệ">khoa <b>học</b> công nghệ</a></li>
                <li><a title="hướng dẫn học sinh" href="/doc/s/hướng+dẫn+học+sinh">hướng dẫn <b>học</b> sinh</a></li>
            </ul>
        </div>
        <div class="search_bottom grid grid-cols-12 gap-2 p-2 bg-white">
            <div class="search_bottom_left col-span-12 md:col-span-3">
                <ul class="hidden md:block">
                    <li><a href="<?=URL?>/search/document"> <i class="icon"></i>Tài liệu (280) </a></li>
                    <li><a class="active" href="javascript:;"> <i class="icon"></i>Thành viên </a></li>
                    <li><a href="<?=URL?>/search/collection"> <i class="icon"></i>Bộ sưu tập </a></li>
                </ul>
                <div class="flex md:hidden justify-between mb-4 ">
                    <div class="px-4"><a class="whitespace-no-wrap font-bold text-base" href="<?=URL?>/search/document"> <i
                                    class="icon"></i>Tài liệu (280) </a></div>
                    <div class="px-4 border-l border-r border-solid border-gray-500"><a
                                class="text-primary whitespace-no-wrap font-bold text-base" href="javascript:;"> <i
                                    class="icon"></i>Thành viên </a></div>
                    <div class="px-4"><a class="whitespace-no-wrap font-bold text-base" href="<?=URL?>/search/collection"> <i
                                    class="icon"></i>Bộ sưu tập </a></div>
                </div>
                <div style="height: 100%; position: relative" class="hidden md:block">
                    <div style="position: sticky;top: 10px;">
                        <ins class="adsbygoogle" style="display:inline-block;width:300px;height:600px"
                             data-ad-client="ca-pub-2979760623205174" data-ad-slot="8377321249"></ins>
                        <script defer="">(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    </div>
                </div>
            </div>
            <div class="search_bottom_right col-span-12  md:col-span-7">
                <div class="list_user_search">
                    <div><a href="/trang-ca-nhan-98-yeu-khoa-hoc.htm" title="Yeu Khoa Hoc" target="_blank"><img
                                    alt="Yeu Khoa Hoc" title="Yeu Khoa Hoc"
                                    src="https://media.123dok.info/images/user/larger_evm1344824645.jpg"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Yeu Khoa Hoc" href="/trang-ca-nhan-98-yeu-khoa-hoc.htm">Yeu Khoa Hoc</a></h2>
                            <p>Tải lên: 2,245 tài liệu</p>
                            <p>Ngày tham gia: 13/08/2012</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-18339-tai-lieu-sinh-hoc.htm" title="Tài liệu Sinh học "
                            target="_blank"><img alt="Tài liệu Sinh học " title="Tài liệu Sinh học "
                                                 src="https://media.123dok.info/images/default/user_medium.gif"
                                                 onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Tài liệu Sinh học " href="/trang-ca-nhan-18339-tai-lieu-sinh-hoc.htm">Tài
                                    liệu Sinh học </a></h2>
                            <p>Tải lên: 21 tài liệu</p>
                            <p>Ngày tham gia: 03/02/2013</p></div>
                    </div>
                    <div class="ads_show">
                        <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px"
                             data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                        <script defer="">(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    </div>
                    <div><a href="/trang-ca-nhan-40311-nguyen-hoai-hoc.htm" title="Nguyen Hoai Hoc" target="_blank"><img
                                    alt="Nguyen Hoai Hoc" title="Nguyen Hoai Hoc"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Nguyen Hoai Hoc" href="/trang-ca-nhan-40311-nguyen-hoai-hoc.htm">Nguyen Hoai
                                    Hoc</a></h2>
                            <p>Tải lên: 1 tài liệu</p>
                            <p>Ngày tham gia: 01/03/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-47019-hocoto.htm" title="hocoto" target="_blank"><img alt="hocoto"
                                                                                                       title="hocoto"
                                                                                                       src="https://media.123dok.info/images/default/user_medium.gif"
                                                                                                       onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="hocoto" href="/trang-ca-nhan-47019-hocoto.htm">hocoto</a></h2>
                            <p>Tải lên: 1 tài liệu</p>
                            <p>Ngày tham gia: 05/03/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-79218-tap-chi-hoa-hoc-tuoi-tre.htm"
                            title="Tạp Chí Hóa Học &amp; Tuổi Trẻ" target="_blank"><img
                                    alt="Tạp Chí Hóa Học &amp; Tuổi Trẻ" title="Tạp Chí Hóa Học &amp; Tuổi Trẻ"
                                    src="https://media.123dok.info/images/user/larger_bvc1376113310.jpg"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Tạp Chí Hóa Học &amp; Tuổi Trẻ"
                                    href="/trang-ca-nhan-79218-tap-chi-hoa-hoc-tuoi-tre.htm">Tạp Chí Hóa Học &amp; Tuổi
                                    Trẻ</a></h2>
                            <p>Tải lên: 45 tài liệu</p>
                            <p>Ngày tham gia: 19/03/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-91391-hieu-hoc.htm" title="Hiếu Học" target="_blank"><img
                                    alt="Hiếu Học" title="Hiếu Học"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Hiếu Học" href="/trang-ca-nhan-91391-hieu-hoc.htm">Hiếu Học</a></h2>
                            <p>Tải lên: 2 tài liệu</p>
                            <p>Ngày tham gia: 25/03/2013</p></div>
                    </div>
                    <div class="ads_show">
                        <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px"
                             data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                        <script defer="">(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    </div>
                    <div><a href="/trang-ca-nhan-125508-hochay.htm" title="hochay" target="_blank"><img alt="hochay"
                                                                                                        title="hochay"
                                                                                                        src="https://media.123dok.info/images/default/user_medium.gif"
                                                                                                        onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="hochay" href="/trang-ca-nhan-125508-hochay.htm">hochay</a></h2>
                            <p>Tải lên: 65 tài liệu</p>
                            <p>Ngày tham gia: 09/04/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-126202-hochht.htm" title="hochht" target="_blank"><img alt="hochht"
                                                                                                        title="hochht"
                                                                                                        src="https://media.123dok.info/images/default/user_medium.gif"
                                                                                                        onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="hochht" href="/trang-ca-nhan-126202-hochht.htm">hochht</a></h2>
                            <p>Tải lên: 1 tài liệu</p>
                            <p>Ngày tham gia: 09/04/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-174487-hoc-tran.htm" title="Học Trần" target="_blank"><img
                                    alt="Học Trần" title="Học Trần"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Học Trần" href="/trang-ca-nhan-174487-hoc-tran.htm">Học Trần</a></h2>
                            <p>Tải lên: 2 tài liệu</p>
                            <p>Ngày tham gia: 23/05/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-203474-tu-hoc-anh-van.htm" title="Tự Học Anh Văn" target="_blank"><img
                                    alt="Tự Học Anh Văn" title="Tự Học Anh Văn"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Tự Học Anh Văn" href="/trang-ca-nhan-203474-tu-hoc-anh-van.htm">Tự Học Anh
                                    Văn</a></h2>
                            <p>Tải lên: 8 tài liệu</p>
                            <p>Ngày tham gia: 21/06/2013</p></div>
                    </div>
                    <div class="ads_show">
                        <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px"
                             data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                        <script defer="">(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    </div>
                    <div><a href="/trang-ca-nhan-237945-dai-hoc-cong-nghe-thong-tin.htm"
                            title="Đại học Công nghệ Thông Tin" target="_blank"><img alt="Đại học Công nghệ Thông Tin"
                                                                                     title="Đại học Công nghệ Thông Tin"
                                                                                     src="https://media.123dok.info/images/default/user_medium.gif"
                                                                                     onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Đại học Công nghệ Thông Tin"
                                    href="/trang-ca-nhan-237945-dai-hoc-cong-nghe-thong-tin.htm">Đại học Công nghệ Thông
                                    Tin</a></h2>
                            <p>Tải lên: 2 tài liệu</p>
                            <p>Ngày tham gia: 21/07/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-244010-tai-lieu-y-hoc.htm" title="Tài liệu y học" target="_blank"><img
                                    alt="Tài liệu y học" title="Tài liệu y học"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Tài liệu y học" href="/trang-ca-nhan-244010-tai-lieu-y-hoc.htm">Tài liệu y
                                    học</a></h2>
                            <p>Tải lên: 4 tài liệu</p>
                            <p>Ngày tham gia: 26/07/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-252029-hoc-va-hoc.htm" title="Học Và Học" target="_blank"><img
                                    alt="Học Và Học" title="Học Và Học"
                                    src="https://media.123dok.info/images/user/larger_kjt1408972659.jpg"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Học Và Học" href="/trang-ca-nhan-252029-hoc-va-hoc.htm">Học Và Học</a></h2>
                            <p>Tải lên: 1 tài liệu</p>
                            <p>Ngày tham gia: 30/07/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-258551-tin-hoc-soi-xam.htm" title="Tin Học Sói Xám"
                            target="_blank"><img alt="Tin Học Sói Xám" title="Tin Học Sói Xám"
                                                 src="https://media.123dok.info/images/default/user_medium.gif"
                                                 onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Tin Học Sói Xám" href="/trang-ca-nhan-258551-tin-hoc-soi-xam.htm">Tin Học Sói
                                    Xám</a></h2>
                            <p>Tải lên: 4 tài liệu</p>
                            <p>Ngày tham gia: 03/08/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-280781-quang-hoc.htm" title="Quang Học" target="_blank"><img
                                    alt="Quang Học" title="Quang Học"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Quang Học" href="/trang-ca-nhan-280781-quang-hoc.htm">Quang Học</a></h2>
                            <p>Tải lên: 3 tài liệu</p>
                            <p>Ngày tham gia: 14/08/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-323082-do-huy-hoc.htm" title="Đỗ Huy Học" target="_blank"><img
                                    alt="Đỗ Huy Học" title="Đỗ Huy Học"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Đỗ Huy Học" href="/trang-ca-nhan-323082-do-huy-hoc.htm">Đỗ Huy Học</a></h2>
                            <p>Tải lên: 10 tài liệu</p>
                            <p>Ngày tham gia: 07/09/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-367009-bui-thai-hoc.htm" title="Bùi Thái Học" target="_blank"><img
                                    alt="Bùi Thái Học" title="Bùi Thái Học"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Bùi Thái Học" href="/trang-ca-nhan-367009-bui-thai-hoc.htm">Bùi Thái Học</a>
                            </h2>
                            <p>Tải lên: 12 tài liệu</p>
                            <p>Ngày tham gia: 27/09/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-369211-tai-lieu-y-hoc-hoa-hoc.htm" title="Tài Liệu Y Học Hóa Học"
                            target="_blank"><img alt="Tài Liệu Y Học Hóa Học" title="Tài Liệu Y Học Hóa Học"
                                                 src="https://media.123dok.info/images/default/user_medium.gif"
                                                 onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Tài Liệu Y Học Hóa Học"
                                    href="/trang-ca-nhan-369211-tai-lieu-y-hoc-hoa-hoc.htm">Tài Liệu Y Học Hóa Học</a>
                            </h2>
                            <p>Tải lên: 104 tài liệu</p>
                            <p>Ngày tham gia: 28/09/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-379467-chan-nuoi-lop-cao-hoc.htm" title="Chăn nuôi Lớp cao học"
                            target="_blank"><img alt="Chăn nuôi Lớp cao học" title="Chăn nuôi Lớp cao học"
                                                 src="https://media.123dok.info/images/default/user_medium.gif"
                                                 onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Chăn nuôi Lớp cao học"
                                    href="/trang-ca-nhan-379467-chan-nuoi-lop-cao-hoc.htm">Chăn nuôi Lớp cao học</a>
                            </h2>
                            <p>Tải lên: 1 tài liệu</p>
                            <p>Ngày tham gia: 02/10/2013</p></div>
                    </div>
                    <div><a href="/trang-ca-nhan-389145-hoc-di-nha.htm" title="Học Đi Nha" target="_blank"><img
                                    alt="Học Đi Nha" title="Học Đi Nha"
                                    src="https://media.123dok.info/images/default/user_medium.gif"
                                    onerror="this.src='https://media.123dok.info/images/default/user_medium.gif'"></a>
                        <div><h2><a title="Học Đi Nha" href="/trang-ca-nhan-389145-hoc-di-nha.htm">Học Đi Nha</a></h2>
                            <p>Tải lên: 1 tài liệu</p>
                            <p>Ngày tham gia: 05/10/2013</p></div>
                    </div>
                </div>
                <div class="paging"><a class="pagging_active">1</a> <a
                            href="/global/home/search_people.php?q=H%E1%BB%8Dc&amp;page=2" class="pagging_normal">2</a>
                    <a href="/global/home/search_people.php?q=H%E1%BB%8Dc&amp;page=3" class="pagging_normal">3</a> <a
                            href="/global/home/search_people.php?q=H%E1%BB%8Dc&amp;page=4" class="pagging_normal">4</a>
                    <a href="/global/home/search_people.php?q=H%E1%BB%8Dc&amp;page=5" class="pagging_normal">..</a> <a
                            href="/global/home/search_people.php?q=H%E1%BB%8Dc&amp;page=5"
                            class="pagging_normal">&gt;</a></div>
            </div>
            <div style=" position: relative" class="col-span-2 hidden md:block">
                <div style="position: sticky;top: 10px;">
                    <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px"
                         data-ad-client="ca-pub-2979760623205174" data-ad-slot="3807520840"></ins>
                    <script defer="">(adsbygoogle = window.adsbygoogle || []).push({});</script>
                </div>
            </div>
        </div>
    </div>
    <script>fixed_search_bottom_left();

        function fixed_search_bottom_left() {
            var search_bottom = $('.search_bottom');
            var _top = search_bottom.offset().top - 15;
            var _height_list_item = $('.list_item').height();
            var _height_search_bottom_left = $('.search_bottom_left', search_bottom).height();
            $(window).scroll(function () {
                if ($(this).scrollTop() > _top && $(this).scrollTop() < (_top + _height_list_item - _height_search_bottom_left)) {
                    search_bottom.addClass('fixed');
                } else {
                    search_bottom.removeClass('fixed');
                }
            });
        }

        function showMoreSearchResult(obj) {
            $.post('/global/ajax/aja_show_more_search.php', {}, function (data) {
                $('.list_item').append(data);
                fixed_search_bottom_left();
            });
        }</script>
</div>