<link rel="stylesheet" type="text/css"
      href="<?=URL?>/template/styles/search.css"/>
<div id="container">
    <div class="container searchPage mx-auto"><h1 class="text-xl md:text-2xl p-2">Kết quả với từ khóa
            "<strong>Học</strong>"</h1>
        <div class="search_top"><label>Bạn có muốn tìm thêm với từ khóa: </label>
            <ul>
                <li><a title="việc học" href="/doc/s/việc+học">việc <b>học</b></a></li>
                <li><a title="học tiếng anh" href="<?=URL?>/search/document+tiếng+anh"><b>học</b> tiếng anh</a></li>
                <li><a title="học thuyết" href="<?=URL?>/search/document+thuyết"><b>học</b> thuyết</a></li>
                <li><a title="phương pháp học" href="/doc/s/phương+pháp+học">phương pháp <b>học</b></a></li>
                <li><a title="phương pháp học tập" href="/doc/s/phương+pháp+học+tập">phương pháp <b>học</b> tập</a></li>
                <li><a title="khoa học kỹ thuật" href="/doc/s/khoa+học+kỹ+thuật">khoa <b>học</b> kỹ thuật</a></li>
                <li><a title="đại học" href="/doc/s/đại+học">đại <b>học</b></a></li>
                <li><a title="trình độ đại học" href="/doc/s/trình+độ+đại+học">trình độ đại <b>học</b></a></li>
                <li><a title="khoa học công nghệ" href="/doc/s/khoa+học+công+nghệ">khoa <b>học</b> công nghệ</a></li>
                <li><a title="hướng dẫn học sinh" href="/doc/s/hướng+dẫn+học+sinh">hướng dẫn <b>học</b> sinh</a></li>
            </ul>
        </div>
        <div class="search_bottom grid grid-cols-12 gap-2 p-2 bg-white">
            <div class="search_bottom_left col-span-12 md:col-span-3">
                <ul class="hidden md:block">
                    <li><a href="<?=URL?>/search/document"> <i class="icon"></i>Tài liệu (4,320) </a></li>
                    <li><a href="<?=URL?>/search/users"> <i class="icon"></i>Thành viên </a></li>
                    <li><a class="active" href="javascript:;"> <i class="icon"></i>Bộ sưu tập </a></li>
                </ul>
                <div class="flex md:hidden justify-between mb-4">
                    <div class="px-4"><a class="whitespace-no-wrap font-bold text-base" href="<?=URL?>/search/document"> <i
                                    class="icon"></i>Tài liệu (4,320) </a></div>
                    <div class="px-4 border-l border-r border-solid border-gray-500"><a
                                class="whitespace-no-wrap font-bold text-base" href="<?=URL?>/search/users"> <i
                                    class="icon"></i>Thành viên </a></div>
                    <div class="px-4"><a class="text-primary whitespace-no-wrap font-bold text-base"
                                         href="javascript:;"> <i class="icon"></i>Bộ sưu tập </a></div>
                </div>
                <div style="height: 100%; position: relative" class="hidden md:block">
                    <div style="position: sticky;top: 10px;">
                        <ins class="adsbygoogle" style="display:inline-block;width:300px;height:600px"
                             data-ad-client="ca-pub-2979760623205174" data-ad-slot="8377321249"></ins>
                        <script defer="">(adsbygoogle = window.adsbygoogle || []).push({});</script>
                    </div>
                </div>
            </div>
            <div class="search_bottom_right col-span-12  md:col-span-7">
                <div class="listCollection">
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/78-bai-hoc-cho-xu-the.htm"
                                         title="Bai hoc cho xu the"><span>1</span></a>
                        <div><h2><a title="Bai hoc cho xu the" href="/collection/78-bai-hoc-cho-xu-the.htm"
                                    target="_blank">Bai hoc cho xu the</a></h2>
                            <p><a href="/trang-ca-nhan-887-ha-mai.htm">Ha MAi</a></p>
                            <div> Nhung bai <b>hoc</b> de ung xu, giao tiep, dat duoc nhung dieu minh mong muon</div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>1</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3208-bo-suu-tap-ve-mon-tin-hoc-van-phong.htm"
                                         title="Bộ sưu tập về môn Tin học văn phòng"><span>16</span></a>
                        <div><h2><a title="Bộ sưu tập về môn Tin học văn phòng"
                                    href="/collection/3208-bo-suu-tap-ve-mon-tin-hoc-van-phong.htm" target="_blank">Bộ
                                    sưu tập về môn Tin học văn phòng</a></h2>
                            <p><a href="/trang-ca-nhan-44746-trieu-thu-thuy.htm">Triệu Thu Thủy</a></p>
                            <div> Tài liệu " Bộ sưu tập về môn tin <b>học</b> văn phòng " được biên soạn với mục đích
                                trang bị cho <b>học</b> viên những kiến thức và kỹ năng : sử dụng máy tính hợp lý, dễ
                                dàng, thao tác nhanh nhờ giao diện đồ họa ổn định, sử dụng đồng loại... trong đó có bao
                                gồm cả đề cương ôn tập, và đề thi trắc nghiệm của nhiều trường đại <b>học</b></div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>16</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3225-tuyen-tap-bo-ly-thuyet-vanh-cho-thac-si-cao-hoc.htm"
                                         title="Tuyển tập bộ Lý thuyết vành cho Thạc sĩ - Cao học"><span>26</span></a>
                        <div><h2><a title="Tuyển tập bộ Lý thuyết vành cho Thạc sĩ - Cao học"
                                    href="/collection/3225-tuyen-tap-bo-ly-thuyet-vanh-cho-thac-si-cao-hoc.htm"
                                    target="_blank">Tuyển tập bộ Lý thuyết vành cho Thạc sĩ - Cao học</a></h2>
                            <p><a href="/trang-ca-nhan-66270-pham-thi-hai-yen.htm">phạm thị hải yến</a></p>
                            <div> Trong toán học, lý thuyết vành nghiên cứu về vành, các cấu trúc đại số mà trong đó các
                                phép cộng và phép nhân được định nghĩa và có các tính chất tương tự như các tính chất
                                quen thuộc của số nguyên.
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>26</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3265-10-viec-can-thiet-cho-viec-hoc-tieng-anh.htm"
                                         title="10 việc cần thiết cho việc học tiếng anh"><span>1</span></a>
                        <div><h2><a title="10 việc cần thiết cho việc học tiếng anh"
                                    href="/collection/3265-10-viec-can-thiet-cho-viec-hoc-tieng-anh.htm"
                                    target="_blank">10 việc cần thiết cho việc học tiếng anh</a></h2>
                            <p><a href="/trang-ca-nhan-168827-hong-giang.htm">Hồng Giang</a></p>
                            <div> những điều cần thiết và hiệu quả cho người bắt đầu <b>học</b> tiếng anh</div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>1</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3343-bo-suu-tap-hoc-phan-quan-tri-chien-luoc.htm"
                                         title="Bộ sưu tập Học phần Quản Trị Chiến Lược"><span>22</span></a>
                        <div><h2><a title="Bộ sưu tập Học phần Quản Trị Chiến Lược"
                                    href="/collection/3343-bo-suu-tap-hoc-phan-quan-tri-chien-luoc.htm" target="_blank">Bộ
                                    sưu tập Học phần Quản Trị Chiến Lược</a></h2>
                            <p><a href="/trang-ca-nhan-44703-vo-inh-xinh.htm">Vờ_inh xinh</a></p>
                            <div> Quản trị chiến lược (tiếng Anh: strategic management) là khoa <b>học</b> và nghệ thuật
                                về chiến lược nhằm xây dựng phương hướng và mục tiêu kinh doanh, triển khai, thực hiện
                                kế hoạch ngắn hạn và dài hạn trên cơ sở nguồn lực hiện có nhằm giúp cho mỗi tổ chức có
                                thể đạt được các mục tiêu dài hạn của nó.
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>22</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3349-bo-suu-tap-tin-hoc-dai-cuong.htm"
                                         title="Bộ sưu tập Tin Học Đại Cương"><span>22</span></a>
                        <div><h2><a title="Bộ sưu tập Tin Học Đại Cương"
                                    href="/collection/3349-bo-suu-tap-tin-hoc-dai-cuong.htm" target="_blank">Bộ sưu tập
                                    Tin Học Đại Cương</a></h2>
                            <p><a href="/trang-ca-nhan-44703-vo-inh-xinh.htm">Vờ_inh xinh</a></p>
                            <div> Tài liệu " Bộ sưu tập về Tin <b>Học</b> Đại Cương " được biên soạn với mục đích trang
                                bị cho <b>học</b> viên những kiến thức và kỹ năng : sử dụng máy tính hợp lý, dễ dàng,
                                thao tác nhanh nhờ giao diện đồ họa ổn định, sử dụng đồng loại... trong đó có bao gồm cả
                                đề cương ôn tập, và đề thi trắc nghiệm của nhiều trường đại <b>học</b></div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>22</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3351-bo-suu-tap-hoc-phan-tam-ly-hoc.htm"
                                         title="Bộ sưu tập học phần Tâm Lý Học "><span>20</span></a>
                        <div><h2><a title="Bộ sưu tập học phần Tâm Lý Học "
                                    href="/collection/3351-bo-suu-tap-hoc-phan-tam-ly-hoc.htm" target="_blank">Bộ sưu
                                    tập học phần Tâm Lý Học </a></h2>
                            <p><a href="/trang-ca-nhan-44703-vo-inh-xinh.htm">Vờ_inh xinh</a></p>
                            <div> Tâm lý <b>học</b> là ngành khoa <b>học</b> nghiên cứu hoạt động, tinh thần và tư tưởng
                                của con người (cụ thể đó là những cảm xúc, ý chí và hành động). Tâm lý <b>học</b> cũng
                                chú tâm đến sự ảnh hưởng của hoạt động thể chất, trạng thái tâm lý và các yếu tố bên
                                ngoài lên hành vi và tinh thần của con người. Ngành này tập trung vào loài người, tuy
                                một vài khía cạnh của động vật cũng thỉnh thoảng được nghiên cứu.
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>20</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3369-bo-suu-tap-hoc-phan-kinh-te-luong.htm"
                                         title="Bộ sưu tập học phần Kinh Tế Lượng"><span>24</span></a>
                        <div><h2><a title="Bộ sưu tập học phần Kinh Tế Lượng"
                                    href="/collection/3369-bo-suu-tap-hoc-phan-kinh-te-luong.htm" target="_blank">Bộ sưu
                                    tập học phần Kinh Tế Lượng</a></h2>
                            <p><a href="/trang-ca-nhan-44703-vo-inh-xinh.htm">Vờ_inh xinh</a></p>
                            <div> Kinh tế lượng (econometrics) là một bộ phận của Kinh tế học, được hiểu theo nghĩa rộng
                                là môn khoa <b>học</b> kinh tế giao thoa với thống kê <b>học</b> và toán kinh tế. Hiểu
                                theo nghĩa hẹp, là ứng dụng toán, đặc biệt là các phương pháp thống kế vào kinh tế. Kinh
                                tế lượng lý thuyết nghiên cứu các thuộc tính thống kê của các quy trình kinh tế lượng,
                                ví dụ như: xem xét tính hiệu quả của việc lấy mẫu, của thiết kế thực nghiệm..
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>24</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3371-bo-suu-tap-hoc-phan-quot-chu-nghia-xa-hoi-khoa-hoc-quot.htm"
                                         title="Bộ sưu tập học phần &quot; Chủ Nghĩa Xã Hội Khoa Học&quot;"><span>22</span></a>
                        <div><h2><a title="Bộ sưu tập học phần &quot; Chủ Nghĩa Xã Hội Khoa Học&quot;"
                                    href="/collection/3371-bo-suu-tap-hoc-phan-quot-chu-nghia-xa-hoi-khoa-hoc-quot.htm"
                                    target="_blank">Bộ sưu tập học phần " Chủ Nghĩa Xã Hội Khoa Học"</a></h2>
                            <p><a href="/trang-ca-nhan-44703-vo-inh-xinh.htm">Vờ_inh xinh</a></p>
                            <div> Chủ nghĩa xã hội khoa <b>học</b> là thuật ngữ được Friedrich Engels nêu ra[1] để mô tả
                                các lý thuyết về kinh tế-chính trị-xã hội do Karl Marx và ông sáng tạo. Thuật ngữ này
                                đối lập với chủ nghĩa xã hội không tưởng vì nó trình bày một cách có hệ thống và nêu bật
                                lên được những điều kiện và tiền đề cho việc xây dựng chủ nghĩa xã hội khoa <b>học</b>
                                đó là nó chỉ rõ con đường hiện thực dựa vào khoa <b>học</b> để thủ tiêu tình trạng người
                                bóc lột người và đưa ra một tổ chức xã hội mới không biết đến những mâu thuẫn của chủ
                                nghĩa tư bản mà những người theo chủ nghĩa xã hội không tưởng đã mơ ước nhưng không thực
                                hiện được
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>22</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3375-bo-suu-tap-hoc-phan-kinh-te-chinh-tri.htm"
                                         title="Bô sưu tập học phần Kinh tế chính trị"><span>26</span></a>
                        <div><h2><a title="Bô sưu tập học phần Kinh tế chính trị"
                                    href="/collection/3375-bo-suu-tap-hoc-phan-kinh-te-chinh-tri.htm" target="_blank">Bô
                                    sưu tập học phần Kinh tế chính trị</a></h2>
                            <p><a href="/trang-ca-nhan-44703-vo-inh-xinh.htm">Vờ_inh xinh</a></p>
                            <div> Kinh tế chính trị là một môn khoa <b>học</b> xã hội nghiên cứu mối quan hệ giữa kinh
                                tế và chính trị. Thuật ngữ "kinh tế chính trị" được dùng lần đầu tiên năm 1615 bởi
                                Antoine de Montchrétien trong tác phẩm Traité d''économie politique. Thuật ngữ "kinh tế
                                chính trị" xuất hiện do kết hợp các từ có nguồn gốc Hi Lạp với nghĩa là "thiết chế chính
                                trị"
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>26</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3376-bo-suu-tam-hoc-phan-luat-kinh-te.htm"
                                         title="Bộ sưu tầm học phần Luật Kinh Tế"><span>23</span></a>
                        <div><h2><a title="Bộ sưu tầm học phần Luật Kinh Tế"
                                    href="/collection/3376-bo-suu-tam-hoc-phan-luat-kinh-te.htm" target="_blank">Bộ sưu
                                    tầm học phần Luật Kinh Tế</a></h2>
                            <p><a href="/trang-ca-nhan-44703-vo-inh-xinh.htm">Vờ_inh xinh</a></p>
                            <div> Môn Luật kinh tế (Luật Kinh doanh) là môn <b>học</b> bắt buộc đối với các chuyên ngành
                                ... qui định pháp luật về kinh tế giúp cho sinh viên khi hoạt động kinh doanh.
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>23</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3385-bo-suu-tap-nhung-bai-tap-kinh-te-hoc-vi-mo.htm"
                                         title="Bộ sưu tập những bài tập  kinh tế học Vi Mô"><span>23</span></a>
                        <div><h2><a title="Bộ sưu tập những bài tập  kinh tế học Vi Mô"
                                    href="/collection/3385-bo-suu-tap-nhung-bai-tap-kinh-te-hoc-vi-mo.htm"
                                    target="_blank">Bộ sưu tập những bài tập kinh tế học Vi Mô</a></h2>
                            <p><a href="/trang-ca-nhan-44703-vo-inh-xinh.htm">Vờ_inh xinh</a></p>
                            <div> Kinh tế <b>học</b> vi mô (microeconomic) hay là kinh tế tầm nhỏ là một phân ngành chủ
                                yếu của kinh tế học, chuyên nghiên cứu về hành vi kinh tế của các cá nhân ...
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>23</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3449-bai-hoc-kiem-toan-tai-chinh.htm"
                                         title="bài học kiểm toán tài chính"><span>2</span></a>
                        <div><h2><a title="bài học kiểm toán tài chính"
                                    href="/collection/3449-bai-hoc-kiem-toan-tai-chinh.htm" target="_blank">bài học kiểm
                                    toán tài chính</a></h2>
                            <p><a href="/trang-ca-nhan-267947-vo-thu-hoai.htm">võ thu hoài</a></p>
                            <div> bài <b>học</b> kiểm toán tài chính</div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>2</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3450-de-cuong-benh-ly-hoc-thu-y.htm"
                                         title="Đề cương bệnh lý học thú y"><span>1</span></a>
                        <div><h2><a title="Đề cương bệnh lý học thú y"
                                    href="/collection/3450-de-cuong-benh-ly-hoc-thu-y.htm" target="_blank">Đề cương bệnh
                                    lý học thú y</a></h2>
                            <p><a href="/trang-ca-nhan-268439-nguyen-tuan-anh.htm">Nguyen Tuan Anh</a></p>
                            <div> Bệnh lý <b>học</b> thú y</div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>1</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank" href="/collection/3486-hoc-java.htm"
                                         title="Học JAVA"><span>0</span></a>
                        <div><h2><a title="Học JAVA" href="/collection/3486-hoc-java.htm" target="_blank">Học JAVA</a>
                            </h2>
                            <p><a href="/trang-ca-nhan-212154-huynh-long-dai.htm">Huynh Long Dai</a></p>
                            <div><b>Học</b> Tập Java tất cả trong 1</div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>0</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3495-on-thi-dai-hoc-va-cao-dang.htm"
                                         title="Ôn Thi Đại Học và Cao Đẳng"><span>1</span></a>
                        <div><h2><a title="Ôn Thi Đại Học và Cao Đẳng"
                                    href="/collection/3495-on-thi-dai-hoc-va-cao-dang.htm" target="_blank">Ôn Thi Đại
                                    Học và Cao Đẳng</a></h2>
                            <p><a href="/trang-ca-nhan-102890-van-tien.htm">Van Tien</a></p>
                            <div> Ôn Thi Đại <b>Học</b> và Cao Đẳng</div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>1</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank" href="/collection/3523-cao-hoc.htm"
                                         title="cao học"><span>1</span></a>
                        <div><h2><a title="cao học" href="/collection/3523-cao-hoc.htm" target="_blank">cao học</a></h2>
                            <p><a href="/trang-ca-nhan-287236-vithai-lang.htm">vithai lang</a></p>
                            <div> chuong trình cao <b>học</b> quản lý</div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>1</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3544-tu-hoc-seo-toi-uu-hoa-voi-google.htm"
                                         title="Tự học SEO, tối ưu hóa với Google"><span>2</span></a>
                        <div><h2><a title="Tự học SEO, tối ưu hóa với Google"
                                    href="/collection/3544-tu-hoc-seo-toi-uu-hoa-voi-google.htm" target="_blank">Tự học
                                    SEO, tối ưu hóa với Google</a></h2>
                            <p><a href="/trang-ca-nhan-284066-nguyen-marketing.htm">Nguyễn Marketing </a></p>
                            <div> Các tài liệu liên quan đến SEO, tối ưu hóa công cụ tìm kiếm, cho các bạn tự <b>học</b>
                                SEO, tự làm SEO đạt thứ hạng cao với Google
                            </div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>2</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank"
                                         href="/collection/3749-chu-de-khoa-hoc.htm"
                                         title="chủ đề khoa học"><span>1</span></a>
                        <div><h2><a title="chủ đề khoa học" href="/collection/3749-chu-de-khoa-hoc.htm" target="_blank">chủ
                                    đề khoa học</a></h2>
                            <p><a href="/trang-ca-nhan-306354-nguyen-minh-kha.htm">nguyễn minh khá</a></p>
                            <div> Văn hóa <b>hoc</b> đường</div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>1</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                    <div class="iCol"><a class="colDefaultIcon" target="_blank" href="/collection/3843-hoc-anh.htm"
                                         title="HOC ANH"><span>1</span></a>
                        <div><h2><a title="HOC ANH" href="/collection/3843-hoc-anh.htm" target="_blank">HOC ANH</a></h2>
                            <p><a href="/trang-ca-nhan-313891-bach-chi.htm">bach chi</a></p>
                            <div><b>HOC</b></div>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_coll"></i>1</li>
                                <li><i class="icon_view"></i>0</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="paging"><a class="pagging_active">1</a> <a
                            href="/global/home/search_collection.php?q=H%E1%BB%8Dc&amp;page=2"
                            class="pagging_normal">2</a> <a
                            href="/global/home/search_collection.php?q=H%E1%BB%8Dc&amp;page=3"
                            class="pagging_normal">3</a> <a
                            href="/global/home/search_collection.php?q=H%E1%BB%8Dc&amp;page=4"
                            class="pagging_normal">4</a> <a
                            href="/global/home/search_collection.php?q=H%E1%BB%8Dc&amp;page=5"
                            class="pagging_normal">..</a> <a
                            href="/global/home/search_collection.php?q=H%E1%BB%8Dc&amp;page=5" class="pagging_normal">&gt;</a>
                </div>
            </div>
            <div style=" position: relative" class="col-span-2 hidden md:block">
                <div style="position: sticky;top: 10px;">
                    <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px"
                         data-ad-client="ca-pub-2979760623205174" data-ad-slot="3807520840"></ins>
                    <script defer="">(adsbygoogle = window.adsbygoogle || []).push({});</script>
                </div>
            </div>
        </div>
    </div>
    <script>fixed_search_bottom_left();

        function fixed_search_bottom_left() {
            var search_bottom = $('.search_bottom');
            var _top = search_bottom.offset().top - 15;
            var _height_list_item = $('.list_item').height();
            var _height_search_bottom_left = $('.search_bottom_left', search_bottom).height();
            $(window).scroll(function () {
                if ($(this).scrollTop() > _top && $(this).scrollTop() < (_top + _height_list_item - _height_search_bottom_left)) {
                    search_bottom.addClass('fixed');
                } else {
                    search_bottom.removeClass('fixed');
                }
            });
        }

        function showMoreSearchResult(obj) {
            $.post('/global/ajax/aja_show_more_search.php', {}, function (data) {
                $('.list_item').append(data);
                fixed_search_bottom_left();
            });
        }</script>
</div>