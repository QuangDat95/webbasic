<div id="container">
    <div class="container mx-auto grid grid-cols-12 gap-2">
        <div class="col-span-12 mt-2">
            <section class="doc_navicate mt-0">
                <ol class="flex text-gray-500 flex-wrap px-2 md:px-0" itemtype="http://schema.org/BreadcrumbList" itemscope="">
                    <li class="flex hover:text-primary" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><i class="icon_nav_home"></i> <a href="<?= URL ?>" title="trang chủ" itemprop="item"><span itemprop="name">Trang chủ</span></a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li class="cat_nav_pa flex hover:text-primary items-center" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><i class="icon_nav_ctn mx-2"></i> <a itemprop="item" class="cat_nav_top_a" href="javascript:void(0)" title="<?= $this->data[0]['name'] ?>"><span itemprop="name"><?= $this->data[0]['name'] ?></span></a>
                        <meta itemprop="position" content="2">
                    </li>
                </ol>
            </section>
        </div>
        <section class="col-span-12 md:col-span-8">
            <h1 class="tab_cat_tag text-2xl font-bold py-6 text-center bg-pink-700 text-white rounded"><?= $this->data[0]['name'] ?></h1>
            <div class="doc_item_content">
                <div class="module_funtion flex p-2">
                    <div class="doc_file_total p-2 rounded">Tổng hợp: <?php
                                                                        if (isset($_REQUEST['xuhuong'])) {
                                                                            switch ($_REQUEST['xuhuong']) {
                                                                                case 'moidang':
                                                                                    echo "Mới đăng";
                                                                                    break;
                                                                                case 'tainhieu':
                                                                                    echo "Tải nhiều";
                                                                                    break;
                                                                                case 'xemnhieu':
                                                                                    echo "Xem nhiều";
                                                                                    break;
                                                                                default:
                                                                                    echo "Xu hướng";
                                                                                    break;
                                                                            }
                                                                            $xuhuong = $_REQUEST['xuhuong'];
                                                                        } else {
                                                                            $xuhuong = "";
                                                                            echo 'Xu hướng';
                                                                        }
                                                                        if (isset($_REQUEST['type'])) {
                                                                            $typeDoc = $_REQUEST['type'];
                                                                        } else {
                                                                            $typeDoc = "";
                                                                        }
                                                                        if (isset($_REQUEST['money'])) {
                                                                            $money = $_REQUEST['money'];
                                                                        } else {
                                                                            $money = "";
                                                                        }
                                                                        ?>
                        <i class="icon_down_t"></i>
                        <ul class="transition2">

                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=moidang&type=<?= $typeDoc ?>&money=<?= $money ?>" title="Mới đăng">Mới đăng</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=tainhieu&type=<?= $typeDoc ?>&money=<?= $money ?>" title="Tải nhiều">Tải nhiều</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=xemnhieu&type=<?= $typeDoc ?>&money=<?= $money ?>" title="Xem nhiều">Xem nhiều</a></li>
                            <!-- <li><a rel="nofollow" href="/doc-cat/62-cong-nghe-thong-tin.htm?t=dang-hot" title="Đang Hot">Đang Hot</a></li>
                            <li><a rel="nofollow" href="/doc-cat/62-cong-nghe-thong-tin.htm" title="Xu hướng">Xu -->
                            <!-- hướng</a></li> -->
                        </ul>
                    </div>
                    <div class="doc_file_type p-2 rounded">Loại file:
                        <?php
                        if (isset($_REQUEST['type'])) {
                            switch ($_REQUEST['type']) {
                                case 'all':
                                    echo "Tất cả";
                                    break;
                                case 'doc':
                                    echo ".doc";
                                    break;
                                case 'pdf':
                                    echo ".pdf";
                                    break;
                                case 'docx':
                                    echo ".docx";
                                    break;
                                case 'ppt':
                                    echo ".ppt";
                                    break;
                                case 'pptx':
                                    echo ".pptx";
                                    break;
                                case 'pot':
                                    echo ".pot";
                                    break;
                                case 'potx':
                                    echo ".potx";
                                    break;
                                case 'pps':
                                    echo ".pps";
                                    break;
                                case 'ppsx':
                                    echo ".ppsx";
                                    break;
                                default:
                                    echo "Tất cả";
                                    break;
                            }
                        } else {
                            echo 'Tất cả';
                        }
                        ?>
                        <i class="icon_down_t"></i>
                        <ul class="transition2">
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=all&money=<?= $money ?>" title="Tất cả">Tất cả</a>
                            </li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=doc&money=<?= $money ?>" title=".doc">.doc</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=pdf&money=<?= $money ?>" title=".pdf">.pdf</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=docx&money=<?= $money ?>" title=".docx">.docx</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=ppt&money=<?= $money ?>" title=".ppt">.ppt</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=pptx&money=<?= $money ?>" title=".pptx">.pptx</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=pot&money=<?= $money ?>" title=".pot">.pot</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=potx&money=<?= $money ?>" title=".potx">.potx</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=pps&money=<?= $money ?>" title=".pps">.pps</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=ppsx&money=<?= $money ?>" title=".ppsx">.ppsx</a></li>
                        </ul>
                    </div>
                    <div class="doc_file_price p-2 rounded">Giá tiền:
                        <?php
                        if (isset($_REQUEST['money'])) {
                            switch ($_REQUEST['money']) {
                                case 'all':
                                    echo "Tất cả";
                                    break;
                                case 'mienphi':
                                    echo "Miễn phí";
                                    break;
                                case 'cophi':
                                    echo "Có phí";
                                    break;
                                default:
                                    echo "Tất cả";
                                    break;
                            }
                        } else {
                            echo 'Tất cả';
                        }
                        ?>
                        <i class="icon_down_t"></i>
                        <ul class="transition2">
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=<?= $typeDoc ?>&money=all" title="Tất cả">Tất cả</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=<?= $typeDoc ?>&money=mienphi" title="Miễn phí">Miễn phí</a></li>
                            <li><a rel="nofollow" href="<?= URL . '/' . $_GET['url'] ?>?xuhuong=<?= $xuhuong ?>&type=<?= $typeDoc ?>&money=cophi" title="Có phí">Có phí</a></li>
                        </ul>
                    </div>
                </div>
                <div class="cat grid grid-cols-12 gap-2">
                    <?php
                    foreach ($this->documents as $documentItem) {
                    ?>
                        <div class="col-span-6 md:col-span-3">
                            <div class="card-doc"><a class="card-doc-img" href="document/<?= $documentItem['url'] ?>" title="<?= $documentItem['name'] ?>">
                                    <img src="<?= URL . '/' . $documentItem['hinhanh'] ?>" style="height:200px; width: 154px; object-fit:cover;" onerror="this.src='<?= URL ?>/template/images/default/no-image.png'" alt="document/<?= $documentItem['url'] ?>"> <i class="icon i_type_doc i_type_doc2"></i> </a> <a href="<?= URL . '/document/' . $documentItem['url'] ?>" title="<?= $documentItem['name'] ?>" class="card-doc-title"><?= $documentItem['name'] ?></a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>3</li>
                                    <li><i class="icon_view"></i><?= $documentItem['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $documentItem['download'] ?></li>
                                </ul>
                            </div>
                        </div>
                    <?php
                    }
                    ?>

                </div>
                <div class="paging"><a class="pagging_active">1</a> <a href="/doc-cat/62-cong-nghe-thong-tin.htm?t=xu-huong&amp;page=2" class="pagging_normal">2</a> <a href="/doc-cat/62-cong-nghe-thong-tin.htm?t=xu-huong&amp;page=3" class="pagging_normal">3</a> <a href="/doc-cat/62-cong-nghe-thong-tin.htm?t=xu-huong&amp;page=4" class="pagging_normal">4</a> <a href="/doc-cat/62-cong-nghe-thong-tin.htm?t=xu-huong&amp;page=5" class="pagging_normal">..</a> <a href="/doc-cat/62-cong-nghe-thong-tin.htm?t=xu-huong&amp;page=5" class="pagging_normal">&gt;</a></div>
            </div>
        </section>

        <aside class="col-span-12 md:col-span-4 relative">
            <div class="topic_tag">
                <h4>Chủ đề liên quan</h4>
                <div class="list_tag_topic">
                    <?php
                    $tags = '';
                    foreach ($this->tags as $tag) {
                        $tags .= ',' . $tag['tag'];
                    }
                    $tags = substr($tags, 1);
                    $tags = explode(',', $tags);
                    foreach ($tags as $tagItem) {
                        if ($tagItem != '') { ?>
                            <a href="<?= URL ?>/tag?keyword=<?= $tagItem ?>" title="<?= $tagItem ?>"> <?= $tagItem ?> </a>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="md:hidden">
                <ins class="adsbygoogle" style="display: inline-block; width: 300px; height: 0px;" data-ad-client="ca-pub-2979760623205174" data-ad-slot="6900588045" data-adsbygoogle-status="done" data-ad-status="unfilled">
                    <ins id="aswift_11_expand" tabindex="0" title="Advertisement" aria-label="Advertisement" style="border: none; height: 0px; width: 300px; margin: 0px; padding: 0px; position: relative; visibility: visible; background-color: transparent; display: inline-table;">
                        <ins id="aswift_11_anchor" style="border: none; height: 0px; width: 300px; margin: 0px; padding: 0px; position: relative; visibility: visible; background-color: transparent; display: block; overflow: hidden; opacity: 0;">
                            <iframe id="aswift_11" name="aswift_11" style="left:0;position:absolute;top:0;border:0;width:300px;height:250px;" sandbox="allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" width="300" height="250" frameborder="0" src="https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-2979760623205174&amp;output=html&amp;h=250&amp;slotname=6900588045&amp;adk=2446890373&amp;adf=3619996105&amp;pi=t.ma~as.6900588045&amp;w=300&amp;lmt=1634283514&amp;psa=1&amp;format=300x250&amp;url=https%3A%2F%2F123docz.net%2Fdoc-cat%2F62-cong-nghe-thong-tin.htm&amp;flash=0&amp;wgl=1&amp;uach=WyJXaW5kb3dzIiwiMTAuMC4wIiwieDg2IiwiIiwiOTQuMC40NjA2LjgxIixbXSxudWxsLG51bGwsIjY0Il0.&amp;tt_state=W3siaXNzdWVyT3JpZ2luIjoiaHR0cHM6Ly9hdHRlc3RhdGlvbi5hbmRyb2lkLmNvbSIsInN0YXRlIjo3fV0.&amp;dt=1634283514149&amp;bpp=1&amp;bdt=314&amp;idt=106&amp;shv=r20211013&amp;mjsv=m202110070201&amp;ptt=9&amp;saldr=aa&amp;abxe=1&amp;cookie=ID%3D9f7fbbf5479d7853-2215916e9ccc001f%3AT%3D1634281834%3ART%3D1634281834%3AS%3DALNI_MYd1o6OD3Ryvg_YKExnr48-ZaNZUg&amp;prev_fmts=728x90%2C336x280%2C728x90%2C336x280%2C728x90%2C336x280%2C728x90%2C336x280%2C728x90%2C336x280%2C300x600&amp;correlator=2083601501718&amp;frm=20&amp;pv=1&amp;ga_vid=405090249.1634281696&amp;ga_sid=1634283514&amp;ga_hid=1429859830&amp;ga_fc=0&amp;u_tz=420&amp;u_his=5&amp;u_h=864&amp;u_w=1536&amp;u_ah=824&amp;u_aw=1536&amp;u_cd=24&amp;adx=-12245933&amp;ady=-12245933&amp;biw=1519&amp;bih=754&amp;scr_x=0&amp;scr_y=0&amp;eid=31062580%2C31061829%2C31063139%2C21067496&amp;oid=2&amp;pvsid=2461139875726755&amp;pem=725&amp;ref=https%3A%2F%2F123docz.net%2Fdocument%2F634736-ngon-ngu-mo-ta-phan-cung-voi-vhdl-bai-tap-tham-khao.htm&amp;eae=0&amp;fc=896&amp;brdim=0%2C0%2C0%2C0%2C1536%2C0%2C1536%2C824%2C1536%2C754&amp;vis=1&amp;rsz=%7C%7CenEr%7C&amp;abl=CS&amp;pfx=0&amp;fu=32768&amp;bc=31&amp;ifi=12&amp;uci=a!c&amp;fsb=1&amp;xpc=ddAdhw2X7M&amp;p=https%3A//123docz.net&amp;dtd=109" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!c" data-google-query-id="CL7_0YT1y_MCFQGpvQodhr8NPw" data-load-complete="true"></iframe>
                        </ins>
                    </ins>
                </ins>
                <script defer="">
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </aside>
        <div class="clear"></div>
        <div class="doc_common container mx-auto px-2 col-span-12 md:col-span-12">
            <h3 class="my-4 px-4 font-bold text-2xl">Tài liệu chung</h3>
            <div class="grid md:grid-cols-4 grid-cols-1 md:gap-4">

                <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Tài liệu mới nhất</h4>
                    <div class="total_doc_item_common px-2 md:px-0" id="tailieu-moinhat">
                        <?php
                        foreach ($this->moinhat as $moinhat) { ?>
                            <div class="item_common">
                                <a href="<?= URL ?>/document/<?= $moinhat['url'] ?>" title="1" class="text-lg md:text-base">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <?= $moinhat['name'] ?>
                                </a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>8</li>
                                    <li><i class="icon_view"></i><?= $moinhat['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $moinhat['download'] ?></li>
                                </ul>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Tài liệu tải nhiều</h4>
                    <div class="total_doc_item_common px-2 md:px-0" id="tailieu-moinhat">
                        <?php
                        foreach ($this->tainhieu as $tainhieu) { ?>
                            <div class="item_common">
                                <a href="<?= URL ?>/document/<?= $tainhieu['url'] ?>" title="1" class="text-lg md:text-base">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <?= $tainhieu['name'] ?>
                                </a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>8</li>
                                    <li><i class="icon_view"></i><?= $tainhieu['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $tainhieu['download'] ?></li>
                                </ul>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Tài liệu xem nhiều</h4>
                    <div class="total_doc_item_common px-2 md:px-0" id="tailieu-moinhat">
                        <?php
                        foreach ($this->xemnhieu as $xemnhieu) { ?>
                            <div class="item_common">
                                <a href="<?= URL ?>/document/<?= $xemnhieu['url'] ?>" title="1" class="text-lg md:text-base">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <?= $xemnhieu['name'] ?>
                                </a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>8</li>
                                    <li><i class="icon_view"></i><?= $xemnhieu['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $xemnhieu['download'] ?></li>
                                </ul>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Tài liệu nổi bật</h4>
                    <div class="total_doc_item_common px-2 md:px-0" id="tailieu-moinhat">
                        <?php
                        foreach ($this->noibat as $noibat) { ?>
                            <div class="item_common">
                                <a href="<?= URL ?>/document/<?= $noibat['url'] ?>" title="1" class="text-lg md:text-base">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <?= $noibat['name'] ?>
                                </a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>8</li>
                                    <li><i class="icon_view"></i><?= $noibat['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $noibat['download'] ?></li>
                                </ul>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>