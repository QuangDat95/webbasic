<link rel="stylesheet" type="text/css" href="<?= URL ?>/template/styles/detail.min.css" />
<div id="container" oncopy="return addLink();">
    <div class="grid grid-cols-12 gap-2">
        <div class="col-span-12 md:col-span-10 md:col-start-3 my-2">
            <section class="doc_navicate mt-0">
                <ol class="flex text-gray-500 flex-wrap px-2 md:px-0" itemtype="http://schema.org/BreadcrumbList" itemscope="">
                    <li class="flex hover:text-primary" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><i class="icon_nav_home"></i> <a href="<? URL ?>" title="Trang chủ" itemprop="item"><span itemprop="name">Trang chủ</span></a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li class="cat_nav_pa flex hover:text-primary items-center" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><i class="icon_nav_ctn mx-2"></i> <a itemprop="item" class="cat_nav_top_a" href="<?= URL ?>/document/1/<?= $this->document[0]['categoryurl'] ?>" title="<?= $this->document[0]['category'] ?>"><span itemprop="name"><?= $this->document[0]['category'] ?></span></a>
                        <meta itemprop="position" content="2">
                    </li>
                    <li class="cat_nav_pa flex hover:text-primary items-center" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><i class="icon_nav_ctn mx-2"></i> <a itemprop="item" class="cat_nav_top_a" href="javascript:;" title="<?= $this->document[0]['name'] ?>"><span itemprop="name"><?= $this->document[0]['name'] ?></span></a>
                        <meta itemprop="position" content="2">
                    </li>
                </ol>
            </section>
        </div>
        <div style=" position: relative;" class="col-span-2 hidden md:block px-1 text-center">
            <div style="position: sticky;top: 10px;">
                <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="3807520840"></ins>
                <script defer="">
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
        <div class="detailLeft col-span-12 md:col-span-7">
            <div class="detailInfo">
                <h1 class="text-2xl font-bold px-2 break-words"> <?= $this->document[0]['name'] ?> <span class="icon i_type_doc i_type_doc2"></span></h1>
                <div class="hidden md:block text-center">
                    <ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443"></ins>
                    <script defer="">
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <div class="block md:hidden text-center">
                    <ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="6900588045"></ins>
                    <script defer="">
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <div class="my-2 text-gray-500 px-2 text-xs"><span><i class="icon i_numpage mr-2"></i>31</span> <span><i class="icon i_numview mr-2"></i><?= $this->document[0]['view'] ?></span> <span><i class="icon i_numdown"></i><?= $this->document[0]['download'] ?></span></div>
                <div class="flex  px-2 text-xs text-gray-500  md:flex-row justify-between flex-wrap">
                    <div class="infoUser flex mb-2 mr-4"><a rel="nofollow" href="javascript:;" class="w-12 h-12"> <img width="48" height="48" class="h-full lazy object-cover loaded" title="yeutailieu3889" src="https://media.123dok.info/images/default/user_small.png" onerror="this.src='https://media.123dok.info/images/default/user_small.png'" data-ll-status="loaded"> </a>
                        <div class="ml-2"><a rel="nofollow" title="yeutailieu3889" href="/trang-ca-nhan-368649-yeutailieu3889.htm"> yeutailieu3889 </a>
                            <div class="md:flex items-center mb-2">
                                <div><a rel="nofollow" href="javascript:;" class="smsUser" onclick="showmessega(this,0)" id_use="368649"><i class="icon_doc_mail"></i>Gửi tin nhắn </a></div>
                                <span class="mx-2 hidden md:block">|</span>
                                <div><a rel="nofollow" class="notifi_document text-gray-500 hover:text-primary" id="GTM_Report_Click" onclick="showFormNotifiDocument(634736, 'Ngôn ngữ mô tả phần cứng với VHDL - Bài tập tham khảo')" href="javascript:;"> Báo tài liệu vi phạm</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="flex items-center md:mb-2 text-base">
                        <?php if (isset($_SESSION[USERID])) { ?>
                            <div class="mr-2 text-sm border border-solid border-gray-500 rounded px-2 py-2 hover:text-primary hover:border-primary cursor-pointer" docid="<?= $this->document[0]['id'] ?>" uid="<?= $_SESSION['customer']['customer'] ?>" onclick="showAddCollection(this)"> Thêm vào bộ sưu tập
                            </div>
                            <a rel="nofollow" href="javascript:;" onclick="downloadDocument(634736, 1634281681, 'dc8bdbe8348e9d3e707ed486abe11bff' );" class="btn_download py-2 mr-2 hidden md:flex ">
                                <span id="GTM_Document_Download_Top_Free" class="py-2 px-3 " "="">
                            Tải xuống
                            </span>
                            <label>39</label> 
                            </a>
                            <?php if ($this->document[0]['agent_price'] > 0) { ?>
                            <span class=" hidden md:block">
                                    <i class="icon i_money"></i><?= number_format($this->document[0]['agent_price']) ?>₫
                                </span>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="mr-2 text-sm border border-solid border-gray-500 rounded px-2 py-2 hover:text-primary hover:border-primary cursor-pointer" docid="<?= $this->document[0]['id'] ?>" uid="<?= $_SESSION['customer']['customer'] ?>" onclick="popup_login()"> Thêm vào bộ sưu tập
                            </div>
                            <a rel="nofollow" href="javascript:;" onclick="popup_down_not_login();" class="btn_download py-2 mr-2 hidden md:flex ">
                                <span id="GTM_Document_Download_Top_Free" class="py-2 px-3 " "="">
                                Tải xuống
                                </span>
                            <label>39</label> 
                            </a>
                        <?php
                        } ?>
                        
                        <!--                    <a rel=" nofollow" href="http://123doc.net/statics-detail/913-tong-ket-nam-2015-cua-123doc.htm" -->
                                    <!--                       target="_blank" class="py-2 ml-2">-->
                                    <!--                         <label id="GTM_Document_Upload" class="bg-primary text-white  p-2 rounded text-base">Upload tăng doanh thu</label>-->
                                    <!--                    </a>-->
                    </div>
                </div>
            </div>
            <div class="detailContent" id="contentDocument" onselect="return false;">
                <div id="blueseed_preroll_bound">
                    <div id="blueseed_preroll"></div>
                </div>
                <ul id="mainContent">
                    <li class="loaddingDoc page-content" style="display: none; height: 2.85249px;">
                        <div class="loaddingDoc w-full bg-white border border-solid border-gray-500 flex justify-center items-center" style="display: none;"><img style="width:100%; height:auto" width="612" height="792" alt="Ngôn ngữ mô tả phần cứng với VHDL - Bài tập tham khảo" src="https://media.123dok.info/images/document/13/to/jk/larger_jkr1380748803.jpg">
                        </div>
                    </li>
                    <li id="page-rel-1" class="page-content" style="height: 1129.59px;">
                        <div class="d" style="width: 612px; height: 792px; position: relative; transform-origin: 0px 0px; transform: scale(1.42625);">
                            <div id="p1" data-page-no="1" class="p">
                                <div class="b" style="background-image:url(https://d1.store123doc.com//13/to/jk/ngon-ngu-mo-ta-phan-cung-voi-vhdl-b-0-13807488031733/p1.png);background-position:0 0;background-size:612.000000px 792.000000px;background-repeat:no-repeat;">
                                    <div style="left:110.519956px;bottom:746.279701px;" class="l t1 h0"><span class="f1 s1 c0 l0 w0 r0">Tr<span class="f2">ườ</span>ng <span class="f2">Đ</span>HSPKT H<span class="f2">ư</span>ng Yên <span class="_ _0"> </span> <span class="_ _1"> </span>Tìm hi<span class="f2">ể</span>u v<span class="f2">ề</span> VHDL </span></div>
                                    <div style="left:110.519956px;bottom:727.079709px;" class="l t2 h1"><span class="f3 s2 c0 l0 w0 r0"> </span></div>
                                    <div style="left:290.759884px;bottom:37.079985px;" class="l t3 h2"><span class="f4 s3 c0 l0 w0 r0">-<span class="f1"> 119 -</span> </span></div>
                                    <div style="left:185.999926px;bottom:706.799717px;" class="l t4 h3"><span class="f5 s4 c0 l0 w0 r0">Ch<span class="f6">ươ</span>ng 9: Bài t<span class="f6">ậ</span>p tham kh<span class="f6">ả</span>o </span></div>
                                    <div style="left:110.519956px;bottom:688.439725px;" class="l t2 h4"><span class="f1 s2 c0 l0 w0 r0"> </span></div>
                                    <div style="left:110.519956px;bottom:672.239731px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>Ph<span class="f2">ầ</span>n n<span class="_ _3"></span>ày chúng ta <span class="_ _3"></span>s<span class="f2">ẽ</span> trình bày <span class="_ _3"></span>các m<span class="f2">ạ</span>ch sau: </span>
                                    </div>
                                    <div style="left:110.519611px;bottom:656.039738px;" class="l t2 h4"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>Barrel s<span class="_ _3"></span>hifter </span></div>
                                    <div style="left:110.519611px;bottom:639.959744px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>B<span class="f2">ộ</span> so s<span class="_ _3"></span>ánh không <span class="_ _3"></span>d<span class="f2">ấ</span>u và có d<span class="f2">ấ</span>u. </span>
                                    </div>
                                    <div style="left:110.519592px;bottom:623.759750px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>B<span class="f2">ộ</span> c<span class="f2">ộ</span>ng </span></div>
                                    <div style="left:110.519951px;bottom:607.559757px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>B<span class="f2">ộ</span> chia <span class="_ _3"></span>d<span class="f2">ấ</span>u ch<span class="f2">ấ</span>m <span class="_ _3"></span>t<span class="f2">ĩ</span>nh. </span></div>
                                    <div style="left:110.520116px;bottom:591.479763px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>B<span class="f2">ộ</span> <span class="f2">đ</span>i<span class="f2">ề</span>u <span class="_ _3"></span>khi<span class="f2">ể</span>n máy bán<span class="_ _3"></span> hàng. </span></div>
                                    <div style="left:110.519859px;bottom:575.279770px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>B<span class="f2">ộ</span> nh<span class="f2">ậ</span>n <span class="_ _3"></span>d<span class="f2">ữ</span> li<span class="f2">ệ</span>u n<span class="f2">ố</span>i ti<span class="f2">ế</span>p. </span></div>
                                    <div style="left:110.519976px;bottom:559.199776px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>B<span class="f2">ộ</span> chuy<span class="f2">ể</span>n <span class="f2"><span class="_ _3"></span>đổ</span>i so<span class="_ _3"></span>ng song s<span class="_ _3"></span>ang n<span class="f2">ố</span>i ti<span class="f2"><span class="_ _3"></span>ế</span>p. </span></div>
                                    <div style="left:110.520187px;bottom:542.879783px;" class="l t2 h4"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _5"> </span>+ <span class="_ _4"> </span>SSD </span></div>
                                    <div style="left:110.520187px;bottom:526.799789px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>B<span class="f2">ộ</span> phát <span class="_ _3"></span>tín hi<span class="f2">ệ</span>u </span></div>
                                    <div style="left:110.520128px;bottom:510.719796px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0"> <span class="_ _2"> </span>+ <span class="_ _4"> </span>B<span class="f2">ộ</span> nh<span class="f2">ớ</span> </span></div>
                                    <div style="left:110.519550px;bottom:494.039802px;" class="l t2 h6"><span class="f5 s2 c0 l0 w0 r0">9.1. <span class="_ _6"> </span>Barrel Sh<span class="_ _3"></span>ifter. </span></div>
                                    <div style="left:144.359565px;bottom:478.319809px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">S<span class="f2">ơ</span> <span class="f2"><span class="_ _7"></span>đồ</span> <span class="_ _7"></span>c<span class="f2">ủ</span>a <span class="_ _7"></span>m<span class="f2">ạ</span>ch <span class="_ _7"></span>c<span class="f2">ủ</span>a <span class="_ _7"></span>b<span class="f2">ộ</span> <span class="_ _7"></span>d<span class="f2">ị</span>ch <span class="_ _7"></span>barrel <span class="f2"><span class="_ _7"></span>đượ</span>c <span class="_ _7"></span>ch<span class="f2">ỉ</span> <span class="_ _7"></span>ra <span class="_ _7"></span>trong <span class="_ _7"></span>hình <span class="_ _7"></span>9.1. <span class="f2"><span class="_ _7"></span>Đầ</span>u<span class="_ _3"></span> <span class="_ _7"></span>vào </span></div>
                                    <div style="left:110.519730px;bottom:462.119815px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">là <span class="_ _3"></span>vector <span class="_ _8"></span>8 b<span class="_ _3"></span>it. <span class="f2"><span class="_ _3"></span>Đầ</span>u <span class="_ _8"></span>ra <span class="_ _8"></span>là <span class="_ _3"></span>phiên <span class="_ _3"></span>b<span class="f2">ả</span>n <span class="_ _8"></span>d<span class="f2">ị</span>ch <span class="_ _3"></span>c<span class="f2">ủ</span>a <span class="f2"><span class="_ _8"></span>đầ</span>u <span class="_ _8"></span>vào, <span class="_ _3"></span>v<span class="f2">ớ</span>i <span class="_ _8"></span>l<span class="f2">ượ</span>ng <span class="_ _3"></span>d<span class="f2">ị</span>ch <span class="f2"><span class="_ _8"></span>đượ</span>c <span class="f2"><span class="_ _8"></span>đị</span>nh </span></div>
                                    <div style="left:110.520214px;bottom:446.039822px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">ngh<span class="f2">ĩ</span>a <span class="_ _7"></span>b<span class="f2">ở</span>i <span class="_ _7"></span> <span class="_ _7"></span>8 <span class="f2"><span class="_ _7"></span>đầ</span>u <span class="_ _7"></span>vào <span class="_ _7"></span>“shift” <span class="_ _7"></span>(t<span class="f2">ừ</span> <span class="_ _7"></span>o <span class="f2"><span class="_ _7"></span>đế</span>n <span class="_ _7"></span>7). <span class="_ _7"></span>M<span class="f2">ạ</span>ch <span class="_ _9"></span>g<span class="f2">ồ</span>m <span class="_ _8"></span>có <span class="_ _9"></span>3 <span class="_ _8"></span>b<span class="f2">ộ</span> <span class="_ _7"></span>d<span class="f2">ị</span>ch <span class="_ _7"></span>barrel <span class="_ _9"></span>riêng </span>
                                    </div>
                                    <div style="left:110.519738px;bottom:429.839828px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">l<span class="f2">ẻ</span>, <span class="_ _a"> </span>m<span class="f2">ỗ</span>i <span class="_ _a"> </span>m<span class="f2">ộ</span>t <span class="_ _9"></span>cái <span class="_ _a"> </span>gi<span class="f2">ố</span>ng<span class="_ _3"></span> <span class="_ _a"> </span>nh<span class="f2">ư</span> <span class="_ _a"> </span>trong <span class="_ _a"> </span>ví <span class="_ _9"></span>d<span class="f2">ụ</span> <span class="_ _a"> </span>6.9. <span class="_ _a"> </span>Nh<span class="f2">ư</span><span class="_ _3"></span>ng <span class="_ _a"> </span>chúng <span class="_ _a"> </span>ta <span class="_ _a"> </span>ph<span class="f2">ả</span>i <span class="_ _a"> </span>chu <span class="_ _9"></span>ý <span class="_ _a"> </span>r<span class="f2">ằ</span>ng, </span></div>
                                    <div style="left:110.519946px;bottom:413.639835px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">barrel <span class="f2"><span class="_ _3"></span>đầ</span>u <span class="_ _3"></span>tiên <span class="_ _3"></span>có ch<span class="f2"><span class="_ _3"></span>ỉ</span> c<span class="_ _3"></span>ó 1<span class="_ _3"></span> <span class="f2">đầ</span>u<span class="_ _3"></span> “0” <span class="f2"><span class="_ _3"></span>đượ</span>c <span class="_ _8"></span>k<span class="f2">ế</span>t n<span class="f2"><span class="_ _3"></span>ố</span>i v<span class="f2"><span class="_ _3"></span>ớ</span>i <span class="_ _3"></span>m<span class="f2">ộ</span>t b<span class="f2">ộ</span> <span class="_ _8"></span>d<span class="f2">ồ</span>n k<span class="_ _3"></span>ênh,<span class="_ _3"></span> tro<span class="_ _3"></span>ng <span class="_ _3"></span>khi </span></div>
                                    <div style="left:110.519682px;bottom:397.559841px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">barrel <span class="_ _a"> </span>th<span class="f2">ứ</span> <span class="_ _b"> </span>2 <span class="_ _a"> </span>có <span class="_ _b"> </span>2 <span class="f2"><span class="_ _a"></span>đầ</span>u <span class="_ _a"> </span>vào <span class="_ _b"> </span>“0” <span class="_ _a"> </span>và <span class="_ _b"> </span>barrel <span class="_ _a"> </span>cu<span class="f2">ố</span>i <span class="_ _b"> </span>cùng <span class="_ _b"> </span>có <span class="_ _a"> </span>t<span class="f2">ớ</span>i <span class="_ _a"> </span>4 <span class="f2"><span class="_ _b"> </span>đầ</span>u <span class="_ _a"> </span>vào <span class="_ _b"> </span>“0”. <span class="f2"><span class="_ _a"></span>Để</span> </span></div>
                                    <div style="left:110.519664px;bottom:381.359847px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">vector <span class="_ _7"></span>l<span class="f2">ớ</span>n <span class="_ _7"></span>h<span class="f2">ơ</span>n <span class="_ _9"></span>thì <span class="_ _7"></span>chúng <span class="_ _7"></span>ta <span class="_ _9"></span>ph<span class="f2">ả</span>i <span class="_ _7"></span>d<span class="f2">ữ</span> <span class="_ _7"></span>2 <span class="f2"><span class="_ _9"></span>đầ</span>u <span class="_ _7"></span>vào <span class="_ _7"></span>là <span class="_ _7"></span>“0”. <span class="_ _9"></span>Ví <span class="_ _7"></span>d<span class="f2">ụ</span> <span class="_ _7"></span>n<span class="f2">ế</span>u <span class="_ _9"></span>shift <span class="_ _7"></span>= <span class="_ _7"></span>“001” </span></div>
                                    <div style="left:110.520165px;bottom:365.159854px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">thì <span class="_ _8"></span>ch<span class="f2">ỉ</span> <span class="_ _7"></span>barrel <span class="f2"><span class="_ _7"></span>đầ</span>u <span class="_ _7"></span>tiên <span class="_ _9"></span>gây <span class="_ _8"></span>ra <span class="_ _7"></span>d<span class="f2">ị</span>ch,<span class="_ _3"></span> <span class="_ _8"></span>còn <span class="_ _8"></span>n<span class="f2">ế</span>u <span class="_ _9"></span>shift <span class="_ _8"></span>= <span class="_ _9"></span>“111” <span class="_ _8"></span>thì <span class="_ _7"></span>t<span class="f2">ấ</span>t <span class="_ _7"></span>các <span class="f2"><span class="_ _7"></span>đề</span>u <span class="_ _7"></span>gây <span class="_ _7"></span>ra<span class="_ _3"></span> </span></div>
                                    <div style="left:110.520493px;bottom:349.079860px;" class="l t2 h5"><span class="f1 s2 c0 l0 w0 r0">d<span class="f2">ị</span>ch. </span></div>
                                    <div style="left:457.559728px;bottom:86.519965px;" class="l t2 h4"><span class="f1 s2 c0 l0 w0 r0"> </span></div>
                                    <div style="left:245.759590px;bottom:73.199971px;" class="l t2 h5"><span class="f7 s2 c0 l0 w0 r0">Hình 9.<span class="_ _3"></span>1. B<span class="f8">ộ</span> d<span class="f8">ị</span>ch barrel </span>
                                    </div>
                                </div>
                                <div class="j" data-data="{&quot;ctm&quot;:[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="flex flex-col md:flex-row px-2">
                <div class="text-white w-full bg-primary rounded py-2 text-center text-xl mb-2 md:mb-0 md:mr-2 "><a class="block" rel="nofollow" id="GTM_Document_Seemore" href="javascript:;" onclick="showMoreDocContent(this, 4)">Xem thêm </a></div>
                <?php if (isset($_SESSION[USERID])) { ?>
                    <div class="detailActionDownload" id="actionDownload"><a rel="nofollow" class="block pr-8" href="javascript:;" id="GTM_Document_Download_Bottom_Free" onclick="downloadDocument(634736, 1634281681, 'dc8bdbe8348e9d3e707ed486abe11bff' );">
                            <i class="icon"></i>Tải xuống <span class="">(<?= $this->document[0]['agent_price'] > 0 ? number_format($this->document[0]['agent_price']) . 'đ - ' : "" ?>31 trang)</span> </a> <label>39</label>
                    </div>
                <?php } else { ?>
                    <div class="detailActionDownload" id="actionDownload"><a rel="nofollow" class="block pr-8" href="javascript:;" id="GTM_Document_Download_Bottom_Free" onclick="popup_down_not_login();">
                            <i class="icon"></i>Tải xuống <span class="">(31 trang)</span> </a> <label>39</label>
                    </div>
                <?php
                }
                ?>
            </div>

            <!-- <div class="flex flex-col md:flex-row px-2">
                    <div class="text-white w-full bg-primary rounded py-2 text-center text-xl mb-2 md:mb-0 md:mr-2 "><a class="block" rel="nofollow" id="GTM_Document_Seemore" href="javascript:;" onclick="showMoreDocContent(this, 4)">Xem thêm </a></div>
                    
                </div> -->

            <div class="detailBar fixed w-full left-0 bottom-0 bg-gray-100 z-10 border-t border-solid border-gray-300 ">
                <div class=" flex grid grid-cols-12 py-2">
                    <div class=" detailBar_left flex col-span-12 md:col-span-7 md:col-start-3 justify-center md:justify-between">
                        <div class="md:flex items-center hidden">
                            <div class="detailNumPage mr-10 flex">
                                <div id="scrollPage" class=" w-20 bg-white text-right border border-solid border-gray-400 px-1 mr-1">1
                                </div>
                                <span>/ 31 trang</span>
                            </div>
                            <ul class="flex">
                                <li class="mr-6"><a rel="nofollow" href="javascript:;" onclick="showBoxEmbed(this);"><i class="icon i_link" id="GTM_Document_Embed"></i></a> <span><label>Nhúng link</label></span>
                                    <div class="docEmbed hidden"><textarea id="textEmbedIF" class="textCopyIF" readonly="" onclick="this.select();"></textarea> <label class="setting-size">Kích thước tài liệu: <select id="opstSizeIF" class="opstSizeIF">
                                                <option value="auto"> - Tự động -</option>
                                                <option value="800x600">800 x 600</option>
                                                <option value="400x600">400 x 600</option>
                                            </select> </label> <a rel="nofollow" href="javascript:;" onclick="closeBoxEmbed();">Đóng</a></div>
                                </li>
                                <li onmousemove="remmove_ac(this)" class="mr-6"><a rel="nofollow" href="javascript:;" onclick="toggleFullScreen();"><i class="icon i_zoom" id="GTM_Document_Fullscreen"></i></a> <span class=""><label>Xem toàn màn hình</label></span></li>
                                <?php if (isset($_SESSION[USERID])) { ?>
                                    <li class="mr-6"><a rel="nofollow" href="javascript:;" uid="<?= $_SESSION['customer']['customer'] ?>" docid="<?= $this->document[0]['id'] ?>" onclick="showAddCollection(this)"><i class="icon i_add"></i></a>
                                        <span><label>Thêm vào bộ sưu tập</label></span>
                                    </li>
                                <?php
                                } else { ?>
                                    <li class="mr-6"><a rel="nofollow" href="javascript:;" uid="<?= $_SESSION['customer']['customer'] ?>" docid="<?= $this->document[0]['id'] ?>" onclick="popup_login()"><i class="icon i_add"></i></a>
                                        <span><label>Thêm vào bộ sưu tập</label></span>
                                    </li>
                                <?php
                                } ?>

                            </ul>
                        </div>
                        <?php if (isset($_SESSION[USERID])) { ?>
                            <div class="detailDownload flex flex-no-wrap items-center px-8">
                                <a rel="nofollow" href="javascript:;" class="btn_download flex whitespace-no-wrap mr-2 " onclick="downloadDocument(634736, 1634281681, 'dc8bdbe8348e9d3e707ed486abe11bff' );">
                                    <span id="GTM_Document_Download_Bottomneo_Free" class="">Tải xuống <span class="lowercase p-0"><?= $this->document[0]['type_file'] != '' ? ('.' . $this->document[0]['type_file']) : '' ?></span></span> <label>39</label> </a> <?= $this->document[0]['agent_price'] > 0 ? '<span><i class="icon i_money"></i>'.number_format($this->document[0]['agent_price']).'₫</span>' : "" ?> <label class="ml-2 whitespace-no-wrap">(31 trang)</label>
                            </div>
                        <?php } else { ?>
                            <div class="detailDownload flex flex-no-wrap items-center px-8">
                                <a rel="nofollow" href="javascript:;" class="btn_download flex whitespace-no-wrap mr-2 " onclick="popup_down_not_login();">
                                    <span id="GTM_Document_Download_Bottomneo_Free" class="">Tải xuống <span class="lowercase p-0"><?= $this->document[0]['type_file'] != '' ? ('.' . $this->document[0]['type_file']) : '' ?></span></span> <label>39</label> </a> <label class="ml-2 whitespace-no-wrap">(31 trang)</label>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="col-span-3 text-center text-primary font-bold  hidden md:flex justify-center items-center">
                        <a rel="nofollow" href="javascript:;" id="GTM_Document_History" onclick="showHistoryDownload();">Lịch sử tải xuống</a>
                    </div>
                </div>
            </div>
            <div class="detailDatamining">
                <h2 class="text-2xl font-normal p-2 uppercase">Thành viên thường xem thêm</h2>
                <ul class="grid grid-cols-12 gap-2">
                    <?php
                    foreach ($this->thuongxem as $thuongxemItem) {  ?>
                        <li class="col-span-6 md:col-span-3">
                            <div class="card-doc " onclick="actionDocRelated(this)">
                                <a class="card-doc-img" href="/document/1010906-tai-lieu-hdtv-truyen-hinh-do-net-cao-docx.htm" title="Tài liệu HDTV truyền hình độ nét cao docx">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <img style="width:124px; height:176px" width="124" height="176" alt="Tài liệu HDTV truyền hình độ nét cao docx" class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="https://media.123dok.info/images/document/14/nu/vf/medium_vfi1390656009.jpg" onerror="this.src='https://media.123dok.info/images/default/doc_normal.png'">
                                </a>
                                <a class="card-doc-title" href="<?= $thuongxemItem['url'] ?>" title="<?= $thuongxemItem['name'] ?>"><?= $thuongxemItem['name'] ?></a>
                                <a rel="nofollow" class="card-doc-user-name" href="/trang-ca-nhan-424428-tailieuhay-989.htm" title="tailieuhay_989">tailieuhay_989</a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>16</li>
                                    <li><i class="icon_view"></i><?= $thuongxemItem['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $thuongxemItem['download'] ?></li>
                                </ul>
                            </div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="box_detailRight px-2 md:hidden">
                <h2 class="text-2xl uppercase mb-2">Tài liệu liên quan</h2>
                <ul class="listDetail_relase">
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Ngôn ngữ mô tả phần cứng với VHDL - Bài tập
                            tham khảo</label>
                        <div><a href="/document/634736-ngon-ngu-mo-ta-phan-cung-voi-vhdl-bai-tap-tham-khao.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Ngôn ngữ mô tả phần
                                cứng với VHDL - Bài tập tham khảo</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/634736-ngon-ngu-mo-ta-phan-cung-voi-vhdl-bai-tap-tham-khao.htm">31</a>
                                </li>
                                <li><i class="icon_view"></i>5,726</li>
                                <li><i class="icon_down"></i>37</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Ngôn ngữ mô tả phần cứng với VHDL - Giới
                            thiệu</label>
                        <div><a href="/document/634737-ngon-ngu-mo-ta-phan-cung-voi-vhdl-gioi-thieu.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Ngôn ngữ mô tả phần cứng với VHDL - Giới thiệu</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/634737-ngon-ngu-mo-ta-phan-cung-voi-vhdl-gioi-thieu.htm">6</a>
                                </li>
                                <li><i class="icon_view"></i>738</li>
                                <li><i class="icon_down"></i>4</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Ngôn ngữ mô tả phần cứng với VHDL</label>
                        <div><a href="/document/2808-ngon-ngu-mo-ta-phan-cung-voi-vhdl.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Ngôn ngữ mô tả phần cứng với VHDL</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/2808-ngon-ngu-mo-ta-phan-cung-voi-vhdl.htm">150</a>
                                </li>
                                <li><i class="icon_view"></i>6,087</li>
                                <li><i class="icon_down"></i>90</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Chương 3 NGÔN NGỮ mô tả PHẦN CỨNG
                            VHDL</label>
                        <div><a href="/document/2319532-chuong-3-ngon-ngu-mo-ta-phan-cung-vhdl.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Chương 3 NGÔN NGỮ mô tả PHẦN CỨNG VHDL</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/2319532-chuong-3-ngon-ngu-mo-ta-phan-cung-vhdl.htm">40</a>
                                </li>
                                <li><i class="icon_view"></i>699</li>
                                <li><i class="icon_down"></i>1</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>ngôn ngữ mô tả phần cứng verilog</label>
                        <div><a href="/document/1650724-ngon-ngu-mo-ta-phan-cung-verilog.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">ngôn ngữ mô tả phần cứng verilog</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1650724-ngon-ngu-mo-ta-phan-cung-verilog.htm">26</a>
                                </li>
                                <li><i class="icon_view"></i>705</li>
                                <li><i class="icon_down"></i>0</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Chương II: Ngôn ngữ mô tả phần cứng
                            VHDL</label>
                        <div><a href="/document/2391377-chuong-ii-ngon-ngu-mo-ta-phan-cung-vhdl.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Chương II: Ngôn ngữ mô tả phần cứng VHDL</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/2391377-chuong-ii-ngon-ngu-mo-ta-phan-cung-vhdl.htm">20</a>
                                </li>
                                <li><i class="icon_view"></i>796</li>
                                <li><i class="icon_down"></i>0</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Ngôn ngữ mô tả phần cứng VHDL</label>
                        <div><a href="/document/1235802-ngon-ngu-mo-ta-phan-cung-vhdl.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Ngôn ngữ mô tả phần cứng VHDL</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1235802-ngon-ngu-mo-ta-phan-cung-vhdl.htm">137</a>
                                </li>
                                <li><i class="icon_view"></i>1,824</li>
                                <li><i class="icon_down"></i>1</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Tổng hợp các bài tập mẫu hay viết bằng ngôn
                            ngữ mô tả phần cứng VHDL</label>
                        <div>
                            <a href="/document/1419265-tong-hop-cac-bai-tap-mau-hay-viet-bang-ngon-ngu-mo-ta-phan-cung-vhdl.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Tổng hợp các bài tập
                                mẫu hay viết bằng ngôn ngữ mô tả phần cứng VHDL</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1419265-tong-hop-cac-bai-tap-mau-hay-viet-bang-ngon-ngu-mo-ta-phan-cung-vhdl.htm">32</a>
                                </li>
                                <li><i class="icon_view"></i>1,973</li>
                                <li><i class="icon_down"></i>2</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Ngôn ngữ mô tả phần cứng verilog doc</label>
                        <div><a href="/document/1794530-ngon-ngu-mo-ta-phan-cung-verilog-doc.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Ngôn ngữ mô tả phần cứng verilog doc</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1794530-ngon-ngu-mo-ta-phan-cung-verilog-doc.htm">24</a>
                                </li>
                                <li><i class="icon_view"></i>425</li>
                                <li><i class="icon_down"></i>0</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Verilog - Ngôn ngữ mô tả phần cứng
                            potx</label>
                        <div><a href="/document/1794534-verilog-ngon-ngu-mo-ta-phan-cung-potx.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Verilog - Ngôn ngữ mô tả phần cứng potx</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1794534-verilog-ngon-ngu-mo-ta-phan-cung-potx.htm">32</a>
                                </li>
                                <li><i class="icon_view"></i>541</li>
                                <li><i class="icon_down"></i>0</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Giáo trình NGÔN NGỮ MÔ TẢ PHẦN CỨNG VERILOG
                            TS. Vũ Đức Lung ThS. Lâm Đức Khải Ks. Phan Đình Duy</label>
                        <div>
                            <a href="/document/2391379-giao-trinh-ngon-ngu-mo-ta-phan-cung-verilog-ts-vu-duc-lung-ths-lam-duc-khai-ks-phan-dinh-duy.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Giáo trình NGÔN NGỮ
                                MÔ TẢ PHẦN CỨNG VERILOG TS. Vũ Đức Lung ThS. Lâm Đức Khải Ks. Phan Đình Duy</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/2391379-giao-trinh-ngon-ngu-mo-ta-phan-cung-verilog-ts-vu-duc-lung-ths-lam-duc-khai-ks-phan-dinh-duy.htm">303</a>
                                </li>
                                <li><i class="icon_view"></i>961</li>
                                <li><i class="icon_down"></i>3</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc1"></i> <label>Đồ án ngôn ngữ mô tả phần cứng HDL</label>
                        <div><a href="/document/2420764-do-an-ngon-ngu-mo-ta-phan-cung-hdl.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Đồ án ngôn ngữ mô tả phần cứng HDL</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/2420764-do-an-ngon-ngu-mo-ta-phan-cung-hdl.htm">15</a>
                                </li>
                                <li><i class="icon_view"></i>564</li>
                                <li><i class="icon_down"></i>2</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>NGÔN NGỮ MÔ TẢ PHẦN CỨNG HDL code mau
                            FSM</label>
                        <div><a href="/document/3497324-ngon-ngu-mo-ta-phan-cung-hdl-code-mau-fsm.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">NGÔN NGỮ MÔ TẢ PHẦN CỨNG HDL code mau FSM</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/3497324-ngon-ngu-mo-ta-phan-cung-hdl-code-mau-fsm.htm">4</a>
                                </li>
                                <li><i class="icon_view"></i>493</li>
                                <li><i class="icon_down"></i>1</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc3"></i> <label>ngôn ngữ mô tả phần cứng HDL</label>
                        <div><a href="/document/3765195-ngon-ngu-mo-ta-phan-cung-hdl.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">ngôn ngữ mô tả phần cứng HDL</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/3765195-ngon-ngu-mo-ta-phan-cung-hdl.htm">60</a>
                                </li>
                                <li><i class="icon_view"></i>784</li>
                                <li><i class="icon_down"></i>0</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Ngôn ngữ mô tả phần cứng VERILOG</label>
                        <div><a href="/document/1236642-ngon-ngu-mo-ta-phan-cung-verilog.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Ngôn ngữ mô tả phần cứng VERILOG</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1236642-ngon-ngu-mo-ta-phan-cung-verilog.htm">236</a>
                                </li>
                                <li><i class="icon_view"></i>1,578</li>
                                <li><i class="icon_down"></i>26</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc1"></i> <label>BÀI GIẢNG THIẾT KẾ SỐ DÙNG NGÔN NGỮ MÔ TẢ
                            PHẦN CỨNG docx</label>
                        <div><a href="/document/1273459-bai-giang-thiet-ke-so-dung-ngon-ngu-mo-ta-phan-cung-docx.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">BÀI GIẢNG THIẾT KẾ
                                SỐ DÙNG NGÔN NGỮ MÔ TẢ PHẦN CỨNG docx</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1273459-bai-giang-thiet-ke-so-dung-ngon-ngu-mo-ta-phan-cung-docx.htm">131</a>
                                </li>
                                <li><i class="icon_view"></i>1,242</li>
                                <li><i class="icon_down"></i>17</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Tạo một ngôn ngữ mô tả UI XML pot</label>
                        <div><a href="/document/2087209-tao-mot-ngon-ngu-mo-ta-ui-xml-pot.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Tạo một ngôn ngữ mô tả UI XML pot</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/2087209-tao-mot-ngon-ngu-mo-ta-ui-xml-pot.htm">61</a>
                                </li>
                                <li><i class="icon_view"></i>325</li>
                                <li><i class="icon_down"></i>0</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>Tài liệu Báo cáo " Trao đổi một số ý kiến về
                            sử dụng ngôn ngữ mô tả khi áp dụng AACR2 trong biên mục ở Việt Nam " pot</label>
                        <div>
                            <a href="/document/1083881-tai-lieu-bao-cao-trao-doi-mot-so-y-kien-ve-su-dung-ngon-ngu-mo-ta-khi-ap-dung-aacr2-trong-bien-muc-o-viet-nam-pot.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">Tài liệu Báo cáo "
                                Trao đổi một số ý kiến về sử dụng ngôn ngữ mô tả khi áp dụng AACR2 trong biên mục ở Việt
                                Nam " pot</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1083881-tai-lieu-bao-cao-trao-doi-mot-so-y-kien-ve-su-dung-ngon-ngu-mo-ta-khi-ap-dung-aacr2-trong-bien-muc-o-viet-nam-pot.htm">6</a>
                                </li>
                                <li><i class="icon_view"></i>582</li>
                                <li><i class="icon_down"></i>0</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>PHƯƠNG PHÁP VÀ KIỂU LẬP TRÌNH NGÔN NGỮ MÔ TẢ
                            ĐỐI TƯỢNG</label>
                        <div><a href="/document/1404871-phuong-phap-va-kieu-lap-trinh-ngon-ngu-mo-ta-doi-tuong.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">PHƯƠNG PHÁP VÀ KIỂU
                                LẬP TRÌNH NGÔN NGỮ MÔ TẢ ĐỐI TƯỢNG</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1404871-phuong-phap-va-kieu-lap-trinh-ngon-ngu-mo-ta-doi-tuong.htm">388</a>
                                </li>
                                <li><i class="icon_view"></i>499</li>
                                <li><i class="icon_down"></i>0</li>
                            </ul>
                        </div>
                    </li>
                    <li><i class="icon i_type_doc i_type_doc2"></i> <label>GIỚI THIỆU NGÔN NGỮ MÔ TẢ ĐỐI TƯỢNG</label>
                        <div><a href="/document/1404896-gioi-thieu-ngon-ngu-mo-ta-doi-tuong.htm" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base">GIỚI THIỆU NGÔN NGỮ MÔ TẢ ĐỐI TƯỢNG</a>
                            <ul class="doc_tk_cnt">
                                <li><i class="icon_doc"></i> <a href="/doc_search_title/1404896-gioi-thieu-ngon-ngu-mo-ta-doi-tuong.htm">111</a>
                                </li>
                                <li><i class="icon_view"></i>593</li>
                                <li><i class="icon_down"></i>1</li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="detailDescription  mb-4">
                <h2 class="text-2xl p-2 uppercase">Thông tin tài liệu</h2>
                <div>
                    <p>Ngày đăng: <?= date('d/m/Y', strtotime($this->document[0]['updated'])) ?></p>
                    <div class="des_content">
                        <?= $this->document[0]['description'] ?>
                    </div>
                    <!-- <a rel="nofollow" href="javascript:;" onclick="showFullDes(this)" class="showMoreDes">- Xem thêm
                        -</a>
                    <p>Xem thêm: <a href="/doc_search_title/634736-ngon-ngu-mo-ta-phan-cung-voi-vhdl-bai-tap-tham-khao.htm">Ngôn
                            ngữ mô tả phần cứng với VHDL - Bài tập tham khảo</a>, <a href="https://text.123docz.net/document/634736-ngon-ngu-mo-ta-phan-cung-voi-vhdl-bai-tap-tham-khao.htm">Ngôn
                            ngữ mô tả phần cứng với VHDL - Bài tập tham khảo</a>, <a href="https://f.123docz.net/634736-ngon-ngu-mo-ta-phan-cung-voi-vhdl-bai-tap-tham-khao.htm">Ngôn
                            ngữ mô tả phần cứng với VHDL - Bài tập tham khảo</a></p> -->
                </div>
            </div>
            <div class="box_detailRight">
                <h2 class="text-2xl font-medium p-2 uppercase">Từ khóa liên quan</h2>
                <div class="listTag">
                    <ul>
                        <?php
                        foreach ($this->relatedkeywords as $keyword) { ?>
                            <li><a title="<?= $keyword['name'] ?>" href="<?= URL ?>/document/<?= $keyword['url'] ?>"><?= $keyword['name'] ?></a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div id="to_top" class="w-10 h-10  fixed z-10 right-0 bottom-0 mr-4 mb-16 bg-white rounded-full cursor-pointer" style="display: none;">
                <svg viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="text-secondary w-10 h-10  rounded-full opacity-75 hover:opacity-100">
                    <circle cx="12" cy="12" r="10"></circle>
                    <polyline points="16 12 12 8 8 12"></polyline>
                    <line x1="12" y1="16" x2="12" y2="8"></line>
                </svg>
            </div>
        </div>
        <div class="detailRight col-span-12 lg:col-span-3 sm:px-3 relative p-2 ">
            <div class="box_detailRight hidden md:block">
                <h2 class="text-2xl  uppercase mb-2">Tài liệu liên quan</h2>
                <ul class="listDetail_relase">
                    <?php
                    foreach ($this->tailieulienquan as $tailieu) { ?>
                        <li><i class="icon i_type_doc i_type_doc2"></i> <label><?= $tailieu['name'] ?></label>
                            <div><a href="<?= $tailieu['url'] ?>" target="_blank" gtm-element="GTM_Document_Suggest_right" gtm-label="GTM_Document_Suggest_right" class="text-lg md:text-base"><?= $tailieu['name'] ?></a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i> <a href="/doc_search_title/634736-ngon-ngu-mo-ta-phan-cung-voi-vhdl-bai-tap-tham-khao.htm">31</a>
                                    </li>
                                    <li><i class="icon_view"></i><?= $tailieu['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $tailieu['download'] ?></li>
                                </ul>
                            </div>
                        </li>
                    <?php
                    }
                    ?>

                </ul>
            </div>
        </div>
        <div class="doc_common container mx-auto px-2 col-span-12 md:col-span-12">
            <h3 class="my-4 px-4 font-bold text-2xl">Tài liệu chung</h3>
            <div class="grid md:grid-cols-4 grid-cols-1 md:gap-4">

                <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Tài liệu mới nhất</h4>
                    <div class="total_doc_item_common px-2 md:px-0" id="tailieu-moinhat">
                        <?php
                        foreach ($this->moinhat as $moinhat) { ?>
                            <div class="item_common">
                                <a href="<?= URL ?>/document/<?= $moinhat['url'] ?>" title="1" class="text-lg md:text-base">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <?= $moinhat['name'] ?>
                                </a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>8</li>
                                    <li><i class="icon_view"></i><?= $moinhat['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $moinhat['download'] ?></li>
                                </ul>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Tài liệu tải nhiều</h4>
                    <div class="total_doc_item_common px-2 md:px-0" id="tailieu-moinhat">
                        <?php
                        foreach ($this->tainhieu as $tainhieu) { ?>
                            <div class="item_common">
                                <a href="<?= URL ?>/document/<?= $tainhieu['url'] ?>" title="1" class="text-lg md:text-base">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <?= $tainhieu['name'] ?>
                                </a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>8</li>
                                    <li><i class="icon_view"></i><?= $tainhieu['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $tainhieu['download'] ?></li>
                                </ul>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Tài liệu xem nhiều</h4>
                    <div class="total_doc_item_common px-2 md:px-0" id="tailieu-moinhat">
                        <?php
                        foreach ($this->xemnhieu as $xemnhieu) { ?>
                            <div class="item_common">
                                <a href="<?= URL ?>/document/<?= $xemnhieu['url'] ?>" title="1" class="text-lg md:text-base">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <?= $xemnhieu['name'] ?>
                                </a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>8</li>
                                    <li><i class="icon_view"></i><?= $xemnhieu['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $xemnhieu['download'] ?></li>
                                </ul>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="item_doc_common mb-4">
                    <h4 class="title_cm0 px-4 py-4 w-full text-white font-bold text-lg rounded">Tài liệu nổi bật</h4>
                    <div class="total_doc_item_common px-2 md:px-0" id="tailieu-moinhat">
                        <?php
                        foreach ($this->noibat as $noibat) { ?>
                            <div class="item_common">
                                <a href="<?= URL ?>/document/<?= $noibat['url'] ?>" title="1" class="text-lg md:text-base">
                                    <i class="icon i_type_doc i_type_doc1"></i>
                                    <?= $noibat['name'] ?>
                                </a>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>8</li>
                                    <li><i class="icon_view"></i><?= $noibat['view'] ?></li>
                                    <li><i class="icon_down"></i><?= $noibat['download'] ?></li>
                                </ul>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script defer="" type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2/document/js/detail.mine822.js"></script>
<script defer="" type="text/javascript" src="<?= URL ?>/template/static_v2/web_v2/document/js/detail.min.js?v=126771"></script>
<div id="blueseed_checking"></div>
<script defer type="text/javascript">
    var docName = "<?= $this->document[0]['name'] ?>";
    var isGenTagTeaserAuto = 0;
    var timeGenTeaser = 0;
    var checkMd5Teaser = 0;
    var t1 = [1635668353, '0223a60e2ff89a24fc79db9e65b10051'];
    var strCss = 'https://d1.store123doc.com/14/y/da/dak1399362434-51536-13993624349334/dak1399362434.css';
    var strIdCat = "Cao /u0111/u1eb3ng - /u0110/u1ea1i h/u1ecdc_101";
    var docMoneySafe = "0";
    var docId = "<?= $this->document[0]['id'] ?>";
    var enableBlueSheet_Inread = 0;
    var enableTestABD = (docId == 3367166) ? true : false;
    enableTestABD = false;
    var blockDownload = false;
    $(document).ready(function() {
        $.ajax({
            url: '/documents/ajax/ajax_check_block_storage.php',
            type: 'GET',
            dataType: 'json',
            data: {
                doc_id: docId
            },
            success: function(data) {
                blockDownload = data.data.block == 1 ? true : false;
                console.log('domain: ' + data.data.domain);
                console.log('block: ' + data.data.block);
                console.log('blockDownload: ' + blockDownload);
            }
        })
    });
    // var __hide_preroll = GetCookie('hide_preroll'); 
    //cookie check        var enable_blueseed_preroll = false; //off blueseed        //,enable_blueseed_preroll = (priceDoc == '' && !__hide_preroll) ? true : false;        if(typeof(enable_blueseed_preroll) != 'undefined' && enable_blueseed_preroll)        {            var b_width = '100%';            var b_height = '520';            var b_tag = 'http://blueserving.com/vast.xml?key=afa47f5506ab59577be9b3ce35342023&cat=giaoduc&country=vietnam'            var b_autostart = true;            var b_hidecontrol = true;            var b_div_player = 'blueseed_preroll';            var b_skip_offset = 5;            var _bs = document.createElement('script'); _bs.type = 'text/javascript';            _bs.src = 'http://lab.blueserving.com/libs/bs-preroll-html5.js';            var s = document.getElementsByTagName('header');s[0].parentNode.insertBefore(_bs, s[0]);            var d = new Date();            var now = d.getTime(), then = d.setHours(24,0,0,0);            //SetCookie('hide_preroll', true, (then - now)/3600000);            SetCookie('hide_preroll', true, 1);            (function () {                if (document.getElementById('bscelid') == null) {                    var bss = document.createElement('script');                    bss.type = 'text/javascript';                    bss.id = "bscelid";                    bss.src = 'http://lab.blueserving.com/libs/bsc.js';                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bss);                }            })();            document.write("<script defer type='text/javascript' class='bscelc'><//script>");            var bsa = document.createElement('script');            bsa.type = 'text/javascript';            bsa.id = "bscelid";            bsa.src = 'http://lab.blueserving.com/libs/bsca.js';            document.getElementById('blueseed_checking').appendChild(bsa);            var ___str = '<video src="http://cdn1.blueseed.tv/BSTVC/bsc.mp4" autoplay style="display:none"></video>';            $('#blueseed_checking').append(___str);        }        /**         * tắt quang cáo bluseed         */        function closeprerollHandler()        {            $('.bg_transparent_bs').remove();        }        function startprerollHandler(){            var _div = '<div class="bg_transparent_bs"><label class="skipads"> Bạn có thể bỏ qua quảng cáo sau 5 giây </label></div>';            $(_div).appendTo('html');            var __top = $('#contentDocument').offset().top;            var __left = $('#contentDocument').offset().left;            $(window).scrollTop(__top);            $('.skipads').css({                top: '30px',                left : (920 + __left) + 'px'            });            $('#blueseed_preroll_bound').css({                top: '30px',                left : __left + 'px'            });        }    
</script>
<script defer type="text/javascript">
    var dataLayer = dataLayer || [];
    dataLayer.push({
        'event': 'product_detail',
        'ecommerce': {
            'detail': {
                'actionField': {
                    'list': ''
                },
                // 'detail'
                // actions have an optional list property.
                'products': [{
                    'name': docName, // Name or ID is required.                            
                    'id': docId,
                    'price': docMoneySafe,
                    'category': strIdCat
                }]
            }
        }
    });
</script>
<!-- // var _showBox = GetCookie('__show_popup');if(!_showBox && (typeof(_showNotifiUser) == 'undefined' || _showNotifiUser)) { setTimeout(function(){ colorbox(2, '/documents/popup/pop_upload_earn_money.php', 'Upload tăng doanh thu', 650, 500, function(){ SetCookie('__show_popup', true, 24); } , function (){}); }, 1000); } -->
</script>
<script defer type="text/javascript">
    function popupDocReq() {
        $('body').find('.boxAddMoney').remove();
        var _width = ($(window).width() - 800) / 2 - 50 + 'px';
        var html = '<div class="bg_transparent" onclick="closeBox();"></div><div style="position: absolute;left:25%;top:300px;" class="boxAddMoney"></div>';
        $('body').append(html);
        $.ajax({
            url: '/global/ajax/aja_send_req_doc.php',
            type: 'POST',
            dataType: 'html',
            beforeSend: function() {
                $("<div id='loading-excel'></div>").appendTo("body");
            },
            complete: function() {
                $("#loading-excel").remove();
            },
            success: function(data) {
                $('.boxAddMoney').append(data);
            }
        })
    }
</script>