<div id="container">
    <div class="ad_user hidden md:block"></div>
    <div class="ad_user_menu bg-white">
        <div class="mx-auto container grid grid-cols-12">
            <ul class="flex justify-between col-span-12 md:col-span-6 md:col-start-4 px-2">
            <li class="py-4 px-1 mb:mx-2 whitespace-no-wrap <?= $_REQUEST['type'] == 1 ? 'text-primary border-b border-solid border-primary' : '' ?> "> <a href="<?= URL ?>/user/home?type=1">Tất cả (0)</a> </li>
                <li class="py-4 px-1 md:mx-2 whitespace-no-wrap <?= $_REQUEST['type'] == 2 ? 'text-primary border-b border-solid border-primary' : '' ?> "> <a href="<?= URL ?>/user/home?type=2">Miễn phí (0)</a> </li>
                <li class="py-4 px-1 mb:mx-2  whitespace-no-wrap <?= $_REQUEST['type'] == 3 ? 'text-primary border-b border-solid border-primary' : '' ?> "> <a href="<?= URL ?>/user/home?type=3">Trả phí (0)</a> </li>
                <li class="py-4 px-1 mb:mx-2  whitespace-no-wrap <?= $_URL['1'] == 'my_collection' ? 'text-primary border-b border-solid border-primary' : '' ?> "> <a href="<?= URL ?>/user/my_collection">Bộ sưu tập (<?= COUNT($this->mycoll) ?>)</a> </li>
            </ul>
            <div class="col-span-12 md:col-span-2 py-4 px-2 md:text-right">
                <span class="doc_file_down p-2 border border-solid border-gray-400 rounded">Xem nhiều<i class="icon_down_t"></i>
                    <ul class="transition2 text-left">
                        <li>
                            <a href="/users/home/user_home.php?use_id=8424741&amp;type=1&amp;t=tat-ca">Tất cả</a>
                        </li>
                        <li>
                            <a href="/users/home/user_home.php?use_id=8424741&amp;type=2&amp;t=xem-nhieu">Xem nhiều</a>
                        </li>
                        <li>
                            <a href="/users/home/user_home.php?use_id=8424741&amp;type=3&amp;t=tai-nhieu">Tải nhiều</a>
                        </li>
                        <li>
                            <a href="/users/home/user_home.php?use_id=8424741&amp;type=4&amp;t=thich-nhieu">Thích nhiều</a>
                        </li>
                    </ul>
                </span>
            </div>
        </div>
    </div>
    <div class="container mx-auto grid grid-cols-12 gap-2">
        <div class="ad_user_side_left col-span-12 md:col-span-3 transform md:-translate-y-24 px-2 md:px-0">
            <div class="flex py-2 md:block">
                <div class="ad_user_avatar w-24 h-24 md:w-40 md:h-40 mr-6 md:mr-auto">
                    <img src="https://media.123dok.info/images/default/user_small.png" alt="avatar">
                    <form class="form" name="step_2" id="step_2" action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="action" id="action" value="action_avatar">
                        <input type="hidden" id="x" name="x">
                        <input type="hidden" id="y" name="y">
                        <input type="hidden" id="w" name="w">
                        <input type="hidden" id="h" name="h">
                        <a class="ad_user_over_avatar transition0-5 edit_avatar" href="javascript:;">
                            <input type="file" class="input_avatar" name="image" id="image" accept="image/*" onclick="this.value=''" onchange="previewAvatar()">
                            <i class="icon_upload_picture"></i>
                            <span>Tải ảnh đại diện</span>
                        </a>
                    </form>
                </div>

                <div>
                    <h3 class="text-2xl  ">Hùng Nguyễn</h3>
                    <a class="ad_user_messeger my-4 block" href="/tin-nhan-8424741-hung-nguyen.htm">
                        <i class="icon_doc_mail filter"></i>
                        Tin nhắn
                    </a>
                    <div class="ad_user_list_view">
                        <ul class="flex text-center">
                            <li class="pr-4">
                                <span class="ad_user_title">Lượt xem</span>
                                <span class="ad_user_number">0</span>
                            </li>
                            <li class="border-l border-r border-solid border-gray-400 px-4">
                                <span class="ad_user_title">Tài liệu</span>
                                <span class="ad_user_number">0</span>
                            </li>
                            <li class="pl-4">
                                <span class="ad_user_title">Lượt tải</span>
                                <span class="ad_user_number">0</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ad_user_menu_left">
                <ul>
                    <li><a href="<?= URL ?>/user/home" class="block icon_tq active_tabU">Tổng quan</a></li>
                    <li><a href="<?= URL ?>/user/doc_control" class="block icon_qltl">Quản lý tài liệu</a></li>
                    <li><a href="<?= URL ?>/user/finance_control" class="block icon_qltc">Quản lý tài chính</a></li>
                    <li><a href="<?= URL ?>/user/profile" class="block icon_ttcn">Thông tin cá nhân</a></li>
                </ul>
            </div>
            <div class="ad_user_image hidden md:block">
            <img src="<?= URL ?>/template/images/default/logo.png" width="200px" style="margin: 0 auto;" alt="">
            <!-- <i class="icon_image_h"></i> -->
            </div>
        </div>
        <div class="ad_user_side_right col-span-12 md:col-span-9 px-2">
            <div class="text-2xl px-2 my-4">
                <h3><strong><?= $this->coll[0]['name'] ?></strong></h3>
                <div class="italic text-gray-600 text-sm"><?= $this->coll[0]['description'] ?></div>
            </div>
            <div class="box_content searchPage seoTag">
                <div class="search_bottom">
                    <div class="seo_bottom_left">
                        <?php 
                            foreach($this->docs as $doc){ ?>
                                <div class="seo_item mb-0">
                                    <div class="doc_list_cnt list_cnt_small list div_del mb-4 grid grid-cols-12">
                                        <a class="doc_cnt_img col-span-4 md:col-span-2 border border-solid border-gray-400 -mt-2 ml-6 md:mx-auto" href="/document/9529-giao-trinh-tin-hoc-van-phong.htm" target="_blank" title="Giáo trình tin học văn phòng">
                                            <img alt="Giáo trình tin học văn phòng" src="<?= URL ?>/<?= $doc['hinhanh'] ?>">
                                        </a>
                                        <div class="col-span-8 md:col-span-9 -mt-2">
                                            <a class="block pr-2" href="<?= URL ?>/document/<?= $doc['url'] ?>" target="_blank" title="<?= $doc['name'] ?>">
                                                <h4 class="text-base md:text-2xl doc_title_cnt"><?= $doc['name'] ?> <i class="icon i_type_doc i_type_doc1" style="position: initial;"></i>
                                                </h4>
                                            </a>
                                            <div>
                                                <p>
                                                    <label>Danh mục: </label>
                                                    <a href="<?= URL ?>/document/1/<?= $doc['urlcate'] ?>" title="Tin học văn phòng"><?= $doc['category'] ?></a>
                                                </p>
                                                <p class="doc_man_date static">Ngày tải lên :30/08/2012, 08:54 </p>
                                                <ul class="doc_tk_cnt">
                                                    <li>
                                                        <i class="icon_doc"></i>73
                                                    </li>
                                                    <li>
                                                        <i class="icon_view"></i><?= $doc['view'] ?>
                                                    </li>
                                                    <li>
                                                        <i class="icon_down"></i><?= $doc['download'] ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
                        ?>
                       
                        <!-- <div class="seo_item mb-0">
                            <div class="doc_list_cnt list_cnt_small list div_del mb-4 grid grid-cols-12">
                                <a class="doc_cnt_img col-span-4 md:col-span-2 border border-solid border-gray-400 -mt-2 ml-6 md:mx-auto" href="/document/12238-nhung-dac-diem-co-ban-cua-giai-cap-cong-nhan-doc.htm" target="_blank" title="Những đặc điểm cơ bản của giai cấp công nhân.DOC">
                                    <img alt="Những đặc điểm cơ bản của giai cấp công nhân.DOC" src="https://media.123dok.info/images/document/12/pt/tq/medium_tqq1346562757.png">
                                </a>

                                <div class="col-span-8 md:col-span-9 -mt-2">
                                    <a class="block pr-2" href="/document/12238-nhung-dac-diem-co-ban-cua-giai-cap-cong-nhan-doc.htm" target="_blank" title="Những đặc điểm cơ bản của giai cấp công nhân.DOC">
                                        <h4 class="text-base md:text-2xl doc_title_cnt">Những đặc điểm cơ bản của giai cấp công nhân.DOC <i class="icon i_type_doc i_type_doc1" style="position: initial;"></i>
                                        </h4>
                                    </a>
                                    <div>
                                        <p>
                                            <label>Danh mục: </label>
                                            <a href="/doc-cat/171-ke-toan.htm" title="Kế toán">Kế toán</a>
                                        </p>
                                        <p class="doc_man_date static">Ngày tải lên :02/09/2012, 12:12 </p>
                                        <ul class="doc_tk_cnt">
                                            <li>
                                                <i class="icon_doc"></i>24
                                            </li>
                                            <li>
                                                <i class="icon_view"></i>78,699
                                            </li>
                                            <li>
                                                <i class="icon_down"></i>107
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="ads_show"><ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="9854054443" data-adsbygoogle-status="done" data-ad-status="filled"><ins id="aswift_0_expand" tabindex="0" title="Advertisement" aria-label="Advertisement" style="border: none; height: 90px; width: 728px; margin: 0px; padding: 0px; position: relative; visibility: visible; background-color: transparent; display: inline-table;"><ins id="aswift_0_anchor" style="border: none; height: 90px; width: 728px; margin: 0px; padding: 0px; position: relative; visibility: visible; background-color: transparent; display: block;"><iframe id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;border:0;width:728px;height:90px;" sandbox="allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" width="728" height="90" frameborder="0" src="https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-2979760623205174&amp;output=html&amp;h=90&amp;slotname=9854054443&amp;adk=3426631024&amp;adf=4000822716&amp;pi=t.ma~as.9854054443&amp;w=728&amp;lmt=1635393384&amp;psa=1&amp;format=728x90&amp;url=https%3A%2F%2F123docz.net%2Fcollection%2F27807-sdfkjhjdksfhjsdhfjhjdkhfj.htm&amp;flash=0&amp;wgl=1&amp;uach=WyJXaW5kb3dzIiwiMTAuMC4wIiwieDg2IiwiIiwiOTUuMC40NjM4LjU0IixbXSxudWxsLG51bGwsIjY0Il0.&amp;dt=1635393384619&amp;bpp=7&amp;bdt=5405&amp;idt=42&amp;shv=r20211020&amp;mjsv=m202110200101&amp;ptt=9&amp;saldr=aa&amp;abxe=1&amp;cookie=ID%3Dfec19286e603ab92-22a8b34daecc00b6%3AT%3D1634721732%3ART%3D1634721732%3AS%3DALNI_MaY77AWGnrrPhWurJt3gVzfQ22cow&amp;correlator=8416827082085&amp;frm=20&amp;pv=2&amp;ga_vid=1372386773.1634721731&amp;ga_sid=1635393385&amp;ga_hid=1263580067&amp;ga_fc=1&amp;u_tz=420&amp;u_his=50&amp;u_h=864&amp;u_w=1536&amp;u_ah=824&amp;u_aw=1536&amp;u_cd=24&amp;adx=557&amp;ady=602&amp;biw=1519&amp;bih=754&amp;scr_x=0&amp;scr_y=0&amp;oid=2&amp;pvsid=4480481354409089&amp;pem=500&amp;ref=https%3A%2F%2F123docz.net%2Fusers%2Fhome%2Fmy_collection.php%3Fuse_id%3D8424741%26type%3D0%26t%3Dmoi-dang&amp;eae=0&amp;fc=896&amp;brdim=0%2C0%2C0%2C0%2C1536%2C0%2C1536%2C824%2C1536%2C754&amp;vis=1&amp;rsz=%7C%7CoeE%7C&amp;abl=CS&amp;pfx=0&amp;alvm=r20211026&amp;fu=0&amp;bc=31&amp;ifi=1&amp;uci=a!1&amp;fsb=1&amp;xpc=7hyJqc3UUE&amp;p=https%3A//123docz.net&amp;dtd=83" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!1" data-google-query-id="CIyNuI6b7PMCFZZxYAod3kQD9A" data-load-complete="true"></iframe></ins></ins></ins>
                            <script defer="">
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div> -->
                        <div class="paging">
                        </div>
                    </div>
                    <!-- <div class="seo_bottom_right"><ins class="adsbygoogle" style="display:inline-block;width:300px;height:600px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="8377321249" data-adsbygoogle-status="done" data-ad-status="filled"><ins id="aswift_1_expand" tabindex="0" title="Advertisement" aria-label="Advertisement" style="border: none; height: 600px; width: 300px; margin: 0px; padding: 0px; position: relative; visibility: visible; background-color: transparent; display: inline-table;"><ins id="aswift_1_anchor" style="border: none; height: 600px; width: 300px; margin: 0px; padding: 0px; position: relative; visibility: visible; background-color: transparent; display: block;"><iframe id="aswift_1" name="aswift_1" style="left:0;position:absolute;top:0;border:0;width:300px;height:600px;" sandbox="allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" width="300" height="600" frameborder="0" src="https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-2979760623205174&amp;output=html&amp;h=600&amp;slotname=8377321249&amp;adk=1228536798&amp;adf=3624423020&amp;pi=t.ma~as.8377321249&amp;w=300&amp;lmt=1635393384&amp;psa=1&amp;format=300x600&amp;url=https%3A%2F%2F123docz.net%2Fcollection%2F27807-sdfkjhjdksfhjsdhfjhjdkhfj.htm&amp;flash=0&amp;wgl=1&amp;uach=WyJXaW5kb3dzIiwiMTAuMC4wIiwieDg2IiwiIiwiOTUuMC40NjM4LjU0IixbXSxudWxsLG51bGwsIjY0Il0.&amp;dt=1635393384626&amp;bpp=2&amp;bdt=5412&amp;idt=83&amp;shv=r20211020&amp;mjsv=m202110200101&amp;ptt=9&amp;saldr=aa&amp;abxe=1&amp;cookie=ID%3Dfec19286e603ab92-22a8b34daecc00b6%3AT%3D1634721732%3ART%3D1634721732%3AS%3DALNI_MaY77AWGnrrPhWurJt3gVzfQ22cow&amp;prev_fmts=728x90&amp;correlator=8416827082085&amp;frm=20&amp;pv=1&amp;ga_vid=1372386773.1634721731&amp;ga_sid=1635393385&amp;ga_hid=1263580067&amp;ga_fc=1&amp;u_tz=420&amp;u_his=50&amp;u_h=864&amp;u_w=1536&amp;u_ah=824&amp;u_aw=1536&amp;u_cd=24&amp;adx=460&amp;ady=714&amp;biw=1519&amp;bih=754&amp;scr_x=0&amp;scr_y=0&amp;oid=2&amp;pvsid=4480481354409089&amp;pem=500&amp;ref=https%3A%2F%2F123docz.net%2Fusers%2Fhome%2Fmy_collection.php%3Fuse_id%3D8424741%26type%3D0%26t%3Dmoi-dang&amp;eae=0&amp;fc=896&amp;brdim=0%2C0%2C0%2C0%2C1536%2C0%2C1536%2C824%2C1536%2C754&amp;vis=1&amp;rsz=%7C%7CleE%7C&amp;abl=CS&amp;pfx=0&amp;alvm=r20211026&amp;fu=0&amp;bc=31&amp;ifi=2&amp;uci=a!2&amp;fsb=1&amp;xpc=8sW8XHGpeL&amp;p=https%3A//123docz.net&amp;dtd=87" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!2" data-google-query-id="CLiZuI6b7PMCFZNuYAodrEoBEg" data-load-complete="true"></iframe></ins></ins></ins>
                        <script defer="">
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>