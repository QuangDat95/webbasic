<div id="container">
    <div class="ad_user hidden md:block"></div>
    <div class="ad_user_menu bg-white">
        <div class="mx-auto container grid grid-cols-12">
            <ul class="flex justify-between col-span-12 md:col-span-6 md:col-start-4 px-2">
                <li class="py-4 px-1 mb:mx-2 whitespace-no-wrap <?= $_REQUEST['type'] == 1 ? 'text-primary border-b border-solid border-primary' : '' ?> "> <a href="<?= URL ?>/user/home?type=1">Tất cả (0)</a> </li>
                <li class="py-4 px-1 md:mx-2 whitespace-no-wrap <?= $_REQUEST['type'] == 2 ? 'text-primary border-b border-solid border-primary' : '' ?> "> <a href="<?= URL ?>/user/home?type=2">Miễn phí (0)</a> </li>
                <li class="py-4 px-1 mb:mx-2  whitespace-no-wrap <?= $_REQUEST['type'] == 3 ? 'text-primary border-b border-solid border-primary' : '' ?> "> <a href="<?= URL ?>/user/home?type=3">Trả phí (0)</a> </li>
                <li class="py-4 px-1 mb:mx-2  whitespace-no-wrap <?= $_URL['1'] == 'my_collection' ? 'text-primary border-b border-solid border-primary' : '' ?> "> <a href="<?= URL ?>/user/my_collection">Bộ sưu tập (<?= COUNT($this->mycoll) ?>)</a> </li>
            </ul>
            <div class="col-span-12 md:col-span-2 py-4 px-2 md:text-right"> <span class="doc_file_down p-2 border border-solid border-gray-400 rounded">Xem nhiều<i class="icon_down_t"></i>
                    <ul class="transition2 text-left">
                        <li> <a href="/users/home/user_home.php?use_id=8424741&amp;type=1&amp;t=tat-ca">Tất cả</a> </li>
                        <li> <a href="/users/home/user_home.php?use_id=8424741&amp;type=2&amp;t=xem-nhieu">Xem nhiều</a> </li>
                        <li> <a href="/users/home/user_home.php?use_id=8424741&amp;type=3&amp;t=tai-nhieu">Tải nhiều</a> </li>
                        <li> <a href="/users/home/user_home.php?use_id=8424741&amp;type=4&amp;t=thich-nhieu">Thích nhiều</a> </li>
                    </ul>
                </span> </div>
        </div>
    </div>
    <div class="container mx-auto grid grid-cols-12 gap-2">
        <div class="ad_user_side_left col-span-12 md:col-span-3 transform md:-translate-y-24 px-2 md:px-0">
            <div class="flex py-2 md:block">
                <div class="ad_user_avatar w-24 h-24 md:w-40 md:h-40 mr-6 md:mr-auto"> <img src="https://media.123dok.info/images/default/user_small.png" alt="avatar">
                    <form class="form" name="step_2" id="step_2" action="" method="POST" enctype="multipart/form-data"> <input type="hidden" name="action" id="action" value="action_avatar"> <input type="hidden" id="x" name="x"> <input type="hidden" id="y" name="y"> <input type="hidden" id="w" name="w"> <input type="hidden" id="h" name="h"> <a class="ad_user_over_avatar transition0-5 edit_avatar" href="javascript:;"> <input type="file" class="input_avatar" name="image" id="image" accept="image/*" onclick="this.value=''" onchange="previewAvatar()"> <i class="icon_upload_picture"></i> <span>Tải ảnh đại diện</span> </a> </form>
                </div>
                <div>
                    <h3 class="text-2xl ">Hùng Nguyễn</h3> <a class="ad_user_messeger my-4 block" href="/tin-nhan-8424741-hung-nguyen.htm"> <i class="icon_doc_mail filter"></i> Tin nhắn </a>
                    <div class="ad_user_list_view">
                        <ul class="flex text-center">
                            <li class="pr-4"> <span class="ad_user_title">Lượt xem</span> <span class="ad_user_number">0</span> </li>
                            <li class="border-l border-r border-solid border-gray-400 px-4"> <span class="ad_user_title">Tài liệu</span> <span class="ad_user_number">0</span> </li>
                            <li class="pl-4"> <span class="ad_user_title">Lượt tải</span> <span class="ad_user_number">0</span> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ad_user_menu_left">
                <ul>
                    <li><a href="<?=URL?>/user/home" class="block icon_tq active_tabU">Tổng quan</a></li>
                    <li><a href="<?=URL?>/user/doc_control" class="block icon_qltl">Quản lý tài liệu</a></li>
                    <li><a href="<?=URL?>/user/finance_control" class="block icon_qltc">Quản lý tài chính</a></li>
                    <li><a href="<?=URL?>/user/profile" class="block icon_ttcn">Thông tin cá nhân</a></li>
                </ul>
            </div>
            <div class="ad_user_image hidden md:block">
            <img src="<?= URL ?>/template/images/default/logo.png" width="200px" style="margin: 0 auto;" alt="">
            <!-- <i class="icon_image_h"></i> -->
            </div>
        </div>
        <div class="ad_user_side_right col-span-12 md:col-span-9 p-2">
            <form id="formdoc">
                <div class="grid grid-cols-2 md:grid-cols-4 gap-2"> </div> Đang cập nhật dữ liệu...
            </form>
        </div>
    </div>
    <script defer="">
        $(function() {
            var el = $('input.doc_check_del');
            var parent = $('input[name="checkoption"]');
            parent.change(function() {
                if (parent.is(':checked')) {
                    el.attr('checked', 'checked');
                } else {
                    el.removeAttr('checked');
                }
            }).change();
        });
        $(document).ready(function() {
            $(".deleteitem").click(function(event) {
                var _this = $(this);
                var _id = _this.attr('bid');
                if (!confirm('Bạn muốn xóa tài liệu đã chọn')) return;
                $.ajax({
                    url: '/users/ajax/aja_user_control.php',
                    type: 'POST',
                    data: {
                        id: _id,
                        type: 'delete'
                    },
                    dataType: 'json',
                    success: function(data) {
                        var del_div = $('.doc_list_cnt');
                        if (data.code == 0) {
                            _this.closest(del_div).slideUp(400);
                        } else {
                            alert(data.message);
                        }
                    }
                }); /* Act on the event */
            });
        });

        function deleteitem() {
            var _form = $("#formdoc");
            var myform = _form.serializeArray();
            console.log(myform);
            if (myform.length < 1) {
                alert('Bạn chưa chọn tài liệu!');
                return;
            }
            if (!confirm('Bạn muốn xóa tài liệu đã chọn')) return;
            $.ajax({
                url: '/users/ajax/aja_user_control.php',
                type: 'POST',
                data: {
                    form: myform,
                    type: 'delete'
                },
                dataType: 'json',
                success: function(data) {
                    var del_div = $('.doc_list_cnt');
                    if (data.code == 0) {
                        _form.find('input[type=checkbox]:checked').closest(del_div).hide(400);
                    } else {
                        alert(data.message);
                    }
                }
            });
        }
    </script>
</div>