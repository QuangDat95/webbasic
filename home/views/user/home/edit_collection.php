<div id="container">
    <div class="ad_user hidden md:block"></div>
    <div class="bg-white py-4">
        <div class="container mx-auto grid grid-cols-12 relative p-2 md:p-0">
            <div class="ad_user_avatar_small  md:absolute left-0 top-0  transform md:-translate-y-1/2 row-span-2"> <img class="" src="https://media.123dok.info/images/default/user_small.png" alt="avatar"> </div>
            <div class="col-start-4 col-span-8 md:col-start-2 flex items-center px-4">
                <h3 class="ad_user_name text-xl md:text-2xl  mr-4">Hùng Nguyễn</h3> <a class="ad_user_messeger" href="/tin-nhan-8424741-hung-nguyen.htm"> <i class="icon_doc_mail filter"></i> Tin nhắn </a>
            </div>
            <div class="ad_user_list_view md:col-start-10 md:col-span-3 col-start-4 col-span-8 px-4">
                <ul class="flex text-center">
                    <li class="pr-4"> <span class="ad_user_title">Lượt xem</span> <span class="ad_user_number">0</span> </li>
                    <li class="border-l border-r border-solid border-gray-400 px-4"> <span class="ad_user_title">Tài liệu</span> <span class="ad_user_number">0</span> </li>
                    <li class="pl-4"> <span class="ad_user_title">Lượt tải</span> <span class="ad_user_number">0</span> </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mx-auto container  grid grid-cols-12 gap-2 ">
        <h3 class="text-2xl col-start-4 col-span-9 px-2 my-4">Chỉnh sửa bộ sưu tập</h3>
    </div>
    <div class="container mx-auto grid grid-cols-12 gap-2">
        <div class="ad_user_side_left col-span-12 md:col-span-3 px-2 md:pr-0">
            <div class="ad_user_menu_left">
                <ul>
                    <li><a href="<?= URL ?>/user/home" class="block icon_tq active_tabU">Tổng quan</a></li>
                    <li><a href="<?= URL ?>/user/doc_control" class="block icon_qltl">Quản lý tài liệu</a></li>
                    <li><a href="<?= URL ?>/user/finance_control" class="block icon_qltc">Quản lý tài chính</a></li>
                    <li><a href="<?= URL ?>/user/profile" class="block icon_ttcn">Thông tin cá nhân</a></li>
                </ul>
            </div>
            <div class="ad_user_image hidden md:block">
            <img src="<?= URL ?>/template/images/default/logo.png" width="200px" style="margin: 0 auto;" alt="">
            <!-- <i class="icon_image_h"></i> -->
            </div>
        </div>
        <div class="ad_user_side_right col-span-12 md:col-span-9 px-2">
            <div class="ucd_content">
                <div class="ucd_top1">
                    <div class="ucc_form">
                        <div class="ucc_form_content w-full px-2 md:px-10">
                            <form method="POST" action="<?= URL ?>/user/saveeditcoll"  class="grid grid-cols-12">
                                <input type="hidden" name="col_id" id="col_id" value="<?= $this->coll[0]['id'] ?>">
                                <input type="hidden" value="action_edit" name="action">
                                <span class="name_coll grid col-span-3">Tên: </span>
                                <input class="col_name ipt_coll grid col-span-9 w-full" type="text" id="col_name" name="col_name" value="<?= $this->coll[0]['name'] ?>">
                                <span class="name_coll name_coll2 grid col-span-3">Mô tả: </span>
                                <textarea name="col_description" rows="5" class="ipt_description grid col-span-9 w-full"><?= $this->coll[0]['description'] ?></textarea>
                                <span class="name_coll grid col-span-3">Từ khoá: </span>
                                <div class="tags flex grid col-span-9 w-full" data-wmin="100">
                                    <ul class="list-tag"></ul> <input class="text-input-tag ipt_tag" id="" data-tag="true" type="text" name="col_tags" placeholder="Nhập từ khóa cách nhau bằng dấu phẩy.">
                                    <ul class="list-result" style="left: -1px; top: 42px;"></ul>
                                </div> 
                                    <input class="ipt_coll" id="id_tag_remove" type="hidden" name="id_tag_remove" value=""> 
                                <button class="col-start-4 grid mx-auto smt_edit_info text-white w-40" type="submit"  id="btSubmit1">Chỉnh sửa </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="doc_item_cnt list_collection">
                    <h3 class="text-2xl col-start-4 col-span-9 px-2 my-4">Tài liệu trong bộ sưu tập</h3>
                    <?php
                    if ($this->docs) { ?>
                        <div class="grid grid-cols-2 md:grid-cols-4 gap-2 mb-3">
                            <?php
                            foreach ($this->docs as $doc) { ?>

                                <div class="card-doc col-span-1 relative">
                                    <i class="collection icon i_type_doc i_type_doc1"></i>
                                    <div class="card-doc-user-hover">
                                        <a class="doc_del_item icon del_item_coll" href="javascript:void(0)" onclick="delldoc(<?= $doc['id'] ?>,<?= $this->coll[0]['id'] ?>)" data="<?= $doc['id'] ?>">Xoá</a>
                                    </div> <a class="card-doc-img" href="<?= URL ?>/document/<?= $doc['url'] ?>" title="<?= $doc['name'] ?>">
                                        <img src="<?= URL ?>/<?= $doc['hinhanh'] ?>" onerror="this.src='<?= URL ?>/template/images/default/no-image.png" alt="<?= $doc['name'] ?>"> </a>
                                    <a href="<?= URL ?>/document/<?= $doc['url'] ?>" title="<?= $doc['name'] ?>">
                                        <span class="card-doc-title collection"><?= $doc['name'] ?></span> </a>
                                    <span class="ml-4 text-gray-500 text-xs">Ngày đăng: <?= date('d/m/Y', strtotime($doc['updated'])) ?></span>
                                    <ul class="doc_tk_cnt">
                                        <li> Trạng thái: Công khai</li>
                                        <li> <i class="icon_doc"></i> 24</li>
                                    </ul>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    <?php
                    } else { ?>
                        Chưa có tài liệu nào trong bộ sưu tập này!
                    <?php
                    }
                    ?>
                    <div class="clear"></div>
                    <div class="sdv_paging">
                        <div class="sdv_paging_content"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function delldoc(docid, collid) {
        if (confirm("Bạn có chắc chắn muốn xóa?"))
            window.location.href = '<?= URL ?>/user/delldoc?docid=' + docid + '&collid=' + collid;
    }

    // $('#btSubmit1').on('click', function (){
    //     var form = document.getElementById("edit-col");
    //     form.submit();
    // })
</script>