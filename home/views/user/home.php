<link rel="stylesheet" type="text/css"
      href="<?=URL?>template/styles/user.css"/>
<div id="container">
    <div class="ad_user hidden md:block"></div>
    <div class="ad_user_menu bg-white">
        <div class="mx-auto container grid grid-cols-12">
            <ul class="flex justify-between col-span-12 md:col-span-6 md:col-start-4 px-2">
                <li><a href="<?=URL?>/user/home" class="block icon_tq">Tổng quan</a></li>
                <li><a href="<?=URL?>/user/doc_control" class="block icon_qltl">Quản
                        lý tài liệu</a></li>
                <li><a href="<?=URL?>/user/finance_control" class="block icon_qltc">Quản lý tài chính</a>
                </li>
                <li><a href="<?=URL?>/user/profile" class="block icon_ttcn active_tabU">Thông tin cá
                        nhân</a></li>
            </ul>
            <div class="col-span-12 md:col-span-2 py-4 px-2 md:text-right"><span
                    class="doc_file_down p-2 border border-solid border-gray-400 rounded">Xem nhiều<i
                        class="icon_down_t"></i>                    <ul class="transition2 text-left">                                                    <li>                                <a
                                href="/users/home/user_home.php?use_id=8196231&amp;type=1&amp;t=tat-ca">Tất cả</a>                            </li>                                                        <li>                                <a
                                href="/users/home/user_home.php?use_id=8196231&amp;type=2&amp;t=xem-nhieu">Xem nhiều</a>                            </li>                                                        <li>                                <a
                                href="/users/home/user_home.php?use_id=8196231&amp;type=3&amp;t=tai-nhieu">Tải nhiều</a>                            </li>                                                        <li>                                <a
                                href="/users/home/user_home.php?use_id=8196231&amp;type=4&amp;t=thich-nhieu">Thích nhiều</a>                            </li>                                                </ul>                </span>
            </div>
        </div>
    </div>
    <div class="container mx-auto grid grid-cols-12 gap-2">
        <div class="ad_user_side_left col-span-12 md:col-span-3 transform md:-translate-y-24 px-2 md:px-0">
            <div class="flex py-2 md:block">
                <div class="ad_user_avatar w-24 h-24 md:w-40 md:h-40 mr-6 md:mr-auto"><img
                        src="https://media.123dok.info/images/default/user_small.png" alt="avatar">
                    <form class="form" name="step_2" id="step_2" action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="action" id="action" value="action_avatar"> <input type="hidden"
                                                                                                     id="x" name="x">
                        <input type="hidden" id="y" name="y"> <input type="hidden" id="w" name="w"> <input type="hidden"
                                                                                                           id="h"
                                                                                                           name="h"> <a
                            class="ad_user_over_avatar transition0-5 edit_avatar" href="javascript:;"> <input
                                type="file" class="input_avatar" name="image" id="image" accept="image/*"
                                onclick="this.value=''" onchange="previewAvatar()"> <i
                                class="icon_upload_picture"></i> <span>Tải ảnh đại diện</span> </a></form>
                </div>
                <div><h3 class="text-2xl  ">Nguyễn Thế Quỳnh</h3>                <a class="ad_user_messeger my-4 block"
                                                                                    href="/tin-nhan-8196231-nguyen-the-quynh.htm">
                        <i class="icon_doc_mail filter"></i> Tin nhắn </a>
                    <div class="ad_user_list_view">
                        <ul class="flex text-center">
                            <li class="pr-4"><span class="ad_user_title">Lượt xem</span> <span
                                    class="ad_user_number">10</span></li>
                            <li class="border-l border-r border-solid border-gray-400 px-4"><span class="ad_user_title">Tài liệu</span>
                                <span class="ad_user_number">2</span></li>
                            <li class="pl-4"><span class="ad_user_title">Lượt tải</span> <span
                                    class="ad_user_number">0</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ad_user_menu_left">
                <ul>
                    <li><a href="/trang-ca-nhan-8196231-nguyen-the-quynh.htm" class="block icon_tq active_tabU">Tổng
                            quan</a></li>
                    <li><a href="/users/home/user_control_doc.php?a=0&amp;use_id=8196231" class="block icon_qltl">Quản
                            lý tài liệu</a></li>
                    <li><a href="/thong-tin-tai-khoan.htm?use_id=8196231" class="block icon_qltc">Quản lý tài chính</a>
                    </li>
                    <li><a href="/thay-doi-thong-tin-ca-nhan.htm" class="block icon_ttcn">Thông tin cá nhân</a></li>
                    <li><a href="/thong-tin-tai-khoan/thong-ke-doanh-thu.htm?use_id=8196231" class="block icon_tk">Thống
                            kê</a></li>
                </ul>
            </div>
            <div class="user_top_txt"><span class="line_ngang_short"></span>
                <div class="user_top_stt"><span class="stt_txt stt_txt2 transition0-5" href="javascript://"><i
                            class="icon_nhay"></i>Bạn đang nghĩ gì?</span>
                    <div class="stt_form">
                        <form action="" name=""><textarea class="stt_input" rows="4"></textarea> <a href="javascript://"
                                                                                                    class="stt_edit"> <i
                                    class="stt_edits icon"></i>Sửa</a> <a href="javascript://" class="stt_delete"><i
                                    class="stt_deletes icon"></i>Xóa</a></form>
                    </div>
                    <a href="javascript:;" class="click_more" style="display: none;">...Xem thêm</a> <a
                        href="javascript:;" class="click_hide">Rút gọn</a></div>
                <div class="clear"></div>
            </div>
            <div class="ad_user_image hidden md:block"><i class="icon_image_h"></i></div>
        </div>
        <div class="ad_user_side_right col-span-12 md:col-span-9 p-2">
            <form id="formdoc">
                <div class="grid grid-cols-2 md:grid-cols-4 gap-2">
                    <div class="card-doc col-span-1 relative ">
                        <div class="card-doc-user-hover">
                            <!--<a class="doc_del_item icon deleteitem" href="javascript:;" bid="" >Xoá</a>--> <a
                                class="doc_edit_item icon" href="/document/edit-d9514549.htm">Sửa</a> <a
                                class="doc_add_item icon" href="javascript:;" uid="8196231" docid="9514549"
                                onclick="showAddCollection(this)">thêm</a></div>
                        <a class="card-doc-img" href="/document/9514549-fpt-ecovax-phu-luc-gems-vn.htm"
                           title="tài liệu 1"> <img
                                src="https://media.123dok.info/images/document/2021_10/14/medium_xfi1634207734.jpg"
                                alt="FPT ecovax   phụ lục   GEMS VN"
                                onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';">
                            <!--                            <img src="http://static.123doc.local/images/default/doc_normal.png" alt="-->
                            <!--" />--> <i class="icon i_type_doc i_type_doc3"></i> </a> <a class="card-doc-title"
                                                                                            href="/document/9514549-fpt-ecovax-phu-luc-gems-vn.htm">
                            FPT ecovax phụ lục GEMS VN </a>
                        <ul class="doc_tk_cnt">
                            <li><i class="icon_doc"></i>2 trang</li>
                            <li><i class="icon_view"></i>0</li>
                            <li><i class="icon_down"></i>0</li>
                        </ul>
                    </div>
                    <div class="card-doc col-span-1 relative ">
                        <div class="card-doc-user-hover">
                            <!--<a class="doc_del_item icon deleteitem" href="javascript:;" bid="" >Xoá</a>--> <a
                                class="doc_edit_item icon" href="/document/edit-d9210887.htm">Sửa</a> <a
                                class="doc_add_item icon" href="javascript:;" uid="8196231" docid="9210887"
                                onclick="showAddCollection(this)">thêm</a></div>
                        <a class="card-doc-img" href="/document/9210887-fix-loi-fix-bug-website.htm" title="tài liệu 1">
                            <img src="https://media.123dok.info/images/document/2021_09/11/medium_eif1631341862.jpg"
                                 alt="Fix lỗi, fix bug website"
                                 onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';">
                            <!--                            <img src="http://static.123doc.local/images/default/doc_normal.png" alt="-->
                            <!--" />--> <i class="icon i_type_doc i_type_doc3"></i> </a> <a class="card-doc-title"
                                                                                            href="/document/9210887-fix-loi-fix-bug-website.htm">
                            Fix lỗi, fix bug website </a>
                        <ul class="doc_tk_cnt">
                            <li><i class="icon_doc"></i>5 trang</li>
                            <li><i class="icon_view"></i>11</li>
                            <li><i class="icon_down"></i>0</li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script defer="">$(function () {
            var el = $('input.doc_check_del');
            var parent = $('input[name="checkoption"]');
            parent.change(function () {
                if (parent.is(':checked')) {
                    el.attr('checked', 'checked');
                } else {
                    el.removeAttr('checked');
                }
            }).change();
        });
        $(document).ready(function () {
            $(".deleteitem").click(function (event) {
                var _this = $(this);
                var _id = _this.attr('bid');
                if (!confirm('Bạn muốn xóa tài liệu đã chọn')) return;
                $.ajax({
                    url: '/users/ajax/aja_user_control.php',
                    type: 'POST',
                    data: {id: _id, type: 'delete'},
                    dataType: 'json',
                    success: function (data) {
                        var del_div = $('.doc_list_cnt');
                        if (data.code == 0) {
                            _this.closest(del_div).slideUp(400);
                        } else {
                            alert(data.message);
                        }
                    }
                });        /* Act on the event */
            });
        });

        function deleteitem() {
            var _form = $("#formdoc");
            var myform = _form.serializeArray();
            console.log(myform);
            if (myform.length < 1) {
                alert('Bạn chưa chọn tài liệu!');
                return;
            }
            if (!confirm('Bạn muốn xóa tài liệu đã chọn')) return;
            $.ajax({
                url: '/users/ajax/aja_user_control.php',
                type: 'POST',
                data: {form: myform, type: 'delete'},
                dataType: 'json',
                success: function (data) {
                    var del_div = $('.doc_list_cnt');
                    if (data.code == 0) {
                        _form.find('input[type=checkbox]:checked').closest(del_div).hide(400);
                    } else {
                        alert(data.message);
                    }
                }
            });
        }</script>
</div>