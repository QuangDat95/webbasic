<link rel="stylesheet" type="text/css"
      href="<?= URL ?>template/styles/user.css"/>
<div id="container">
    <div class="ad_user hidden md:block"></div>
    <div class="bg-white py-4">
        <div class="container mx-auto grid grid-cols-12 relative p-2 md:p-0">
            <div class="ad_user_avatar_small  md:absolute left-0 top-0  transform md:-translate-y-1/2 row-span-2"><img
                        class="" src="https://media.123dok.info/images/default/user_small.png" alt="avatar"></div>
            <div class="col-start-4 col-span-8 md:col-start-2 flex items-center px-4"><h3
                        class="ad_user_name text-xl md:text-2xl  mr-4">Nguyễn Thế Quỳnh</h3>                    <a
                        class="ad_user_messeger" href="/tin-nhan-8196231-nguyen-the-quynh.htm"> <i
                            class="icon_doc_mail filter"></i> Tin nhắn </a></div>
            <div class="ad_user_list_view md:col-start-10 md:col-span-3 col-start-4 col-span-8 px-4">
                <ul class="flex text-center">
                    <li class="pr-4"><span class="ad_user_title">Lượt xem</span> <span class="ad_user_number">10</span>
                    </li>
                    <li class="border-l border-r border-solid border-gray-400 px-4"><span
                                class="ad_user_title">Tài liệu</span> <span class="ad_user_number">2</span></li>
                    <li class="pl-4"><span class="ad_user_title">Lượt tải</span> <span class="ad_user_number">0</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mx-auto container  grid grid-cols-12 gap-2 "><h3 class="text-2xl col-start-4 col-span-9 px-2 my-4">Quản
            lý tài liệu</h3></div>
    <div class="container mx-auto  grid grid-cols-12 gap-2">
        <div class="ad_user_side_left col-span-12 md:col-span-3 px-2 md:pr-0">
            <div class="ad_user_menu_left">
                <ul>
                    <li><a href="<?=URL?>/user/home" class="block icon_tq">Tổng quan</a></li>
                    <li><a href="<?=URL?>/user/doc_control" class="block icon_qltl active_tabU">Quản lý tài liệu</a></li>
                    <li><a href="<?=URL?>/user/finance_control" class="block icon_qltc">Quản lý tài chính</a></li>
                    <li><a href="<?=URL?>/user/profile" class="block icon_ttcn">Thông tin cá nhân</a></li>
                </ul>
            </div>
            <div class="ad_user_image hidden md:block">
            <img src="<?= URL ?>/template/images/default/logo.png" width="200px" style="margin: 0 auto;" alt="">
            <!-- <i class="icon_image_h"></i> -->
            </div>
        </div>
        <div class="ad_user_side_right col-span-12 md:col-span-9 px-2">
            <div class="content_user_info">
                <div class="top_doc_type flex justify-between p-2 rounded mb-4 flex-col md:flex-row">
                    <ul class="flex flex-wrap mb-2 md:mb-0">
                        <li class="md:mr-4 w-1/2 md:w-auto active_man"><a for="man_radio"
                                                                          href="/users/home/user_control_doc.php?a=0&amp;use_id=8196231">
                                <i class="icon_type_radio"></i> <span>Được duyệt<em> (2)</em></span> </a></li>
                        <li class="md:mr-4 w-1/2 md:w-auto "><a for="man_radio"
                                                                href="/users/home/user_control_doc.php?a=6&amp;use_id=8196231">
                                <i class="icon_type_radio"></i> <span><i class="warn warning"></i>Thiếu thông tin<em> (2)</em></span>
                            </a></li>
                        <li class="md:mr-4 w-1/2 md:w-auto "><a for="man_radio"
                                                                href="/users/home/user_control_doc.php?a=1&amp;use_id=8196231">
                                <i class="icon_type_radio"></i> <span>Chờ duyệt<em> (2)</em></span> </a></li>
                        <li onclick="clickActiveAdvanced(this)" class=""><label for="man_radio4"> <input type="hidden"
                                                                                                         name="man_radio"
                                                                                                         id="man_radio4"
                                                                                                         class="man_radio">
                                <i class="icon_type_radio"></i> <span>Lọc nâng cao</span> </label></li>
                    </ul>
                    <div class="search_md transition1">
                        <form id="user_find_doc" name="search_md" method="GET"
                              action="/users/home/user_control_doc.php?a=0&amp;use_id=8196231"><input type="hidden"
                                                                                                      name="use_id"
                                                                                                      value="8196231">
                            <input type="hidden" name="a" value="0"> <input placeholder="Tìm kiếm..." value="" name="q">
                            <a href="javascript:;" onclick="document.getElementById('user_find_doc').submit();"
                               class="icon_search_md icon"></a></form>
                    </div>
                </div>
                <div class="doc_manager_filter hidden">
                    <form id="user_find_doc_advanced" name="search_md" method="GET"
                          action="/users/home/user_control_doc.php?a=0&amp;use_id=8196231"><input type="hidden"
                                                                                                  name="use_id"
                                                                                                  value="8196231">
                        <input type="hidden" name="s" value="0"> <input type="hidden" name="fa" value="0"> <input
                                type="hidden" name="fd" value="0"> <input type="hidden" name="uadv" value="1">
                        <div class="mn_list_document"><h4>Duyệt tài liệu</h4>
                            <label onclick="controlDocCheck(this)" for="all_mn_test"> <input type="radio"
                                                                                             class="class_fa" name="fa"
                                                                                             checked="checked"
                                                                                             value="0"> Được duyệt
                                (<span>2</span>) </label> <label onclick="controlDocCheck(this)" for="all_mn_test">
                                <input type="radio" class="class_fa" name="fa" value="1"> Chờ duyệt (<span>2</span>)
                            </label> <label onclick="controlDocCheck(this)" for="all_mn_test"> <input type="radio"
                                                                                                      class="class_fa"
                                                                                                      name="fa"
                                                                                                      value="2"> Cập
                                nhật để duyệt lại (<span>0</span>) </label> <label onclick="controlDocCheck(this)"
                                                                                   for="all_mn_test"> <input
                                        type="radio" class="class_fa" name="fa" value="3"> Tài liệu trùng
                                (<span>0</span>) </label> <label onclick="controlDocCheck(this)" for="all_mn_test">
                                <input type="radio" class="class_fa" name="fa" value="4"> Vi phạm quy định 123doc
                                (<span>0</span>) </label> <label onclick="controlDocCheck(this)" for="all_mn_test">
                                <input type="radio" class="class_fa" name="fa" value="6"> <i class="warn warning"></i>Thiếu
                                thông tin (<span>2</span>) </label></div>
                        <div class="mn_list_document"><h4>Đặc tính tài liệu</h4>
                            <label onclick="controlDocCheck(this)" for="mn_attr_all"> <input type="radio" name="fd"
                                                                                             checked="checked"
                                                                                             value="0"> Tất cả </label>
                            <label onclick="controlDocCheck(this)" for="mn_attr_all"> <input type="radio" name="fd"
                                                                                             value="1"> Có giá bán (0)
                            </label> <label onclick="controlDocCheck(this)" for="mn_attr_all"> <input type="radio"
                                                                                                      name="fd"
                                                                                                      value="2"> Miễn
                                phí (2) </label></div>
                        <div class="mn_list_document"><h4>Sắp xếp</h4>
                            <label onclick="controlDocCheck(this)" for="mn_attr_all"> <input type="radio" name="s"
                                                                                             checked="checked"
                                                                                             value="0"> Mới đăng
                            </label> <label onclick="controlDocCheck(this)" for="mn_attr_all"> <input type="radio"
                                                                                                      name="s"
                                                                                                      value="1"> Xem
                                nhiều </label> <label onclick="controlDocCheck(this)" for="mn_attr_all"> <input
                                        type="radio" name="s" value="2"> Xem ít </label> <label
                                    onclick="controlDocCheck(this)" for="mn_attr_all"> <input type="radio" name="s"
                                                                                              value="3"> Tải nhiều
                            </label> <label onclick="controlDocCheck(this)" for="mn_attr_all"> <input type="radio"
                                                                                                      name="s"
                                                                                                      value="4"> Tải ít
                            </label></div>
                        <div class="mn_list_document_last"><h4>Nhập giá bán</h4>                        <input
                                    type="text" name="doc_price" class="check_edit_price" value=""
                                    placeholder="Nhập giá và chọn áp dụng">
                            <p>Ví dụ: 5000 hoặc 2000-5000</p></div>
                        <div class="clear"></div>
                        <div class="line_ngang"></div>
                        <div class="btn_action"><a href="javascript:;"
                                                   onclick="document.getElementById('user_find_doc_advanced').submit();"
                                                   class="mn_send_result">Áp dụng</a></div>
                        <div class="clear"></div>
                    </form>
                </div>
                <div class="mn_doc_upload"></div>
                <div class="mn_list_view_more">
                    <div class="list_doc_man">
                        <div class="doc_list_cnt list_cnt_small list div_del mb-4 grid grid-cols-12"><input
                                    type="checkbox" class="edit_doc ck_man_doc_t" name="edit_doc[]" value="9514549"> <a
                                    class="doc_cnt_img col-span-4 md:col-span-2 border border-solid border-gray-400 -mt-2 ml-6 md:mx-auto"
                                    href="/document/9514549-fpt-ecovax-phu-luc-gems-vn.htm"
                                    title="FPT ecovax   phụ lục   GEMS VN"> <img
                                        src="https://media.123dok.info/images/document/2021_10/14/medium_xfi1634207734.jpg"
                                        alt="FPT ecovax   phụ lục   GEMS VN"
                                        onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';">
                                <!--                                <img src="http://static.123doc.local/images/default/doc_normal.png" alt="-->
                                <!--" />-->                            </a>
                            <div class="col-span-8 md:col-span-7 -mt-2"><a class="block pr-2"
                                                                           title="FPT ecovax   phụ lục   GEMS VN"
                                                                           href="/document/9514549-fpt-ecovax-phu-luc-gems-vn.htm">
                                    <h4 class="doc_title_cnt text-base md:text-2xl"> FPT ecovax phụ lục GEMS VN</h4></a>
                                <span class="doc_man_date static md:absolute top-0">Ngày tải lên :14/10/2021, 17:35 </span>
                                <ul class="nav_doc_man flex items-center">
                                    <li class="pr-2"><a href="/doc-cat/35-kinh-doanh-tiep-thi.htm">Kinh Doanh - Tiếp
                                            Thị</a> <i class="icon_nav_ctn ml-2"></i></li>
                                    <li class="pr-2"><a href="/doc-cat/36-internet-marketing.htm">Internet Marketing</a>
                                        <i class="icon_nav_ctn ml-2"></i></li>
                                </ul>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>2</li>
                                    <li><i class="icon_view"></i>0</li>
                                    <li><i class="icon_down"></i>0</li>
                                    <li><span class="cl_price">Miễn phí</span></li>
                                </ul>
                                <div class="doc_man_hover">
                                    <div class="flex"><a class="man_doc_del icon deleteitem pl-8 block w-20"
                                                         bid="9514549" href="javascript:;"><span>Xoá</span></a> <a
                                                class="man_doc_edit icon block w-20 pl-8"
                                                href="/document/edit-d9514549.htm"><span>Sửa</span></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="doc_list_cnt list_cnt_small list div_del mb-4 grid grid-cols-12"><input
                                    type="checkbox" class="edit_doc ck_man_doc_t" name="edit_doc[]" value="9210887"> <a
                                    class="doc_cnt_img col-span-4 md:col-span-2 border border-solid border-gray-400 -mt-2 ml-6 md:mx-auto"
                                    href="/document/9210887-fix-loi-fix-bug-website.htm"
                                    title="Fix lỗi, fix bug website"> <img
                                        src="https://media.123dok.info/images/document/2021_09/11/medium_eif1631341862.jpg"
                                        alt="Fix lỗi, fix bug website"
                                        onerror="this.src='https://media.123dok.info/images/default/doc_normal.png';">
                                <!--                                <img src="http://static.123doc.local/images/default/doc_normal.png" alt="-->
                                <!--" />-->                            </a>
                            <div class="col-span-8 md:col-span-7 -mt-2"><a class="block pr-2"
                                                                           title="Fix lỗi, fix bug website"
                                                                           href="/document/9210887-fix-loi-fix-bug-website.htm">
                                    <h4 class="doc_title_cnt text-base md:text-2xl"> Fix lỗi, fix bug website</h4></a>
                                <span class="doc_man_date static md:absolute top-0">Ngày tải lên :11/09/2021, 13:31 </span>
                                <ul class="nav_doc_man flex items-center">
                                    <li class="pr-2"><a href="/doc-cat/62-cong-nghe-thong-tin.htm">Công Nghệ Thông
                                            Tin</a> <i class="icon_nav_ctn ml-2"></i></li>
                                    <li class="pr-2"><a href="/doc-cat/68-ky-thuat-lap-trinh.htm">Kỹ thuật lập trình</a>
                                        <i class="icon_nav_ctn ml-2"></i></li>
                                </ul>
                                <ul class="doc_tk_cnt">
                                    <li><i class="icon_doc"></i>5</li>
                                    <li><i class="icon_view"></i>11</li>
                                    <li><i class="icon_down"></i>0</li>
                                    <li><span class="cl_price">Miễn phí</span></li>
                                </ul>
                                <div class="doc_man_hover">
                                    <div class="flex"><a class="man_doc_del icon deleteitem pl-8 block w-20"
                                                         bid="9210887" href="javascript:;"><span>Xoá</span></a> <a
                                                class="man_doc_edit icon block w-20 pl-8"
                                                href="/document/edit-d9210887.htm"><span>Sửa</span></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="paging"><a class="pagging_active">1</a> <a
                                    href="/users/home/user_control_doc.php?a=0&amp;use_id=8196231&amp;page=2"
                                    class="pagging_normal">2</a> <a
                                    href="/users/home/user_control_doc.php?a=0&amp;use_id=8196231&amp;page=3"
                                    class="pagging_normal">3</a> <a
                                    href="/users/home/user_control_doc.php?a=0&amp;use_id=8196231&amp;page=4"
                                    class="pagging_normal">4</a> <a
                                    href="/users/home/user_control_doc.php?a=0&amp;use_id=8196231&amp;page=5"
                                    class="pagging_normal">..</a> <a
                                    href="/users/home/user_control_doc.php?a=0&amp;use_id=8196231&amp;page=5"
                                    class="pagging_normal">&gt;</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="edit_pri_doc py-2 hidden">
        <div class="cnt_edit_doc text-center"><p><input type="checkbox" id="id_ck" class="ck_edit"
                                                        data-child="edit_doc"> <label for="id_ck">Chọn tất cả</label>
                <span>                Chọn tất cả sẽ sửa giá toàn bộ tài liệu            </span></p>        <a
                    class="man_doc_edit icon" onclick="showEditPrice()" href="javascript:;">Sửa giá bán</a></div>
    </div>
</div>