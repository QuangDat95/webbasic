<link rel="stylesheet" type="text/css" href="<?= URL ?>template/styles/user.css" />
<div id="container">
    <div class="ad_user hidden md:block"></div>
    <div class="bg-white py-4">
        <div class="container mx-auto grid grid-cols-12 relative p-2 md:p-0">
            <div class="ad_user_avatar_small  md:absolute left-0 top-0  transform md:-translate-y-1/2 row-span-2"><img class="" src="https://media.123dok.info/images/default/user_small.png" alt="avatar"></div>
            <div class="col-start-4 col-span-8 md:col-start-2 flex items-center px-4">
                <h3 class="ad_user_name text-xl md:text-2xl  mr-4">Nguyễn Thế Quỳnh</h3> <a class="ad_user_messeger" href="/tin-nhan-8196231-nguyen-the-quynh.htm"> <i class="icon_doc_mail filter"></i> Tin nhắn </a>
            </div>
            <div class="ad_user_list_view md:col-start-10 md:col-span-3 col-start-4 col-span-8 px-4">
                <ul class="flex text-center">
                    <li class="pr-4"><span class="ad_user_title">Lượt xem</span> <span class="ad_user_number">10</span>
                    </li>
                    <li class="border-l border-r border-solid border-gray-400 px-4"><span class="ad_user_title">Tài liệu</span> <span class="ad_user_number">2</span></li>
                    <li class="pl-4"><span class="ad_user_title">Lượt tải</span> <span class="ad_user_number">0</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mx-auto container  grid grid-cols-12 gap-2 ">
        <h3 class="text-2xl col-start-4 col-span-9 px-2 my-4">Quản
            lý tài chính</h3>
    </div>
    <div class="container mx-auto  grid grid-cols-12 gap-2">
        <div class="ad_user_side_left col-span-12 md:col-span-3 px-2 md:pr-0">
            <div class="ad_user_menu_left">
                <ul>
                    <li><a href="<?= URL ?>/user/home" class="block icon_tq">Tổng quan</a></li>
                    <li><a href="<?= URL ?>/user/doc_control" class="block icon_qltl">Quản lý tài liệu</a></li>
                    <li><a href="<?= URL ?>/user/finance_control" class="block icon_qltc active_tabU">Quản lý tài chính</a></li>
                    <li><a href="<?= URL ?>/user/profile" class="block icon_ttcn">Thông tin cá nhân</a></li>
                </ul>
            </div>
            <div class="ad_user_image hidden md:block">
            <img src="<?= URL ?>/template/images/default/logo.png" width="200px" style="margin: 0 auto;" alt="">
            <!-- <i class="icon_image_h"></i> -->
            </div>
        </div>
        <div class="ad_user_side_right col-span-12 md:col-span-9 px-2">
            <div class="content_user_info bg_none">
                <div class="grid grid-cols-12 gap-2">
                    <div class="account_balance col-span-12 md:col-span-12">
                        <h4>Thông tin số dư tài khoản</h4>
                        <p class="title_bal">Số dư tài khoản doanh thu</p>
                        <p><span class="number_money_bal">0đ</span> <a href="javascript:;" onclick="showTransfers(this)" class="send_bal">Chuyển tiền</a></p>
                        <p class="title_bal">Số dư tài khoản mua tài liệu <a class="icon_question icon"> <span class="title_bal_hidden">Số tiền dùng để mua tài liệu trên 123doc<em class="i_square_error"></em></span> </a></p>
                        <p><span class="number_money_bal">0đ</span><a href="javascript:;" onclick="popupPayment_open();" class="send_bal">Nạp tiền</a></p>
                        <p class="title_bal">Số dư RFD tạm tính</p>
                        <p><span class="number_money_bal">0đ</span></p>
                        <p class="title_bal">Số dư thưởng UPLOAD tạm tính tháng 10</p>
                        <p><span class="number_money_bal">0đ</span></p>
                    </div>
                    <!-- <div class="config_withdrawal col-span-12 md:col-span-6">
                        <h4>Cấu hình rút tiền</h4>
                        <p class="title_bal edit_pal">Rút tiền về tài khoản: <span>quynh@vdata.com.vn</span></p>
                        <p class="title_bal edit_pal">Nhà cung cấp: <span>BAOKIM.VN</span></p>
                        <p class="title_bal">Đặt lệnh rút tiền tháng 10/2021</p>
                        <form method="POST" action="" id="smt_money">
                            <div class="box_smt_money">
                                <p class="bal_ck_option"><input type="radio" name="ck_money" id="ck_money_2" checked="checked" value="not_checked" class="active_blue"><label for="ck_money_2">Rút toàn bộ doanh thu</label> <input type="radio" name="ck_money" id="ck_money_1" value="checked"><label for="ck_money_1">Rút theo tuỳ chọn</label></p>
                                <p><input type="hidden" name="money_auto" value="0"> <input type="text" name="input_pal" class="input_pal" placeholder="Nhập số tiền muốn rút" style="display: none;"></p>
                            </div>
                            <input type="hidden" name="order" value="order">
                            <button name="order" class="smt_edit_info">Lập lệnh</button>
                        </form>
                        <p><span class="title_bal">Lịch sử đặt lệnh rút tiền</span></p>
                        <div class="box_history_money">
                            <ul class="h-12 px-3 text-sm scrollbar-thin scrollbar-thumb-gray-500 scrollbar-track-gray-400 scrollbar-thumb-rounded-full scrollbar-track-rounded-full"></ul>
                        </div>
                        <p></p>
                        <p class="note_pal"><strong>Ghi chú:</strong>Lệnh rút tiền được duyệt từ ngày 1-5 của tháng tiếp
                            theo. </p>
                    </div> -->
                </div>
                <div class="pal_history_surplus" id="list_history_finance">
                    <h4>Lịch sử biến động số dư</h4>
                    <ul class="tab_pal_his">
                        <li class="active_pal px-2" data-tab="#act_finance"><a href="javascript:;">Tài khoản doanh
                                thu</a></li>
                        <li data-tab="#act_buy" class="px-2"><a href="javascript:;">Tài khoản mua tài liệu</a></li>
                    </ul>
                    <div id="act_finance" class="content_act">
                        <div class="pal_top_search">
                            <form id="fil_statistic" method="GET" action="/thong-tin-tai-khoan.htm?use_id=8196231" class="flex mt-4 flex-wrap justify-between"><input type="hidden" name="use_id" value="8196231"> <input type="text" class="code_deal" name="cod" value="" placeholder="Mã giao dịch">
                                <div class="doc_file_deal fil_date_picker_range border border-solid border-gray-400 rounded" id="reportrange"><span>10/01/2021 - 10/31/2021</span> <input type="hidden" id="pal_date_range" name="dateRange" value=""> <i class="icon_date_picker_range"></i></div>
                                <select name="typemoney" class="sl_tkdt mt-2 md:m-0">
                                    <option>Tất cả</option>
                                    <option value="0">Tất cả</option>
                                    <option value="3">Bán tài liệu</option>
                                    <option value="2">Share tài liệu</option>
                                    <option value="1">Xem tài liệu</option>
                                    <option value="5">Đại lý tài liệu</option>
                                    <option value="4">Đăng tài liệu</option>
                                    <option value="7">Thưởng sự kiện</option>
                                    <option value="10">RFD</option>
                                    <option value="9">Giảm trừ</option>
                                    <option value="8">Rút tiền</option>
                                    <option value="12">Chuyển tiền mua tài liệu</option>
                                </select>
                                <div onclick="statisticSubmitFil(this);" class="smt_edit_info mt-2 md:m-0">Tìm kiếm
                                </div>
                            </form>
                        </div>
                        <div class="line_ngang"></div>
                        <div class="content_account_evenue overflow-x-scroll md:overflow-x-visible">
                            <div class="top_title_evenue">
                                <div class="doc_file_change w-full md:w-1/4">0-20<i class="icon_down_t"></i>
                                    <ul class="transition2">
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=1&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-20</a>
                                        </li>
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=2&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-40</a>
                                        </li>
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=3&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-60</a>
                                        </li>
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=4&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-80</a>
                                        </li>
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=5&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-100</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <table class="tb_tk_evenue">
                                <thead class="title_tk_eve">
                                    <tr>
                                        <td width="16%">Mã giao dịch</td>
                                        <td width="14%">Loại giao dịch</td>
                                        <td>Tên giao dịch</td>
                                        <td width="12%">Trang thái</td>
                                        <td width="12%">Số tiền</td>
                                        <td width="10%">Số dư tích luỹ</td>
                                    </tr>
                                </thead>
                            </table>
                            <div class="top_title_evenue">
                                <div class="doc_file_change">0-20<i class="icon_down_t"></i>
                                    <ul class="transition2">
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=1&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-20</a>
                                        </li>
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=2&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-40</a>
                                        </li>
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=3&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-60</a>
                                        </li>
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=4&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-80</a>
                                        </li>
                                        <li>
                                            <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=5&amp;dateRange=&amp;cod=&amp;page=1&amp;typemoney=0" data-pjax="#list_history_finance">0-100</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="paging"><a class="pagging_active">1</a> <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=1&amp;dateRange=thisMonth&amp;cod=&amp;page=1&amp;typemoney=0&amp;page=2" class="pagging_normal">2</a> <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=1&amp;dateRange=thisMonth&amp;cod=&amp;page=1&amp;typemoney=0&amp;page=3" class="pagging_normal">3</a> <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=1&amp;dateRange=thisMonth&amp;cod=&amp;page=1&amp;typemoney=0&amp;page=4" class="pagging_normal">4</a> <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=1&amp;dateRange=thisMonth&amp;cod=&amp;page=1&amp;typemoney=0&amp;page=5" class="pagging_normal">..</a> <a href="/thong-tin-tai-khoan.htm?use_id=8196231&amp;l=1&amp;dateRange=thisMonth&amp;cod=&amp;page=1&amp;typemoney=0&amp;page=5" class="pagging_normal">&gt;</a></div>
                    </div>
                    <div id="act_buy" class="content_act hide overflow-x-scroll">
                        <table class="tb_tk_evenue">
                            <thead class="title_tk_eve">
                                <tr>
                                    <td width="16%">Mã giao dịch</td>
                                    <td width="14%">Loại giao dịch</td>
                                    <td>Tên giao dịch</td>
                                    <td width="12%">Trang thái</td>
                                    <td width="12%">Số tiền</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="act_rfd" class="content_act hide"></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <script defer="">
        $(document).ready(function() {
            $('.input_pal').hide();
            $('#ck_money_1').change(function() {
                if ($(this).is(':checked')) {
                    $('.input_pal').slideDown(300);
                }
            });
            $('#ck_money_2').change(function() {
                if ($(this).is(':checked')) {
                    $('.input_pal').slideUp(300);
                }
            });
            $('button[name="order"]').click(function(event) {
                event.preventDefault();
                openPopup('/global/popup/notification_success.php?type=0&title=Thông%20báo&reload=0&msg=', 600, '', true, false, false, function() {
                    var html = '<p>' + 'Từ ngày <b>01/09/2021</b> với mỗi lệnh rút tiền trên 123doc, BQT sẽ thu thêm <b class="text-red-500">10% thuế</b>. ' + 'Thay đổi này đc áp dụng theo <b>Mục (đ) khoản 1 điều 8 Thông tư 40/2021/TT-BTC</b> của Bộ Tài chính ban hành ngày <b>01/08/2021</b>.<br><br>' + '<span class="text-sm">Chi tiết xem tại: <a target="_blank" href="https://123docz.net/statics-detail/941-thong-bao-v-v-khau-tru-thue-tren-doanh-thu-123doc.htm" class="text-primary">Thông báo v/v khấu trừ thuế trên doanh thu 123doc</a></span>' + '</p>';
                    $('#msg-popup').parent().addClass('px-10');
                    $('#msg-popup').parent().removeClass('mt-3 mb-5');
                    $('#msg-popup').removeClass('text-2xl').addClass('text-base');
                    $('#msg-popup').html(html);
                    $('#notify_close_btn').text('Tiếp tục');
                    $('#notify_close_btn').click(function() {
                        $('#smt_money').submit();
                    });
                });
            });
        })
    </script>
</div>