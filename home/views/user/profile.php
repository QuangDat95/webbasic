<link rel="stylesheet" type="text/css" href="<?= URL ?>template/styles/user.css" />
<div id="container">
    <div class="ad_user hidden md:block"></div>
    <div class="bg-white py-4">
        <div class="container mx-auto grid grid-cols-12 relative p-2 md:p-0">
            <div class="ad_user_avatar_small  md:absolute left-0 top-0  transform md:-translate-y-1/2 row-span-2"><img class="" src="https://media.123dok.info/images/default/user_small.png" alt="avatar"></div>
            <div class="col-start-4 col-span-8 md:col-start-2 flex items-center px-4">
                <h3 class="ad_user_name text-xl md:text-2xl  mr-4"><?= $_SESSION['customer']['name'] ?></h3> <a class="ad_user_messeger" href="/tin-nhan-8196231-nguyen-the-quynh.htm"> <i class="icon_doc_mail filter"></i> Tin nhắn </a>
            </div>
            <div class="ad_user_list_view md:col-start-10 md:col-span-3 col-start-4 col-span-8 px-4">
                <ul class="flex text-center">
                    <li class="pr-4"><span class="ad_user_title">Lượt xem</span> <span class="ad_user_number">10</span>
                    </li>
                    <li class="border-l border-r border-solid border-gray-400 px-4"><span class="ad_user_title">Tài liệu</span> <span class="ad_user_number">2</span></li>
                    <li class="pl-4"><span class="ad_user_title">Lượt tải</span> <span class="ad_user_number">0</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mx-auto container  grid grid-cols-12 gap-2 ">
        <h3 class="text-2xl col-start-4 col-span-9 px-2 my-4">Thông tin cá nhân</h3>
    </div>
    <div class="container mx-auto  grid grid-cols-12 gap-2">
        <div class="ad_user_side_left col-span-12 md:col-span-3 px-2 md:pr-0">
            <div class="ad_user_menu_left">
                <ul>
                    <li><a href="<?= URL ?>/user/home" class="block icon_tq">Tổng quan</a></li>
                    <li><a href="<?= URL ?>/user/doc_control" class="block icon_qltl">Quản lý tài liệu</a></li>
                    <li><a href="<?= URL ?>/user/finance_control" class="block icon_qltc">Quản lý tài chính</a></li>
                    <li><a href="<?= URL ?>/user/profile" class="block icon_ttcn active_tabU">Thông tin cá nhân</a></li>
                    <!--                    <li><a href="/thong-tin-tai-khoan/thong-ke-doanh-thu.htm?use_id=8196231" class="block icon_tk">Thống-->
                    <!--                            kê</a></li>-->
                </ul>
            </div>
            <div class="ad_user_image hidden md:block">
                <img src="<?= URL ?>/template/images/default/logo.png" width="200px" style="margin: 0 auto;" alt="">
                <!-- <i class="icon_image_h"></i> -->
            </div>
        </div>
        <div class="ad_user_side_right col-span-12 md:col-span-9 px-2">
            <div class="content_user_info">
                <div class="info_user_cnt p-4">
                    <div class="edit_from_user">
                        <h4 class="title_user_info"> tên hiện thị <a href="javascript:;" class="icon_edit_title filter icon" onclick="edit_user(this); return">Sửa</a>
                        </h4>
                        <div class="user_result">
                            <h3 class="ad_user_name edit_name"><?= $_SESSION['customer']['name'] ?></h3>
                        </div>
                        <div class="user_no_result">
                            <form method="POST" name="fr_name" class="fr_name" action=""><span class="ip_name_edit"> <input name="txtUsername" value="<?= $_SESSION['customer']['name'] ?>" class="edit_ip_name" onkeydown="CountLeft(this.form.txtUsername,this.form.cmt_disabled,0)" onkeyup="CountLeft(this.form.txtUsername,this.form.cmt_disabled,0)"> <em><input disabled="disabled" name="cmt_disabled" class="number_cmt">/30</em> </span>
                                <div class="mt-4">
                                    <button type="submit" name="frm_name" onclick="return Checklenght()" class="smt_edit_info" value="frm_name">Lưu
                                    </button>
                                    <a href="javascript:;" class="smt_exit" onclick="exit_user(this); return">Huỷ</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="line_array"></div>
                    <!-- <div class="edit_from_user">
                        <h4 class="title_user_info"> thông tin <a href="javascript:;" class="icon_edit_info filter icon" onclick="edit_user(this); return">Sửa</a>
                        </h4>
                        <div class="user_result">
                            <p><span class="edit_info_left">Ngày sinh</span><span class="equal_info_right">1/01/1970</span></p>
                            <p><span class="edit_info_left">Giới tính</span><span class="equal_info_right">Khác</span>
                            </p>
                            <p><span class="edit_info_left">Địa chỉ</span><span class="equal_info_right"></span></p>
                        </div>
                        <div class="user_no_result">
                            <form action="" method="POST" enctype="multipart/form-data">
                                <p><span class="edit_info_left">Ngày sinh</span>
                                    <span class="equal_info_right"> <input type="text" name="user_date_edit" class="user_date_edit hasDatepicker" value="1/01/1970" id="dp1634810899126"> </span>
                                </p>
                                <p><span class="edit_info_left">Giới tính</span> <span class="equal_info_right"> <select class="user_sex_edit" name="rGender">
                                            <option value="0">Khác</option>
                                            <option value="0">Khác</option>
                                            <option value="1">Nam</option>
                                            <option value="2">Nữ</option>
                                        </select> </span>
                                </p>
                                <p><span class="edit_info_left">Địa chỉ</span> <span class="equal_info_right"> <input type="text" name="txtAdd" id="txtAdd" value="" class="user_address_edit" placeholder="Ex: Tp Ha Noi"> </span></p>
                                <div class="mt-2">
                                    <button type="submit" name="smt_name" class="smt_edit_info smt_info" value="smt_name">Lưu
                                    </button>
                                    <a href="javascript:;" class="smt_exit smt_exit_right" onclick="exit_user(this); return">Huỷ</a>
                                </div>
                            </form>
                        </div>
                    </div> -->
                    <div class="edit_from_user">
                        <h4 class="title_user_info"> Thông tin<a href="javascript:;" class="icon_edit_title filter icon" onclick="edit_user(this); return">Sửa</a></h4>
                        <div class="user_result">
                            <!-- <p><span class="edit_info_left">Họ và tên</span><span class="equal_info_right">Nguyễn Thế Quỳnh</span></p> -->
                            <p><span class="edit_info_left">Ngày sinh</span><span class="equal_info_right"><?= date('d/m/Y', strtotime($_SESSION['customer']['birthday'])) ?></span></p>
                            <p><span class="edit_info_left">Giới tính</span><span class="equal_info_right"><?php if($_SESSION['customer']['sex'] == 1) echo 'Nam'; else if($_SESSION['customer']['sex'] == 2) echo 'Nữ'; else echo 'Khác'; ?></span></p>
                            <p><span class="edit_info_left">Số CMND/CCCD</span><span class="equal_info_right"><?= isset($_SESSION['customer']['cccd']) ? $_SESSION['customer']['cccd'] : '---' ?></span>
                            </p>
                            <p><span class="edit_info_left">Địa chỉ thường trú</span><span class="equal_info_right"><?= isset($_SESSION['customer']['address']) ? $_SESSION['customer']['address'] : '---' ?></span></p>
                            <p><span class="edit_info_left">Số điện thoại</span><span class="equal_info_right"><?= isset($_SESSION['customer']['phone']) ? $_SESSION['customer']['phone'] : '---' ?></span></p>
                            <p><span class="edit_info_left">Email</span><span class="equal_info_right"><?= isset($_SESSION['customer']['email']) ? $_SESSION['customer']['email'] : '---' ?></span>
                            </p>
                        </div>
                        <div class="user_no_result">
                            <form action="" name="user_edit_pay" id="tax_declarations_form" method="post">
                                <!-- <p><span class="edit_info_left">Họ và tên</span> <span class="equal_info_right"> <input type="text" name="tax_fullname" value="Nguyễn Thế Quỳnh" class="user_address_edit"> </span></p> -->
                                <p><span class="edit_info_left">Ngày sinh</span> <span class="equal_info_right"> <input type="text" name="tax_birthday" class="user_date_edit hasDatepicker" value="<?= date('d/m/Y', strtotime($_SESSION['customer']['birthday'])) ?>" id="dp1634810899127"  placeholder="Ex: dd/mm/yyyy"> </span>
                                </p>
                                <p><span class="edit_info_left">Giới tính</span> <span class="equal_info_right"> <select class="user_sex_edit" name="tax_gender">
                                            <option value="">Chọn giới tính</option>
                                            <option value="0" <?= $_SESSION['customer']['sex'] == 0 ? 'selected' : '' ?>>Khác</option>
                                            <option value="1" <?= $_SESSION['customer']['sex'] == 1 ? 'selected' : '' ?>>Nam</option>
                                            <option value="2" <?= $_SESSION['customer']['sex'] == 2 ? 'selected' : '' ?>>Nữ</option>
                                        </select> </span>
                                </p>
                                <p><span class="edit_info_left">Số CMND/CCCD</span> <span class="equal_info_right"> <input type="text" name="tax_cmnd" value="" class="user_address_edit" placeholder="Ex: 03xxxxxxx658" value="<?= $_SESSION['customer']['cccd'] ?>"> </span>
                                </p>
                                <p><span class="edit_info_left">Địa chỉ thường trú</span> <span class="equal_info_right"> <input type="text" name="tax_address" value="" class="user_address_edit" placeholder="Ex: Số 2, Vương Thừa Vũ, Khương Trung, Thanh Xuân, Hà Nội" value="<?= $_SESSION['customer']['address'] ?>"> </span>
                                </p>
                                <p><span class="edit_info_left">Số điện thoại</span> <span class="equal_info_right"> <input type="text" name="tax_phone" value="" class="user_address_edit" placeholder="Ex: 03xxxxxxxx" value="<?= $_SESSION['customer']['phone'] ?>"> </span></p>
                                <p><span class="edit_info_left">Email</span> <span class="equal_info_right"> <input type="text" name="tax_email" value="<?= $_SESSION['customer']['email'] ?>" class="user_address_edit" placeholder="Ex: example@gmail.com"> </span></p>
                                <div class="flex mt-2">
                                    <div class="edit_info_left hidden md:block"></div>
                                    <div class="equal_info_right"><input type="hidden" name="smt_name" value="form_tax_declarations">
                                        <button type="submit" class="smt_edit_info smt_info" id="change_tax_declaration_btn">Lưu
                                        </button>
                                        <a href="javascript:;" class="bg-secondary inline-block px-5 rounded smt_edit_info text-white" onclick="exit_user(this); return">Huỷ</a>
                                    </div>
                                </div>
                            </form>
                            <div id="table-history-service"></div>
                        </div>
                    </div>
                    <div class="line_array"></div>
                    <div class="edit_from_user">
                        <h4 class="title_user_info"> Mật khẩu <a href="javascript:;" class="icon_edit_title filter icon" onclick="edit_user(this); return">Sửa</a>
                        </h4>
                        <div class="user_no_result">
                            <form action="" name="user_edit_password" method="post">
                                <p><span class="edit_info_left">Mật khẩu hiện tại</span>
                                    <span class="equal_info_right"> <input type="password" class="user_edit_password" name="old_password" placeholder="****************"> </span>
                                </p>
                                <p><span class="edit_info_left">Mật khẩu mới</span> <span class="equal_info_right"> <input type="password" class="user_edit_password" name="new_password" placeholder="****************"> </span>
                                </p>
                                <p><span class="edit_info_left">Nhập lại Mật khẩu mới</span> <span class="equal_info_right"> <input type="password" class="user_edit_password" name="re_new_password" placeholder="****************"> </span>
                                </p>
                                <button type="submit" name="smt_name" class="smt_edit_info smt_info" value="form_password">Lưu
                                </button>
                                <a href="javascript:;" class="smt_exit smt_exit_right" onclick="exit_user(this); return">Huỷ</a>
                            </form>
                        </div>
                    </div>
                    <!-- <div class="line_array"></div> -->
                    <!-- <div class="edit_from_user">
                        <h4 class="title_user_info"> Thông tin tài khoản rút tiền <a href="javascript:;" class="icon_edit_title filter icon" onclick="edit_user(this); return">Sửa</a></h4>
                        <div class="user_result">
                            <p><span class="edit_info_left">Nhà cung cấp dịch vụ</span><span class="equal_info_right">BAOKIM.VN</span></p>
                            <p><span class="edit_info_left">Email tài khoản</span><span class="equal_info_right">quynh@vdata.com.vn</span>
                            </p>
                        </div>
                        <div class="user_no_result">
                            <form action="" name="user_edit_pay" id="change_withdraw_service_form" method="post">
                                <p><span class="edit_info_left">Nhà cung cấp dịch vụ</span> <span class="equal_info_right"> <select class="user_edit_network" name="services">
                                            <option value="" disabled="">Chọn nhà cung cấp dịch vụ</option>
                                            <option value="0" checked="">BAOKIM.VN</option>
                                        </select> </span>
                                </p>
                                <p><span class="edit_info_left">Email tài khoản</span> <span class="equal_info_right"> <input type="text" class="user_edit_email" name="user_withdraw_email" placeholder="Ex: email@example.com"> </span>
                                </p>
                                <p><span class="edit_info_left">Mật khẩu <em style="color: red; font-size: .7em;">tài khoản 123doc</em></span>
                                    <span class="equal_info_right"> <input type="password" class="user_edit_password" name="password" placeholder="****************"> </span>
                                </p>
                                <div class="flex">
                                    <div class="edit_info_left hidden md:block"></div>
                                    <div class="equal_info_right">
                                        <div class="g-recaptcha" id="g-recaptcha-withdraw">
                                            <div style="width: 304px; height: 78px;">
                                                <div>
                                                    <iframe title="reCAPTCHA" src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LdWCt4ZAAAAAJqD3A-EGGCraFAQCoUSLUoTNkhf&amp;co=aHR0cHM6Ly8xMjNkb2N6Lm5ldDo0NDM.&amp;hl=vi&amp;v=YhkYx1k-yvvb8OonJPmOpoJY&amp;size=normal&amp;cb=mhm2nc2r9q5e" width="304" height="78" role="presentation" name="a-zbusnd27cgp1" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>
                                                </div>
                                                <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>
                                            </div>
                                            <iframe style="display: none;"></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex mt-2">
                                    <div class="edit_info_left hidden md:block"></div>
                                    <div class="equal_info_right">
                                        <button type="submit" style="display: none" name="smt_name" id="submit_btn" value="form_withdraw_serivce"></button>
                                        <button class="smt_edit_info smt_info" id="change_withdraw_service_btn">Lưu
                                        </button>
                                        <a href="javascript:;" class="bg-secondary inline-block px-5 rounded smt_edit_info text-white" onclick="exit_user(this); return">Huỷ</a> <a href="javascript:;" onclick="showHistory(); return" class="smt_exit smt_exit_right">Xem
                                            lịch sử</a>
                                    </div>
                                </div>
                            </form>
                            <div id="table-history-service"></div>
                        </div>
                    </div>  -->
                    <!-- THÔNG TIN KÊ KHAI THUẾ -->
                    <!-- <div class="line_array"></div> -->
                    <!-- <div class="edit_from_user">
                        <h4 class="title_user_info"> Thông tin kê khai thuế <a href="javascript:;" class="icon_edit_title filter icon" onclick="edit_user(this); return">Sửa</a></h4>
                        <div class="user_result">
                            <p><span class="edit_info_left">Họ và tên</span><span class="equal_info_right">Nguyễn Thế Quỳnh</span></p>
                            <p><span class="edit_info_left">Ngày sinh</span><span class="equal_info_right">1/01/1970</span></p>
                            <p><span class="edit_info_left">Giới tính</span><span class="equal_info_right">Khác</span>
                            </p>
                            <p><span class="edit_info_left">Số CMND/CCCD</span><span class="equal_info_right">---</span>
                            </p>
                            <p><span class="edit_info_left">Địa chỉ thường trú</span><span class="equal_info_right">---</span></p>
                            <p><span class="edit_info_left">Số điện thoại</span><span class="equal_info_right">---</span></p>
                            <p><span class="edit_info_left">Email</span><span class="equal_info_right">quynh@vdata.com.vn</span>
                            </p>
                        </div>
                        <div class="user_no_result">
                            <form action="" name="user_edit_pay" id="tax_declarations_form" method="post">
                                <p><span class="edit_info_left">Họ và tên</span> <span class="equal_info_right"> <input type="text" name="tax_fullname" value="Nguyễn Thế Quỳnh" class="user_address_edit"> </span></p>
                                <p><span class="edit_info_left">Ngày sinh</span> <span class="equal_info_right"> <input type="text" name="tax_birthday" class="user_date_edit hasDatepicker" value="1/01/1970" id="dp1634810899127"> </span>
                                </p>
                                <p><span class="edit_info_left">Giới tính</span> <span class="equal_info_right"> <select class="user_sex_edit" name="tax_gender">
                                            <option value="Khác">Khác</option>
                                            <option value="0">Khác</option>
                                            <option value="1">Nam</option>
                                            <option value="2">Nữ</option>
                                        </select> </span>
                                </p>
                                <p><span class="edit_info_left">Số CMND/CCCD</span> <span class="equal_info_right"> <input type="text" name="tax_cmnd" value="" class="user_address_edit" placeholder="Ex: 03xxxxxxx658"> </span>
                                </p>
                                <p><span class="edit_info_left">Địa chỉ thường trú</span> <span class="equal_info_right"> <input type="text" name="tax_address" value="" class="user_address_edit" placeholder="Ex: Thái Thịnh, Đống Đa, Hà Nội"> </span>
                                </p>
                                <p><span class="edit_info_left">Số điện thoại</span> <span class="equal_info_right"> <input type="text" name="tax_phone" value="" class="user_address_edit" placeholder="Ex: 03xxxxxxxx"> </span></p>
                                <p><span class="edit_info_left">Email</span> <span class="equal_info_right"> <input type="text" name="tax_email" value="quynh@vdata.com.vn" class="user_address_edit"> </span></p>
                                <div class="flex mt-2">
                                    <div class="edit_info_left hidden md:block"></div>
                                    <div class="equal_info_right"><input type="hidden" name="smt_name" value="form_tax_declarations">
                                        <button type="submit" class="smt_edit_info smt_info" id="change_tax_declaration_btn">Lưu
                                        </button>
                                        <a href="javascript:;" class="bg-secondary inline-block px-5 rounded smt_edit_info text-white" onclick="exit_user(this); return">Huỷ</a>
                                    </div>
                                </div>
                            </form>
                            <div id="table-history-service"></div>
                        </div>
                    </div> -->
                    <div class="line_array"></div>
                    <h4 class="title_user_info"><a rel="nofollow" href="<?= URL ?>/blog/dieu-khoan-su-dung">Điều khoản sử dụng</a></h4>
                    <div class="line_array"></div>
                    <h4 class="title_user_info"><a rel="nofollow" href="<?= URL ?>/blog/chich-sach-bao-mat">Chính sách bảo mật</a></h4>
                    <div class="line_array"></div>
                    <h4 class="title_user_info"><a rel="nofollow" href="<?= URL ?>/blog/thong-tin-nha-phat-trien">Thông tin nhà phát triển</a></h4>
                </div>
            </div>
        </div>
    </div>
    <script defer="" src="https://static.store123doc.com/static_v2/common/js/jquery-ui-1.9.2.custom.js"></script>
    <script defer="" src="https://static.store123doc.com/static_v2/common/js/moment.js"></script>
    <script defer="" src="https://static.store123doc.com/static_v2/common/js/daterangepicker.js"></script>
    <script src="https://static.store123doc.com/static_v2/web_v2//common/js/jquery-3.5.1.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://static.store123doc.com/static_v2/common/css/jquery-ui-1.9.2.custom.min.css">
    <script src="https://www.google.com/recaptcha/api.js?hl=vi&amp;onload=CaptchaCallback&amp;render=explicit" async="" defer=""></script>
    <script type="text/javascript">
        var CaptchaCallback = function() {
            g_recaptcha_withdraw = genCaptcha('g-recaptcha-withdraw');
        };
    </script>
</div>