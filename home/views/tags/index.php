<link rel="stylesheet" type="text/css" href="<?= URL ?>/template/styles/search.css" />
<div id="container">
    <div class="container mx-auto searchPage seoTag">
        <h1 class="text-xl md:text-2xl p-2">Tài liệu về "<strong> <?= $this->keyword ?> </strong>" <?= count($this->data) ?> kết quả</h1>
        <div class="search_top px-2"> <label>Bạn có muốn tìm thêm với từ khóa: </label>
            <ul>
                <?php 
                    $tags = '';
                    foreach($this->tags as $tag){
                        $tags .= ','.$tag['tag'];
                    }
                    $tags = substr($tags, 1);
                    $tags = explode(',',$tags);
                    foreach($tags as $tagItem){?>
                        <li><a href="<?= URL ?>/tag?keyword=<?= $tagItem ?>"><?= $tagItem ?></a></li>
                    <?php
                    }
                ?>
            </ul>
        </div>
        <div class="search_bottom grid grid-cols-12 gap-2 p-2 ">
            <div style="height: 100%; position: relative" class="hidden md:block text-center md:col-span-2">
                <!-- <div style="position: sticky;top: 10px;"> <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px" data-ad-client="ca-pub-2979760623205174" data-ad-slot="3807520840" data-adsbygoogle-status="done" data-ad-status="filled"><ins id="aswift_0_expand" tabindex="0" title="Advertisement" aria-label="Advertisement" style="border: none; height: 600px; width: 160px; margin: 0px; padding: 0px; position: relative; visibility: visible; background-color: transparent; display: inline-table;"><ins id="aswift_0_anchor" style="border: none; height: 600px; width: 160px; margin: 0px; padding: 0px; position: relative; visibility: visible; background-color: transparent; display: block;"><iframe id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;border:0;width:160px;height:600px;" sandbox="allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" width="160" height="600" frameborder="0" src="https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-2979760623205174&amp;output=html&amp;h=600&amp;slotname=3807520840&amp;adk=3077490338&amp;adf=4093402434&amp;pi=t.ma~as.3807520840&amp;w=160&amp;lmt=1635132838&amp;psa=1&amp;format=160x600&amp;url=https%3A%2F%2F123docz.net%2Ftags%2F975783-tai-lieu-tieng-anh-b1.htm&amp;flash=0&amp;wgl=1&amp;uach=WyJXaW5kb3dzIiwiMTAuMC4wIiwieDg2IiwiIiwiOTQuMC40NjA2LjgxIixbXSxudWxsLG51bGwsIjY0Il0.&amp;dt=1635132838386&amp;bpp=4&amp;bdt=7252&amp;idt=110&amp;shv=r20211020&amp;mjsv=m202110200101&amp;ptt=9&amp;saldr=aa&amp;abxe=1&amp;cookie=ID%3Dfec19286e603ab92-22a8b34daecc00b6%3AT%3D1634721732%3ART%3D1634721732%3AS%3DALNI_MaY77AWGnrrPhWurJt3gVzfQ22cow&amp;correlator=6425897098763&amp;frm=20&amp;pv=2&amp;ga_vid=1372386773.1634721731&amp;ga_sid=1635132839&amp;ga_hid=57580417&amp;ga_fc=1&amp;u_tz=420&amp;u_his=8&amp;u_h=864&amp;u_w=1536&amp;u_ah=824&amp;u_aw=1536&amp;u_cd=24&amp;adx=150&amp;ady=172&amp;biw=1519&amp;bih=754&amp;scr_x=0&amp;scr_y=0&amp;eid=31063260%2C21065725%2C31062524&amp;oid=2&amp;pvsid=920307539309675&amp;pem=500&amp;ref=https%3A%2F%2F123docz.net%2Ftags%2F823915-tai-lieu-tieng-anh.htm&amp;eae=0&amp;fc=896&amp;brdim=0%2C0%2C0%2C0%2C1536%2C0%2C1536%2C824%2C1536%2C754&amp;vis=1&amp;rsz=%7C%7CeE%7C&amp;abl=CS&amp;pfx=0&amp;fu=0&amp;bc=31&amp;ifi=1&amp;uci=a!1&amp;fsb=1&amp;xpc=0V7x1OgOh1&amp;p=https%3A//123docz.net&amp;dtd=126" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!1" data-google-query-id="CJvzx8DQ5PMCFQHVvQodZXAFmQ" data-load-complete="true"></iframe></ins></ins></ins>
                    <script defer="">
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div> -->
            </div>
            <div class="seo_bottom_left col-span-12 md:col-span-7">
                <div class="list_item">
                    <?php
                        foreach($this->data as $item){ ?>
                        <div class="seo_item">
                            <div class="grid grid-cols-12"> <a href="<?= URL ?>/document/<?= $item['url'] ?>" title="<?= $item['name'] ?>" gtm-element="GTM_Timkiem_Click_result" gtm-label="GTM_Timkiem_Click_result" class="col-span-3 md:col-span-2"> <img alt="pce practice test sample" class="lazy error" src="<? URL ?>.'/'.<?= $item['hinhanh'] ?>" data-src="https://media.123dok.info/images/document/2014_07/25/medium_bib1406258069.jpg" onerror="this.src='<?= URL ?>/template/images/default/no-image.png';" data-ll-status="error"> </a>
                                <div class="col-span-9 md:col-span-10 pr-2">
                                    <h2> <a href="<?= URL ?>/document/<?= $item['url'] ?>" title="<?= $item['name'] ?>" gtm-element="GTM_Timkiem_Click_result" gtm-label="GTM_Timkiem_Click_result" class="break-all"><?= $item['name'] ?><i class="icon i_type_doc i_type_doc2"></i> </a> </h2>
                                    <div>
                                        <p> <label>Danh mục: </label> <a href="/doc-cat/77-anh-ngu-pho-thong.htm" title="Anh ngữ phổ thông"><?= $item['category'] ?></a> </p>
                                        <div> </div>
                                        <ul class="doc_tk_cnt">
                                            <li> <i class="icon_doc"></i>20 </li>
                                            <li> <i class="icon_view"></i><?= $item['view'] ?> </li>
                                            <li> <i class="icon_down"></i><?= $item['download'] ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                </div>
                <div class="paging"></div>
                <div class="keyword_wrap_bottom">
                    <!-- <span class="tag_title">Tìm thêm: </span>  -->
                 
                    

                    <!-- <a class="tag-show" href="/timkiem/bao+cao+thi+nghiem+cong+trinh.htm">bao cao thi nghiem cong trinh</a> 
                    <a class="tag-show" href="/timkiem/thuc+tap+tai+kho.htm">thuc tap tai kho</a> 
                    <a class="tag-show" href="/timkiem/chuong+trinh+phan+tich+cu+phap.htm">chuong trinh phan tich cu phap</a> 
                    <a class="tag-show" href="/timkiem/phan+tu+huu+han.htm">phan tu huu han</a> -->
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>
    </div>
</div>