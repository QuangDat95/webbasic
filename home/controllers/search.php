<?php

class search extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function document()
    {
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $this->model->updatesearch($keyword,'document');
        $this->view->keyword = $keyword;
        $this->view->data = $this->model->getdocument($keyword);
        $this->view->render('search/document');
    }

    function collection()
    {
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $this->view->keyword = $keyword;
        // $this->view->data = $this->model->getcollection($keyword);
        $this->view->render('search/collection');
    }

    function users()
    {
        $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
        $this->view->keyword = $keyword;
        // $this->view->data = $this->model->getuser($keyword);
        $this->view->render('search/users');
    }

}

?>
