<?php

class tag extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        $this->view->data = $this->model->getdata($keyword);
        $this->view->tags = $this->model->gettags($keyword);
        $this->view->keyword = $keyword;
        $this->view->render('tags/index');
    }

}
?>