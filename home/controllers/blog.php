<?php

class blog extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index($url,$page)
    {
        $this->view->blogcate = $this->model->getblogcate();
        $this->view->data = $this->model->getdata($url);
        $this->view->render('blog/index');
    }

    function detail($url)
    {
        $this->view->blogcate = $this->model->getblogcate();
        $this->view->blogitem = $this->model->getblog($url);
        $this->view->tinlienquan = $this->model->gettinlienquan($url);
        $this->view->render('blog/detail');
    }

}

?>
