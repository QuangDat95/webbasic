<?php

class user extends controller
{
    function __construct()
    {
        parent::__construct();
        
    }

    function home()
    {
        $uid = $_SESSION['customer']['customer'];
        $this->view->mycoll = $this->model->getMyColl($uid);
        $this->view->render('user/home/index');
    }

    function doc_control()
    {
        $this->view->render('user/doc_control');
    }

    function profile()
    {
        
        $this->view->render('user/profile');
    }
    
    function finance_control()
    {
        $this->view->render('user/finance_control');
    }

    function my_collection()
    {
        $uid = $_SESSION['customer']['customer'];
        $this->view->mycoll = $this->model->getMyColl($uid);
        $this->view->render('user/home/my_collection');
    }

    function collection($url)
    {
        $uid = $_SESSION['customer']['customer'];
        $this->view->mycoll = $this->model->getMyColl($uid);
        $this->view->coll = $this->model->getColl($url,'');
        $this->view->docs = $this->model->getDocs($url);
        $this->view->render('user/home/collection');
    }

    function edit_collection($url)
    {
        $this->view->coll = $this->model->getColl($url,'');
        $this->view->docs = $this->model->getDocs($url);
        $this->view->render('user/home/edit_collection');
    }

    function saveeditcoll()
    {
        $collid = $_REQUEST['id'];
        $data = ['status'=>0];
        if ($this->model->dellDoc($collid,$data)) {
            $coll = $this->model->getColl('',$collid);
            header("Location: edit_collection/".$coll[0]['url']);  
        } else {
            header("Location: edit_collection");  
        }
    }

    function dellcoll()
    {
        $id = $_REQUEST['id'];
        $data = ['status'=>0];
        if ($this->model->dellColl($id,$data)) {
            header("Location: my_collection");  
        } else {
            header("Location: my_collection");  
        }
    }

    function delldoc()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        $docid = $_REQUEST['docid'];
        $collid = $_REQUEST['collid'];
        $data = ['status'=>0];
        if ($this->model->dellDoc($docid,$collid,$data)) {
            $coll = $this->model->getColl('',$collid);
            header("Location: edit_collection/".$coll[0]['url']);  
        } else {
            header("Location: edit_collection");  
        }
    }

    function upload()
    {
        $this->view->render('index/upload');
    }

}

?>
