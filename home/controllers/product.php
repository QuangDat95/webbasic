<?php

class product extends controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    function index($url, $page)
    {
        $xuhuong = isset($_REQUEST['xuhuong']) ? $_REQUEST['xuhuong'] : 'moidang';
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        $money = isset($_REQUEST['money']) ? $_REQUEST['money'] : '';
        $this->view->data = $this->model->getdata($url, $page);
        $this->view->documents = $this->model->getdocuments($url, $xuhuong, $type, $money);
        $this->view->tags = $this->model->gettags($url);
        $this->view->moinhat = $this->model->getmoinhat();
        $this->view->tainhieu = $this->model->gettainhieu();
        $this->view->xemnhieu = $this->model->getxemnhieu();
        $this->view->noibat = $this->model->getnoibat();
        $this->view->render('document/index');
    }

    function detail($url)
    {
        $this->view->document = $this->model->getdocument($url);
        $this->view->tailieulienquan = $this->model->gettailieulienquan($url);
        $this->view->relatedkeywords = $this->model->getrelatedkeywords($url);
        $this->view->thuongxem = $this->model->getthuongxem($url);
        $this->view->moinhat = $this->model->getmoinhat();
        $this->view->tainhieu = $this->model->gettainhieu();
        $this->view->xemnhieu = $this->model->getxemnhieu();
        $this->view->noibat = $this->model->getnoibat();
        $this->view->render('document/detail');
    }

    function addcollection()
    {
        // $docid = $_REQUEST['docid'];
        $name = $_REQUEST['col_name'];
        var_dump($name);

        $description = $_REQUEST['col_des'];
        $tag = $_REQUEST['col_tags'];
        $customer = $_SESSION['customer']['customer'];
        $data = array(
            'name' => $name,
            'description' => $description,
            'tag' => $tag,
            'customer' => $customer,
            'status' => 1
        );
        if ($this->model->addCollection($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        echo json_encode($jsonObj);
    }

    
}

?>
