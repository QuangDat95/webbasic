<?php

class index extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->view->data = $this->model->getdata();
        $this->view->sidebar = $this->model->getsidebar();
        $this->view->slide = $this->model->getslide();
        $this->view->tailieuchung = $this->model->gettailieuchung();
        $this->view->tlnoibat = $this->model->gettlnoibat();
        $this->view->tlmoi = $this->model->gettlmoi();
        $this->view->tlxemnhieu = $this->model->gettlxemnhieu();
        $this->view->tltainhieu = $this->model->gettltainhieu();
        $this->view->render('index/index');
    }

    function gettailieuchild()
    {
        $id = $_REQUEST['id'];
        $jsonObj = [];
        $data = $this->model->gettailieuchild($id);
        if ($data) {
            $jsonObj['success'] = true;
            $jsonObj['data'] = $data;
        } else {
            $jsonObj['success'] = false;
        }
        echo json_encode($jsonObj);

    }

    function formlogin()
    {
        $this->view->render('index/formlogin');
    }

    function checklogin()
    {
        $this->view->render('index/checklogin');
    }

    function checklogin2()
    {
        $this->view->render('index/checklogin2');
    }

    function pop_download_not_login_v2()
    {
        $this->view->render('index/pop_download_not_login_v2');
    }

    function show_history_download()
    {
        $this->view->render('index/show_history_download');
    }

    function gioithieu()
    {
        $this->view->render('index/gioithieu');
    }

    function register()
    {
        if (isset($_REQUEST['use_fullname'])) {
            $use_fullname = $_REQUEST['use_fullname'];
            $use_phone = $_REQUEST['use_phone'];
            $use_email = $_REQUEST['use_email'];
            $use_password = $_REQUEST['use_password'];
            $re_use_password = $_REQUEST['re_use_password'];
            $checkemail = $this->model->checkEmail($use_email);
            if ($use_password == $re_use_password) {
                if ($checkemail == 0) {
                    $customer = ['name' => $use_fullname];
                    $customerid = $this->model->insertCustomer($customer);
                    if ($customerid > 0) {
                        $data = [
                            'email' => $use_email,
                            'phone' => $use_phone,
                            'password' => md5(md5($use_password)),
                            'action_code' => '',
                            'customer' => $customerid,
                            'status' => 1
                        ];
                        $userid = $this->model->insertUser($data);
                        if ($userid > 0) {
                            $jsonObj['success'] = true;
                            $jsonObj['email'] = $use_email;
                            $jsonObj['msg'] = 'Đăng ký thành công';
                        } else {
                            $jsonObj['success'] = true;
                            $jsonObj['data'] = '';
                            $jsonObj['msg'] = 'Đăng ký không thành công';
                        }
                    }
                } else {
                    $jsonObj['success'] = false;
                    $jsonObj['key'] = 'use_email';
                    $jsonObj['msg'] = 'Email của bạn đã được sử dụng';
                }
            } else {
                $jsonObj['success'] = false;
                $jsonObj['key'] = 're_use_password';
                $jsonObj['msg'] = 'Mật khẩu nhập lại không trùng khớp';
            }
        } else {
            $jsonObj['success'] = false;
            $jsonObj['data'] = '';
            $jsonObj['msg'] = 'Lỗi trong quá trình đăng ký';
        }
        echo json_encode($jsonObj);
    }

    function login()
    {
        $jsonObj = [];
        if (isset($_REQUEST['txtName'])) {
            $username = $_REQUEST['txtName'];
            // $username = $_REQUEST['username'];
            $username = str_replace("'", "", $username);
            $username = str_replace('"', "", $username);
            $username = str_replace("*", "", $username);
            $username = str_replace("/", "", $username);
            $username = str_replace("(", "", $username);
            $username = str_replace(")", "", $username);
            $username = str_replace("=", "", $username);
            $password = md5(md5($_REQUEST['txtPass']));
            $userdata = $this->model->checkLogin($username, $password);
            if (count($userdata) > 0) {
                $_SESSION[USERID]    = true;
                $_SESSION['customer'] = $userdata[0];
                // $cookie_name = "username";
                // $cookie_value = $username;
                // setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
            } else {
                $jsonObj['msg'] = 'Tài khoản hoặc mật khẩu không đúng';
            }
        } else {
            $jsonObj['msg'] = 'Đăng nhập thất bại';
        }
        echo json_encode($jsonObj);
    }

    function logout()
    {
        session_destroy();
        header('Location: ' . URL);
    }

    function upload()
    {
        $this->view->render('index/upload');
    }

    function saveupload()
    {
        $name = $_REQUEST['name'];
        $data = [
            'id' => 1,
            'name' => $name
        ];
        $jsonObj['result'] = $data;
        echo json_encode($jsonObj);
    }

    function loaddata($id)
    {
        return $this->model->loadData($id);
    }

    function getcollection()
    {
        $uid = $_REQUEST['uid'];
        $docid = $_REQUEST['docid'];
        $json = $this->model->getCollection($uid,$docid);
        // echo $json;
        if ($json) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['data'] = $json;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        echo json_encode($jsonObj);
    }

    function listcollection()
    {
        $this->view->render('index/listcollection');
    }

    function addcollection()
    {
        $this->view->render('index/addcollection');
    }

    function addmoney()
    {
        $this->view->render('index/addmoney');
    }

    function showapprdocument()
    {
        $this->view->render('index/showapprdocument');
    }

    function saveaddCollection()
    {
        $name = $_REQUEST['col_name'];
        $url = functions::convertname($name);
        $docid = $_REQUEST['doc_id'];
        $description = $_REQUEST['col_des'];
        $tag = $_REQUEST['col_tags'];
        $customer = $_SESSION['customer']['customer'];
        $data = array(
            'name' => $name,
            'url' => $url,
            'description' => $description,
            'tag' => $tag,
            'customer' => $customer,
            'status' => 1
        );
       
        $json = $this->model->addCollection($data,$docid);
        if ($json) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
            $jsonObj['data'] = $this->loaddata($json);
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        echo json_encode($jsonObj);
    }

    function addtocoll()
    {
        $document_id = $_REQUEST['doc_id'];
        $collection_id = $_REQUEST['col_id'];
        $status = $_REQUEST['status'];
        $data = array(
            'datetime' => NOW,
            'document_id' => $document_id,
            'collection_id' => $collection_id,
            'status' => $status
        );
        $json = $this->model->updateColsub($data);
        if ($json) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        echo json_encode($jsonObj);
    }

}

?>
