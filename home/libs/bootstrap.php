<?php

class bootstrap
{
    function __construct()
    {
        $this->view = new view();
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        $module = (empty($url[0])) ? 'index' : $url[0];
        $module = ($module == 'product')?'document':$module;
        if (file_exists('home/controllers/' . $module . '.php')) {
            require 'home/controllers/' . $module . '.php';
            $controller = new $module;
            $controller->loadModel($module); // load model tương ứng
            $method = (isset($url[1])) ? $url[1] : 'index';
            switch ($module) {
                case 'product':
                    if (count($url) >= 3) {
                        $controller->index($url[2], $url[1]);
                    } else if (count($url) == 2) {
                        $controller->detail($url[1]);
                    } 
                    break;
                case 'blog':
                    if (count($url) >= 3) {
                        $controller->index($url[2], $url[1]);
                    } else if (count($url) == 2) {
                        $controller->detail($url[1]);
                    } else {
                        $this->view->thongbao = 'Không tìm thấy method!';
                        $this->view->render('common/thongbao');
                    }
                    break;
                case 'search':
                    if (!empty($url[1]) && $url[1]=='product') {
                        $controller->product();
                    } else if (!empty($url[1]) && $url[1]=='blog') {
                        $controller->blog();
                    }else if (!empty($url[1]) && $url[1]=='users') {
                        $controller->users();
                    } else {
                        $this->view->thongbao = 'Không tìm thấy method!';
                        $this->view->render('common/thongbao');
                    }
                    break;
                case 'user':
                    if (!empty($url[1]) && $url[1]=='home') {
                        $controller->home();
                    }
                    break;
                case 'index':

                default:
                    if (method_exists($controller, $method)) {
                        if (isset($url[2]))
                            $controller->{$method}($url[2]);
                        else
                            $controller->{$method}();
                    } else {
                        $this->view->thongbao = 'Không tìm thấy method!';
                        $this->view->render('common/thongbao');
                    }
                    break;
            }
        } else {
            $this->view->thongbao = 'Không tìm thấy controller!';
            $this->view->render('common/thongbao');
        }
    }
}

?>
