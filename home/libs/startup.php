<?php
session_start();
date_default_timezone_set("Asia/Ho_Chi_Minh");
ini_set('display_errors', 1);
define('HEADER', 'layouts/header.php');
define('FOOTER', 'layouts/footer.php');
define('HOMNAY', date('Y-m-d'),true);
define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('MOBILE', $mobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]));
if ($_SERVER['SERVER_NAME'] == 'localhost') {
    define('DB_NAME', 'webbasic');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('URL', 'http://' . $_SERVER['HTTP_HOST'] . '/webbasic');
    define('CMS', 'http://' . $_SERVER['HTTP_HOST'] . '/webbasic/cms');
    define('HOME_DIR', 'http://' . $_SERVER['HTTP_HOST'].'/webbasic/home');
    define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/webbasic/home');
} else {
    define('DB_NAME', 'webbasic_db');
    define('DB_USER', 'webbasic_db');
    define('DB_PASS', '123456');
    define('URL', 'http://' . $_SERVER['HTTP_HOST']);
    define('CMS', 'http://' . $_SERVER['HTTP_HOST'].'/cms');
    define('HOME_DIR', 'http://' . $_SERVER['HTTP_HOST'].'/home');
    define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT']);
}
define('SID', md5(session_id() . URL));
?>
