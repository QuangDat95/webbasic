<?php

class model
{
    function __construct()
    {
        $this->db = new database();
    }

    //  Các hàm dùng chung
    function mainmenu()
    {
        $dieukien = " WHERE status=1 AND parentid = 0 ";
        $query = $this->db->query("SELECT * FROM menu $dieukien ORDER BY sort_order ASC");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function submenu($id)
    {
        $dieukien = " WHERE status=1 AND parentid > 0 ";
        $query = $this->db->query("SELECT * FROM menu $dieukien ORDER BY parentid ASC, sort_order ASC");
        $submenu = $query->fetchAll(PDO::FETCH_ASSOC);
        return $submenu;
    }

    function sendmail($from, $email, $cc, $subject, $noidung)
    {
        $ok = false;
        require_once 'libs/mailin.php';
        $mailin = new Mailin('info@vdata.com.vn', 'UIhEdDsN8qnWvCzA');
        $mailin->
        addTo($email, 'Khách hàng')->
        addCc($cc, 'Khách hàng')->
        setFrom('no-reply@vdata.com.vn', $from)->
        setReplyTo('info@vdata.com.vn', $from)->
        setSubject($subject)->
        setHtml('
           <!DOCTYPE html><html>
           <head>
              <style>
              table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}
              td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}
              th {background-color: #dddddd;}
              </style>
           </head>
           <body>
                 <p>' . $noidung . ' </p>
                 <p>Nội dung email này là riêng tư và không được phép tiết lộ. Nếu bạn nhận được email này do nhầm lẫn, vui lòng xóa bỏ hoặc thông báo lại cho chúng tôi qua địa chỉ email  <a href="mailto:info@vdata.com.vn" target="_blank">info@vdata.com.vn</a>. VDATA xin chân thành cảm ơn!</p>
           </div>
           </body>
           </html>');
        $ok = $mailin->send();
        return $ok;
    }

    function mobilemenu()
    {
        $query = $this->db->query("SELECT id, name, link, icon FROM menu
           WHERE active=1 AND id!=3 AND mobile=1 AND parentid IN (SELECT id FROM menu WHERE active=1 AND parentid=0) ");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function lairut($id)
    {
        $qr = $this->db->query("SELECT SUM(so_tien) as so_tien FROM socai WHERE tien_gui=$id 
            AND loai=1 AND tinh_trang>0 AND loai_doi_ung=3 AND phan_loai=1 AND phan_loai2=3");
        $tp = $qr->fetchAll(PDO::FETCH_ASSOC);
        if ($tp && $tp[0]['so_tien'] > 0)
            $lairut = $tp[0]['so_tien'];
        else $lairut = 0;
        return $lairut;
    }
    // các hàm thao tác trên database
    function insert($table, $array)
    {
        $cols = array();
        $bind = array();
        foreach ($array as $key => $value) {
            $cols[] = $key;
            $value = str_replace("'", "", $value);
            $bind[] = "'" . $value . "'";
        }
        $query = $this->db->query("INSERT IGNORE INTO " . $table . " (" . implode(",", $cols) . ") VALUES (" . implode(",", $bind) . ")");
        return $query;
    }

    function update($table, $array, $where)
    {
        $set = array();
        foreach ($array as $key => $value) {
            $value = str_replace("'", "", $value);
            $set[] = $key . " = '" . $value . "'";
        }
        $query = $this->db->query("UPDATE IGNORE " . $table . " SET " . implode(",", $set) . " WHERE " . $where);
        return $query;
    }

    function delete($table, $where = '')
    {
        if ($where == '') {
            $query = $this->db->query("DELETE FROM " . $table);
        } else {
            $query = $this->db->query("DELETE FROM " . $table . " WHERE " . $where);
        }
        return $query;
    }

    function getthongtin()
    {
        $result = array();
        $dieukien = " WHERE status = 1 AND position = 1 ";
        $query    = $this->db->query("SELECT *
            FROM blog $dieukien ORDER BY sort_order ASC LIMIT 4 ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gethtkh()
    {
        $result = array();
        $dieukien = " WHERE status = 1 AND position = 2 ";
        $query    = $this->db->query("SELECT *
            FROM blog $dieukien ORDER BY sort_order ASC LIMIT 4 ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettrogiup()
    {
        $result = array();
        $dieukien = " WHERE status = 1 AND position = 3 ";
        $query    = $this->db->query("SELECT *
            FROM blog $dieukien ORDER BY sort_order ASC LIMIT 4 ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getinfo()
    {
        $result = array();
        $dieukien = " WHERE status = 1 ";
        $query    = $this->db->query("SELECT *
            FROM infomation $dieukien ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getkeywords()
    {
        $result = array();
        $query    = $this->db->query("SELECT * FROM search ORDER BY count DESC ");
        if ($query)
            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}

?>
