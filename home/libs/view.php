<?php

class view
{
    function __construct()
    {
        //echo 'this is the view';
    }

    public function render($name, $noInclude = false)
    {
        if (file_exists('home/views/' . $name . '.php')) {
            switch ($name){
                case 'index/formlogin':
                    require 'home/views/index/formlogin.php';
                    break;
                case 'index/checklogin':
                    require 'home/views/index/checklogin.php';
                    break;
                case 'index/checklogin2':
                    require 'home/views/index/checklogin2.php';
                    break;
                default:
                    $model = new model();
                    $this->menucap1 = $model->mainmenu();
                    $this->menucap2 = $model->submenu(0);
                    $this->thongtin = $model->getthongtin();
                    $this->htkh = $model->gethtkh();
                    $this->trogiup = $model->gettrogiup();
                    $this->info = $model->getinfo();
                    $this->keywords = $model->getkeywords();
                    require("home/layouts/header.php");
                    require 'home/views/' . $name . '.php';
                    require("home/layouts/footer.php");
                    break;
            }
        }
    }

}